cmake_minimum_required (VERSION 2.8)
project(PAUPER CXX)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG "${OUTPUT_DIRECTORY}")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE "${OUTPUT_DIRECTORY}")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_DEBUG "${OUTPUT_DIRECTORY}")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_RELEASE "${OUTPUT_DIRECTORY}")
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_DEBUG "${OUTPUT_DIRECTORY}")
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_RELEASE "${OUTPUT_DIRECTORY}")

include(cxx14.cmake)
include(g.cmake)
include(sanitizer.cmake)

include(zander.cmake)
include(util.cmake)

option(FORCE_PPR_SINGLE_THREADED "Forces ppr to use a single threaded implementation" OFF)
option(ENABLE_PPR_LOGGING "Enable the ppr logging engine when compiling" ON)
option(PPR_LOGGING_TRACE "Enable the ppr logging engine to log at the level of traces by default" OFF)
option(PPR_LUA_ENABLE "Enable the Lua virtual machine support Lua" ON)
option(PPR_LUAJIT_ENABLE "Enable the LuaJIT virtual machine instead of the standard Lua" ON)
option(PPR_SFML_ENABLE "Enable the SFML drivers for ppr" ON)
option(PPR_SDL2_ENABLE "Enable the SDL2 drivers for ppr" OFF)

if (FORCE_PPR_SINGLE_THREADED)
    add_definitions(-DPPR_SINGLE_THREADED)
endif(FORCE_PPR_SINGLE_THREADED)

if (ENABLE_PPR_LOGGING)
    add_definitions(-DPPR_ENABLE_LOGS)
endif(ENABLE_PPR_LOGGING)

if (PPR_LOGGING_TRACE)
    add_definitions(-DPPR_DEFAULT_LOG_LEVEL_TRACE)
endif(PPR_LOGGING_TRACE)

if (PPR_SFML_ENABLE)
    add_definitions(-DPPR_SFML_ENABLE)
endif(PPR_SFML_ENABLE)

if (PPR_SDL2_ENABLE)
    add_definitions(-DPPR_SDL2_ENABLE)
endif(PPR_SDL2_ENABLE)

add_subdirectory(src)

enable_testing()
add_subdirectory(test)
