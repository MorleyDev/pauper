#ifndef PPR_MODEL_CONFIGURATION_HPP_INCLUDED
#define PPR_MODEL_CONFIGURATION_HPP_INCLUDED

#include <thread>
#include <string>
#include <cstdint>

namespace ppr {
	namespace model {
		struct Configuration {
			std::string driver = std::string("default");

			struct Graphics {
				std::size_t width = 0;
				std::size_t height = 0;
				bool fullscreen = false;
				bool vsync = false;
			} graphics;

			struct Audio {
				double musicVolume = 1.0;
				double soundVolume = 1.0;
			} audio;

			std::size_t threadPoolSize = std::thread::hardware_concurrency();
			int logging = -1;
		};
	}
}

#include "../tl/serialise.hpp"

namespace ppr {
	namespace model {
	    template<typename ARCHIVE> void serialize(ARCHIVE& archive, Configuration& c) {
		    archive(tl::make_nvp("driver", c.driver));
		    archive(tl::make_nvp("graphics", c.graphics));
		    archive(tl::make_nvp("audio", c.audio));
			archive(tl::make_nvp("threads", c.threadPoolSize));
			archive(tl::make_nvp("logging", c.logging));
	    }
	    template<typename ARCHIVE> void serialize(ARCHIVE& archive, Configuration::Graphics& c) {
		    archive(tl::make_nvp("width", c.width));
		    archive(tl::make_nvp("height", c.height));
		    archive(tl::make_nvp("fullscreen", c.fullscreen));
		    archive(tl::make_nvp("vsync", c.vsync));
	    }
	    template<typename ARCHIVE> void serialize(ARCHIVE& archive, Configuration::Audio& c) {
		    archive(tl::make_nvp("music", c.musicVolume));
		    archive(tl::make_nvp("sound", c.soundVolume));
	    }
	}
}

#endif//PPR_MODEL_CONFIGURATION_HPP_INCLUDED
