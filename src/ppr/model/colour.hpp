#ifndef PAUPER_MODEL_COLOUR_HPP_INCLUDED
#define PAUPER_MODEL_COLOUR_HPP_INCLUDED

#include <cstdint>

namespace ppr {
	namespace model {
		struct colour {
			colour()
					: red(),
					  green(),
					  blue(),
					  alpha() {
			}

			colour(colour const& c)
					: red(c.red),
					  green(c.green),
					  blue(c.blue),
					  alpha(c.alpha) {
			}

			colour& operator=(colour const& c) {
				red = c.red;
				green = c.green;
				blue = c.blue;
				alpha = c.alpha;
				return *this;
			}

			colour(std::uint8_t r, std::uint8_t g, std::uint8_t b, std::uint8_t alpha = 255u)
					: red(r),
					  green(g),
					  blue(b),
					  alpha(alpha) {
			}

			std::uint8_t red;
			std::uint8_t green;
			std::uint8_t blue;
			std::uint8_t alpha;
		};
	}
}

#include "../tl/serialise.hpp"

namespace ppr {
	namespace model {
	    template<typename ARCHIVE> void serialize(ARCHIVE& archive, colour& c) {
		    archive(tl::make_nvp("red", c.red));
			archive(tl::make_nvp("green", c.green));
			archive(tl::make_nvp("blue", c.blue));
			archive(tl::make_nvp("alpha", c.alpha));
	    }
	}
}

#endif//PAUPER_MODEL_COLOUR_HPP_INCLUDED
