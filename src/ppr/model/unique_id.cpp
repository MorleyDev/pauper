#include "unique_id.hpp"
#include <atomic>

ppr::model::unique_id::unique_id(std::size_t value)
		: m_value(value) {
}

ppr::model::unique_id::unique_id()
		: m_value(0) {
}

ppr::model::unique_id::unique_id(unique_id const& param)
		: m_value(param.m_value) {
}

ppr::model::unique_id & ppr::model::unique_id::operator=(unique_id const& param) {
	m_value = param.m_value;
	return *this;
}

bool ppr::model::unique_id::empty() const {
	return m_value == 0;
}

bool ppr::model::unique_id::operator==(unique_id const& other) const {
	return m_value == other.m_value;
}

bool ppr::model::unique_id::operator!=(unique_id const& other) const {
	return m_value != other.m_value;
}

bool ppr::model::unique_id::operator>(unique_id const& other) const {
	return m_value > other.m_value;
}

bool ppr::model::unique_id::operator>=(unique_id const& other) const {
	return m_value >= other.m_value;
}

bool ppr::model::unique_id::operator<(unique_id const& other) const {
	return m_value < other.m_value;
}

bool ppr::model::unique_id::operator<=(unique_id const& other) const {
	return m_value <= other.m_value;
}

namespace {
    static std::atomic<std::size_t> g_uniqueIdValue(0);
}

ppr::model::unique_id ppr::model::unique_id::generate() {
	return unique_id(++g_uniqueIdValue);
}
