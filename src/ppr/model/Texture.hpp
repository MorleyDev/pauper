#ifndef PAUPER_MODEL_TEXTURE_HPP_INCLUDED
#define PAUPER_MODEL_TEXTURE_HPP_INCLUDED

#include "../extern.hpp"
#include <cstdint>

namespace ppr {
	namespace model {
		class Texture {
		private:
			std::int32_t m_width;
			std::int32_t m_height;

		public:
			Texture(std::int32_t width, std::int32_t height)
					: m_width(width), m_height(height) {
			}

			PPR_EXTERN virtual ~Texture();

			inline std::int32_t width() const { return m_width; }
			inline std::int32_t height() const  { return m_height; }
		};
	}
}

#endif//PAUPER_MODEL_TEXTURE_HPP_INCLUDED
