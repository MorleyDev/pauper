#ifndef PAUPER_MODEL_MUSICSTREAM_HPP_INCLUDED
#define PAUPER_MODEL_MUSICSTREAM_HPP_INCLUDED

#include "../extern.hpp"

#include <chrono>

namespace ppr {
	namespace model {
	    class MusicStream {
	    private:
		    std::chrono::milliseconds m_duration;

	    public:
		    PPR_EXTERN explicit MusicStream(std::chrono::milliseconds duration);
		    PPR_EXTERN virtual ~MusicStream();

		    std::chrono::milliseconds getDuration() const { return m_duration; }
	    };
	}
}

#endif//PAUPER_MODEL_MUSICSTREAM_HPP_INCLUDED
