#ifndef PPR_MODEL_SHADER_HPP_INCLUDED
#define PPR_MODEL_SHADER_HPP_INCLUDED

#include "../extern.hpp"

#include "ShaderType.hpp"
#include "position.hpp"
#include "colour.hpp"
#include "Texture.hpp"

#include <string>

namespace ppr {
	namespace model {
		class Shader {
		private:
			ShaderType m_type;

		public:
			PPR_EXTERN explicit Shader(ShaderType type);
			PPR_EXTERN virtual ~Shader();

			ShaderType getType() const { return m_type; }

			virtual void set(std::string, double) = 0;
			virtual void set(std::string, pos2f) = 0;
			virtual void set(std::string, colour) = 0;
			virtual void set(std::string, Texture&) = 0;
			virtual void setCurrentTexture(std::string) = 0;
		};
	}
}

#endif//PPR_MODEL_SHADER_HPP_INCLUDED

