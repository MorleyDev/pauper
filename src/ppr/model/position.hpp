#ifndef PPR_MODEL_POSITION_HPP_INCLUDED
#define PPR_MODEL_POSITION_HPP_INCLUDED

#include <cstdint>

namespace ppr {
	namespace model {
		template<typename T, unsigned D> struct position;

	    template<typename T> struct position<T,2> {
		    position()
			        : x(),
			          y() {
	        }

		    position(T x, T y)
				    : x(x),
				      y(y) {
		    }

		    position(position const& orig)
				    : x(orig.x),
				      y(orig.y) {
		    }

		    position& operator=(position const& orig) {
			    x = orig.x;
			    y = orig.y;
			    return *this;
		    }

	        T x;
		    T y;
	    };

		typedef position<double,2> pos2f;
	    typedef position<std::int32_t,2> pos2i;
	}
}

#endif//PPR_MODEL_POSITION_HPP_INCLUDED
