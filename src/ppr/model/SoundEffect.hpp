#ifndef PPR_MODEL_SOUNDEFFECT_HPP_INCLUDED
#define PPR_MODEL_SOUNDEFFECT_HPP_INCLUDED

#include "../extern.hpp"
#include <chrono>

namespace ppr {
	namespace model {
		class SoundEffect {
		private:
			std::chrono::milliseconds m_duration;

		public:
			PPR_EXTERN explicit SoundEffect(std::chrono::milliseconds duration);
			PPR_EXTERN virtual ~SoundEffect();

			std::chrono::milliseconds getDuration() const { return m_duration; }
		};
	}
}

#endif//PPR_MODEL_SOUNDEFFECT_HPP_INCLUDED
