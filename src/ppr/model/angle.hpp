#ifndef PPR_MODEL_ANGLE_HPP_INCLUDED_HPP
#define PPR_MODEL_ANGLE_HPP_INCLUDED_HPP

#include "../tl/maths/degrees.hpp"

namespace ppr {
	namespace model {
		class angle {
		private:
			double m_radians;

		public:
			angle()
					: m_radians() {
			}

			angle(double radians)
					: m_radians(radians) {
			}

			angle(angle const& orig)
					: m_radians(orig.m_radians) {
			}

			angle& operator=(angle const& orig) {
				m_radians = orig.m_radians;
				return *this;
			}

			static angle fromRadians(double radians) {
				return angle(radians);
			}

			static angle fromDegrees(double degrees) {
				return angle(tl::maths::degrees_to_radians(degrees));
			}

			double asDegrees() const {
				return tl::maths::radians_to_degrees(m_radians);
			}

			double asRadians() const {
				return m_radians;
			}

			bool operator==(angle const& o) const { return m_radians == o.m_radians; }
			bool operator!=(angle const& o) const { return m_radians != o.m_radians; }
			bool operator>=(angle const& o) const { return m_radians >= o.m_radians; }
			bool operator<=(angle const& o) const { return m_radians <= o.m_radians; }
			bool operator>(angle const& o) const { return m_radians > o.m_radians; }
			bool operator<(angle const& o) const { return m_radians < o.m_radians; }

			angle operator+(angle const& o) const { return fromRadians(m_radians + o.m_radians); }
			angle operator-(angle const& o) const { return fromRadians(m_radians - o.m_radians); }
			angle operator*(double const& o) const { return fromRadians(m_radians * o); }
			angle operator/(double const& o) const { return fromRadians(m_radians / o); }

			angle& operator+=(angle const& o) { m_radians += o.m_radians; return *this; }
			angle& operator-=(angle const& o) { m_radians -= o.m_radians; return *this; }
			angle& operator*=(double const& o) { m_radians *= o; return *this; }
			angle& operator/=(double const& o) { m_radians /= o; return *this; }
		};
	}
}

#endif//PPR_MODEL_ANGLE_HPP_INCLUDED_HPP
