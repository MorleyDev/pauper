#ifndef PPR_MODEL_ROTATION_HPP_INCLUDED
#define PPR_MODEL_ROTATION_HPP_INCLUDED

#include "position.hpp"
#include "angle.hpp"

namespace ppr {
	namespace model {
		struct rotation {
			rotation()
					: origin(0.0, 0.0),
					  angle(0.0) {
			}

			rotation(model::pos2f origin, model::angle angle)
					: origin(origin),
					  angle(angle) {
			}

			pos2f origin;
			model::angle angle;
		};
	}
}

#endif//PPR_MODEL_ROTATION_HPP_INCLUDED
