#ifndef PPR_MODEL_SHADERTYPE_HPP_INCLUDED
#define PPR_MODEL_SHADERTYPE_HPP_INCLUDED

namespace ppr {
    namespace model {
        enum class ShaderType {
	        Vertex,
	        Fragment,
	        VertexFragment
        };
    }
}

#endif//PPR_MODEL_SHADERTYPE_HPP_INCLUDED

