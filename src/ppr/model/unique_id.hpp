#ifndef PAUPER_MODEL_UNIQUE_ID_HPP_INCLUDED
#define PAUPER_MODEL_UNIQUE_ID_HPP_INCLUDED

#include "../extern.hpp"

#include <cstdlib>

namespace ppr {
    namespace model {
        class unique_id {
        private:
	        std::size_t m_value;

	        PPR_EXTERN explicit unique_id(std::size_t value);

        public:
	        PPR_EXTERN unique_id();

	        PPR_EXTERN unique_id(unique_id const&);

	        PPR_EXTERN unique_id & operator=(unique_id const&);

	        PPR_EXTERN bool empty() const;

	        PPR_EXTERN bool operator==(unique_id const& other) const;
	        PPR_EXTERN bool operator!=(unique_id const& other) const;
	        PPR_EXTERN bool operator>(unique_id const& other) const;
	        PPR_EXTERN bool operator>=(unique_id const& other) const;
	        PPR_EXTERN bool operator<(unique_id const& other) const;
	        PPR_EXTERN bool operator<=(unique_id const& other) const;

	        PPR_EXTERN static unique_id generate();
        };
    }
}

#endif//PAUPER_MODEL_UNIQUE_ID_HPP_INCLUDED
