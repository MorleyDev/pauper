#ifndef PPR_MODEL_RECTANGLE_HPP_INCLUDED
#define PPR_MODEL_RECTANGLE_HPP_INCLUDED

#include <cstdint>
#include <cmath>

namespace ppr {
	namespace model {
		template<typename T> struct rectangle {
			rectangle()
					: x(), y(),
					  width(), height() {
			}

			rectangle(T x, T y, T w, T h)
					: x(x), y(y), width(w), height(h) {
			}

			rectangle(rectangle const& orig)
					: x(orig.x),
					  y(orig.y),
					  width(orig.width),
					  height(orig.height) {
			}

			rectangle& operator=(rectangle const& orig) {
				x = orig.x;
				y = orig.y;
				width = orig.width;
				height = orig.height;
				return *this;
			}

			T x;
			T y;

			T width;
			T height;
		};
		typedef rectangle<std::double_t> rectf;
	    typedef rectangle<std::int32_t> recti;
	}
}

#endif//PPR_MODEL_RECTANGLE_HPP_INCLUDED
