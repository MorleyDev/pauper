#ifndef PPR_RUN_HPP_INCLUDED
#define PPR_RUN_HPP_INCLUDED

#include "config.hpp"
#include "extern.hpp"
#include <future>

namespace ppr {
	class Game;

    extern PPR_EXTERN void run(Game& game);
}

#endif//PPR_RUN_HPP_INCLUDED
