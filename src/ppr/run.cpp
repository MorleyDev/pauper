#include "run.hpp"
#include "Game.hpp"
#include "tl/chrono.hpp"
#include "util/Log.hpp"

#ifdef PPR_SINGLE_THREADED
#include <thread>

	void ppr::run(Game& game) {
		PPR_CREATE_LOG(Run);
		PPR_WRITE_LOG(Run, Info, "Started");

	    std::size_t updateCount = 0;
	    const std::chrono::microseconds updateRate(10000);

	    std::size_t drawCount = 0;
	    const std::chrono::microseconds drawRate(1000);

	    const auto startTime = ppr::tl::chrono::now();
	    auto nextUpdate = startTime + updateRate;
	    auto nextDraw = startTime;

	    while (game.isAlive()) {
		    std::this_thread::sleep_until(std::min(nextUpdate, nextDraw));

		    const auto currentTime = tl::chrono::now();

		    while (nextUpdate <= currentTime) {
			    game.update(std::chrono::microseconds(updateRate)).wait();
			    game.poll_events().wait();
			    game.clean();

			    ++updateCount;
			    nextUpdate += updateRate;
		    }

		    if (nextDraw <= currentTime) {
			    game.draw();

			    ++drawCount;
			    nextDraw = currentTime + drawRate;
		    }

		    game.poll_system();
	    }
	    const auto endTime = tl::chrono::now();

		PPR_WRITE_LOG(Run, Info, "[Update FPS: %]", (updateCount / tl::chrono::to_seconds(endTime - startTime).count()));
		PPR_WRITE_LOG(Run, Info, "[Draw FPS: %]", (drawCount / tl::chrono::to_seconds(endTime - startTime).count()));
	}
#else //!PPR_SINGLE_THREADED

namespace ppr {
	std::future<void> create_update_thread(Game& game);
	std::future<void> create_local_draw_thread(Game& game);
	std::future<void> create_local_system_draw_thread(Game& game);
	std::future<void> create_system_thread(Game& game);
}

void ppr::run(Game& game) {
	PPR_CREATE_LOG(Run);
	PPR_WRITE_LOG(Run, Info, "Started");

	if (game.hasUniqueSystemThread()) {
		auto systemThread = create_system_thread(game);
		auto updateThread = create_update_thread(game);
		auto drawThread = create_local_draw_thread(game);
		PPR_WRITE_LOG(Run, Info, "Launched threads");

		drawThread.get();
		updateThread.get();
		systemThread.get();
		PPR_WRITE_LOG(Run, Info, "Done");
		return;
	}
	auto updateThread = create_update_thread(game);
	auto drawThread = create_local_system_draw_thread(game);
	PPR_WRITE_LOG(Run, Info, "Launched threads");

	drawThread.get();
	updateThread.get();
	PPR_WRITE_LOG(Run, Info, "Done");
}

std::future<void> ppr::create_update_thread(Game& game) {
	return std::async(std::launch::async, [&game]() {
		PPR_CREATE_LOG(UpdateThread);
		PPR_WRITE_LOG(UpdateThread, Info, "Launched...");

	    std::size_t updateCount = 0;
	    const std::chrono::microseconds updateRate(10000);

	    const auto startTime = ppr::tl::chrono::now();
	    auto nextUpdate = startTime + updateRate;
	    while (game.isAlive()) {
		    std::this_thread::sleep_until(nextUpdate);

		    const auto currentTime = tl::chrono::now();

		    while (nextUpdate <= currentTime) {
			    auto events = game.poll_events();
			    auto update = game.update(std::chrono::microseconds(updateRate));
			    events.wait();
			    update.wait();
			    game.clean();

			    ++updateCount;
			    nextUpdate += updateRate;
		    }
	    }
	    const auto endTime = tl::chrono::now();

		PPR_WRITE_LOG(UpdateThread, Info, "FPS: %", (updateCount / tl::chrono::to_seconds(endTime - startTime).count()));
		PPR_WRITE_LOG(UpdateThread, Info, "Closing...");
	});
}

std::future<void> ppr::create_system_thread(Game& game) {
	return std::async(std::launch::async, [&game]() {
		PPR_CREATE_LOG(SystemThread);
		PPR_WRITE_LOG(SystemThread, Info, "Launched...");

		while (game.isAlive()) {
			game.poll_system();
			std::this_thread::sleep_for(std::chrono::microseconds(1000));
		}
		PPR_WRITE_LOG(SystemThread, Info, "Closing...");
	});
}

std::future<void> ppr::create_local_draw_thread(Game& game) {
	return std::async(std::launch::deferred, [&game]() {
		PPR_CREATE_LOG(DrawThread);
		PPR_WRITE_LOG(DrawThread, Info, "Launched...");

		std::size_t drawCount = 0;
		const std::chrono::microseconds drawRate(1000);

		const auto startTime = ppr::tl::chrono::now();
		auto nextDraw = startTime;
		while (game.isAlive()) {
			std::this_thread::sleep_until(nextDraw);

			const auto currentTime = tl::chrono::now();

			if (nextDraw > currentTime) {
				continue;
			}

			game.draw();

			++drawCount;
			nextDraw = currentTime + drawRate;
		}
		const auto endTime = tl::chrono::now();

		PPR_WRITE_LOG(DrawThread, Info, "FPS: %", (drawCount / tl::chrono::to_seconds(endTime - startTime).count()));
		PPR_WRITE_LOG(DrawThread, Info, "Closing...");
	});
}

std::future<void> ppr::create_local_system_draw_thread(Game& game) {
	return std::async(std::launch::deferred, [&game]() {
		PPR_CREATE_LOG(DrawThread);
		PPR_WRITE_LOG(DrawThread, Info, "Launched...");

		std::size_t drawCount = 0;
		const std::chrono::microseconds drawRate(1000);

		const auto startTime = ppr::tl::chrono::now();
		auto nextDraw = startTime;
		while (game.isAlive()) {
			game.poll_system();
			std::this_thread::sleep_until(nextDraw);

			const auto currentTime = tl::chrono::now();
			if (nextDraw > currentTime) {
				continue;
			}

			game.draw();

			++drawCount;
			nextDraw = currentTime + drawRate;
		}
		const auto endTime = tl::chrono::now();

		PPR_WRITE_LOG(DrawThread, Info, "FPS: %", (drawCount / tl::chrono::to_seconds(endTime - startTime).count()));
		PPR_WRITE_LOG(DrawThread, Info, "Closing...");
	});
}

#endif//PPR_SINGLE_THREADED
