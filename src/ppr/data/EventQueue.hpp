#ifndef PAUPER_DATA_EVENTQUEUE_HPP_INCLUDED
#define PAUPER_DATA_EVENTQUEUE_HPP_INCLUDED

#include "../extern.hpp"
#include "../tl/any.hpp"
#include <vector>
#include <mutex>
#include <typeindex>
#include <memory>

namespace ppr {
	namespace data {
		class EventQueue {
		private:
			typedef std::vector< std::pair<std::type_index, tl::any> > queue;
			std::mutex m_queueLock;
			queue m_eventQueue;

			PPR_EXTERN queue _dequeue();

		public:
			template <typename T> void enqueue(T const& data) {
				enqueue(typeid(T), tl::any(data));
			}
			PPR_EXTERN void enqueue(std::type_index type, tl::any data);

			template<typename F> void dequeue(F handleEvents) {
				queue q;
				do {
					q = _dequeue();
					for (auto const& d : q)
						handleEvents(d.first, d.second);
				} while(!q.empty());
			}
		};
	}
}

#endif//PAUPER_DATA_EVENTQUEUE_HPP_INCLUDED
