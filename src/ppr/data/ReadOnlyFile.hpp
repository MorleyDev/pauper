#ifndef PAUPER_DATA_READONLYFILE_HPP_INCLUDED
#define PAUPER_DATA_READONLYFILE_HPP_INCLUDED

#include "../extern.hpp"
#include <memory>
#include <istream>

namespace ppr {
	namespace data {
	    class ReadOnlyFile {
	    private:
		    std::unique_ptr<std::istream> m_stream;
		    std::size_t m_fileSize;
		    std::size_t m_position;

	    public:
		    PPR_EXTERN ReadOnlyFile(std::unique_ptr<std::istream> stream, std::size_t fileSize);
		    PPR_EXTERN ~ReadOnlyFile();

		    PPR_EXTERN ReadOnlyFile(ReadOnlyFile&& orig);
		    PPR_EXTERN ReadOnlyFile& operator=(ReadOnlyFile&& orig);

		    std::size_t size() const { return m_fileSize; }
		    std::size_t position() const { return m_position; }
		    bool eof() const { return m_position == m_fileSize; }

		    PPR_EXTERN void seek(std::size_t position);
		    PPR_EXTERN std::size_t read(char* out, std::size_t count);
	    };
	}
}

#endif//PAUPER_DATA_READONLYFILE_HPP_INCLUDED
