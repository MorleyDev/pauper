#ifndef PAUPER_DATA_RENDERER_HPP_INCLUDED
#define PAUPER_DATA_RENDERER_HPP_INCLUDED

#include "../extern.hpp"
#include "RenderTarget.hpp"
#include "RenderTexture.hpp"
#include <memory>

namespace ppr {
	namespace data {
		class ReadOnlyFile;
		class SpriteBatch;

		class Renderer : public RenderTarget {
		public:
			PPR_EXTERN Renderer();
			PPR_EXTERN virtual ~Renderer();

			virtual std::unique_ptr<RenderTexture> createRenderTexture(std::size_t width, std::size_t height) = 0;
			virtual std::unique_ptr<model::Texture> createTexture(ReadOnlyFile& sourceFile) = 0;
			virtual std::unique_ptr<model::Texture> createTexture(std::uint8_t const* data, std::size_t width, std::size_t height) = 0;
			virtual std::unique_ptr<model::Shader> createShader(ReadOnlyFile& sourceFile, model::ShaderType type) = 0;
			virtual std::unique_ptr<model::Shader> createShader(ReadOnlyFile& vertexSourceFile, ReadOnlyFile& fragmentSourceFile) = 0;
		};
	}
}

#endif//PAUPER_DATA_RENDERER_HPP_INCLUDED
