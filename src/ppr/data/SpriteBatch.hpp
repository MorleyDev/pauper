#ifndef PPR_DATA_SPRITEBATCH_HPP_INCLUDED
#define PPR_DATA_SPRITEBATCH_HPP_INCLUDED

#include "../extern.hpp"
#include "../model/position.hpp"
#include "../model/rotation.hpp"
#include "../model/rectangle.hpp"
#include "../model/colour.hpp"

namespace ppr {
	namespace data {
		class SpriteBatch {
		public:
			PPR_EXTERN virtual ~SpriteBatch();

			virtual SpriteBatch& draw(model::rectf destination, model::recti source, model::rotation rotation, model::colour colour) = 0;

			virtual SpriteBatch& draw(model::rectf destination, model::recti source, model::colour colour) {
				return draw(destination, source, model::rotation(model::pos2f(0.0,0.0), 0.0), colour);
			}

			virtual SpriteBatch& draw(model::rectf destination, model::recti source, model::rotation rotation) {
				return draw(destination, source, rotation, model::colour(255,255,255,255));
			}

			inline SpriteBatch& draw(model::rectf destination, model::recti source) {
				return draw(destination, source, model::colour(255,255,255,255));
			}
		};
	}
}

#endif//PPR_DATA_SPRITEBATCH_HPP_INCLUDED
