#ifndef PAUPER_DATA_RENDERTEXTURE_HPP
#define PAUPER_DATA_RENDERTEXTURE_HPP

#include "../extern.hpp"
#include "RenderTarget.hpp"

namespace ppr {
	namespace data {
		class RenderTexture : public RenderTarget {
		public:
			PPR_EXTERN RenderTexture();
			PPR_EXTERN virtual ~RenderTexture();

			virtual model::Texture& getTexture() = 0;
		};
	}
}


#endif //PAUPER_DATA_RENDERTEXTURE_HPP
