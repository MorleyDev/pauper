//
// Created by Jason on 07/02/2016.
//

#ifndef PAUPER_DATA_RENDERTARGET_HPP
#define PAUPER_DATA_RENDERTARGET_HPP

#include "../extern.hpp"
#include "../model/colour.hpp"
#include "../model/rectangle.hpp"
#include "../model/rotation.hpp"

namespace ppr {
	namespace model {
		class Shader;
		class Texture;
		enum class ShaderType;
	}

	namespace data {
		class SpriteBatch;

		class RenderTarget {
		private:
			model::colour m_clearColour;

		public:
			PPR_EXTERN RenderTarget();
			PPR_EXTERN virtual ~RenderTarget();

			virtual void clear() = 0;
			virtual void display() = 0;

			virtual void setLogicalSize(model::rectf size) = 0;
			virtual model::rectf getLogicalSize() const = 0;

			inline void setClearColour(model::colour c) { m_clearColour = c; }
			inline model::colour getClearColour() const { return m_clearColour; }

			virtual std::unique_ptr<SpriteBatch> createBatch(model::Texture&) = 0;

			PPR_EXTERN void drawTexture(model::Texture& texture, model::rectf destination);
			PPR_EXTERN void drawTexture(model::Texture& texture, model::rectf destination, model::recti source);
			PPR_EXTERN void drawTexture(model::Texture& texture, model::rectf destination, model::recti source, model::colour colour);
			PPR_EXTERN void drawTexture(model::Texture& texture, model::rectf destination, model::recti source, model::rotation rotation);
			virtual void drawTexture(model::Texture& texture, model::rectf destination, model::recti source, model::rotation rotation, model::colour colour) = 0;

			virtual void drawRectangle(model::rectf destination, model::colour colour, bool isFilled = true) = 0;

			virtual void bindShader(model::Shader& shader) = 0;
			virtual void unbindShader() = 0;
		};
	}
}


#endif //PAUPER_DATA_RENDERTARGET_HPP
