#ifndef PPR_DATA_VIRTUALMACHINE_HPP_INCLUDED
#define PPR_DATA_VIRTUALMACHINE_HPP_INCLUDED

#include "../tl/optional.hpp"
#include "../tl/any.hpp"

#include <vector>
#include <functional>

namespace ppr {
	namespace data {
		class VirtualMachine {
		public:
			typedef std::function<std::vector<tl::any> (std::vector<tl::any>)> Function;
			typedef double Number;
			typedef bool Boolean;
			typedef std::string String;
			struct Table { };

			VirtualMachine();
			virtual ~VirtualMachine();

			virtual void attach(std::string) = 0;

			virtual tl::any get(std::string) = 0;
			template<typename T> T get(std::string name) { return get(name).as<T>(); }

			virtual void set(std::string, Function) = 0;
			virtual void set(std::string, Number) = 0;
			virtual void set(std::string, Boolean) = 0;
			virtual void set(std::string, String) = 0;
			virtual void set(std::string, Table) = 0;
		};
	}
}

#endif//PPR_DATA_VIRTUALMACHINE_HPP_INCLUDED
