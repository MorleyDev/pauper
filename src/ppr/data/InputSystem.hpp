#ifndef PAUPER_DATA_INPUTSYSTEM_HPP_INCLUDED
#define PAUPER_DATA_INPUTSYSTEM_HPP_INCLUDED

#include "../extern.hpp"
#include "../tl/signals.hpp"
#include "../tl/exchange.hpp"
#include "../model/KeyCode.hpp"

#include <functional>
#include <initializer_list>
#include <atomic>
#include <map>
#include <cstdint>

namespace ppr {
	namespace data {
		class InputSystem {
		public:
			enum class ButtonState {
				Pressed,
				Released
			};

			struct MousePosition {
				std::int32_t x;
				std::int32_t y;
			};

			struct TouchPosition {
				std::uint32_t finger;
				std::int32_t x;
				std::int32_t y;
			};

		private:
			std::atomic<bool> m_isTextMode;

			tl::signals<model::KeyCode, ButtonState> m_onKeyboard;
			tl::signals<model::KeyCode> m_onText;
			tl::signals<int8_t, ButtonState, MousePosition> m_onMouseClick;
			tl::signals<MousePosition> m_onMouseMove;
			tl::signals<TouchPosition, ButtonState> m_onTouch;
			tl::signals<TouchPosition> m_onTouchMove;

			std::map<model::KeyCode, ButtonState> m_pressedKeys;

		protected:
			inline void signalKeyboard(model::KeyCode code, ButtonState state) {
				auto pair = m_pressedKeys.insert(std::make_pair(code, state));
				if (!pair.second) {
					// If no change, return
					if (tl::exchange(pair.first->second, state) == state) {
						return;
					}
				}

				if (m_isTextMode.load()) {
					return;
				}
				m_onKeyboard.signal(code, state);
			}
			inline void signalText(model::KeyCode code) {
				if (!m_isTextMode.load()) {
					return;
				}
				m_onText.signal(code);
			}
			inline void signalMouseClick(std::int8_t code, ButtonState state, MousePosition position) {
				m_onMouseClick.signal(code, state, position);
			}
			inline void signalMouseMove(MousePosition position) {
				m_onMouseMove.signal(position);
			}
			inline void signalTouch(TouchPosition position, ButtonState state) {
				m_onTouch.signal(position, state);
			}
			inline void signalTouchMove(TouchPosition position) {
				m_onTouchMove.signal(position);
			}

		public:
			PPR_EXTERN InputSystem();
			PPR_EXTERN virtual ~InputSystem();

			/*!
			* Text mode is used for GUIs entering text into a box, and allows for clicking away from the box with the
			* mouse or typing with text. It disables keyboard messages and sends text messages instead.
			*
			* On mobile this should be inherited from to bring up a virtual keyboard.
			*/
			virtual void enableTextMode() { m_isTextMode.store(true); }
			virtual void disableTextMode() { m_isTextMode.store(false); }
			inline bool isTextMode() const { return m_isTextMode.load(); }

			inline void onKeyboard(std::function<void (model::KeyCode, ButtonState)> h) { m_onKeyboard.listen(h); }
			inline void onText(std::function<void (model::KeyCode)> h) { m_onText.listen(h); }

			inline void onMouseClick(std::function<void (std::int8_t, ButtonState, MousePosition)> h) { m_onMouseClick.listen(h); }
			inline void onMouseMove(std::function<void (MousePosition)> h) { m_onMouseMove.listen(h); }

			inline void onTouch(std::function<void (TouchPosition, ButtonState)> h) { m_onTouch.listen(h); }
			inline void onTouchMove(std::function<void (TouchPosition)> h) { m_onTouchMove.listen(h); }
		};
	}
}

#endif//PAUPER_DATA_INPUTSYSTEM_HPP_INCLUDED
