#include "FolderFileSystem.hpp"
#include "../../util/Log.hpp"
#include <fstream>
#include <sys/stat.h>

namespace {
	PPR_CREATE_LOG(FolderFileSystem);
}

ppr::tl::optional<std::size_t> ppr::data::fs::FolderFileSystem::getFileSize(std::string uri) const {
	struct stat stat_buf;
	return stat(uri.c_str(), &stat_buf) == 0
			? tl::some(static_cast<std::size_t>(stat_buf.st_size))
			: tl::none();
}

ppr::tl::optional<ppr::data::ReadOnlyFile> ppr::data::fs::FolderFileSystem::open(std::string uri) const {
	std::string targetUri = m_fileSystemRoot + "/" + uri;
	auto size = getFileSize(targetUri);
	if (!size) {
		PPR_WRITE_LOG(FolderFileSystem, Info, "Attempt to get size of file % failed.", targetUri);
		return tl::none();
	}

	std::unique_ptr<std::ifstream> file(new std::ifstream(targetUri, std::ios::binary));
	if (!file->is_open()) {
		PPR_WRITE_LOG(FolderFileSystem, Info, "Attempt to open file ", targetUri, " failed.");
		return tl::none();
	}
	PPR_WRITE_LOG(FolderFileSystem, Info, "Opened file: %", targetUri);
	return tl::some(ReadOnlyFile(std::move(file), *size));
}

ppr::data::fs::FolderFileSystem::~FolderFileSystem() {
}

ppr::data::fs::FolderFileSystem::FolderFileSystem(std::string root)
		: m_fileSystemRoot(root) {
}
