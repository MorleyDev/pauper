#ifndef PAUPER_DATA_FS_FOLDERFILESYSTEM_HPP_INCLUDED
#define PAUPER_DATA_FS_FOLDERFILESYSTEM_HPP_INCLUDED

#include "../../extern.hpp"
#include "../FileSystem.hpp"

namespace ppr {
	namespace data {
		namespace fs {
			class FolderFileSystem : public FileSystem {
			private:
				std::string m_fileSystemRoot;

                PPR_EXTERN tl::optional<std::size_t> getFileSize(std::string uri) const;

			public:
				PPR_EXTERN explicit FolderFileSystem(std::string root);
				PPR_EXTERN virtual ~FolderFileSystem();

				PPR_EXTERN virtual tl::optional<ReadOnlyFile> open(std::string uri) const;
			};
		}
	}
}

#endif//PAUPER_DATA_FS_FOLDERFILESYSTEM_HPP_INCLUDED
