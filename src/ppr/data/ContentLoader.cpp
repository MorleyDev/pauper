#include "ContentLoader.hpp"

ppr::data::ContentLoader::ContentLoader(ppr::data::FileSystem& fs, 
                                        ppr::data::Renderer& renderer,
                                        ppr::data::MusicPlayer& music,
                                        ppr::data::SoundEffectPlayer& sound)
	: m_filesystem(fs),
	  m_renderer(renderer),
	  m_music(music),
	  m_sound(sound) {

}
