#include "ContextFactory.hpp"
#include "SysContext.hpp"

#ifdef PPR_SFML_ENABLE
#include "sfml2/SfmlContext.hpp"
#endif//PPR_SFML_ENABLE

#ifdef PPR_SDL2_ENABLE
#include "sdl2/Sdl2SysContext.hpp"
#endif//PPR_SDL2_ENABLE

#include "null/NullSysContext.hpp"

std::unique_ptr<ppr::data::SysContext> ppr::data::createContext(std::string driver) {
#ifdef PPR_SFML_ENABLE
	if (driver == "default" || driver == "sfml2") {
		return tl::make_unique<sfml2::SfmlContext>();
	}
#endif//PPR_SFML_ENABLE

#ifdef PPR_SDL2_ENABLE
	if (driver == "default" || driver == "sdl2") {
		return tl::make_unique<sdl2::Sdl2SysContext>();
	}
#endif//PPR_SDL2_ENABLE

	if (driver == "default" || driver == "null") {
		return tl::make_unique<null::NullSysContext>();
	}

	throw std::runtime_error("Unrecognised driver: " + driver);
}

std::vector<std::string> ppr::data::getSupportedContexts() {
	return std::vector<std::string>({
#ifdef PPR_SFML_ENABLE
		"sfml2",
#endif//PPR_SFML_ENABLE
#ifdef PPR_SDL2_ENABLE
		"sdl2",
#endif//PPR_SDL2_ENABLE
		"null"
	});
}
