#ifndef PPR_DATA_LUA_LUAVIRTUALMACHINE_HPP_INCLUDED
#define PPR_DATA_LUA_LUAVIRTUALMACHINE_HPP_INCLUDED

#include "../../extern.hpp"
#include "../VirtualMachine.hpp"
#include <lua.hpp>

namespace ppr {
	namespace data {
		namespace lua {
			class LuaVirtualMachine : public VirtualMachine {
			private:
				std::unique_ptr<lua_State, void (*)(lua_State*)> m_state;
				std::vector<Function> m_functionTable;

				struct LuaFunction {
				private:
					lua_State* m_state;
					int m_ref;
					std::shared_ptr<int> references;

				public:
					LuaFunction(lua_State* vm, int ref);
					LuaFunction(LuaFunction const&);
					LuaFunction& operator=(LuaFunction const&);
					LuaFunction(LuaFunction&&);
					LuaFunction& operator=(LuaFunction&&);
					~LuaFunction();
					std::vector<tl::any> operator()(std::vector<tl::any>);
				};

			public:
				PPR_EXTERN LuaVirtualMachine();
				PPR_EXTERN virtual ~LuaVirtualMachine();

				PPR_EXTERN virtual void attach(std::string);
				PPR_EXTERN virtual tl::any get(std::string);

				PPR_EXTERN virtual void set(std::string, Function);
				PPR_EXTERN virtual void set(std::string, Number);
				PPR_EXTERN virtual void set(std::string, Boolean);
				PPR_EXTERN virtual void set(std::string, String);
				PPR_EXTERN virtual void set(std::string, Table);

			private:
				static ppr::tl::any getTop(lua_State* state);
				static void push(lua_State* state, ppr::tl::any value);

				static int luaVM_CFunction(lua_State *L);
			};
		}
	}
}

#endif//PPR_DATA_LUA_LUAVIRTUALMACHINE_HPP_INCLUDED
