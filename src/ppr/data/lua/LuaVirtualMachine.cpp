#include "LuaVirtualMachine.hpp"

ppr::data::lua::LuaVirtualMachine::LuaVirtualMachine()
	: m_state(luaL_newstate(), lua_close),
	  m_functionTable() {
}

ppr::data::lua::LuaVirtualMachine::~LuaVirtualMachine() {
}

void ppr::data::lua::LuaVirtualMachine::attach(std::string code) {
	luaL_dostring(m_state.get(), code.c_str());
}

ppr::tl::any ppr::data::lua::LuaVirtualMachine::get(std::string var) {
	lua_getfield(m_state.get(), LUA_GLOBALSINDEX, var.c_str());
	return getTop(m_state.get());
}

ppr::tl::any ppr::data::lua::LuaVirtualMachine::getTop(lua_State* state) {
	ppr::tl::any result;
	switch(lua_type(state, -1)) {
		case LUA_TNUMBER:
			result = tl::any(static_cast<Number>(lua_tonumber(state, -1)));
			lua_pop(state, 1);
			break;

		case LUA_TBOOLEAN:
			result = tl::any(lua_toboolean(state, -1) != 0);
			lua_pop(state, 1);
			break;

		case LUA_TSTRING:
			result = tl::any(std::string(lua_tostring(state, -1)));
			lua_pop(state, 1);
			break;

		case LUA_TFUNCTION: {
			auto ref = luaL_ref(state, LUA_REGISTRYINDEX);
			result = tl::any(Function(LuaFunction(state, ref)));
			break;
		}

		case LUA_TLIGHTUSERDATA:
			result = tl::any(lua_touserdata(state, -1));
			lua_pop(state, 1);
			break;

		case LUA_TTABLE:
		case LUA_TUSERDATA:
		case LUA_TTHREAD:

		case LUA_TNIL:
		case LUA_TNONE:
		default:
			lua_pop(state, 1);
			break;
	}
	return result;
}

void ppr::data::lua::LuaVirtualMachine::set(std::string var, Function val) {
	auto index = m_functionTable.size();
	m_functionTable.push_back(std::move(val));

	lua_pushnumber(m_state.get(), static_cast<Number>(index));
	lua_pushlightuserdata(m_state.get(), static_cast<void*>(this));
	lua_pushcclosure(m_state.get(), luaVM_CFunction, 2);
	lua_setfield(m_state.get(), LUA_GLOBALSINDEX, var.c_str());
}

int ppr::data::lua::LuaVirtualMachine::luaVM_CFunction(lua_State* state) {
	auto argCount = lua_gettop(state);
	std::vector<ppr::tl::any> argVector(static_cast<std::size_t>(argCount));
	for (auto i = argCount; i > 0; --i) {
		argVector[i - 1] = getTop(state);
	}
	auto index = static_cast<std::size_t>(lua_tointeger(state, lua_upvalueindex(1)));
	auto self = static_cast<LuaVirtualMachine*>(lua_touserdata(state, lua_upvalueindex(2)));
	auto resVector = self->m_functionTable[index](argVector);
	for (auto& a : resVector) {
		push(state, a);
	}
	return resVector.size();
}

void ppr::data::lua::LuaVirtualMachine::set(std::string var, Number val) {
	lua_pushnumber(m_state.get(), val);
	lua_setfield(m_state.get(), LUA_GLOBALSINDEX, var.c_str());
}

void ppr::data::lua::LuaVirtualMachine::set(std::string var, Boolean val) {
	lua_pushboolean(m_state.get(), val ? 1 : 0);
	lua_setfield(m_state.get(), LUA_GLOBALSINDEX, var.c_str());
}

void ppr::data::lua::LuaVirtualMachine::set(std::string var, String val) {
	lua_pushstring(m_state.get(), val.c_str());
	lua_setfield(m_state.get(), LUA_GLOBALSINDEX, var.c_str());
}

void ppr::data::lua::LuaVirtualMachine::set(std::string var, Table) {
	lua_newtable(m_state.get());
	lua_setfield(m_state.get(), LUA_GLOBALSINDEX, var.c_str());
}

ppr::data::lua::LuaVirtualMachine::LuaFunction::LuaFunction(lua_State* vm, int ref)
	: m_state(vm),
	  m_ref(ref),
	  references(std::make_shared<int>(1)) {
}

ppr::data::lua::LuaVirtualMachine::LuaFunction::~LuaFunction() {
	if (!references) {
		return;
	}
	if ( --(*references) == 0 ) {
		luaL_unref(m_state, LUA_REGISTRYINDEX, m_ref);
	}
}

ppr::data::lua::LuaVirtualMachine::LuaFunction::LuaFunction(LuaFunction const& f)
	: m_state(f.m_state),
	  m_ref(f.m_ref),
	  references(f.references) {
	++(*references);
}

ppr::data::lua::LuaVirtualMachine::LuaFunction::LuaFunction(LuaFunction&& f)
	: m_state(tl::exchange(f.m_state, nullptr)),
	  m_ref(f.m_ref),
	  references(std::move(f.references)) {
}

ppr::data::lua::LuaVirtualMachine::LuaFunction& ppr::data::lua::LuaVirtualMachine::LuaFunction::operator=(LuaFunction const& o) {
	if (o.references == references) {
		return *this;
	}
	if ( references && --(*references) == 0 ) {
		luaL_unref(m_state, LUA_REGISTRYINDEX, m_ref);
	}
	m_state = o.m_state;
	m_ref = o.m_ref;
	references = o.references;
	return *this;
}

ppr::data::lua::LuaVirtualMachine::LuaFunction& ppr::data::lua::LuaVirtualMachine::LuaFunction::operator=(LuaFunction&& o) {
	if (o.references == references) {
		return *this;
	}
	if ( references && --(*references) == 0 ) {
		luaL_unref(m_state, LUA_REGISTRYINDEX, m_ref);
	}
	m_state = tl::exchange(o.m_state, nullptr);
	m_ref = o.m_ref;
	references = std::move(o.references);
	return *this;
}

std::vector<ppr::tl::any> ppr::data::lua::LuaVirtualMachine::LuaFunction::operator()(std::vector<ppr::tl::any> arguments) {
	lua_rawgeti(m_state, LUA_REGISTRYINDEX, m_ref);
	for(auto& a : arguments) {
		push(m_state, a);
	}
	lua_call(m_state, static_cast<int>(arguments.size()), LUA_MULTRET);

	auto resultCount = lua_gettop(m_state);
	std::vector<ppr::tl::any> results(static_cast<std::size_t>(resultCount));
	for(auto i = resultCount; i > 0; --i) {
		results[i-1] = getTop(m_state);
	}
	return results;
}

void ppr::data::lua::LuaVirtualMachine::push(lua_State* state, ppr::tl::any value) {
	if (value.is<Number>()) {
		lua_pushnumber(state, value.as<Number>());
	} else if (value.is<String>()) {
		lua_pushstring(state, value.as<String>().c_str());
	} else if (value.is<Boolean>()) {
		lua_pushboolean(state, value.as<Boolean>() ? 1 : 0);
	} else {
		throw std::runtime_error("Not valid VM variable type");
	}
}
