#ifndef PPR_DATA_NULL_NULLSPRITEBATCH_HPP_INCLUDED
#define PPR_DATA_NULL_NULLSPRITEBATCH_HPP_INCLUDED

#include "../SpriteBatch.hpp"

namespace ppr {
	namespace data {
		namespace null {
			class NullSpriteBatch : public SpriteBatch {
			public:
				virtual SpriteBatch& draw(model::rectf destination,
				                          model::recti source,
				                          model::rotation rotation,
				                          model::colour colour) {
					return *this;
				}
			};
		}
	}
}

#endif//PPR_DATA_NULL_NULLSPRITEBATCH_HPP_INCLUDED
