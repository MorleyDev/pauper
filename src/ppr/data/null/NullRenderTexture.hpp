#ifndef PAUPER_DATA_NULL_NULLRENDERTEXTURE_HPP
#define PAUPER_DATA_NULL_NULLRENDERTEXTURE_HPP

#include "../RenderTexture.hpp"
#include "NullSpriteBatch.hpp"
#include "../../tl/make_unique.hpp"
#include <memory>

namespace ppr {
	namespace data {
		namespace null {
			class NullRenderTexture : public RenderTexture {
			private:
				std::unique_ptr<model::Texture> m_texture;

			public:
				NullRenderTexture() : m_texture(tl::make_unique<model::Texture>(0,0)) {
				}
				virtual model::Texture& getTexture() { return *m_texture; }

				virtual void setLogicalSize(model::rectf size) { }
				virtual model::rectf getLogicalSize() const { return model::rectf(0.0,0.0,640.0,480.0); }

				virtual void clear() { }
				virtual void display() { }

				virtual std::unique_ptr<SpriteBatch> createBatch(model::Texture&) { return tl::make_unique<NullSpriteBatch>(); }

				virtual void drawTexture(model::Texture& texture,
										 model::rectf destination, model::recti source,
										 model::rotation rotation,
										 model::colour colour) { }

				virtual void drawRectangle(model::rectf destination, model::colour colour, bool isFilled = true) { }

				virtual void bindShader(model::Shader& shader) { }
				virtual void unbindShader() { }

			};
		}
	}
}

#endif //PAUPER_DATA_NULL_NULLRENDERTEXTURE_HPP
