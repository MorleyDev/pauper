#ifndef PPR_DATA_NULL_NULLSHADER_HPP_INCLUDED
#define PPR_DATA_NULL_NULLSHADER_HPP_INCLUDED

#include "../../model/Shader.hpp"

namespace ppr {
	namespace data {
		namespace null {
			class NullShader : public model::Shader {
			public:
				NullShader(model::ShaderType type) : model::Shader(type) { }

				virtual void set(std::string, double) { }
				virtual void set(std::string, model::pos2f) { }
				virtual void set(std::string, model::colour) { }
				virtual void set(std::string, model::Texture&) { }
				virtual void setCurrentTexture(std::string) { }
			};
		}
	}
}

#endif//PPR_DATA_NULL_NULLSHADER_HPP_INCLUDED
