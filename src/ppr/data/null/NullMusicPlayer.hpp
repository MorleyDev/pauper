#ifndef PPR_DATA_NULL_NULLMUSICPLAYER_HPP_INCLUDED
#define PPR_DATA_NULL_NULLMUSICPLAYER_HPP_INCLUDED

#include "../MusicPlayer.hpp"
#include "../ReadOnlyFile.hpp"
#include "../../model/MusicStream.hpp"
#include "../../tl/make_unique.hpp"

namespace ppr {
	namespace data {
		namespace null {
			class NullMusicPlayer : public MusicPlayer {
			public:
				virtual std::unique_ptr<model::MusicStream> createMusicStream(ReadOnlyFile) {
					return tl::make_unique<model::MusicStream>(std::chrono::milliseconds(0));
				};

				virtual void play(model::MusicStream& stream, bool loop) { }
				virtual bool isPlaying(model::MusicStream const& stream) const { return false; }
				virtual void stop(model::MusicStream& stream) { }
				virtual void stopAll() { }

				virtual void setVolume(volume v) { }
				virtual volume getVolume() const { return volume(1.0); }

				virtual void update() { }
			};
		}
	}
}

#endif//PPR_DATA_NULL_NULLMUSICPLAYER_HPP_INCLUDED
