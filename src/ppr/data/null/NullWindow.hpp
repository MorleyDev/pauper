#ifndef PPR_DATA_NULL_NULLWINDOW_HPP_INCLUDED
#define PPR_DATA_NULL_NULLWINDOW_HPP_INCLUDED

#include "../Window.hpp"
#include "NullRenderer.hpp"
#include "../InputSystem.hpp"

namespace ppr {
	namespace data {
		namespace null {
			class NullWindow : public Window {
			private:
				NullRenderer m_renderer;
				InputSystem m_inputSystem;

			public:
				virtual Renderer& renderer() { return m_renderer; }
				virtual InputSystem& input() { return m_inputSystem; }

				virtual void onClose(std::function<void(void)>) { }

				virtual void poll() { }

				virtual model::pos2i resolution() const  { return model::pos2i(0,0); };
				virtual std::vector<model::pos2i> availableResolutions() const { return std::vector<model::pos2i>(); };

				virtual bool isPollTiedToCreationThread() const { return true; }
			};
		}
	}
}

#endif//PPR_DATA_NULL_NULLWINDOW_HPP_INCLUDED
