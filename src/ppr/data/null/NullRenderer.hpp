#ifndef PPR_DATA_NULL_NULLRENDERER_HPP_INCLUDED
#define PPR_DATA_NULL_NULLRENDERER_HPP_INCLUDED

#include "NullShader.hpp"
#include "NullSpriteBatch.hpp"
#include "NullRenderTexture.hpp"
#include "../Renderer.hpp"
#include "../ReadOnlyFile.hpp"
#include "../../model/Texture.hpp"
#include "../../tl/make_unique.hpp"

namespace ppr {
	namespace data {
		namespace null {
			class NullRenderer : public Renderer {
			public:
				virtual void setLogicalSize(model::rectf size) { }
				virtual model::rectf getLogicalSize() const { return model::rectf(0.0,0.0,640.0,480.0); }

				virtual void clear() { }
				virtual void display() { }

				virtual std::unique_ptr<SpriteBatch> createBatch(model::Texture&) { return tl::make_unique<NullSpriteBatch>(); }

				virtual void drawTexture(model::Texture& texture,
				                         model::rectf destination, model::recti source,
				                         model::rotation rotation,
				                         model::colour colour) { }

				virtual void drawRectangle(model::rectf destination, model::colour colour, bool isFilled = true) { }

				virtual void bindShader(model::Shader& shader) { }
				virtual void unbindShader() { }

				virtual std::unique_ptr<RenderTexture> createRenderTexture(std::size_t width, std::size_t height) { return tl::make_unique<NullRenderTexture>(); }
				virtual std::unique_ptr<model::Texture> createTexture(ReadOnlyFile& sourceFile) { return tl::make_unique<model::Texture>(0,0); }
				virtual std::unique_ptr<model::Texture> createTexture(std::uint8_t const* data, std::size_t width, std::size_t height) { return tl::make_unique<model::Texture>(width, height); }
				virtual std::unique_ptr<model::Shader> createShader(ReadOnlyFile& sourceFile, model::ShaderType type) { return tl::make_unique<NullShader>(type); }
				virtual std::unique_ptr<model::Shader> createShader(ReadOnlyFile& vertexSourceFile, ReadOnlyFile& fragmentSourceFile) { return tl::make_unique<NullShader>(model::ShaderType::VertexFragment); }
			};
		}
	}
}

#endif//PPR_DATA_NULL_NULLRENDERER_HPP_INCLUDED
