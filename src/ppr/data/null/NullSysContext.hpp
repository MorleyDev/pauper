#ifndef PPR_DATA_NULL_NULLSYSCONTEXT_HPP_INCLUDED
#define PPR_DATA_NULL_NULLSYSCONTEXT_HPP_INCLUDED

#include "../SysContext.hpp"
#include "NullWindow.hpp"
#include "NullMusicPlayer.hpp"
#include "NullSoundEffectPlayer.hpp"

namespace ppr {
	namespace data {
		namespace null {
			class NullSysContext : public SysContext {
			public:
				virtual std::unique_ptr<Window> createWindow(std::string, std::size_t w, std::size_t h, bool fullscreen, bool vsync) {
					return tl::make_unique<NullWindow>();
				}

				virtual std::unique_ptr<MusicPlayer> createMusicPlayer(){
					return tl::make_unique<NullMusicPlayer>();
				}

				virtual std::unique_ptr<SoundEffectPlayer> createSoundEffectPlayer() {
					return tl::make_unique<NullSoundEffectPlayer>();
				}
			};
		}
	}
}

#endif//PPR_DATA_NULL_NULLSYSCONTEXT_HPP_INCLUDED
