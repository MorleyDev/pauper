#ifndef PAUPER_DATA_FILESYSTEM_HPP_INCLUDED
#define PAUPER_DATA_FILESYSTEM_HPP_INCLUDED

#include "../extern.hpp"
#include "ReadOnlyFile.hpp"
#include "../tl/optional.hpp"

namespace ppr {
	namespace data {
		class FileSystem {
		public:
            PPR_EXTERN FileSystem();
			PPR_EXTERN virtual ~FileSystem();

			virtual tl::optional<ReadOnlyFile> open(std::string) const = 0;
		};
	}
}

#endif//PAUPER_DATA_FILESYSTEM_HPP_INCLUDED
