#ifndef PPR_DATA_CONTENTLOADER_HPP_INCLUDED
#define PPR_DATA_CONTENTLOADER_HPP_INCLUDED

#include "../extern.hpp"
#include "../tl/optional.hpp"

#include <stdexcept>

namespace ppr {
	namespace data {
	    template<typename T> struct LoadContentFrom;

		class FileSystem;
		class Renderer;
	    class MusicPlayer;
		class SoundEffectPlayer;

		class ContentLoader {
		private:
			FileSystem& m_filesystem;
			Renderer& m_renderer;
			MusicPlayer& m_music;
			SoundEffectPlayer& m_sound;

		protected:
			inline FileSystem& files() { return m_filesystem; }
			inline Renderer& renderer() { return m_renderer; }
			inline MusicPlayer& music() { return m_music; }
			inline SoundEffectPlayer& sound() { return m_sound; }

		public:
			ContentLoader& operator=(ContentLoader const&) = delete;
			ContentLoader(ContentLoader const&) = delete;

			PPR_EXTERN ContentLoader(FileSystem& fs, Renderer& renderer, MusicPlayer& music, SoundEffectPlayer& sound);

			template<typename > friend struct LoadContentFrom;

			template<typename T, typename... ARGS>
			typename LoadContentFrom<T>::content_type load(ARGS&&... content) {
				auto loaded = LoadContentFrom<T>()(this, std::forward<ARGS>(content)...);
				if (!loaded)
					throw std::runtime_error(std::string("Could not load content"));
				return std::move(*loaded);
			}

			template<typename T, typename... ARGS>
			tl::optional<typename LoadContentFrom<T>::content_type> tryLoad(ARGS&&... content) {
				return LoadContentFrom<T>()(this, std::forward<ARGS>(content)...);
			}
		};
	}
}

#endif//PPR_DATA_CONTENTLOADER_HPP_INCLUDED
