#include "ReadOnlyFile.hpp"
#include "../tl/exchange.hpp"
#include <algorithm>

ppr::data::ReadOnlyFile::ReadOnlyFile(std::unique_ptr<std::istream> stream, std::size_t fileSize)
		: m_stream(std::move(stream)),
		  m_fileSize(fileSize),
		  m_position(0) {
}

ppr::data::ReadOnlyFile::~ReadOnlyFile() {
}

ppr::data::ReadOnlyFile::ReadOnlyFile(ReadOnlyFile&& orig)
		: m_stream(std::move(orig.m_stream)),
		  m_fileSize(ppr::tl::exchange(orig.m_fileSize, 0)),
		  m_position(ppr::tl::exchange(orig.m_position, 0)) {
}

ppr::data::ReadOnlyFile& ppr::data::ReadOnlyFile::operator=(ReadOnlyFile&& orig) {
    m_stream = std::move(orig.m_stream);
    m_fileSize = ppr::tl::exchange(orig.m_fileSize, 0);
    m_position = ppr::tl::exchange(orig.m_position, 0);
    return *this;
}

std::size_t ppr::data::ReadOnlyFile::read(char* out, std::size_t count) {
	auto charsToTake = std::min(m_fileSize - m_position, count);
	if (charsToTake == 0) {
		return 0;
	}

	m_stream->read(out, charsToTake);
	m_position += charsToTake;
	return charsToTake;
}

void ppr::data::ReadOnlyFile::seek(size_t position) {
	m_stream->seekg(position);
	m_position = std::min(position, m_fileSize);
}
