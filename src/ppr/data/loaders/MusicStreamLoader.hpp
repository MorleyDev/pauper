#ifndef PPR_DATA_LOADERS_MUSICSTREAMLOADER_HPP_INCLUDED
#define PPR_DATA_LOADERS_MUSICSTREAMLOADER_HPP_INCLUDED

#include "../MusicPlayer.hpp"
#include "../../model/MusicStream.hpp"

namespace ppr {
	namespace data {
		template<> struct LoadContentFrom<model::MusicStream> {
			typedef std::unique_ptr<model::MusicStream> content_type;

			tl::optional<content_type> operator()(ContentLoader* loader, std::string content) {
				auto file = loader->files().open(content);
				if (!file) {
					return tl::none();
				}
				try {
					return tl::some(loader->music().createMusicStream(std::move(*file)));
				} catch(std::runtime_error e) {
					return tl::none();
				}
			}
		};
	}
}

#endif//PPR_DATA_LOADERS_MUSICSTREAMLOADER_HPP_INCLUDED
