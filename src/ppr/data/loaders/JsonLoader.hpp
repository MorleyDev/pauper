#ifndef PPR_DATA_LOADERS_LOADER_HPP_INCLUDED
#define PPR_DATA_LOADERS_LOADER_HPP_INCLUDED

#include "../../tl/serialise.hpp"
#include "../FileSystem.hpp"

namespace ppr {
    namespace data {
        template<typename T>
        struct LoadContentFrom {
	        typedef T content_type;

	        tl::optional <T> operator()(ContentLoader* loader, std::string content) {
		        auto file = loader->files().open(content);
		        if (!file) {
					return tl::some(T());
				}

		        auto size = file->size() - file->position();
		        std::unique_ptr<char[]> buffer(new char[size]);
		        size = file->read(buffer.get(), size);

		        return tl::some(tl::from_json<T>(std::string(buffer.get(), size)));
	        }
        };
    }
}

#endif//PPR_DATA_LOADERS_LOADER_HPP_INCLUDED
