#ifndef PPR_DATA_LOADERS_STRINGLOADER_HPP_INCLUDED
#define PPR_DATA_LOADERS_STRINGLOADER_HPP_INCLUDED

#include <string>

namespace ppr {
	namespace data {
		template<> struct LoadContentFrom<std::string> {
			typedef std::string content_type;

			tl::optional<content_type> operator()(ContentLoader* loader, std::string content) {
				auto file = loader->files().open(content);
				if (!file) {
					return tl::none();
				}

				auto fileSize = file->size() - file->position();
				std::unique_ptr<char[]> characters(new char[fileSize]);
				fileSize = file->read(characters.get(), fileSize);

				return tl::some(std::string(characters.get(), fileSize));
			};
		};
	}
}

#endif//PPR_DATA_LOADERS_STRINGLOADER_HPP_INCLUDED
