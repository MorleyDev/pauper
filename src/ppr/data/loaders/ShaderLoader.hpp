#ifndef PPR_DATA_LOADERS_SHADERLOADER_HPP_INCLUDED
#define PPR_DATA_LOADERS_SHADERLOADER_HPP_INCLUDED

#include "../Renderer.hpp"
#include "../../model/Shader.hpp"
#include "../../tl/optional.hpp"
#include <string>
#include <memory>

namespace ppr {
    namespace data {

        template<> struct LoadContentFrom<model::Shader> {
	        typedef std::unique_ptr<model::Shader> content_type;

	        tl::optional<content_type> operator()(ContentLoader* loader, std::string content, model::ShaderType type) {
		        auto file = loader->files().open(content);
		        if (!file) {
					return tl::none();
				}
		        try {
			        return tl::some(loader->renderer().createShader(*file, type));
		        } catch(std::runtime_error e) {
			        return tl::none();
		        }
	        }

	        tl::optional<content_type> operator()(ContentLoader* loader, std::string vertexFilename, std::string fragmentFilename) {
		        auto vertexFile = loader->files().open(vertexFilename);
		        auto fragmentFile = loader->files().open(fragmentFilename);;
		        if (!vertexFile || !fragmentFile) {
					return tl::none();
				}
		        try {
			        return tl::some(loader->renderer().createShader(*vertexFile, *fragmentFile));
		        } catch(std::runtime_error e) {
			        return tl::none();
		        }
	        }
        };
    }
}

#endif//PPR_DATA_LOADERS_SHADERLOADER_HPP_INCLUDED
