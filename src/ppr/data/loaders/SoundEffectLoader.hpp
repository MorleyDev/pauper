#ifndef PPR_DATA_LOADERS_SOUNDEFFECTLOADER_HPP_INCLUDED
#define PPR_DATA_LOADERS_SOUNDEFFECTLOADER_HPP_INCLUDED

#include "../SoundEffectPlayer.hpp"
#include "../../model/SoundEffect.hpp"

namespace ppr {
    namespace data {
        template<> struct LoadContentFrom<model::SoundEffect> {
	        typedef std::unique_ptr<model::SoundEffect> content_type;

	        tl::optional<content_type> operator()(ContentLoader* loader, std::string content) {
		        auto file = loader->files().open(content);
		        if (!file) {
					return tl::none();
				}
		        try {
			        return tl::some(loader->sound().createSoundEffect(*file));
		        } catch(std::runtime_error e) {
			        return tl::none();
		        }
	        }
        };
    }
}


#endif//PPR_DATA_LOADERS_SOUNDEFFECTLOADER_HPP_INCLUDED
