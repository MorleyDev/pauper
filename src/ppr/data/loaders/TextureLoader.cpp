#include "../../config.hpp"
#include "TextureLoader.hpp"

#ifdef PPR_SFML_ENABLE
#include <SFML/Graphics.hpp>

std::unique_ptr<ppr::model::Texture> ppr::data::_detail::createTextureFromFile(Renderer& renderer, ReadOnlyFile& file) {
	std::unique_ptr<char[]> buffer(new char[file.size()]);
	auto readSize = file.read(buffer.get(), file.size());
	sf::Image image;
	if (!image.loadFromMemory(buffer.get(), readSize)) {
		throw std::runtime_error("Could not load file from memory");
	}

	return renderer.createTexture(image.getPixelsPtr(), image.getSize().x, image.getSize().y);
}
#else //!PPR_SFML_ENABLE

std::unique_ptr<ppr::model::Texture> ppr::data::_detail::createTextureFromFile(Renderer& renderer, ReadOnlyFile& file) {
	return renderer.createTexture(file);
}
#endif//PPR_SFML_ENABLE