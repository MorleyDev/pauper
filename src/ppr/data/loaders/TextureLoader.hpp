#ifndef PPR_DATA_LOADERS_TEXTURELOADER_HPP_INCLUDED
#define PPR_DATA_LOADERS_TEXTURELOADER_HPP_INCLUDED

#include "../Renderer.hpp"
#include "../ContentLoader.hpp"
#include "../ReadOnlyFile.hpp"
#include "../FileSystem.hpp"
#include "../../model/Texture.hpp"
#include "../../tl/optional.hpp"

namespace ppr {
	namespace data {
		namespace _detail {
			PPR_EXTERN std::unique_ptr<model::Texture> createTextureFromFile(Renderer& renderer, ReadOnlyFile& file);
		}

		template<> struct LoadContentFrom<model::Texture> {
			typedef std::unique_ptr<model::Texture> content_type;

			tl::optional<content_type> operator()(ContentLoader* loader, std::string content) {
				auto file = loader->files().open(content);
				if (!file) {
					return tl::none();
				}

				try {
					return tl::some(_detail::createTextureFromFile(loader->renderer(), *file));
				} catch(std::runtime_error e) {
					return tl::none();
				}
			}
		};
	}
}

#endif//PPR_DATA_LOADERS_TEXTURELOADER_HPP_INCLUDED
