#ifndef PPR_DATA_LOADERS_ALL_HPP_INCLUDED
#define PPR_DATA_LOADERS_ALL_HPP_INCLUDED

#include "JsonLoader.hpp"
#include "MusicStreamLoader.hpp"
#include "ShaderLoader.hpp"
#include "SoundEffectLoader.hpp"
#include "StringLoader.hpp"
#include "TextureLoader.hpp"

#endif//PPR_DATA_LOADERS_ALL_HPP_INCLUDED
