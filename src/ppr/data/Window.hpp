#ifndef PAUPER_DATA_WINDOW_HPP_INCLUDED
#define PAUPER_DATA_WINDOW_HPP_INCLUDED

#include "../extern.hpp"
#include "../model/position.hpp"

#include <memory>
#include <functional>
#include <vector>

namespace ppr {
	namespace data {
		class Renderer;
		class InputSystem;

	    class Window {
	    public:
		    PPR_EXTERN virtual ~Window();

		    virtual Renderer& renderer() = 0;
		    virtual InputSystem& input() = 0;

		    virtual void onClose(std::function<void(void)>) = 0;

		    virtual void poll() = 0;

			virtual model::pos2i resolution() const = 0;
			virtual std::vector<model::pos2i> availableResolutions() const = 0;

			virtual bool isPollTiedToCreationThread() const = 0;
		};
	}
}

#endif//PAUPER_DATA_WINDOW_HPP_INCLUDED
