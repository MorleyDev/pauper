#ifndef PPR_DATA_MODELSTORE_HPP_INCLUDED
#define PPR_DATA_MODELSTORE_HPP_INCLUDED

#include <ppr/tl/optional.hpp>
#include <vector>
#include <functional>
#include <algorithm>

namespace ppr {
	namespace data {
		template<typename TModel, typename TUnderlying = std::vector<TModel>> class ModelStore {
		public:
			virtual ~ModelStore() { }

			virtual void add(TModel model) = 0;

			virtual TUnderlying filter(std::function<bool (TModel const&)> predicate) const = 0;
			virtual tl::optional<TModel> first(std::function<bool (TModel const&)> predicate) const = 0;

			virtual void each(std::function<void (TModel const&)> operand) const = 0;
			virtual void mutate(std::function<void (TModel&)> mutator) = 0;

			virtual void removeIf(std::function<bool (TModel const&)> operand) = 0;
			virtual void removeAll() = 0;

			virtual bool all(std::function<bool (TModel const&)> predicate) const = 0;
			virtual bool any(std::function<bool (TModel const&)> predicate) const = 0;
			virtual bool empty() const = 0;

			template<typename T> std::vector<T> map(std::function<T (TModel const&)> predicate) const {
				auto models = clone();

				std::vector<T> mapped(models.size());
				std::transform(std::begin(models), std::end(models), std::begin(mapped), predicate);
				return mapped;
			}

		protected:
			virtual void replace(TUnderlying replacement) = 0;
			virtual TUnderlying clone() const = 0;
		};
	}
}

#endif//PPR_DATA_MODELSTORE_HPP_INCLUDED
