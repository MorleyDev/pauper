#include "FileSystemStack.hpp"

ppr::data::FileSystemStack::FileSystemStack()
        : m_fileSystem() {
}

ppr::data::FileSystemStack::~FileSystemStack() {

}

void ppr::data::FileSystemStack::push(std::unique_ptr<ppr::data::FileSystem> filesystem) {
	m_fileSystem.push_back(std::move(filesystem));
}

ppr::tl::optional<ppr::data::ReadOnlyFile> ppr::data::FileSystemStack::open(std::string filename) const {
	for(auto i = m_fileSystem.rbegin(); i != m_fileSystem.rend(); ++i) {
		auto file = (*i)->open(filename);
		if (file) {
			return std::move(file);
		}
	}
	return tl::none();
}

