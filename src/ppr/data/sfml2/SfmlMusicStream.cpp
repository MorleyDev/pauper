#include "SfmlMusicStream.hpp"

ppr::data::sfml2::SfmlMusicStream::~SfmlMusicStream() {
}

ppr::data::sfml2::SfmlMusicStream::SfmlMusicStream(std::unique_ptr<ppr::data::sfml2::SfmlFileStream<ppr::data::ReadOnlyFile>> source, std::unique_ptr<sf::Music> music)
		: MusicStream(std::chrono::milliseconds(music->getDuration().asMilliseconds())),
		  m_internal(std::make_shared<MusicStreamDetails>(std::move(music), std::move(source))) {
}
