#ifndef PAUPER_DATA_SFML2_SFMLRENDERER_HPP_INCLUDED
#define PAUPER_DATA_SFML2_SFMLRENDERER_HPP_INCLUDED

#include <SFML/Graphics.hpp>
#include "../Renderer.hpp"
#include "../../tl/optional.hpp"
#include <atomic>

namespace ppr {
	namespace data {
		namespace sfml2 {
			class SfmlRenderer : public Renderer {
			private:
				sf::RenderWindow& m_renderWindow;
				model::rectf m_logicalSize;

				std::atomic<bool> m_refreshLogicalSize;
				std::shared_ptr<sf::Shader> m_activeShader;

			public:
				explicit SfmlRenderer(sf::RenderWindow& window);
				virtual ~SfmlRenderer();

				virtual void setLogicalSize(model::rectf size);
				virtual model::rectf getLogicalSize() const { return m_logicalSize; }

				virtual void clear();
				virtual void display();

				virtual std::unique_ptr<RenderTexture> createRenderTexture(std::size_t width, std::size_t height);
				virtual std::unique_ptr<SpriteBatch> createBatch(model::Texture&);
				virtual std::unique_ptr<model::Texture> createTexture(std::uint8_t const* data, std::size_t width, std::size_t height);
				virtual std::unique_ptr<model::Texture> createTexture(ReadOnlyFile& sourceFile);
				virtual std::unique_ptr<model::Shader> createShader(ReadOnlyFile& sourceFile, model::ShaderType type);
				virtual std::unique_ptr<model::Shader> createShader(ReadOnlyFile& vertexSourceFile, ReadOnlyFile& fragmentSourceFile);

				virtual void drawTexture(model::Texture& texture, model::rectf destination, model::recti source,
				                         model::rotation rotation,
				                         model::colour colour);

				virtual void drawRectangle(model::rectf destination, model::colour colour, bool isFilled = true);

				void notifySfmlEvent(sf::Event const& e);

				virtual void bindShader(model::Shader& shader);
				virtual void unbindShader();
			};
		}
	}
}

#endif//PAUPER_DATA_SFML2_SFMLRENDERER_HPP_INCLUDED
