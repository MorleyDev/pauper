#include "SfmlSpriteBatch.hpp"
#include "SfmlTexture.hpp"

ppr::data::sfml2::SfmlSpriteBatch::SfmlSpriteBatch(sf::RenderTarget& target, std::shared_ptr<sf::Shader>& activeShader, ppr::model::Texture& source)
		: m_target(target),
		  m_activeShader(activeShader),
		  m_source(source),
		  m_vertexArray(sf::PrimitiveType::Quads) {
}

ppr::data::sfml2::SfmlSpriteBatch::~SfmlSpriteBatch() {
	sf::RenderStates state;
	static_cast<SfmlTexture&>(m_source).assure();
	state.texture = &static_cast<SfmlTexture&>(m_source).getTexture();
	state.shader = m_activeShader.get();

	m_target.draw(m_vertexArray, state);
}

ppr::data::SpriteBatch& ppr::data::sfml2::SfmlSpriteBatch::draw(ppr::model::rectf destination, ppr::model::recti source,
                                                                model::rotation rotation,
                                                                model::colour colour) {

	model::pos2f posTopLeft(destination.x, destination.y);
	model::pos2f posTopRight(destination.x + destination.width, destination.y);
	model::pos2f posBottomRight(destination.x + destination.width, destination.y + destination.height);
	model::pos2f posBottomLeft(destination.x, destination.y + destination.height);

	if(rotation.angle != 0.0) {
		auto sin = std::sin(rotation.angle.asRadians());
		auto cos = std::cos(rotation.angle.asRadians());

		auto rotate = [destination,  rotation, cos, sin](model::pos2f p) -> model::pos2f {
		    p.x -=  destination.x + rotation.origin.x;
		    p.y -=  destination.y + rotation.origin.y;

		    return model::pos2f(p.x * cos - p.y * sin + destination.x + rotation.origin.x,
				                p.x * sin + p.y * cos + destination.y + rotation.origin.y);
		};

		posTopLeft = rotate(posTopLeft);
		posTopRight = rotate(posTopRight);
		posBottomRight = rotate(posBottomRight);
		posBottomLeft = rotate(posBottomLeft);
	}


	sf::Color sfColour(colour.red, colour.green, colour.blue, colour.alpha);
	m_vertexArray.append(sf::Vertex(sf::Vector2f(posTopLeft.x, posTopLeft.y), sfColour, sf::Vector2f(source.x, source.y)));
	m_vertexArray.append(sf::Vertex(sf::Vector2f(posBottomLeft.x, posBottomLeft.y), sfColour, sf::Vector2f(source.x, source.y + source.height)));
	m_vertexArray.append(sf::Vertex(sf::Vector2f(posBottomRight.x, posBottomRight.y), sfColour, sf::Vector2f(source.x + source.width, source.y + source.height)));
	m_vertexArray.append(sf::Vertex(sf::Vector2f(posTopRight.x, posTopRight.y), sfColour, sf::Vector2f(source.x + source.width, source.y)));
	return *this;
}
