#ifndef PAUPER_DATA_SFML2_SFMLRENDERTEXTURE_HPP
#define PAUPER_DATA_SFML2_SFMLRENDERTEXTURE_HPP

#include "../RenderTexture.hpp"
#include "SfmlTexture.hpp"
#include <SFML/Graphics/RenderTexture.hpp>

namespace ppr {
	namespace data {
		namespace sfml2 {
			class SfmlRenderTexture : public RenderTexture {
			private:
				std::unique_ptr<sf::RenderTexture> m_renderTexture;
				std::unique_ptr<SfmlRenderTextureReference> m_internalTexture;
				ppr::model::pos2i m_physicalSize;
				ppr::model::rectf m_logicalSize;
				std::shared_ptr<sf::Shader> m_activeShader;

			public:
				SfmlRenderTexture(std::size_t width, std::size_t height);
				virtual ~SfmlRenderTexture();

				virtual void clear();
				virtual void display();

				virtual void setLogicalSize(model::rectf size);
				virtual model::rectf getLogicalSize() const;

				virtual std::unique_ptr<SpriteBatch> createBatch(model::Texture &texture);

				virtual void drawTexture(model::Texture &texture, model::rectf destination, model::recti source,
										 model::rotation rotation, model::colour colour);

				virtual void drawRectangle(model::rectf destination, model::colour colour, bool isFilled);

				virtual void bindShader(model::Shader &shader);

				virtual void unbindShader();

				virtual model::Texture& getTexture();

			private:
				void assure();
			};
		}
	}
}

#endif //PAUPER_DATA_SFML2_SFMLRENDERTEXTURE_HPP
