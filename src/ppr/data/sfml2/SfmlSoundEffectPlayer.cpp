#include "SfmlSoundEffectPlayer.hpp"
#include "SfmlFileStream.hpp"
#include "../../tl/make_unique.hpp"
#include "../../util/Log.hpp"
#include "SfmlSoundEffect.hpp"

namespace {
	PPR_CREATE_LOG(SfmlSoundEffectPlayer);
}

ppr::data::sfml2::SfmlSoundEffectPlayer::~SfmlSoundEffectPlayer() {

	if (m_playingSounds.empty()) {
		return;
	}

	PPR_WRITE_LOG(SfmlSoundEffectPlayer, Warn, "Destroyed with % sound effects still playing", m_playingSounds.size());
	stopAll();
}

ppr::data::sfml2::SfmlSoundEffectPlayer::SfmlSoundEffectPlayer()
		: m_mutex(),
		  m_globalVolume(1.0),
		  m_playingSounds() {
	sf::Listener::setPosition(0.0, 0.0, 0.0);
	PPR_WRITE_LOG(SfmlSoundEffectPlayer, Trace, "Created");
}

std::unique_ptr<ppr::model::SoundEffect> ppr::data::sfml2::SfmlSoundEffectPlayer::createSoundEffect(ppr::data::ReadOnlyFile& sourceFile) {

	auto buffer = std::make_shared<sf::SoundBuffer>();
	struct _RAII {
		std::size_t startPosition;
		ppr::data::ReadOnlyFile& m_file;

		_RAII(ppr::data::ReadOnlyFile& file)
				: startPosition(file.position()), m_file(file) {
		}

		~_RAII() {
			m_file.seek(startPosition);
		}
	};

	_RAII filePositionReset(sourceFile);

	SfmlFileStream<ReadOnlyFile&> stream(sourceFile);

	if (!buffer->loadFromStream(stream)) {
		throw std::runtime_error("Could not parse sound file");
	}

	return tl::make_unique<ppr::data::sfml2::SfmlSoundEffect>(buffer);
}

void ppr::data::sfml2::SfmlSoundEffectPlayer::play(ppr::model::SoundEffect& soundeffect,
                                                   ppr::model::pos2f position,
                                                   ppr::data::SoundEffectPlayer::volume vol) {
	auto sfSoundEffect = static_cast<ppr::data::sfml2::SfmlSoundEffect&>(soundeffect);

	auto sound = tl::make_unique<sf::Sound>(*(sfSoundEffect.m_internal));
	sound->setRelativeToListener(true);
	sound->setVolume(static_cast<float>(vol.get() * m_globalVolume.load() * 100.0));
	sound->setPosition(static_cast<float>(position.x), static_cast<float>(position.y), 0.0f);
	sound->play();

	std::lock_guard<std::mutex> lock(m_mutex);
	m_playingSounds.emplace_back(std::move(sound), sfSoundEffect.m_internal, vol.get());
}

void ppr::data::sfml2::SfmlSoundEffectPlayer::update() {
	std::lock_guard<std::mutex> lock(m_mutex);
	for(auto i = m_playingSounds.begin(); i != m_playingSounds.end();) {
		if (i->sound->getStatus() == sf::Sound::Status::Stopped) {
			m_playingSounds.erase(i++);
		} else {
			++i;
		}
	}
}

void ppr::data::sfml2::SfmlSoundEffectPlayer::setVolume(ppr::data::sfml2::SfmlSoundEffectPlayer::volume v) {
	m_globalVolume.store(v.get());

	std::lock_guard<std::mutex> lock(m_mutex);
	auto volume = static_cast<float>(m_globalVolume.load());
	for(auto i = m_playingSounds.begin(); i != m_playingSounds.end(); ++i) {
		i->sound->setVolume(static_cast<float>(i->volume * volume * 100.0));
	}
}

void ppr::data::sfml2::SfmlSoundEffectPlayer::stopAll() {
	PPR_WRITE_LOG(SfmlSoundEffectPlayer, Trace, "Stopping all sound effects");

	std::lock_guard<std::mutex> lock(m_mutex);
	std::for_each(std::begin(m_playingSounds), std::end(m_playingSounds), [](PlayingSoundDetails& sound) {
		if (sound.sound->getStatus() == sf::Sound::Stopped) {
			return;
		}
		sound.sound->stop();
	});
	m_playingSounds.clear();
}
