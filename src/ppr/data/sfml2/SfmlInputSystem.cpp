#include "SfmlInputSystem.hpp"

ppr::data::sfml2::SfmlInputSystem::SfmlInputSystem() {

}

ppr::data::sfml2::SfmlInputSystem::~SfmlInputSystem() {

}

void ppr::data::sfml2::SfmlInputSystem::notifySfmlEvent(sf::Event const& e) {
	switch(e.type) {
		case sf::Event::TextEntered: {
			signalText(getKeyCode(e.key.code));
			break;
		}

		case sf::Event::KeyPressed: {
			signalKeyboard(getKeyCode(e.key.code), ButtonState::Pressed);
			break;
		}

		case sf::Event::KeyReleased: {
			signalKeyboard(getKeyCode(e.key.code), ButtonState::Released);
			break;
		}

		case sf::Event::MouseWheelMoved:
			break;

		case sf::Event::MouseButtonPressed: {
			signalMouseClick(e.mouseButton.button, ButtonState::Pressed, {e.mouseButton.x, e.mouseButton.y});
			break;
		}

		case sf::Event::MouseButtonReleased: {
			signalMouseClick(e.mouseButton.button, ButtonState::Released, {e.mouseButton.x, e.mouseButton.y});
			break;
		}

		case sf::Event::MouseMoved: {
			signalMouseMove({e.mouseMove.x, e.mouseMove.y});
			break;
		}

		case sf::Event::MouseEntered:break;
		case sf::Event::MouseLeft:break;
		case sf::Event::JoystickButtonPressed:break;
		case sf::Event::JoystickButtonReleased:break;
		case sf::Event::JoystickMoved:break;
		case sf::Event::JoystickConnected:break;
		case sf::Event::JoystickDisconnected:break;

		case sf::Event::TouchBegan: {
			signalTouch({e.touch.finger, e.touch.x, e.touch.y}, ButtonState::Pressed);
			break;
		}

		case sf::Event::TouchMoved: {
			signalTouchMove({e.touch.finger, e.touch.x, e.touch.y});
			break;
		}

		case sf::Event::TouchEnded: {
			signalTouch({e.touch.finger, e.touch.x, e.touch.y}, ButtonState::Released);
			break;
		}

		case sf::Event::SensorChanged:
			break;

		default:
			break;
	}
}

ppr::model::KeyCode ppr::data::sfml2::SfmlInputSystem::getKeyCode(sf::Keyboard::Key key) const {
	switch(key) {
		case sf::Keyboard::A: return model::KeyCode::A;
		case sf::Keyboard::B: return model::KeyCode::B;
		case sf::Keyboard::C: return model::KeyCode::C;
		case sf::Keyboard::D: return model::KeyCode::D;
		case sf::Keyboard::E: return model::KeyCode::E;
		case sf::Keyboard::F: return model::KeyCode::F;
		case sf::Keyboard::G: return model::KeyCode::G;
		case sf::Keyboard::H: return model::KeyCode::H;
		case sf::Keyboard::I: return model::KeyCode::I;
		case sf::Keyboard::J: return model::KeyCode::J;
		case sf::Keyboard::K: return model::KeyCode::K;
		case sf::Keyboard::L: return model::KeyCode::L;
		case sf::Keyboard::M: return model::KeyCode::M;
		case sf::Keyboard::N: return model::KeyCode::N;
		case sf::Keyboard::O: return model::KeyCode::O;
		case sf::Keyboard::P: return model::KeyCode::P;
		case sf::Keyboard::Q: return model::KeyCode::Q;
		case sf::Keyboard::R: return model::KeyCode::R;
		case sf::Keyboard::S: return model::KeyCode::S;
		case sf::Keyboard::T: return model::KeyCode::T;
		case sf::Keyboard::U: return model::KeyCode::U;
		case sf::Keyboard::V: return model::KeyCode::V;
		case sf::Keyboard::W: return model::KeyCode::W;
		case sf::Keyboard::X: return model::KeyCode::X;
		case sf::Keyboard::Y: return model::KeyCode::Y;
		case sf::Keyboard::Z: return model::KeyCode::Z;

		case sf::Keyboard::Num0: return model::KeyCode::Num0;
		case sf::Keyboard::Num1: return model::KeyCode::Num1;
		case sf::Keyboard::Num2: return model::KeyCode::Num2;
		case sf::Keyboard::Num3: return model::KeyCode::Num3;
		case sf::Keyboard::Num4: return model::KeyCode::Num4;
		case sf::Keyboard::Num5: return model::KeyCode::Num5;
		case sf::Keyboard::Num6: return model::KeyCode::Num6;
		case sf::Keyboard::Num7: return model::KeyCode::Num7;
		case sf::Keyboard::Num8: return model::KeyCode::Num8;
		case sf::Keyboard::Num9: return model::KeyCode::Num9;

		case sf::Keyboard::Escape: return model::KeyCode::Escape;
		case sf::Keyboard::LControl: return model::KeyCode::LControl;
		case sf::Keyboard::LShift: return model::KeyCode::LShift;
		case sf::Keyboard::LAlt: return model::KeyCode::LAlt;
		case sf::Keyboard::LSystem: return model::KeyCode::LSystem;
		case sf::Keyboard::RControl: return model::KeyCode::RControl;
		case sf::Keyboard::RShift: return model::KeyCode::RShift;
		case sf::Keyboard::RAlt: return model::KeyCode::RAlt;
		case sf::Keyboard::RSystem: return model::KeyCode::RSystem;
		case sf::Keyboard::Menu: return model::KeyCode::Menu;
		case sf::Keyboard::LBracket: return model::KeyCode::LBracket;
		case sf::Keyboard::RBracket: return model::KeyCode::RBracket;
		case sf::Keyboard::SemiColon: return model::KeyCode::SemiColon;
		case sf::Keyboard::Comma: return model::KeyCode::Comma;
		case sf::Keyboard::Period: return model::KeyCode::Period;
		case sf::Keyboard::Quote: return model::KeyCode::Quote;
		case sf::Keyboard::Slash: return model::KeyCode::Slash;
		case sf::Keyboard::BackSlash: return model::KeyCode::BackSlash;
		case sf::Keyboard::Tilde: return model::KeyCode::Tilde;
		case sf::Keyboard::Equal: return model::KeyCode::Equal;
		case sf::Keyboard::Dash: return model::KeyCode::Dash;
		case sf::Keyboard::Space: return model::KeyCode::Space;
		case sf::Keyboard::Return: return model::KeyCode::Return;
		case sf::Keyboard::BackSpace: return model::KeyCode::BackSpace;
		case sf::Keyboard::Tab: return model::KeyCode::Tab;
		case sf::Keyboard::PageUp: return model::KeyCode::PageUp;
		case sf::Keyboard::PageDown: return model::KeyCode::PageDown;
		case sf::Keyboard::End: return model::KeyCode::End;
		case sf::Keyboard::Home: return model::KeyCode::Home;
		case sf::Keyboard::Insert: return model::KeyCode::Insert;
		case sf::Keyboard::Delete: return model::KeyCode::Delete;
		case sf::Keyboard::Add: return model::KeyCode::Add;
		case sf::Keyboard::Subtract: return model::KeyCode::Subtract;
		case sf::Keyboard::Multiply: return model::KeyCode::Multiply;
		case sf::Keyboard::Divide: return model::KeyCode::Divide;
		case sf::Keyboard::Left: return model::KeyCode::Left;
		case sf::Keyboard::Right: return model::KeyCode::Right;
		case sf::Keyboard::Up: return model::KeyCode::Up;
		case sf::Keyboard::Down: return model::KeyCode::Down;
		case sf::Keyboard::Numpad0: return model::KeyCode::Numpad0;
		case sf::Keyboard::Numpad1: return model::KeyCode::Numpad1;
		case sf::Keyboard::Numpad2: return model::KeyCode::Numpad2;
		case sf::Keyboard::Numpad3: return model::KeyCode::Numpad3;
		case sf::Keyboard::Numpad4: return model::KeyCode::Numpad4;
		case sf::Keyboard::Numpad5: return model::KeyCode::Numpad5;
		case sf::Keyboard::Numpad6: return model::KeyCode::Numpad6;
		case sf::Keyboard::Numpad7: return model::KeyCode::Numpad7;
		case sf::Keyboard::Numpad8: return model::KeyCode::Numpad8;
		case sf::Keyboard::Numpad9: return model::KeyCode::Numpad9;
		case sf::Keyboard::F1: return model::KeyCode::F1;
		case sf::Keyboard::F2: return model::KeyCode::F2;
		case sf::Keyboard::F3: return model::KeyCode::F3;
		case sf::Keyboard::F4: return model::KeyCode::F4;
		case sf::Keyboard::F5: return model::KeyCode::F5;
		case sf::Keyboard::F6: return model::KeyCode::F6;
		case sf::Keyboard::F7: return model::KeyCode::F7;
		case sf::Keyboard::F8: return model::KeyCode::F8;
		case sf::Keyboard::F9: return model::KeyCode::F9;
		case sf::Keyboard::F10: return model::KeyCode::F10;
		case sf::Keyboard::F11: return model::KeyCode::F11;
		case sf::Keyboard::F12: return model::KeyCode::F12;
		case sf::Keyboard::F13: return model::KeyCode::F13;
		case sf::Keyboard::F14: return model::KeyCode::F14;
		case sf::Keyboard::F15: return model::KeyCode::F15;
		case sf::Keyboard::Pause: return model::KeyCode::Pause;

		case sf::Keyboard::KeyCount:
		case sf::Keyboard::Unknown:
		default:
			return static_cast<model::KeyCode>(key);
	}
}
