#ifndef PAUPER_DATA_SFML2_SFMLMUSICPLAYER_HPP_INCLUDED
#define PAUPER_DATA_SFML2_SFMLMUSICPLAYER_HPP_INCLUDED

#include "../MusicPlayer.hpp"
#include "../ReadOnlyFile.hpp"
#include "SfmlMusicStream.hpp"
#include <mutex>
#include <atomic>

namespace ppr {
	namespace data {
		namespace sfml2 {
		    class SfmlMusicPlayer : public MusicPlayer {
		    private:
			    std::set< std::shared_ptr<SfmlMusicStream::MusicStreamDetails> > m_playingMusic;
			    mutable std::mutex m_playingMusicMutex;

			    std::atomic<double> m_volume;

		    public:
			    SfmlMusicPlayer();
			    virtual ~SfmlMusicPlayer();

			    virtual std::unique_ptr<model::MusicStream> createMusicStream(ppr::data::ReadOnlyFile param);

			    virtual void play(model::MusicStream& stream, bool loop);

			    virtual bool isPlaying(const model::MusicStream& stream) const;

			    virtual void stop(model::MusicStream& stream);

				virtual void stopAll();

			    virtual void setVolume(volume v);

			    virtual volume getVolume() const;

			    virtual void update();
		    };
		}
	}
}

#endif//PAUPER_DATA_SFML2_SFMLMUSICPLAYER_HPP_INCLUDED
