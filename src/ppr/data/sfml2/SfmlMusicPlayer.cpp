#include "SfmlMusicPlayer.hpp"
#include "../../tl/make_unique.hpp"
#include "../../util/Log.hpp"

namespace {
	PPR_CREATE_LOG(SfmlMusicPlayer);
}

ppr::data::sfml2::SfmlMusicPlayer::~SfmlMusicPlayer() {
	if (m_playingMusic.empty()) {
		return;
	}

	PPR_WRITE_LOG(SfmlMusicPlayer, Warn, "SFML Music Player destroyed with % music still playing", m_playingMusic.size());
	stopAll();
}

std::unique_ptr<ppr::model::MusicStream> ppr::data::sfml2::SfmlMusicPlayer::createMusicStream(ppr::data::ReadOnlyFile file) {

	auto sourceStream = tl::make_unique<SfmlFileStream<ReadOnlyFile>>(std::move(file));
	auto music = tl::make_unique<sf::Music>();
	if (!music->openFromStream(*sourceStream)) {
		PPR_WRITE_LOG(SfmlMusicPlayer, Error, "Could not open music stream from file");
		throw std::runtime_error("Could not open music stream from file");
	}

	return tl::make_unique<SfmlMusicStream>(std::move(sourceStream), std::move(music));
}

void ppr::data::sfml2::SfmlMusicPlayer::play(ppr::model::MusicStream& stream, bool loop) {
	auto& sfmlStream = static_cast<SfmlMusicStream&>(stream);

	std::lock_guard<std::mutex> lock(m_playingMusicMutex);
	auto musicIt = m_playingMusic.find(sfmlStream.m_internal);
	if (musicIt != m_playingMusic.end() && (*musicIt)->music->getStatus() == sf::SoundSource::Playing) {
		if ((*musicIt)->music->getLoop() != loop) {
			(*musicIt)->music->setLoop(loop);
		}
		return;
	}

	sfmlStream.m_internal->music->setVolume(static_cast<float>(m_volume.load() * 100.0));
	sfmlStream.m_internal->music->setLoop(loop);
	sfmlStream.m_internal->music->play();
	m_playingMusic.insert(sfmlStream.m_internal);
	return;
}

bool ppr::data::sfml2::SfmlMusicPlayer::isPlaying(ppr::model::MusicStream const& stream) const {
	auto& sfmlStream = static_cast<SfmlMusicStream const&>(stream);

	std::lock_guard<std::mutex> lock(m_playingMusicMutex);
	auto musicIt = m_playingMusic.find(sfmlStream.m_internal);
	return (musicIt != m_playingMusic.end()) && (*musicIt)->music->getStatus() == sf::SoundSource::Playing;

}

void ppr::data::sfml2::SfmlMusicPlayer::stop(ppr::model::MusicStream& stream) {
	auto& sfmlStream = static_cast<SfmlMusicStream&>(stream);

	std::lock_guard<std::mutex> lock(m_playingMusicMutex);
	auto found =  m_playingMusic.find(sfmlStream.m_internal);
	if (found == m_playingMusic.end()) {
		return;
	}

	if ((*found)->music->getStatus() != sf::SoundSource::Stopped) {
		(*found)->music->stop();
	}
	m_playingMusic.erase(found);
}

void ppr::data::sfml2::SfmlMusicPlayer::stopAll() {
	PPR_WRITE_LOG(SfmlMusicPlayer, Trace, "Stopping all music");

	std::lock_guard<std::mutex> lock(m_playingMusicMutex);
	std::for_each(std::begin(m_playingMusic), std::end(m_playingMusic), [](std::shared_ptr<SfmlMusicStream::MusicStreamDetails> details) {
		if (details->music->getStatus() == sf::SoundSource::Stopped) {
			return;
		}
		details->music->stop();
	});
	m_playingMusic.clear();
}

void ppr::data::sfml2::SfmlMusicPlayer::setVolume(ppr::data::MusicPlayer::volume v) {
	m_volume.store(v.get());
	auto sfmlVolume = static_cast<float>(v.get() * 100.0);

	std::lock_guard<std::mutex> lock(m_playingMusicMutex);
	std::for_each(m_playingMusic.begin(), m_playingMusic.end(),
			[sfmlVolume](const std::shared_ptr<SfmlMusicStream::MusicStreamDetails>& musicPtr) {
		musicPtr->music->setVolume(sfmlVolume);
	});
}

ppr::data::MusicPlayer::volume ppr::data::sfml2::SfmlMusicPlayer::getVolume() const {
	return ppr::data::MusicPlayer::volume(m_volume.load());
}

void ppr::data::sfml2::SfmlMusicPlayer::update() {

	std::lock_guard<std::mutex> lock(m_playingMusicMutex);
	auto it = m_playingMusic.begin();
	while(it != m_playingMusic.end()) {
		auto current = it++;
		if ((*current)->music->getStatus() != sf::SoundSource::Stopped) {
			continue;
		}
		m_playingMusic.erase(current);
	}
}

ppr::data::sfml2::SfmlMusicPlayer::SfmlMusicPlayer()
	: m_playingMusic(),
      m_playingMusicMutex(),
      m_volume(1.0) {
}

