#include "SfmlWindow.hpp"
#include "../../util/Log.hpp"
#include <set>

namespace {
	PPR_CREATE_LOG(SfmlWindow);

	sf::VideoMode _CreateVideoMode(std::size_t w, std::size_t h, bool fullscreen) {
		if (w == 0 || h == 0) { // If w or h are 0, get and use default values
			auto desktop = sf::VideoMode::getDesktopMode();
			if (fullscreen) { // If fullscreen, just use the desktop resolution
				return desktop;
			}

			// Otherwise try 1080p or 720p.
			w = desktop.width > 1920 && desktop.height > 1080 ? 1920 : 1280;
			h = desktop.width > 1920 && desktop.height > 1080 ? 1080 : 720;
			while(desktop.width < w || desktop.height < h) { // If 720p fails, just start shrinking until it works
				w /= 2;
				h /= 2;
			}
		}
		return sf::VideoMode(w,h);
	}
}

ppr::data::sfml2::SfmlWindow::~SfmlWindow() {
	PPR_WRITE_LOG(SfmlWindow, Info, "Destroyed");
}

ppr::data::sfml2::SfmlWindow::SfmlWindow(std::string title, std::size_t w, std::size_t h, bool fullscreen, bool vsync)
	: m_renderWindow(_CreateVideoMode(w,h,fullscreen), title, fullscreen ? sf::Style::Fullscreen : sf::Style::Default),
	  m_sfmlRenderer(m_renderWindow),
	  m_onClose(),
	  m_inputSystem() {
	m_renderWindow.setVerticalSyncEnabled(vsync);
	m_renderWindow.setMouseCursorVisible(!fullscreen);
	m_renderWindow.setActive(false);

	PPR_WRITE_LOG(SfmlWindow, Info, "Created % (%,%) fullscreen:% vsync:%", title, w, h, fullscreen, vsync);
}

ppr::data::Renderer& ppr::data::sfml2::SfmlWindow::renderer() {
	PPR_WRITE_LOG(SfmlWindow, Info, "Creating renderer");
	return m_sfmlRenderer;
}

void ppr::data::sfml2::SfmlWindow::onClose(std::function<void(void)> handler) {
	m_onClose.listen(handler);
}

void ppr::data::sfml2::SfmlWindow::poll() {
	sf::Event event;
	while(m_renderWindow.pollEvent(event)) {
		switch(event.type) {
			case sf::Event::Closed: {;
				PPR_WRITE_LOG(SfmlWindow, Info, "Close requested");
				m_onClose.signal();
				break;
			}

			case sf::Event::Resized: {
				PPR_WRITE_LOG(SfmlWindow, Info, "Screen resized");
				m_sfmlRenderer.notifySfmlEvent(event);
				break;
			}

			case sf::Event::LostFocus: {;
				PPR_WRITE_LOG(SfmlWindow, Info, "Lost focus");
				break;
			}
			case sf::Event::GainedFocus: {
				PPR_WRITE_LOG(SfmlWindow, Info, "Gained focus");
				break;
			}

			default:
				m_inputSystem.notifySfmlEvent(event);
				break;
		}
	}
}

ppr::data::InputSystem& ppr::data::sfml2::SfmlWindow::input() {
	return m_inputSystem;
}

std::vector<ppr::model::pos2i> ppr::data::sfml2::SfmlWindow::availableResolutions() const {
	std::vector<sf::VideoMode> modes = sf::VideoMode::getFullscreenModes();
	std::vector<ppr::model::pos2i> result;
	result.reserve(modes.size());
	for(auto const& mode : modes) {
		if (std::find_if(result.begin(),result.end(), [mode](model::pos2i size) {
			return size.x == mode.width && size.y == mode.height;
		}) == result.end()) {
			result.emplace_back(mode.width, mode.height);
		}
	}
	return result;
}

ppr::model::pos2i ppr::data::sfml2::SfmlWindow::resolution() const {
	auto size = m_renderWindow.getSize();

	return ppr::model::pos2i(size.x, size.y);
}
