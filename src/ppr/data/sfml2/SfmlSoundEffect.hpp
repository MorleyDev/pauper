#ifndef PPR_DATA_SFML2_SFMLSOUNDEFFECT_HPP_INCLUDED
#define PPR_DATA_SFML2_SFMLSOUNDEFFECT_HPP_INCLUDED

#include "../../model/SoundEffect.hpp"
#include <SFML/Audio.hpp>

#include <memory>
#include <chrono>

namespace ppr {
	namespace data {
		namespace sfml2 {
			class SfmlSoundEffect : public model::SoundEffect {
			public:
				explicit SfmlSoundEffect(std::shared_ptr<sf::SoundBuffer>);
				~SfmlSoundEffect();

				std::shared_ptr<sf::SoundBuffer> m_internal;
			};
		}
	}
}

#endif//PPR_DATA_SFML2_SFMLSOUNDEFFECT_HPP_INCLUDED

