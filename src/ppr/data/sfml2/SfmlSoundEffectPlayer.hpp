#ifndef PPR_DATA_SFML2_SFMLSOUNDEFFECTPLAYER_HPP_INCLUDED
#define PPR_DATA_SFML2_SFMLSOUNDEFFECTPLAYER_HPP_INCLUDED

#include "../SoundEffectPlayer.hpp"
#include <SFML/Audio.hpp>
#include <list>
#include <mutex>
#include <atomic>


namespace ppr {
	namespace data {
		namespace sfml2 {
			class SfmlSoundEffectPlayer : public SoundEffectPlayer {
			private:
				mutable std::mutex m_mutex;
				std::atomic<double> m_globalVolume;

				struct PlayingSoundDetails {
					PlayingSoundDetails(std::unique_ptr<sf::Sound> s, std::shared_ptr<sf::SoundBuffer> b, double v)
							: sound(std::move(s)),
							  buffer(b),
							  volume(v) {
					}

					std::unique_ptr<sf::Sound> sound;
					std::shared_ptr<sf::SoundBuffer> buffer;
					double volume;
				};
				std::list<PlayingSoundDetails> m_playingSounds;

			public:
				SfmlSoundEffectPlayer();
				~SfmlSoundEffectPlayer();

				virtual std::unique_ptr<model::SoundEffect> createSoundEffect(ReadOnlyFile& sourceFile);

				virtual void play(model::SoundEffect& sound, ppr::model::pos2f position, volume vol);

				virtual void stopAll();

				virtual void update();

				virtual volume getVolume() {
					return m_globalVolume.load();
				}

				virtual void setVolume(volume v);
			};
		}
	}
}

#endif//PPR_DATA_SFML2_SFMLSOUNDEFFECTPLAYER_HPP_INCLUDED
