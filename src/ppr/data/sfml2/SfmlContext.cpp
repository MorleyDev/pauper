#include "SfmlContext.hpp"
#include "SfmlWindow.hpp"
#include "SfmlMusicPlayer.hpp"
#include "SfmlSoundEffectPlayer.hpp"
#include "../../tl/make_unique.hpp"
#include "../../util/Log.hpp"

#ifndef _WIN32
#include <X11/Xlib.h>
#endif//!_WIN32

namespace {
	PPR_CREATE_LOG(SfmlContext);
}

std::unique_ptr<ppr::data::Window> ppr::data::sfml2::SfmlContext::createWindow(std::string string, std::size_t w, std::size_t h, bool fullscreen, bool vsync) {
	PPR_WRITE_LOG(SfmlContext, Info, "Creating SFML window % (%,%) fullscreen:% vsync:%", string, w, h, fullscreen, vsync);
	return tl::make_unique<SfmlWindow>(string, w, h, fullscreen, vsync);
}

std::unique_ptr<ppr::data::MusicPlayer> ppr::data::sfml2::SfmlContext::createMusicPlayer() {
	PPR_WRITE_LOG(SfmlContext, Info, "Creating SFML Music Player");
	return tl::make_unique<SfmlMusicPlayer>();
}

std::unique_ptr<ppr::data::SoundEffectPlayer> ppr::data::sfml2::SfmlContext::createSoundEffectPlayer() {
	PPR_WRITE_LOG(SfmlContext, Info, "Creating SFML Sound Effect Player");
	return tl::make_unique<SfmlSoundEffectPlayer>();
}

ppr::data::sfml2::SfmlContext::SfmlContext() {
#ifndef _WIN32
	XInitThreads();
#endif//!w_WIN32
}
