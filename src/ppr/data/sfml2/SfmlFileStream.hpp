#ifndef PAUPER_DATA_SFML2_SFMLREADONLYFILESTREAM_HPP_INCLUDED
#define PAUPER_DATA_SFML2_SFMLREADONLYFILESTREAM_HPP_INCLUDED

#include <SFML/System.hpp>

namespace ppr {
	namespace data {
		namespace sfml2 {
		    template<typename File> class SfmlFileStream : public sf::InputStream {
		    private:
			    File m_file;

		    public:
			    SfmlFileStream(File file)
					    : m_file(std::forward<File>(file)) {
			    }

			    virtual ~SfmlFileStream() {
			    }

			    virtual sf::Int64 read(void* data, sf::Int64 size) {
				    return static_cast<sf::Int64>(m_file.read(static_cast<char*>(data), static_cast<std::size_t>(size)));
			    }

			    virtual sf::Int64 seek(sf::Int64 position) {
				    m_file.seek(static_cast<std::size_t>(position));
				    return static_cast<sf::Int64>(m_file.position());
			    }

			    virtual sf::Int64 tell() {
				    return static_cast<sf::Int64>(m_file.position());
			    }

			    virtual sf::Int64 getSize() {
				    return static_cast<sf::Int64>(m_file.size());
			    }
		    };
		}
	}
}

#endif//PAUPER_DATA_SFML2_SFMLREADONLYFILESTREAM_HPP_INCLUDED
