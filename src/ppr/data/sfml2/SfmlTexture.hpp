#ifndef PAUPER_DATA_SFML2_SFMLTEXTURE_HPP_INCLUDED
#define PAUPER_DATA_SFML2_SFMLTEXTURE_HPP_INCLUDED

#include "../../model/Texture.hpp"
#include <SFML/Graphics.hpp>
#include <memory>

namespace ppr {
	namespace data {
		namespace sfml2 {
			class SfmlTexture : public model::Texture {
			public:
				SfmlTexture(std::size_t width, std::size_t height);
				virtual ~SfmlTexture();

				virtual void assure() = 0;
				virtual sf::Texture const& getTexture() const = 0;
			};

			class SfmlStandardTexture : public SfmlTexture {
			private:
				std::unique_ptr<sf::Texture> m_texture;
				std::unique_ptr<sf::Image> m_image;

			public:
				SfmlStandardTexture(std::size_t width, std::size_t height, std::unique_ptr<sf::Image> image);
				virtual ~SfmlStandardTexture();

				virtual void assure();
				virtual sf::Texture const& getTexture() const { return *m_texture; }
			};

			class SfmlRenderTextureReference : public SfmlTexture {
			private:
				sf::Texture const* m_texture;

			public:
				SfmlRenderTextureReference(sf::Texture const& texture);
				virtual ~SfmlRenderTextureReference();

				void updateTexture(sf::Texture const& texture);
				virtual void assure(){ /* no-op */ }
				virtual sf::Texture const& getTexture() const { return *m_texture; }
			};
		}
	}
}

#endif//PAUPER_DATA_SFML2_SFMLTEXTURE_HPP_INCLUDED
