#include "SfmlShader.hpp"
#include "SfmlTexture.hpp"
#include <ppr/util/Log.hpp>

PPR_CREATE_LOG(SfmlShader);

ppr::data::sfml2::SfmlShader::SfmlShader(model::ShaderType type, std::string shaderText)
		: model::Shader(type),
		  m_shader(),
		  m_shaderText(std::move(shaderText)),
		  m_shaderText2() {
}

ppr::data::sfml2::SfmlShader::SfmlShader(model::ShaderType type, std::string shaderText, std::string shaderText2)
		: model::Shader(type),
		  m_shader(),
		  m_shaderText(std::move(shaderText)),
		  m_shaderText2(std::move(shaderText2)) {

}

ppr::data::sfml2::SfmlShader::~SfmlShader() {
}

void ppr::data::sfml2::SfmlShader::set(std::string name, double value) {
	assure();
	m_shader->setParameter(name, static_cast<float>(value));
}

void ppr::data::sfml2::SfmlShader::set(std::string name, ppr::model::pos2f value) {
	assure();
	m_shader->setParameter(name, sf::Vector2f(static_cast<float>(value.x), static_cast<float>(value.y)));
}

void ppr::data::sfml2::SfmlShader::set(std::string name, ppr::model::colour colour) {
	assure();
	m_shader->setParameter(name, (sf::Color(colour.red, colour.green, colour.blue, colour.alpha)) );
}

void ppr::data::sfml2::SfmlShader::set(std::string name, ppr::model::Texture& tex) {
	assure();
	static_cast<SfmlTexture&>(tex).assure();
	m_shader->setParameter(name, static_cast<SfmlTexture&>(tex).getTexture());
}

void ppr::data::sfml2::SfmlShader::setCurrentTexture(std::string name) {
	assure();
	m_shader->setParameter(name, sf::Shader::CurrentTexture);
}

void ppr::data::sfml2::SfmlShader::assure() {
	if (m_shader) {
		return;
	}

	sf::Shader::Type sfShaderType;
	switch(getType()) {
		case model::ShaderType::Vertex: {
			sfShaderType = sf::Shader::Vertex;
			break;
		}
		case model::ShaderType::Fragment: {
			sfShaderType = sf::Shader::Fragment;
			break;
		}
		case model::ShaderType::VertexFragment: {
			m_shader = std::make_shared<sf::Shader>();
			if (!m_shader->loadFromMemory(m_shaderText, m_shaderText2)) {
				PPR_WRITE_LOG(SfmlShader, Error, "Could not create shader from shader files");
				throw std::runtime_error("Could not create shader from shader file");
			}
			m_shaderText.clear();
			m_shaderText2.clear();
			return;
		}
	};

	m_shader = std::make_shared<sf::Shader>();
	if (!m_shader->loadFromMemory(m_shaderText, sfShaderType)) {
		PPR_WRITE_LOG(SfmlShader, Error, "Could not create shader from shader file");
		throw std::runtime_error("Could not create shader from shader file");
	}

	m_shaderText.clear();
}
