#ifndef PAUPER_DATA_SFML2_SFMLINPUTSYSTEM_HPP_INCLUDED
#define PAUPER_DATA_SFML2_SFMLINPUTSYSTEM_HPP_INCLUDED

#include "../InputSystem.hpp"
#include <SFML/Window.hpp>

namespace ppr {
	namespace data {
		namespace sfml2 {
			class SfmlInputSystem : public InputSystem {
			private:
				model::KeyCode getKeyCode(sf::Keyboard::Key key) const;

			public:
				SfmlInputSystem();
				virtual ~SfmlInputSystem();

				void notifySfmlEvent(sf::Event const& e);
			};
		}
	}
}

#endif//PAUPER_DATA_SFML2_SFMLINPUTSYSTEM_HPP_INCLUDED
