#include "SfmlRenderer.hpp"
#include "SfmlTexture.hpp"
#include "SfmlFileStream.hpp"
#include "SfmlSpriteBatch.hpp"
#include "SfmlShader.hpp"
#include "SfmlRenderTexture.hpp"

#include "../ReadOnlyFile.hpp"
#include "../../tl/make_unique.hpp"
#include "../../tl/maths/degrees.hpp"
#include "../../util/Log.hpp"

#include <stdexcept>

namespace {
	PPR_CREATE_LOG(SfmlRenderer);
}

ppr::data::sfml2::SfmlRenderer::SfmlRenderer(sf::RenderWindow& window)
		: m_renderWindow(window),
          m_logicalSize(),
          m_refreshLogicalSize(false) {
	setLogicalSize({ 0.0, 0.0, static_cast<double>(window.getSize().x), static_cast<double>(window.getSize().y) });
	PPR_WRITE_LOG(SfmlRenderer, Info, "Created (0,0,%,%)", window.getSize().x, window.getSize().y);
}

ppr::data::sfml2::SfmlRenderer::~SfmlRenderer() {
	PPR_WRITE_LOG(SfmlRenderer, Info, "Destroyed");
}

void ppr::data::sfml2::SfmlRenderer::clear() {

	m_renderWindow.setActive(true);
	if (m_refreshLogicalSize.exchange(false)) {
		setLogicalSize(getLogicalSize());
	}

	auto colour = getClearColour();
	sf::Color sfColor(colour.red, colour.green, colour.blue, colour.alpha);

	m_renderWindow.clear(sfColor);
}

void ppr::data::sfml2::SfmlRenderer::display() {
	m_renderWindow.display();
}

std::unique_ptr<ppr::data::SpriteBatch> ppr::data::sfml2::SfmlRenderer::createBatch(ppr::model::Texture& source) {
	return tl::make_unique<SfmlSpriteBatch>(m_renderWindow, m_activeShader, source);
}

std::unique_ptr<ppr::model::Texture> ppr::data::sfml2::SfmlRenderer::createTexture(ppr::data::ReadOnlyFile& sourceFile) {

	struct _RAII {
		std::size_t startPosition;
		ppr::data::ReadOnlyFile& m_file;

		_RAII(ppr::data::ReadOnlyFile& file)
				: startPosition(file.position()), m_file(file) {
		}

		~_RAII() {
			m_file.seek(startPosition);
		}
	};

	_RAII filePositionReset(sourceFile);

	SfmlFileStream<ReadOnlyFile&> stream(sourceFile);
	auto image = tl::make_unique<sf::Image>();
	if(!image->loadFromStream(stream)) {
		throw std::runtime_error("Could not parse image file");
	}
	image->createMaskFromColor(sf::Color(255,0,255));

	auto imageWidth = image->getSize().x;
	auto imageHeight = image->getSize().y;
	return tl::make_unique<SfmlStandardTexture>(imageWidth, imageHeight, std::move(image));
}

std::unique_ptr<ppr::model::Texture> ppr::data::sfml2::SfmlRenderer::createTexture(std::uint8_t const* data, std::size_t width, std::size_t height) {

	auto image = tl::make_unique<sf::Image>();
	image->create(width, height, data);
	image->createMaskFromColor(sf::Color(255,0,255));

	auto imageWidth = image->getSize().x;
	auto imageHeight = image->getSize().y;
	return tl::make_unique<SfmlStandardTexture>(imageWidth, imageHeight, std::move(image));
}

void ppr::data::sfml2::SfmlRenderer::drawTexture(ppr::model::Texture& texture,
                                                 ppr::model::rectf destination,
                                                 ppr::model::recti source,
                                                 model::rotation rotation,
                                                 model::colour colour) {
	static_cast<SfmlTexture&>(texture).assure();
	sf::Texture const& sfTexture = static_cast<SfmlTexture&>(texture).getTexture();
	sf::IntRect sfTextureArea(source.x, source.y, source.width, source.height);

	sf::Vector2f sfScale(static_cast<float>(destination.width / source.width), static_cast<float>(destination.height / source.height));
	sf::Vector2f sfOrigin(static_cast<float>(rotation.origin.x) / sfScale.x, static_cast<float>(rotation.origin.y) / sfScale.y);
	sf::Vector2f sfPosition(static_cast<float>(destination.x) + sfOrigin.x * sfScale.x, static_cast<float>(destination.y) + sfOrigin.y * sfScale.y);
	auto sfAngle = static_cast<float>(rotation.angle.asDegrees());

	sf::Sprite sfSprite(sfTexture, sfTextureArea);
	sfSprite.setPosition(sfPosition);
	sfSprite.setScale(sfScale);
	sfSprite.setOrigin(sfOrigin);
	sfSprite.setRotation(sfAngle);
	sfSprite.setColor(sf::Color(colour.red, colour.green, colour.blue, colour.alpha));
	m_renderWindow.draw(sfSprite, m_activeShader.get());
}

void ppr::data::sfml2::SfmlRenderer::setLogicalSize(model::rectf size) {
	m_logicalSize = size;
	sf::View view(sf::FloatRect(static_cast<float>(size.x), static_cast<float>(size.y), static_cast<float>(size.width), static_cast<float>(size.height)));

	const auto viewWidth = static_cast<float>(size.width);
	const auto viewHeight = static_cast<float>(size.height);
	const auto windowWidth = static_cast<float>(m_renderWindow.getSize().x);
	const auto windowHeight = static_cast<float>(m_renderWindow.getSize().y);
	const auto targetAspectRatio = viewWidth / viewHeight;
	const auto windowAspectRatio = windowWidth / windowHeight;
	const auto scale = windowAspectRatio > targetAspectRatio ? windowHeight / viewHeight : windowWidth / viewWidth;
	const auto cropX = (windowAspectRatio > targetAspectRatio ? (windowWidth - viewWidth * scale) / (2.0f * windowWidth) : 0.0f);
	const auto cropY = (windowAspectRatio < targetAspectRatio ? (windowHeight - viewHeight * scale) / (2.0f * windowHeight) : 0.0f);
	const auto cropW = (viewWidth * scale) / windowWidth;
	const auto cropH = (viewHeight * scale) / windowHeight;

	view.setViewport(sf::FloatRect(cropX, cropY, cropW, cropH));
	m_renderWindow.setView(view);
}

void ppr::data::sfml2::SfmlRenderer::notifySfmlEvent(sf::Event const& e) {
	switch(e.type) {
		case sf::Event::Resized: {
			m_refreshLogicalSize.store(true);
			break;
		}

		default:
			break;
	}
}

void ppr::data::sfml2::SfmlRenderer::drawRectangle(model::rectf destination, model::colour colour, bool isFilled) {
	sf::RectangleShape rectangleShape(sf::Vector2f(static_cast<float>(destination.width), static_cast<float>(destination.height)));
	rectangleShape.setPosition(sf::Vector2f(static_cast<float>(destination.x), static_cast<float>(destination.y)));
	if (!isFilled) {
		rectangleShape.setOutlineColor(sf::Color(colour.red, colour.green, colour.blue, colour.alpha));
		rectangleShape.setOutlineThickness(-1.0f);
		rectangleShape.setFillColor(sf::Color::Transparent);
	} else {
		rectangleShape.setFillColor(sf::Color(colour.red, colour.green, colour.blue, colour.alpha));
	}
	m_renderWindow.draw(rectangleShape, m_activeShader.get());
}

std::unique_ptr<ppr::model::Shader> ppr::data::sfml2::SfmlRenderer::createShader(ppr::data::ReadOnlyFile& sourceFile,
                                                                                 model::ShaderType type) {
	struct _RAII {
		std::size_t startPosition;
		ppr::data::ReadOnlyFile& m_file;

		_RAII(ppr::data::ReadOnlyFile& file)
				: startPosition(file.position()), m_file(file) {
		}

		~_RAII() {
			m_file.seek(startPosition);
		}
	};

	_RAII filePositionReset(sourceFile);

	if (type == model::ShaderType::VertexFragment) {
		throw std::runtime_error("Cannot load vertexfragment shaders from single file");
	}

	auto charsToRead = sourceFile.size() - sourceFile.position();
	std::unique_ptr<char[]> targetArray(new char[charsToRead+1]);
	auto actualSize = sourceFile.read(targetArray.get(), charsToRead);
	targetArray[actualSize] = '\0';
	std::string shaderText(targetArray.get());

	return tl::make_unique<SfmlShader>(type, std::move(shaderText));
}

std::unique_ptr<ppr::model::Shader> ppr::data::sfml2::SfmlRenderer::createShader(ppr::data::ReadOnlyFile& vertexSourceFile, ppr::data::ReadOnlyFile& fragmentSourceFile) {

	struct _RAII {
		std::size_t startPosition;
		ppr::data::ReadOnlyFile& m_file;

		_RAII(ppr::data::ReadOnlyFile& file)
				: startPosition(file.position()), m_file(file) {
		}

		~_RAII() {
			m_file.seek(startPosition);
		}
	};

	_RAII filePositionReset1(vertexSourceFile);
	_RAII filePositionReset2(fragmentSourceFile);

	auto getFileAsString = [](ReadOnlyFile& sourceFile) -> std::string {
		auto charsToRead = sourceFile.size() - sourceFile.position();
		std::unique_ptr<char[]> targetArray(new char[charsToRead+1]);
		auto actualSize = sourceFile.read(targetArray.get(), charsToRead);
		targetArray[actualSize] = '\0';
		return std::string(targetArray.get());
	};

	auto vertexShaderText = getFileAsString(vertexSourceFile);
	auto fragmentShaderText = getFileAsString(fragmentSourceFile);

	return tl::make_unique<SfmlShader>(model::ShaderType::VertexFragment, std::move(vertexShaderText), std::move(fragmentShaderText));
}

void ppr::data::sfml2::SfmlRenderer::bindShader(ppr::model::Shader& shader) {
	static_cast<SfmlShader&>(shader).assure();
	m_activeShader = static_cast<SfmlShader&>(shader).getShader();
}

void ppr::data::sfml2::SfmlRenderer::unbindShader() {
	m_activeShader = std::shared_ptr<sf::Shader>();
}

std::unique_ptr<ppr::data::RenderTexture> ppr::data::sfml2::SfmlRenderer::createRenderTexture(std::size_t width,
																							  std::size_t height) {
	return tl::make_unique<SfmlRenderTexture>(width, height);
}
