#include "SfmlTexture.hpp"
#include "../../tl/make_unique.hpp"

ppr::data::sfml2::SfmlTexture::SfmlTexture(std::size_t width, std::size_t height)
		: Texture(width, height) {
}

ppr::data::sfml2::SfmlTexture::~SfmlTexture() {
}

ppr::data::sfml2::SfmlStandardTexture::SfmlStandardTexture(std::size_t width, std::size_t height, std::unique_ptr<sf::Image> image)
		: SfmlTexture(width, height),
		  m_texture(),
		  m_image(std::move(image)) {

}

ppr::data::sfml2::SfmlStandardTexture::~SfmlStandardTexture() {
}

void ppr::data::sfml2::SfmlStandardTexture::assure() {
	if (!m_image) {
		return;
	}
	m_texture = tl::make_unique<sf::Texture>();
	if (!m_texture->loadFromImage(*m_image)) {
		throw new std::runtime_error("Could not create texture from image");
	}
	m_image.reset();
}

ppr::data::sfml2::SfmlRenderTextureReference::SfmlRenderTextureReference(sf::Texture const& texture)
	: SfmlTexture(texture.getSize().x, texture.getSize().y),
	  m_texture(&texture) {
}

ppr::data::sfml2::SfmlRenderTextureReference::~SfmlRenderTextureReference() {
}

void ppr::data::sfml2::SfmlRenderTextureReference::updateTexture(sf::Texture const& texture) {
	m_texture = &texture;
}
