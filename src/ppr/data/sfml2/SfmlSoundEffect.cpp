#include "SfmlSoundEffect.hpp"

ppr::data::sfml2::SfmlSoundEffect::~SfmlSoundEffect() {

}

ppr::data::sfml2::SfmlSoundEffect::SfmlSoundEffect(std::shared_ptr<sf::SoundBuffer> ptr)
		: model::SoundEffect(std::chrono::milliseconds(ptr->getDuration().asMilliseconds())),
		  m_internal(ptr) {
}
