#ifndef PAUPER_DATA_SFML2_SFMLWINDOW_HPP_INCLUDED
#define PAUPER_DATA_SFML2_SFMLWINDOW_HPP_INCLUDED

#include "../Window.hpp"
#include "../../tl/signals.hpp"
#include "SfmlInputSystem.hpp"
#include "SfmlRenderer.hpp"

#include <SFML/Graphics.hpp>
#include <vector>
#include <atomic>

namespace ppr {
	namespace data {
		namespace sfml2 {
			class SfmlWindow : public Window {
			private:
				sf::RenderWindow m_renderWindow;
				tl::signals<void> m_onClose;
				SfmlInputSystem m_inputSystem;
				SfmlRenderer m_sfmlRenderer;

			public:
				virtual void onClose(std::function<void (void)> function);

				virtual void poll();

				SfmlWindow(std::string title, std::size_t w, std::size_t h, bool fullscreen, bool vsync);
				virtual ~SfmlWindow();

				virtual Renderer& renderer();
				virtual InputSystem& input();

				virtual model::pos2i resolution() const;
				virtual std::vector<model::pos2i> availableResolutions() const;

				virtual bool isPollTiedToCreationThread() const { return true; }
			};
		}
	}
}

#endif//PAUPER_DATA_SFML2_SFMLWINDOW_HPP_INCLUDED
