#ifndef PPR_DATA_SFML2_SFMLSPRITEBATCH_HPP_INCLUDED
#define PPR_DATA_SFML2_SFMLSPRITEBATCH_HPP_INCLUDED

#include "../SpriteBatch.hpp"
#include "../../model/Texture.hpp"
#include <SFML/Graphics.hpp>
#include <memory>

namespace ppr {
	namespace data {
		namespace sfml2 {
			class SfmlSpriteBatch : public SpriteBatch {
			private:
				sf::RenderTarget& m_target;
				std::shared_ptr<sf::Shader>& m_activeShader;
				model::Texture& m_source;
				sf::VertexArray m_vertexArray;

			public:
				SfmlSpriteBatch(sf::RenderTarget& target, std::shared_ptr<sf::Shader>& activeShader, model::Texture& source);
				~SfmlSpriteBatch();

				virtual SpriteBatch& draw(model::rectf destination, model::recti source, model::rotation rotation, model::colour colour);
			};
		}
	}
}

#endif//PPR_DATA_SFML2_SFMLSPRITEBATCH_HPP_INCLUDED
