#ifndef PAUPER_DATA_SFML2_SFMLMUSICSTREAM_HPP_INCLUDED
#define PAUPER_DATA_SFML2_SFMLMUSICSTREAM_HPP_INCLUDED

#include "SfmlFileStream.hpp"
#include "../../model/MusicStream.hpp"
#include "../ReadOnlyFile.hpp"
#include <SFML/Audio.hpp>
#include <memory>
#include <chrono>

namespace ppr {
	namespace data {
		namespace sfml2 {
			class SfmlMusicStream : public model::MusicStream {
			public:
				struct MusicStreamDetails {
					MusicStreamDetails(std::unique_ptr<sf::Music> music, std::unique_ptr<SfmlFileStream<ReadOnlyFile>> source)
							: music(std::move(music)),
							  file(std::move(source)) {

					}

					~MusicStreamDetails() {
						music.reset();
						file.reset();
					}

					std::unique_ptr<sf::Music> music;
					std::unique_ptr<SfmlFileStream<ReadOnlyFile>> file;
				};
				std::shared_ptr<MusicStreamDetails> m_internal;

				SfmlMusicStream(std::unique_ptr<SfmlFileStream<ReadOnlyFile>> source, std::unique_ptr<sf::Music> music);
				virtual ~SfmlMusicStream();
			};
		}
	}
}

#endif//PAUPER_DATA_SFML2_SFMLMUSICSTREAM_HPP_INCLUDED
