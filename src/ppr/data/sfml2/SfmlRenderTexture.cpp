#include "SfmlRenderTexture.hpp"
#include "SfmlSpriteBatch.hpp"
#include "SfmlShader.hpp"
#include <ppr/tl/make_unique.hpp>

ppr::data::sfml2::SfmlRenderTexture::SfmlRenderTexture(std::size_t width, std::size_t height)
		: m_renderTexture(),
		  m_internalTexture(),
		  m_physicalSize(width, height),
		  m_logicalSize(0, 0, static_cast<double>(width), static_cast<double>(height)),
		  m_activeShader() {
}

ppr::data::sfml2::SfmlRenderTexture::~SfmlRenderTexture() {
}

void ppr::data::sfml2::SfmlRenderTexture::assure() {
	if (m_renderTexture) {
		return;
	}
	m_renderTexture = tl::make_unique<sf::RenderTexture>();
	m_renderTexture->create(m_physicalSize.x, m_physicalSize.y);

	m_internalTexture = tl::make_unique<SfmlRenderTextureReference>(m_renderTexture->getTexture());
	setLogicalSize(getLogicalSize());
}

void ppr::data::sfml2::SfmlRenderTexture::clear() {
	assure();

	m_renderTexture->setActive(true);

	auto colour = getClearColour();
	sf::Color sfColor(colour.red, colour.green, colour.blue, colour.alpha);
	m_renderTexture->clear(sfColor);
}

void ppr::data::sfml2::SfmlRenderTexture::display() {
	assure();

	m_renderTexture->display();
	m_internalTexture->updateTexture(m_renderTexture->getTexture());
}

void ppr::data::sfml2::SfmlRenderTexture::setLogicalSize(ppr::model::rectf size) {
	m_logicalSize = size;
	if (!m_renderTexture) {
		return;
	}

	sf::View view(sf::FloatRect(static_cast<float>(size.x), static_cast<float>(size.y), static_cast<float>(size.width), static_cast<float>(size.height)));

	const auto viewWidth = static_cast<float>(size.width);
	const auto viewHeight = static_cast<float>(size.height);
	const auto windowWidth = static_cast<float>(m_renderTexture->getSize().x);
	const auto windowHeight = static_cast<float>(m_renderTexture->getSize().y);
	const auto targetAspectRatio = viewWidth / viewHeight;
	const auto windowAspectRatio = windowWidth / windowHeight;
	const auto scale = windowAspectRatio > targetAspectRatio ? windowHeight / viewHeight : windowWidth / viewWidth;
	const auto cropX = (windowAspectRatio > targetAspectRatio ? (windowWidth - viewWidth * scale) / (2.0f * windowWidth) : 0.0f);
	const auto cropY = (windowAspectRatio < targetAspectRatio ? (windowHeight - viewHeight * scale) / (2.0f * windowHeight) : 0.0f);
	const auto cropW = (viewWidth * scale) / windowWidth;
	const auto cropH = (viewHeight * scale) / windowHeight;

	view.setViewport(sf::FloatRect(cropX, cropY, cropW, cropH));
	m_renderTexture->setView(view);
}

ppr::model::rectf ppr::data::sfml2::SfmlRenderTexture::getLogicalSize() const {
	return m_logicalSize;
}

std::unique_ptr<ppr::data::SpriteBatch> ppr::data::sfml2::SfmlRenderTexture::createBatch(ppr::model::Texture &texture) {
	assure();

	return tl::make_unique<SfmlSpriteBatch>(*m_renderTexture, m_activeShader, texture);
}

void ppr::data::sfml2::SfmlRenderTexture::drawTexture(ppr::model::Texture &texture, ppr::model::rectf destination,
													  ppr::model::recti source, ppr::model::rotation rotation,
													  ppr::model::colour colour) {
	assure();
	static_cast<SfmlTexture&>(texture).assure();

	sf::Texture const& sfTexture = static_cast<SfmlTexture&>(texture).getTexture();
	sf::IntRect sfTextureArea(source.x, source.y, source.width, source.height);

	sf::Vector2f sfScale(static_cast<float>(destination.width / source.width), static_cast<float>(destination.height / source.height));
	sf::Vector2f sfOrigin(static_cast<float>(rotation.origin.x) / sfScale.x, static_cast<float>(rotation.origin.y) / sfScale.y);
	sf::Vector2f sfPosition(static_cast<float>(destination.x) + sfOrigin.x * sfScale.x, static_cast<float>(destination.y) + sfOrigin.y * sfScale.y);
	auto sfAngle = static_cast<float>(rotation.angle.asDegrees());

	sf::Sprite sfSprite(sfTexture, sfTextureArea);
	sfSprite.setPosition(sfPosition);
	sfSprite.setScale(sfScale);
	sfSprite.setOrigin(sfOrigin);
	sfSprite.setRotation(sfAngle);
	sfSprite.setColor(sf::Color(colour.red, colour.green, colour.blue, colour.alpha));
	m_renderTexture->draw(sfSprite, m_activeShader.get());
}

void ppr::data::sfml2::SfmlRenderTexture::drawRectangle(ppr::model::rectf destination, ppr::model::colour colour,
														bool isFilled) {
	assure();

	sf::RectangleShape rectangleShape(sf::Vector2f(static_cast<float>(destination.width), static_cast<float>(destination.height)));
	rectangleShape.setPosition(sf::Vector2f(static_cast<float>(destination.x), static_cast<float>(destination.y)));
	if (!isFilled) {
		rectangleShape.setOutlineColor(sf::Color(colour.red, colour.green, colour.blue, colour.alpha));
		rectangleShape.setOutlineThickness(-1.0f);
		rectangleShape.setFillColor(sf::Color::Transparent);
	} else {
		rectangleShape.setFillColor(sf::Color(colour.red, colour.green, colour.blue, colour.alpha));
	}
	m_renderTexture->draw(rectangleShape, m_activeShader.get());
}

void ppr::data::sfml2::SfmlRenderTexture::bindShader(ppr::model::Shader &shader) {
	assure();
	static_cast<SfmlShader&>(shader).assure();
	m_activeShader = static_cast<SfmlShader&>(shader).getShader();
}

void ppr::data::sfml2::SfmlRenderTexture::unbindShader() {
	assure();
	m_activeShader = std::shared_ptr<sf::Shader>();
}

ppr::model::Texture& ppr::data::sfml2::SfmlRenderTexture::getTexture() {
	assure();
	return *m_internalTexture;
}

