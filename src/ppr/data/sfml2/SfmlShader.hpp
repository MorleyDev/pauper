#ifndef PPR_DATA_SFML2_SFMLSHADER_HPP_INCLUDED
#define PPR_DATA_SFML2_SFMLSHADER_HPP_INCLUDED

#include "../../model/Shader.hpp"
#include <SFML/Graphics.hpp>
#include <memory>

namespace ppr {
	namespace data {
		namespace sfml2 {
			class SfmlShader : public model::Shader {
			private:
				std::shared_ptr<sf::Shader> m_shader;
				std::string m_shaderText;
				std::string m_shaderText2;

			public:
				std::shared_ptr<sf::Shader> getShader() const { return m_shader; }

				SfmlShader(model::ShaderType type, std::string shaderText);
				SfmlShader(model::ShaderType type, std::string shaderText, std::string shaderText2);
				~SfmlShader();

				virtual void set(std::string, double);
				virtual void set(std::string, model::pos2f);
				virtual void set(std::string, model::colour);
				virtual void set(std::string, model::Texture&);
				virtual void setCurrentTexture(std::string);

				void assure();
			};
		}
	}
}

#endif//PPR_DATA_SFML2_SFMLSHADER_HPP_INCLUDED
