#ifndef PAUPER_DATA_SFML2_SFMLCONTEXT_HPP_INCLUDED
#define PAUPER_DATA_SFML2_SFMLCONTEXT_HPP_INCLUDED

#include "../SysContext.hpp"

namespace ppr {
	namespace data {
		namespace sfml2 {
			class SfmlContext : public SysContext {
			public:
				SfmlContext();

				virtual std::unique_ptr<Window> createWindow(std::string string, std::size_t w, std::size_t h, bool fullscreen, bool vsync);
				virtual std::unique_ptr<MusicPlayer> createMusicPlayer();
				virtual std::unique_ptr<SoundEffectPlayer> createSoundEffectPlayer();
			};
		}
	}
}

#endif//PAUPER_DATA_SFML2_SFMLCONTEXT_HPP_INCLUDED
