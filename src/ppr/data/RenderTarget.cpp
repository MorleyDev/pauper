#include "RenderTarget.hpp"
#include "../model/Texture.hpp"

ppr::data::RenderTarget::RenderTarget() : m_clearColour(0,0,0,0) {
}

ppr::data::RenderTarget::~RenderTarget() {

}

void ppr::data::RenderTarget::drawTexture(model::Texture &texture, model::rectf destination) {
	drawTexture(texture, destination, model::recti(0, 0, texture.width(), texture.height()), model::rotation(model::pos2f(0.0,0.0), 0.0));
}

void ppr::data::RenderTarget::drawTexture(model::Texture& texture, model::rectf destination, model::recti source) {
	drawTexture(texture, destination, source, model::rotation(model::pos2f(0.0,0.0), 0.0));
}

void ppr::data::RenderTarget::drawTexture(model::Texture& texture,
										  model::rectf destination, model::recti source,
										  model::rotation rotation) {
	drawTexture(texture, destination, source, rotation, model::colour(255,255,255,255));
}

void ppr::data::RenderTarget::drawTexture(model::Texture& texture, model::rectf destination, model::recti source, model::colour colour) {
	drawTexture(texture, destination, source, model::rotation(model::pos2f(0.0,0.0), 0.0), colour);
}