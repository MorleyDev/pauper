#include "EventQueue.hpp"

ppr::data::EventQueue::queue ppr::data::EventQueue::_dequeue() {
	std::lock_guard<std::mutex> lock(m_queueLock);

	queue tmp;
	tmp.reserve(m_eventQueue.size());
	m_eventQueue.swap(tmp);
	return tmp;
}

void ppr::data::EventQueue::enqueue(std::type_index type, ppr::tl::any data) {
	std::lock_guard<std::mutex> lock(m_queueLock);
	m_eventQueue.push_back(std::make_pair(type, data));
}
