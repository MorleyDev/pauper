#include "InputSystem.hpp"

ppr::data::InputSystem::~InputSystem() {
}

ppr::data::InputSystem::InputSystem()
		: m_isTextMode(false),
		  m_onKeyboard(),
		  m_onText(),
		  m_onMouseClick(),
		  m_onMouseMove(),
		  m_onTouch(),
		  m_onTouchMove() {
}
