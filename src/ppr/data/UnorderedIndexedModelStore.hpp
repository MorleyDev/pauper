#ifndef PPR_DATA_UNORDEREDINDEXEDMODELSTORE_HPP_INCLUDED
#define PPR_DATA_UNORDEREDINDEXEDMODELSTORE_HPP_INCLUDED

#include "IndexedModelStore.hpp"
#include "FlatModelStore.hpp"
#include <mutex>

namespace ppr {
	namespace data {
		template<typename TKey, typename TModel>
		class UnorderedIndexedModelStore : public IndexedModelStore<TKey, TModel, std::unordered_map<TKey,TModel>> {
		private:
			std::unordered_map<TKey, TModel> m_models;
			mutable std::mutex m_readLock;
			mutable std::mutex m_writeLock;

		public:
			UnorderedIndexedModelStore() : m_models(), m_readLock(), m_writeLock() {
			}

			UnorderedIndexedModelStore(UnorderedIndexedModelStore<TKey,TModel> const& copy)
				: m_models(copy.clone()),
				  m_readLock(),
				  m_writeLock() {
			}

			UnorderedIndexedModelStore(UnorderedIndexedModelStore<TKey,TModel>&& orig)
				: m_models(),
				  m_readLock(),
				  m_writeLock() {
				std::lock_guard<std::mutex> lock1(orig.m_writeLock);
				std::lock_guard<std::mutex> lock2(orig.m_readLock);
				orig.m_models.swap(m_models);
			}

			UnorderedIndexedModelStore& operator=(UnorderedIndexedModelStore<TKey,TModel> const& copy) {
				std::lock_guard<std::mutex> lock1(m_writeLock);
				replace(copy.clone());
				return *this;
			}
			UnorderedIndexedModelStore& operator=(UnorderedIndexedModelStore<TKey,TModel>&& orig) {
				std::lock_guard<std::mutex> lock1(m_writeLock);
				std::lock_guard<std::mutex> lock2(m_readLock);
				std::lock_guard<std::mutex> locko1(orig.m_writeLock);
				std::lock_guard<std::mutex> locko2(orig.m_readLock);
				m_models.clear();
				orig.m_models.swap(m_models);
				return *this;
			}

			virtual ~UnorderedIndexedModelStore() { }

			virtual void add(TKey const& key, TModel model) {
				std::lock_guard<std::mutex> lock1(m_writeLock);
				std::lock_guard<std::mutex> lock2(m_readLock);
				m_models.insert(std::make_pair(key,model));
			}
			virtual void add(std::pair<const TKey,TModel> model) {
				std::lock_guard<std::mutex> lock1(m_writeLock);
				std::lock_guard<std::mutex> lock2(m_readLock);
				m_models.insert(model);
			}

			virtual void remove(TKey const& key) {
				std::lock_guard<std::mutex> lock1(m_writeLock);
				std::lock_guard<std::mutex> lock2(m_readLock);
				m_models.erase(key);
			}

			virtual tl::optional<TModel> get(TKey const& key) const {
				std::lock_guard<std::mutex> lock2(m_readLock);
				auto f = m_models.find(key);
				return f != m_models.end()
				 	? tl::some(f->second)
				 	: tl::none();
			}

			virtual std::unordered_map<TKey,TModel> filter(std::function<bool (std::pair<const TKey,TModel> const&)> predicate) const {
				auto models = clone();
				for(auto it = std::begin(models); it != std::end(models);)  {
					if (!predicate(*it)) {
						it = models.erase(it);
					} else {
						++it;
					}
				}
				return std::move(models);
			}

			virtual tl::optional<std::pair<const TKey,TModel>> first(std::function<bool (std::pair<const TKey,TModel> const&)> predicate) const {
				auto models = clone();
				for(auto const& model : models) {
					if (predicate(model)) {
						return tl::some(std::pair<const TKey,TModel>(model.first, model.second));
					}
				}
				return tl::none();
			}

			virtual void each(std::function<void (std::pair<const TKey,TModel> const&)> operand) const {
				auto models = clone();
				std::for_each(models.begin(), models.end(), operand);
			}

			virtual void mutate(std::function<void (std::pair<const TKey,TModel>&)> mutator) {
				std::lock_guard<std::mutex> lock1(m_writeLock);
				auto models = clone();
				std::for_each(models.begin(), models.end(), mutator);
				replace(std::move(models));
			}

			virtual void removeIf(std::function<bool (std::pair<const TKey,TModel> const&)> operand) {
				std::lock_guard<std::mutex> lock1(m_writeLock);
				replace(std::move(filter(std::not1(operand))));
			}

			virtual void removeAll() {
				std::lock_guard<std::mutex> lock1(m_writeLock);
				std::lock_guard<std::mutex> lock2(m_readLock);
				m_models.clear();
			}

			virtual bool all(std::function<bool (std::pair<const TKey,TModel> const&)> predicate) const {
				auto models = clone();
				for(auto const& model : models) {
					if (!predicate(model)) {
						return false;
					}
				}
				return true;
			}

			virtual bool any(std::function<bool (std::pair<const TKey,TModel> const&)> predicate) const {
				auto models = clone();
				for(auto const& model : models) {
					if (predicate(model)) {
						return true;
					}
				}
				return false;
			}

			virtual bool empty() const {
				std::lock_guard<std::mutex> lock2(m_readLock);
				return m_models.empty();
			}

		protected:
			virtual void replace(std::unordered_map<TKey,TModel> replacement) {
				std::lock_guard<std::mutex> lock2(m_readLock);
				m_models.swap(replacement);
			}
			virtual std::unordered_map<TKey,TModel> clone() const {
				std::lock_guard<std::mutex> lock2(m_readLock);
				return m_models;
			}
		};
	}
}

#endif//PPR_DATA_UNORDEREDINDEXEDMODELSTORE_HPP_INCLUDED
