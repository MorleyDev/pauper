#include "Sdl2InputSystem.hpp"
#include <SDL2/SDL.h>

ppr::data::sdl2::Sdl2InputSystem::Sdl2InputSystem(model::pos2i screenSize):
		InputSystem(),
		m_fingerIdMap(),
		m_screenSize(screenSize) {
}

ppr::data::sdl2::Sdl2InputSystem::~Sdl2InputSystem() {
}

void ppr::data::sdl2::Sdl2InputSystem::notifySdlEvent(SDL_Event const &e) {
	switch(e.type) {
		case SDL_TEXTEDITING:
		case SDL_TEXTINPUT:
			// TODO: Figure out how to handle text input
			break;

		case SDL_KEYDOWN:
			signalKeyboard(static_cast<model::KeyCode>(e.key.keysym.sym), ButtonState::Pressed);
			break;
		case SDL_KEYUP:
			signalKeyboard(static_cast<model::KeyCode>(e.key.keysym.sym), ButtonState::Released);
			break;

		case SDL_MOUSEBUTTONDOWN:
			signalMouseClick(e.button.button, ButtonState::Pressed, {e.button.x, e.button.y});
			break;
		case SDL_MOUSEBUTTONUP:
			signalMouseClick(e.button.button, ButtonState::Released, {e.button.x, e.button.y});
			break;
		case SDL_MOUSEMOTION:
			signalMouseMove({e.motion.x, e.motion.y});
			break;

		case SDL_FINGERDOWN: {
			const auto finderId = m_fingerIdMap.insert(std::make_pair(e.tfinger.fingerId, m_fingerIdMap.size())).first->second;
			signalTouch({ finderId, static_cast<std::int32_t>(m_screenSize.x * e.tfinger.x), static_cast<std::int32_t>(m_screenSize.y * e.tfinger.y) }, ButtonState::Pressed);
			break;
		}
		case SDL_FINGERUP: {
			const auto finderId = m_fingerIdMap.insert(std::make_pair(e.tfinger.fingerId, m_fingerIdMap.size())).first->second;
			signalTouch({ finderId, static_cast<std::int32_t>(m_screenSize.x * e.tfinger.x), static_cast<std::int32_t>(m_screenSize.y * e.tfinger.y) }, ButtonState::Released);
			break;
		}
		case SDL_FINGERMOTION: {
			const auto finderId = m_fingerIdMap.insert(std::make_pair(e.tfinger.fingerId, m_fingerIdMap.size())).first->second;
			signalTouchMove({ finderId, static_cast<std::int32_t>(m_screenSize.x * e.tfinger.x), static_cast<std::int32_t>(m_screenSize.y * e.tfinger.y) });
			break;
		}
		default:
			break;
	}
}
