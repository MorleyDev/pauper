#ifndef PPR_DATA_SDL2_SDL2SOUNDEFFECTPLAYER_HPP_INCLUDED
#define PPR_DATA_SDL2_SDL2SOUNDEFFECTPLAYER_HPP_INCLUDED

#include "../SoundEffectPlayer.hpp"
#include "../ReadOnlyFile.hpp"
#include "../../model/SoundEffect.hpp"
#include "../../tl/make_unique.hpp"
#include <chrono>

namespace ppr {
	namespace data {
		namespace sdl2 {
			class Sdl2SoundEffectPlayer : public SoundEffectPlayer {
			public:
				virtual std::unique_ptr<model::SoundEffect> createSoundEffect(ReadOnlyFile& sourceFile) {
					return tl::make_unique<model::SoundEffect>(std::chrono::milliseconds(0));
				}

				virtual void play(model::SoundEffect&, model::pos2f, volume) { }
				virtual void stopAll() { }

				virtual volume getVolume() { return volume(1.0); }
				virtual void setVolume(volume) { }

				virtual void update() { }
			};
		}
	}
}

#endif//PPR_DATA_SDL2_SDL2SOUNDEFFECTPLAYER_HPP_INCLUDED
