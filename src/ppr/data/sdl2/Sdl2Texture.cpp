#include "Sdl2Texture.hpp"
#include <SDL2/SDL.h>

ppr::data::sdl2::Sdl2Texture::Sdl2Texture(std::size_t width, std::size_t height, SDL_Texture* texture)
	: Texture(width, height),
	  m_texture(texture) {
}

ppr::data::sdl2::Sdl2Texture::~Sdl2Texture() {
	SDL_DestroyTexture(m_texture);
}
