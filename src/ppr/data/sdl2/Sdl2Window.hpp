#ifndef PPR_DATA_SDL2_SDL2WINDOW_HPP_INCLUDED
#define PPR_DATA_SDL2_SDL2WINDOW_HPP_INCLUDED

#include "../Window.hpp"
#include "Sdl2Renderer.hpp"
#include "Sdl2InputSystem.hpp"
#include "../../tl/signals.hpp"


struct SDL_Window;

namespace ppr {
	namespace data {
		namespace sdl2 {
			class Sdl2Window : public Window {
			private:
				SDL_Window* m_window;
				std::unique_ptr<Sdl2Renderer> m_renderer;
				model::pos2i m_currentResolution;
				Sdl2InputSystem m_inputSystem;
				tl::signals<void> m_onClose;

			public:
				Sdl2Window(std::string title, std::size_t w, std::size_t h, bool fullscreen, bool vsync);
				virtual ~Sdl2Window();

				virtual Renderer& renderer();
				virtual InputSystem& input();

				virtual void onClose(std::function<void(void)>);
				virtual void poll();

				virtual model::pos2i resolution() const;
				virtual std::vector<model::pos2i> availableResolutions() const;

				virtual bool isPollTiedToCreationThread() const { return true; }
			};
		}
	}
}

#endif//PPR_DATA_SDL2_SDL2WINDOW_HPP_INCLUDED
