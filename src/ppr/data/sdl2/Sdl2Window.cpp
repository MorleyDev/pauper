#include "Sdl2Window.hpp"
#include <SDL2/SDL.h>
#include <SDL2/SDL_video.h>
#include "../../util/Log.hpp"

namespace {
	PPR_CREATE_LOG(Sdl2Window);

	ppr::model::pos2i _CreateVideoMode(std::size_t w, std::size_t h, bool fullscreen) {
		if (w == 0 || h == 0) { // If w or h are 0, get and use default values
			SDL_DisplayMode desktop;
			SDL_GetDesktopDisplayMode(0, &desktop);
			if (fullscreen) { // If fullscreen, just use the desktop resolution
				return ppr::model::pos2i(desktop.w, desktop.h);
			}

			// Otherwise try 1080p or 720p.
			w = desktop.w > 1920 && desktop.h > 1080 ? 1920 : 1280;
			h = desktop.w > 1920 && desktop.h > 1080 ? 1080 : 720;
			while((w > 0 && desktop.w < w) || (h > 0 && desktop.h < h)) { // If 720p fails, just start shrinking until it works
				w /= 2;
				h /= 2;
			}
		}
		return ppr::model::pos2i(w,h);
	}
}

ppr::data::sdl2::Sdl2Window::Sdl2Window(std::string title, std::size_t w, std::size_t h, bool fullscreen, bool vsync)
	: m_window(nullptr),
	  m_renderer(),
	  m_currentResolution(_CreateVideoMode(w, h, fullscreen)),
	  m_inputSystem(m_currentResolution),
	  m_onClose() {
	w = static_cast<std::size_t>(m_currentResolution.x);
	h = static_cast<std::size_t>(m_currentResolution.y);

	m_window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, w, h, SDL_WINDOW_SHOWN | (fullscreen ? SDL_WINDOW_FULLSCREEN : 0));
	if (m_window != nullptr) {
		PPR_WRITE_LOG(Sdl2Window, Trace, "Created SDL2 Window");
		m_renderer = std::make_unique<Sdl2Renderer>(m_window, w, h, fullscreen && vsync);
		return;
	}

	std::string error = SDL_GetError();
	PPR_WRITE_LOG(Sdl2Window, Error, error);
	throw std::runtime_error(error);
}

auto ppr::data::sdl2::Sdl2Window::renderer() -> Renderer& {
	return *m_renderer;
}

auto ppr::data::sdl2::Sdl2Window::input() -> InputSystem& {
	return m_inputSystem;
}

void ppr::data::sdl2::Sdl2Window::onClose(std::function<void(void)> handler) {
	m_onClose.listen(handler);
}

void ppr::data::sdl2::Sdl2Window::poll() {
	SDL_Event event;
	while(SDL_PollEvent(&event) != 0) {
		switch(event.type) {
			case SDL_WINDOWEVENT_CLOSE:
				m_onClose.signal();
				break;

			default:
				m_inputSystem.notifySdlEvent(event);
				break;
		}
	}
}

auto ppr::data::sdl2::Sdl2Window::resolution() const -> model::pos2i {
	return m_currentResolution;
}

auto ppr::data::sdl2::Sdl2Window::availableResolutions() const -> std::vector<model::pos2i> {
	return std::vector<model::pos2i>({ m_currentResolution });
}

ppr::data::sdl2::Sdl2Window::~Sdl2Window() {
	m_renderer.reset();
	if(m_window == nullptr) {
		return;
	}

	SDL_DestroyWindow(m_window);
}
