#ifndef PPR_DATA_SDL2_SDL2SPRITEBATCH_HPP_INCLUDED
#define PPR_DATA_SDL2_SDL2SPRITEBATCH_HPP_INCLUDED

#include "../SpriteBatch.hpp"
#include "../Renderer.hpp"

namespace ppr {
	namespace data {
		namespace sdl2 {
			class Sdl2SpriteBatch : public SpriteBatch {
			private:
				Renderer& m_renderer;
				model::Texture& m_texture;

			public:
				Sdl2SpriteBatch(Renderer& renderer, model::Texture& texture);
				virtual SpriteBatch& draw(model::rectf destination, model::recti source, model::rotation rotation, model::colour colour);
			};
		}
	}
}

#endif//PPR_DATA_SDL2_SDL2SPRITEBATCH_HPP_INCLUDED
