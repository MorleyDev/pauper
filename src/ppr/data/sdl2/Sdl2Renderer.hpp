#ifndef PPR_DATA_SDL2_SDL2RENDERER_HPP_INCLUDED
#define PPR_DATA_SDL2_SDL2RENDERER_HPP_INCLUDED

#include "Sdl2Shader.hpp"
#include "Sdl2SpriteBatch.hpp"
#include "../Renderer.hpp"
#include "../ReadOnlyFile.hpp"
#include "../../model/Texture.hpp"
#include "../../tl/make_unique.hpp"

struct SDL_Window;
struct SDL_Renderer;

namespace ppr {
	namespace data {
		namespace sdl2 {
			class Sdl2Renderer : public Renderer {
			private:
				std::size_t m_windowWidth;
				std::size_t m_windowHeight;
				SDL_Renderer* m_renderer;
				model::rectf m_logicalSize;
				model::recti m_croppedViewportSize;

			public:
				Sdl2Renderer(SDL_Window* window, std::size_t w, std::size_t h, bool useVsync);
				virtual ~Sdl2Renderer();

				virtual void setLogicalSize(model::rectf size);
				virtual model::rectf getLogicalSize() const;

				virtual void clear();
				virtual void display();

				virtual std::unique_ptr<SpriteBatch> createBatch(model::Texture&);
				virtual std::unique_ptr<model::Texture> createTexture(ReadOnlyFile& sourceFile);
				virtual std::unique_ptr<model::Texture> createTexture(std::uint8_t const* data, std::size_t width, std::size_t height);

				virtual std::unique_ptr<model::Shader> createShader(ReadOnlyFile& sourceFile, model::ShaderType type);
				virtual std::unique_ptr<model::Shader> createShader(ReadOnlyFile& vertexSourceFile, ReadOnlyFile& fragmentSourceFile);

				virtual void drawTexture(model::Texture& texture, model::rectf destination, model::recti source, model::rotation rotation, model::colour colour);
				virtual void drawRectangle(model::rectf destination, model::colour colour, bool isFilled = true);

				virtual void bindShader(model::Shader& shader);
				virtual void unbindShader();

			private:
				model::recti toAbsoluteRenderLocation(model::rectf pos) const;
				model::pos2i toAbsoluteRenderLocation(model::pos2f pos) const;
			};
		}
	}
}

#endif//PPR_DATA_SDL2_SDL2RENDERER_HPP_INCLUDED
