#include "Sdl2SysContext.hpp"
#include "Sdl2Window.hpp"
#include "Sdl2MusicPlayer.hpp"
#include "Sdl2SoundEffectPlayer.hpp"

#include "../../util/Log.hpp"

#include <SDL2/SDL.h>

PPR_CREATE_LOG(Sdl2SysContext);

ppr::data::sdl2::Sdl2SysContext::Sdl2SysContext() {
	if (SDL_Init(SDL_INIT_VIDEO) == 0) {
		PPR_WRITE_LOG(Sdl2SysContext, Trace, "Initialised SDL2");
		return;
	}

	auto error = std::string(SDL_GetError());
	PPR_WRITE_LOG(Sdl2SysContext, Error, error);
	throw std::runtime_error(error);
}

ppr::data::sdl2::Sdl2SysContext::~Sdl2SysContext() {
	SDL_Quit();
}

auto ppr::data::sdl2::Sdl2SysContext::createWindow(std::string title, std::size_t w, std::size_t h, bool fullscreen, bool vsync) -> std::unique_ptr<Window> {
	return tl::make_unique<Sdl2Window>(title, w, h, fullscreen, vsync);
}

auto ppr::data::sdl2::Sdl2SysContext::createMusicPlayer() -> std::unique_ptr<MusicPlayer> {
	return tl::make_unique<Sdl2MusicPlayer>();
}

auto ppr::data::sdl2::Sdl2SysContext::createSoundEffectPlayer() -> std::unique_ptr<SoundEffectPlayer> {
	return tl::make_unique<Sdl2SoundEffectPlayer>();
}
