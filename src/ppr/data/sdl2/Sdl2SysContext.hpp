#ifndef PPR_DATA_SDL2_SDL2SYSCONTEXT_HPP_INCLUDED
#define PPR_DATA_SDL2_SDL2SYSCONTEXT_HPP_INCLUDED

#include "../SysContext.hpp"

namespace ppr {
	namespace data {
		namespace sdl2 {
			class Sdl2SysContext : public SysContext {
			public:
				Sdl2SysContext();
				virtual ~Sdl2SysContext();

				virtual std::unique_ptr<Window> createWindow(std::string title, std::size_t w, std::size_t h, bool fullscreen, bool vsync);
				virtual std::unique_ptr<MusicPlayer> createMusicPlayer();
				virtual std::unique_ptr<SoundEffectPlayer> createSoundEffectPlayer();
			};
		}
	}
}

#endif//PPR_DATA_SDL2_SDL2SYSCONTEXT_HPP_INCLUDED
