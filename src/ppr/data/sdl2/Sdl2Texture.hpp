#ifndef PPR_DATA_SDL2_SDL2TEXTURE_HPP
#define PPR_DATA_SDL2_SDL2TEXTURE_HPP

#include "../../model/Texture.hpp"
#include <cstdlib>

struct SDL_Texture;

namespace ppr {
	namespace data {
		namespace sdl2 {
			class Sdl2Texture : public model::Texture {
			private:
				SDL_Texture* m_texture;

			public:
				Sdl2Texture(std::size_t width, std::size_t height, SDL_Texture* texture);
				virtual ~Sdl2Texture();

				SDL_Texture* getTexture() { return m_texture; }
				SDL_Texture* const getTexture() const { return m_texture; }
			};
		}
	}
}


#endif //PPR_DATA_SDL2_SDL2TEXTURE_HPP
