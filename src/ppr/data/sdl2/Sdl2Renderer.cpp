#include "Sdl2Renderer.hpp"
#include "Sdl2Texture.hpp"
#include "../../util/Log.hpp"
#include <SDL2/SDL.h>

PPR_CREATE_LOG(Sdl2Renderer);

ppr::data::sdl2::Sdl2Renderer::Sdl2Renderer(SDL_Window* window, std::size_t w, std::size_t h, bool useVsync)
	: m_windowWidth(w),
	  m_windowHeight(h),
	  m_renderer(nullptr),
	  m_logicalSize(0.0, 0.0, static_cast<std::double_t>(w), static_cast<std::double_t>(h)),
	  m_croppedViewportSize(0,0,0,0) {
	m_renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | (useVsync ? SDL_RENDERER_PRESENTVSYNC : 0));
	if (m_renderer != nullptr) {
		PPR_WRITE_LOG(Sdl2Renderer, Trace, "Created SDL2 Renderer");
		SDL_SetRenderDrawBlendMode(m_renderer, SDL_BLENDMODE_BLEND);
		setLogicalSize(m_logicalSize);
		return;
	}

	auto error = std::string(SDL_GetError());
	PPR_WRITE_LOG(Sdl2Renderer, Error, error);
	throw std::runtime_error(error);
}

ppr::data::sdl2::Sdl2Renderer::~Sdl2Renderer() {
	if (m_renderer == nullptr) {
		return;
	}

	SDL_DestroyRenderer(m_renderer);
}

auto ppr::data::sdl2::Sdl2Renderer::getLogicalSize() const -> model::rectf {
	return m_logicalSize;
}

void ppr::data::sdl2::Sdl2Renderer::setLogicalSize(model::rectf size) {
	m_logicalSize = size;
	m_croppedViewportSize = { 0, 0, static_cast<int>(m_windowWidth), static_cast<int>(m_windowHeight) };

	SDL_Rect viewport;
	viewport.x = m_croppedViewportSize.x;
	viewport.y = m_croppedViewportSize.y;
	viewport.w = m_croppedViewportSize.width;
	viewport.h = m_croppedViewportSize.height;
	SDL_RenderSetViewport(m_renderer, &viewport);
}

void ppr::data::sdl2::Sdl2Renderer::clear() {
	auto clearColour = getClearColour();
	if (SDL_SetRenderDrawColor(m_renderer, clearColour.red, clearColour.green, clearColour.blue, clearColour.alpha) != 0) {
		PPR_WRITE_LOG(Sdl2Renderer, Error, std::string(SDL_GetError()));
		return;
	}
	if (SDL_RenderClear(m_renderer) != 0) {
		PPR_WRITE_LOG(Sdl2Renderer, Error, std::string(SDL_GetError()));
		return;
	}
}

void ppr::data::sdl2::Sdl2Renderer::display() {
	SDL_RenderPresent(m_renderer);
}

auto ppr::data::sdl2::Sdl2Renderer::createBatch(model::Texture& texture) -> std::unique_ptr<SpriteBatch> {
	return tl::make_unique<Sdl2SpriteBatch>(*this, texture);
}

auto ppr::data::sdl2::Sdl2Renderer::createTexture(ReadOnlyFile& sourceFile) -> std::unique_ptr<model::Texture> {
	//TODO: Add support for non-bmp files via SDL_Image

	auto currentPosition = sourceFile.position();
	sourceFile.seek(0);
	auto fileSize = sourceFile.size();

	std::unique_ptr<char[]> buffer(new char[fileSize]);
	auto readSize = sourceFile.read(buffer.get(), fileSize);
	sourceFile.seek(currentPosition);

	auto raw = SDL_RWFromMem(buffer.get(), readSize);
	auto surface = SDL_LoadBMP_RW(raw, 1);
	if (surface == nullptr) {
		PPR_WRITE_LOG(Sdl2Renderer, Error, "Could not create SDL_Surface from image file");
		throw std::runtime_error("Could not create SDL_Surface from image file");
	}
	auto texture = SDL_CreateTextureFromSurface(m_renderer, surface);
	SDL_FreeSurface(surface);
	if (texture == nullptr) {
		PPR_WRITE_LOG(Sdl2Renderer, Error, "Could not create SDL_Texture from SDL_Surface");
		throw std::runtime_error("Could not create SDL_Texture from SDL_Surface");
	}

	int width, height = 0;
	SDL_QueryTexture(texture, nullptr, nullptr, &width, &height);
	return tl::make_unique<Sdl2Texture>(width, height, texture);
}

std::unique_ptr<ppr::model::Texture> ppr::data::sdl2::Sdl2Renderer::createTexture(std::uint8_t const* data,
                                                                                  std::size_t width,
                                                                                  std::size_t height) {
	const auto bufferSize = width*height;
	std::unique_ptr<std::uint32_t[]> buffer(new std::uint32_t[bufferSize]);
	auto pixelFormat = SDL_AllocFormat(SDL_PIXELFORMAT_RGBA8888);
	for(auto i = 0u; i < bufferSize; i++) {
		const auto offset = i * 4;
		auto rgba = SDL_MapRGBA(pixelFormat, data[offset], data[offset + 1], data[offset + 2], data[offset + 3]);
		buffer[i] = rgba == 0xFF00FFFF ? 0x00000000 : rgba;
	}

	const auto pixels = buffer.get();
	auto surface = SDL_CreateRGBSurfaceFrom(pixels, width, height, 32, width*4, pixelFormat->Rmask, pixelFormat->Gmask, pixelFormat->Bmask, pixelFormat->Amask);
	SDL_FreeFormat(pixelFormat);
	if (surface == nullptr) {
		PPR_WRITE_LOG(Sdl2Renderer, Error, "Could not create SDL_Surface from image file");
		throw std::runtime_error("Could not create SDL_Surface from image file");
	}
	auto texture = SDL_CreateTextureFromSurface(m_renderer, surface);
	SDL_FreeSurface(surface);
	if (texture == nullptr) {
		PPR_WRITE_LOG(Sdl2Renderer, Error, "Could not create SDL_Texture from SDL_Surface");
		throw std::runtime_error("Could not create SDL_Texture from SDL_Surface");
	}

	return tl::make_unique<Sdl2Texture>(width, height, texture);
}

auto ppr::data::sdl2::Sdl2Renderer::createShader(ReadOnlyFile& sourceFile, model::ShaderType type) -> std::unique_ptr<model::Shader> {
	return tl::make_unique<Sdl2Shader>(type);
}

auto ppr::data::sdl2::Sdl2Renderer::createShader(ReadOnlyFile& vertexSourceFile, ReadOnlyFile& fragmentSourceFile) -> std::unique_ptr<model::Shader> {
	return tl::make_unique<Sdl2Shader>(model::ShaderType::VertexFragment);
}

void ppr::data::sdl2::Sdl2Renderer::drawTexture(model::Texture& texture, model::rectf destination, model::recti source, model::rotation rotation, model::colour colour) {
	auto trueDestination = toAbsoluteRenderLocation(destination);

	auto sdlTexture = static_cast<Sdl2Texture&>(texture).getTexture();
	if (SDL_SetTextureColorMod(sdlTexture, colour.red, colour.green, colour.blue) != 0) {
		PPR_WRITE_LOG(Sdl2Renderer, Error, std::string(SDL_GetError()));
		return;
	}
	if (SDL_SetTextureBlendMode(sdlTexture, SDL_BLENDMODE_BLEND) != 0) {
		PPR_WRITE_LOG(Sdl2Renderer, Error, std::string(SDL_GetError()));
		return;
	}
	if (SDL_SetTextureAlphaMod(sdlTexture, colour.alpha) != 0) {
		PPR_WRITE_LOG(Sdl2Renderer, Error, std::string(SDL_GetError()));
		return;
	}
	SDL_Rect srcRect; srcRect.x = source.x; srcRect.y = source.y; srcRect.w = source.width; srcRect.h = source.height;
	SDL_Rect dstRect; dstRect.x = trueDestination.x; dstRect.y = trueDestination.y; dstRect.w = trueDestination.width; dstRect.h = trueDestination.height;
	auto rotationOrigin = toAbsoluteRenderLocation(rotation.origin);
	SDL_Point origin; origin.x = rotationOrigin.x; origin.y = rotationOrigin.y;

	if (SDL_RenderCopyEx(m_renderer, sdlTexture, &srcRect, &dstRect, rotation.angle.asDegrees(), &origin, SDL_FLIP_NONE) != 0) {
		PPR_WRITE_LOG(Sdl2Renderer, Error, std::string(SDL_GetError()));
	}
}

void ppr::data::sdl2::Sdl2Renderer::drawRectangle(model::rectf destination, model::colour colour, bool isFilled) {
	auto trueDestination = toAbsoluteRenderLocation(destination);
	if (SDL_SetRenderDrawColor(m_renderer, colour.red, colour.green, colour.blue, colour.alpha) != 0) {
		PPR_WRITE_LOG(Sdl2Renderer, Error, std::string(SDL_GetError()));
		return;
	}
	SDL_Rect rectangle;
	rectangle.x = trueDestination.x;
	rectangle.y = trueDestination.y;
	rectangle.w = trueDestination.width;
	rectangle.h = trueDestination.height;

	if (isFilled) {
		if (SDL_RenderFillRect(m_renderer, &rectangle) != 0) {
			PPR_WRITE_LOG(Sdl2Renderer, Error, std::string(SDL_GetError()));
		}
		return;
	}
	if(SDL_RenderDrawRect(m_renderer, &rectangle) != 0) {
		PPR_WRITE_LOG(Sdl2Renderer, Error, std::string(SDL_GetError()));
	}
}

void ppr::data::sdl2::Sdl2Renderer::bindShader(model::Shader& shader) {
}

void ppr::data::sdl2::Sdl2Renderer::unbindShader() {
}

auto ppr::data::sdl2::Sdl2Renderer::toAbsoluteRenderLocation(model::rectf pos) const -> model::recti {
	auto dX = m_croppedViewportSize.width / m_logicalSize.width;
	auto dY = m_croppedViewportSize.height / m_logicalSize.height;
	pos.x = (pos.x - m_logicalSize.x) * dX;
	pos.y = (pos.y - m_logicalSize.y) * dY;
	pos.width = pos.width * dX;
	pos.height = pos.height * dY;

	return { static_cast<int>(pos.x), static_cast<int>(pos.y), static_cast<int>(pos.width), static_cast<int>(pos.height) };
}


auto ppr::data::sdl2::Sdl2Renderer::toAbsoluteRenderLocation(model::pos2f pos) const -> model::pos2i {
	auto dX = m_croppedViewportSize.width / m_logicalSize.width;
	auto dY = m_croppedViewportSize.height / m_logicalSize.height;
	pos.x = (pos.x - m_logicalSize.x) * dX;
	pos.y = (pos.y - m_logicalSize.y) * dY;

	return { static_cast<int>(pos.x), static_cast<int>(pos.y) };
}
