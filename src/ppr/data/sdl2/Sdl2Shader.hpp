#ifndef PPR_DATA_SDL2_SDL2SHADER_HPP_INCLUDED
#define PPR_DATA_SDL2_SDL2SHADER_HPP_INCLUDED

#include "../../model/Shader.hpp"

namespace ppr {
	namespace data {
		namespace sdl2 {
			class Sdl2Shader : public model::Shader {
			public:
				Sdl2Shader(model::ShaderType type) : model::Shader(type) { }

				virtual void set(std::string, double) { }
				virtual void set(std::string, model::pos2f) { }
				virtual void set(std::string, model::colour) { }
				virtual void set(std::string, model::Texture&) { }
				virtual void setCurrentTexture(std::string) { }
			};
		}
	}
}

#endif//PPR_DATA_SDL2_SDL2SHADER_HPP_INCLUDED
