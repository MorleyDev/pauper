#include "Sdl2SpriteBatch.hpp"

ppr::data::sdl2::Sdl2SpriteBatch::Sdl2SpriteBatch(Renderer& renderer, model::Texture& texture)
	: m_renderer(renderer),
	  m_texture(texture) {
}

auto ppr::data::sdl2::Sdl2SpriteBatch::draw(model::rectf destination, model::recti source, model::rotation rotation, model::colour colour) -> SpriteBatch& {
	m_renderer.drawTexture(m_texture, destination, source, rotation, colour);
	return *this;
}
