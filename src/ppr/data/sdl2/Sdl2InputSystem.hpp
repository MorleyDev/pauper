#ifndef PAUPER_DATA_SDL2_SDL2INPUTSYSTEM_HPP_INCLUDED
#define PAUPER_DATA_SDL2_SDL2INPUTSYSTEM_HPP_INCLUDED

#include "../InputSystem.hpp"
#include "../../model/position.hpp"

union SDL_Event;

namespace ppr {
	namespace data {
		namespace sdl2 {
			class Sdl2InputSystem : public InputSystem {
			private:
				std::map<std::uint64_t, std::uint32_t> m_fingerIdMap;
				model::pos2i m_screenSize;

			public:
				explicit Sdl2InputSystem(model::pos2i screenSize);
				virtual ~Sdl2InputSystem();

				void notifySdlEvent(SDL_Event const& e);
			};
		}
	}
}

#endif//PAUPER_DATA_SDL2_SDL2INPUTSYSTEM_HPP_INCLUDED
