#ifndef PAUPER_DATA_MUSICPLAYER_HPP_INCLUDED
#define PAUPER_DATA_MUSICPLAYER_HPP_INCLUDED

#include "../extern.hpp"
#include "../tl/domain.hpp"
#include <memory>

namespace ppr {
	namespace model {
	    class MusicStream;
	}

	namespace data {
		class ReadOnlyFile;

		class MusicPlayer {
		private:
			static double _volume_lower() { return 0.0; };
			static double _volume_upper() { return 1.0; };

		public:
			typedef tl::domain<double, _volume_lower, _volume_upper> volume;

			PPR_EXTERN virtual ~MusicPlayer();

			virtual std::unique_ptr<model::MusicStream> createMusicStream(ReadOnlyFile) = 0;

			virtual void play(model::MusicStream& stream, bool loop) = 0;
			virtual bool isPlaying(model::MusicStream const& stream) const = 0;
			virtual void stop(model::MusicStream& stream) = 0;
			virtual void stopAll() = 0;

			virtual void setVolume(volume v) = 0;
			virtual volume getVolume() const = 0;

			virtual void update() = 0;
		};
	}
}

#endif//PAUPER_DATA_MUSICPLAYER_HPP_INCLUDED
