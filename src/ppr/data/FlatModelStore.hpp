#ifndef PPR_DATA_FLATMODELSTORE_HPP_INCLUDED
#define PPR_DATA_FLATMODELSTORE_HPP_INCLUDED

#include "ModelStore.hpp"
#include <mutex>

namespace ppr {
	namespace data {
		template<typename TModel, typename TUnderlying = std::vector<TModel>>
		class FlatModelStore : public ppr::data::ModelStore<TModel, TUnderlying> {
		protected:
			TUnderlying m_models;
			mutable std::mutex m_readLock;
			mutable std::mutex m_writeLock;

		public:
			FlatModelStore()
				: m_models(),
				  m_readLock(),
				  m_writeLock(){
			}

			FlatModelStore(FlatModelStore<TModel,TUnderlying> const& other)
				: m_models(other.clone()),
				  m_readLock(),
				  m_writeLock() {
			}

			FlatModelStore(FlatModelStore<TModel,TUnderlying>&& other)
				: m_models(),
				  m_readLock(),
				  m_writeLock() {
				std::lock_guard<std::mutex> lockW(other.m_writeLock);
				std::lock_guard<std::mutex> lockR(other.m_readLock);
				m_models.swap(other.m_models);
			}

			FlatModelStore<TModel,TUnderlying>& operator=(FlatModelStore<TModel,TUnderlying> const& other) {
				std::lock_guard<std::mutex> lockW(m_writeLock);
				std::lock_guard<std::mutex> lockR(m_readLock);
				m_models = other.clone();
				return *this;
			}

			FlatModelStore<TModel,TUnderlying>& operator=(FlatModelStore<TModel,TUnderlying>&& other) {
				std::lock_guard<std::mutex> lockW(m_writeLock);
				std::lock_guard<std::mutex> lockR(m_readLock);
				m_models.clear();

				std::lock_guard<std::mutex> lockOW(other.m_writeLock);
				std::lock_guard<std::mutex> lockOR(other.m_readLock);
				m_models.swap(other.m_models);
				return *this;
			}

			~FlatModelStore() {
			}

			virtual void add(TModel model) {
				std::lock_guard<std::mutex> lockW(m_writeLock);
				std::lock_guard<std::mutex> lockR(m_readLock);
				m_models.emplace_back(std::move(model));
			}

			virtual bool empty() const {
				std::lock_guard<std::mutex> lock(m_readLock);
				return m_models.empty();
			}

			virtual TUnderlying filter(std::function<bool (TModel const&)> predicate) const {
				auto models = clone();
				models.erase( std::remove_if(std::begin(models), std::end(models), std::not1(predicate)), std::end(models) );
				return std::move(models);
			}

			virtual void each(std::function<void (TModel const&)> operand) const {
				auto models = clone();
				std::for_each(std::begin(models), std::end(models), operand);
			}

			virtual void removeIf(std::function<bool (TModel const&)> operand) {
				std::lock_guard<std::mutex> lockW(m_writeLock);
				auto models = clone();
				models.erase( std::remove_if(std::begin(models), std::end(models), operand), std::end(models) );

				replace(std::move(models));
			}

			virtual void removeAll() {
				std::lock_guard<std::mutex> lockW(m_writeLock);
				replace(std::vector<TModel>());
			}

			virtual tl::optional<TModel> first(std::function<bool (TModel const&)> predicate) const {
				auto models = clone();
				auto it = std::find_if(std::begin(models), std::end(models), predicate);
				return it != std::end(models)
					? tl::some(*it)
					: tl::none();
			}

			virtual void mutate(std::function<void (TModel&)> mutator) {
				std::lock_guard<std::mutex> lockW(m_writeLock);
				auto models = clone();
				std::for_each(std::begin(models), std::end(models), mutator);

				replace(std::move(models));
			}

			virtual bool all(std::function<bool (TModel const&)> predicate) const {
				auto models = clone();
				for(auto const& m : models) {
					if (!predicate(m)) {
						return false;
					}
				}
				return true;
			}

			virtual bool any(std::function<bool (TModel const&)> predicate) const {
				auto models = clone();
				for(auto const& m : models) {
					if (predicate(m)) {
						return true;
					}
				}
				return false;
			}

		protected:
			virtual TUnderlying clone() const {
				std::lock_guard<std::mutex> lock(m_readLock);
				return m_models;
			}

			virtual void replace(TUnderlying replacement) {
				std::lock_guard<std::mutex> lock(m_readLock);
				m_models.swap(replacement);
			}
		};
	}
}

#endif//PPR_DATA_FLATMODELSTORE_HPP_INCLUDED