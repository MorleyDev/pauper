#ifndef PAUPER_DATA_SYSCONTEXT_HPP_INCLUDED
#define PAUPER_DATA_SYSCONTEXT_HPP_INCLUDED

#include "../extern.hpp"
#include <memory>
#include <string>

namespace ppr {
	namespace data {
		class Window;
		class MusicPlayer;
		class SoundEffectPlayer;

		class SysContext {
		public:
			PPR_EXTERN virtual ~SysContext();

			virtual std::unique_ptr<Window> createWindow(std::string, std::size_t w, std::size_t h, bool fullscreen, bool vsync) = 0;
			virtual std::unique_ptr<MusicPlayer> createMusicPlayer() = 0;
			virtual std::unique_ptr<SoundEffectPlayer> createSoundEffectPlayer() = 0;
		};
	}
}

#endif//PAUPER_DATA_SYSCONTEXT_HPP_INCLUDED
