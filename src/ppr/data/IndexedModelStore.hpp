#ifndef PPR_DATA_INDEXEDMODELSTORE_HPP_INCLUDED
#define PPR_DATA_INDEXEDMODELSTORE_HPP_INCLUDED

#include "ModelStore.hpp"
#include "../tl/optional.hpp"
#include <unordered_map>

namespace ppr {
	namespace data {
		template<typename TKey, typename TModel, typename TUnderlying = std::vector<std::pair<const TKey,TModel>>>
		class IndexedModelStore : public ModelStore<std::pair<const TKey,TModel>, TUnderlying> {
		public:
			virtual ~IndexedModelStore() {}

			virtual void add(TKey const& key, TModel model) = 0;
			virtual void remove(TKey const& key) = 0;

			virtual tl::optional<TModel> get(TKey const& key) const = 0;
		};
	}
}

#endif//PPR_DATA_INDEXEDMODELSTORE_HPP_INCLUDED
