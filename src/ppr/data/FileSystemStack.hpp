#ifndef PAUPER_DATA_FILESYSTEMSTACK_HPP_INCLUDED
#define PAUPER_DATA_FILESYSTEMSTACK_HPP_INCLUDED

#include "../extern.hpp"
#include "FileSystem.hpp"
#include <vector>

namespace ppr {
	namespace data {
		class FileSystemStack : public FileSystem {
		private:
			std::vector< std::unique_ptr<ppr::data::FileSystem> > m_fileSystem;

		public:
			PPR_EXTERN FileSystemStack();
			PPR_EXTERN virtual ~FileSystemStack();

			PPR_EXTERN void push(std::unique_ptr<ppr::data::FileSystem> filesystem);
			PPR_EXTERN virtual tl::optional<ReadOnlyFile> open(std::string) const;
		};
	}
}

#endif//PAUPER_DATA_FILESYSTEMSTACK_HPP_INCLUDED
