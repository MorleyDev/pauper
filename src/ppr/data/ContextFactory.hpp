#ifndef PPR_DATA_CONTEXTFACTORY_HPP_INCLUDED
#define PPR_DATA_CONTEXTFACTORY_HPP_INCLUDED

#include "../extern.hpp"
#include <memory>
#include <string>
#include <vector>

namespace ppr {
	namespace data {
		class SysContext;

		extern PPR_EXTERN std::unique_ptr<SysContext> createContext(std::string driver);
		extern PPR_EXTERN std::vector<std::string> getSupportedContexts();
	}
}

#endif//PPR_DATA_CONTEXTFACTORY_HPP_INCLUDED
