#ifndef PPR_DATA_SOUNDEFFECTPLAYER_HPP_INCLUDED
#define PPR_DATA_SOUNDEFFECTPLAYER_HPP_INCLUDED

#include "../extern.hpp"
#include "ReadOnlyFile.hpp"
#include "../model/position.hpp"
#include "../tl/domain.hpp"

namespace ppr {
	namespace model {
	    class SoundEffect;
	}

	namespace data {
		class SoundEffectPlayer {
		private:
			static double _volume_lower() { return 0.0; };
			static double _volume_upper() { return 1.0; };

		public:
			typedef tl::domain<double, _volume_lower, _volume_upper> volume;

			PPR_EXTERN virtual ~SoundEffectPlayer();

			virtual std::unique_ptr<model::SoundEffect> createSoundEffect(ReadOnlyFile& sourceFile) = 0;

			virtual void play(model::SoundEffect& sound, model::pos2f position, volume = volume(1.0)) = 0;
			virtual void stopAll() = 0;

			virtual volume getVolume() = 0;
			virtual void setVolume(volume) = 0;

			virtual void update() = 0;
		};
	}
}

#endif//PPR_DATA_SOUNDEFFECTPLAYER_HPP_INCLUDED
