#ifndef PAUPER_GAME_HPP_INCLUDED
#define PAUPER_GAME_HPP_INCLUDED

#include "extern.hpp"
#include "view/ViewStore.hpp"
#include "view/View.hpp"
#include "ctrl/ControllerStore.hpp"
#include "ctrl/Controller.hpp"
#include "data/EventQueue.hpp"
#include "model/Configuration.hpp"

#include <chrono>
#include <atomic>

namespace ppr {
	namespace data {
		class SysContext;
		class Window;
		class Renderer;
		class InputSystem;
		class MusicPlayer;
		class SoundEffectPlayer;
		class FileSystemStack;
		class ContentLoader;
	}

	class Game {
	private:
		model::Configuration m_config;
		tl::thread_pool m_threadPool;

		std::map<std::type_index, std::function<void (tl::any const&)>> m_eventHandlers;
		std::unique_ptr<data::SysContext> m_context;
		std::unique_ptr<data::FileSystemStack> m_fileSystem;
		std::unique_ptr<data::Window> m_window;
		std::unique_ptr<data::MusicPlayer> m_musicPlayer;
		std::unique_ptr<data::SoundEffectPlayer> m_soundEffectPlayer;
		std::unique_ptr<data::ContentLoader> m_content;

		std::atomic<bool> m_isAlive;

	protected:
		data::EventQueue events;
		data::Renderer& renderer;
		data::InputSystem& input;
		ctrl::ControllerStore controllers;
		view::ViewStore views;

		data::SysContext& context;
		data::FileSystemStack& files;
		data::Window& window;
		data::MusicPlayer& music;
		data::SoundEffectPlayer& sound;
		data::ContentLoader& content;

	public:
		PPR_EXTERN static model::Configuration loadConfiguration();
		PPR_EXTERN static void writeConfiguration(model::Configuration const&);

		PPR_EXTERN Game(std::string gameName, model::Configuration config = loadConfiguration());
		PPR_EXTERN virtual ~Game();

		PPR_EXTERN std::future<void> update(std::chrono::microseconds dt);
		PPR_EXTERN std::future<void> poll_events();
		PPR_EXTERN void clean();

		PPR_EXTERN void draw();
		PPR_EXTERN void poll_system();

		inline bool isAlive() const { return m_isAlive.load(); }
		inline void kill() { m_isAlive.store(false); }

		PPR_EXTERN bool hasUniqueSystemThread() const;
	};
}

#endif//PAUPER_GAME_HPP_INCLUDED
