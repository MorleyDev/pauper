#ifndef PPR_EXTERN_HPP_INCLUDED
#define PPR_EXTERN_HPP_INCLUDED

#include "config.hpp"

#ifdef PPR_STATIC
	#define PPR_EXTERN_IMPORT
	#define PPR_EXTERN_EXPORT
	#define PPR_LOCAL
#else // !PPR_STATIC
	#ifdef _MSC_VER 
		#define PPR_EXTERN_IMPORT __declspec(dllimport)
		#define PPR_EXTERN_EXPORT __declspec(dllexport)
		#define PPR_LOCAL
    #else //!_MSC_VER
		#define PPR_EXTERN_IMPORT __attribute__ ((visibility ("default")))
		#define PPR_EXTERN_EXPORT __attribute__ ((visibility ("default")))
		#define PPR_LOCAL __attribute__ ((visibility ("hidden")))
	#endif//_MSC_VER 
#endif // PPR_STATIC

#ifdef PPR_COMPILE
	#define PPR_EXTERN PPR_EXTERN_EXPORT
#else // !PPR_COMPILE
	#define PPR_EXTERN PPR_EXTERN_IMPORT
#endif // PPR_COMPILE

#endif//PPR_EXTERN_HPP_INCLUDED
