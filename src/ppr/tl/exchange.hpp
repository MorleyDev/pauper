#ifndef PPR_TL_EXCHANGE_HPP_INCLUDED
#define PPR_TL_EXCHANGE_HPP_INCLUDED

#include <utility>

namespace ppr {
	namespace tl {
		template<class T>
		T exchange(T& obj, std::nullptr_t new_value)
		{
		    T old_value = std::move(obj);
		    obj = std::forward<T>(T(new_value));
		    return old_value;
		}
		
		// Based on http://en.cppreference.com/w/cpp/utility/exchange#Possible_implementation
		template<class T, class U = T>
		T exchange(T& obj, U&& new_value)
		{
			T old_value = std::move(obj);
			obj = std::forward<U>(new_value);
			return old_value;
		}
	}
}

#endif//PPR_TL_EXCHANGE_HPP_INCLUDED
