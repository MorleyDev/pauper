#ifndef PAUPER_TL_MATHS_DEGREES_HPP_INCLUDED
#define PAUPER_TL_MATHS_DEGREES_HPP_INCLUDED

namespace ppr {
	namespace tl {
		namespace maths {
			const double pi =  3.141592653589;

			inline double degrees_to_radians(double degrees) {
				return degrees * (pi / 180.0);
			}

		    inline double radians_to_degrees(double radians) {
			    return radians * (180.0 / pi);
		    }
		}
	}
}

#endif//PAUPER_TL_MATHS_DEGREES_HPP_INCLUDED
