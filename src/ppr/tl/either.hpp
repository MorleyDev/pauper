#ifndef PPR_TL_EITHER_HPP_INCLUDED
#define PPR_TL_EITHER_HPP_INCLUDED

#include <new>
#include <array>

namespace ppr {
	namespace tl {
		namespace _detail {
			template<unsigned i, unsigned j, unsigned M = (i > j ? i : j)>
			struct _max_size {
				enum { value = M };
			};
		}

		template<typename L, typename R> class either {
		private:
			std::array<char, _detail::_max_size<sizeof(L), sizeof(R)>::value> m_internalData;
			void (either<L,R>::*m_destroy)(void);
			bool m_isLeft;

		public:
			static either<L,R> left(L data) {
				either<L,R> result;
				result.m_destroy = &either<L,R>::destroyLeft;
				result.constructLeft(std::forward<L>(data));
				result.m_isLeft = true;
				return result;
			}

			static either<L,R> right(R data) {
				either<L,R> result;
				result.m_destroy = &either<L,R>::destroyRight;
				result.constructRight(std::forward<R>(data));
				result.m_isLeft = false;
				return result;
			}

			either(const either<L,R>& other) 
			: m_internalData(), 
			  m_destroy(other.m_destroy),
 			  m_isLeft(other.m_isLeft) {
				if (other.isLeft()) {
					constructLeft(other.left());
				} else {
					constructRight(other.right());
				}
			}

			either& operator=(const either<L,R>& other) {
				destroy();
				m_destroy = other.m_destroy;
				m_isLeft = other.m_isLeft;
				if (other.isLeft()) {
					constructLeft(other.left());
				} else {
					constructRight(other.right());
				}
				return *this;
			}

			~either() {
				destroy();
			}

			bool isLeft() const { return m_isLeft; }
			bool isRight() const { return !m_isLeft; }

			template<typename U, typename V> auto fold(U leftFunc, V rightFunc) const -> decltype(std::declval<U>()(std::declval<L>())) {
				return isLeft()
						? leftFunc(left())
						: rightFunc(right());
			}

			L& left() { return *unsafeGetLeft(); }
			L const& left() const { return *unsafeGetLeft(); }

			R& right() { return *unsafeGetRight(); }
			R const& right() const { return *unsafeGetRight(); }

			L* getLeft() {
				return isLeft()
						? unsafeGetLeft()
						: nullptr;
			}

			const L* getLeft() const {
				return isLeft()
						? unsafeGetLeft()
						: nullptr;
			}

			R* getRight() {
				return isRight()
						? unsafeGetRight()
						: nullptr;
			}

			const R* getRight() const {
				return isRight()
						? unsafeGetRight()
						: nullptr;
			}

		private:
			either() : m_internalData(), m_destroy(nullptr), m_isLeft(false) { }

			L* unsafeGetLeft() { return static_cast<L*>(static_cast<void*>(m_internalData.data())); }
			const L* unsafeGetLeft() const { return static_cast<const L*>(static_cast<const void*>(m_internalData.data())); }

			R* unsafeGetRight() { return static_cast<R*>(static_cast<void*>(m_internalData.data())); }
			const R* unsafeGetRight() const { return static_cast<const R*>(static_cast<const void*>(m_internalData.data())); }

			void destroy() { (this->*m_destroy)(); }
			void destroyLeft() { unsafeGetLeft()->~L(); }
			void destroyRight() { unsafeGetRight()->~R(); }

			void constructLeft(L data) { new (m_internalData.data()) L(std::forward<L>(data)); }
			void constructRight(R data) { new (m_internalData.data()) R(std::forward<R>(data)); }
		};

	}
}

#endif//PPR_TL_EITHER_HPP_INCLUDED
