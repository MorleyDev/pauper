#ifndef PPR_TL_FORMAT_HPP_INCLUDED
#define PPR_TL_FORMAT_HPP_INCLUDED

#include <string>
#include <sstream>

namespace ppr {
	namespace tl {
		namespace string {
			namespace _detail {
				inline void format(std::ostream& stream, const char* message) {
					stream << message;
				}

				template<typename T, typename ... ARGS>
				void format(std::ostream& stream, const char* message, T value, ARGS&&... args) {
					while(*message) {
						char c = *message;
						if (*message == '\\' && *(message + 1) == '%') {
							++message;
							stream << *message;
							message++;
							continue;
						}
						if (*message != '%') {
							stream << *(message++);
							continue;
						}
						message++;
						break;
					}
					stream << value;
					format(stream, message, std::forward<ARGS>(args)...);
				}
			}

			template<typename ... ARGS> void format(std::ostream& stream, const char* source, ARGS&&... args) {
				_detail::format(stream, source, std::forward<ARGS>(args)...);
			}
			template<typename ... ARGS> void format(std::ostream& stream, std::string const& source, ARGS&&... args) {
				_detail::format(stream, source.c_str(), std::forward<ARGS>(args)...);
			}

			template<typename ... ARGS> std::string format(std::string const& source, ARGS&&... args) {
				std::stringstream stream;
				format(stream, source.c_str(), std::forward<ARGS>(args)...);
				return stream.str();
			}
			template<typename ... ARGS> std::string format(const char* source, ARGS&&... args) {
				std::stringstream stream;
				format(stream, source, std::forward<ARGS>(args)...);
				return stream.str();
			}

			inline std::string format(std::string const& source) { return source; }
			inline std::string format(const char* source) { return source; }

		}
	}
}

#endif//PPR_TL_FORMAT_HPP_INCLUDED
