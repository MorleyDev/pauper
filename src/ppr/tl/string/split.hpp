#ifndef PPR_TL_SPLIT_STRING_HPP_INCLUDED
#define PPR_TL_SPLIT_STRING_HPP_INCLUDED

#include "../exchange.hpp"
#include "../iterator_pair.hpp"
#include <string>
#include <cassert>
#include <utility>

namespace ppr {
    namespace tl {
        namespace string {
            struct string_split_iterator {
            private:
	            std::string delimiter;
	            std::string underlying;
	            std::size_t currPos;

            public:
	            typedef std::forward_iterator_tag iterator_category;
	            typedef std::string value_type;
	            typedef std::ptrdiff_t difference_type;
	            typedef value_type const* pointer;
	            typedef value_type const& reference;

	            string_split_iterator()
			            : delimiter(),
			              underlying(),
			              currPos(std::string::npos) {
	            }

	            string_split_iterator(std::string d, std::string u, std::size_t c)
			            : delimiter(std::move(d)),
			              underlying(std::move(u)), currPos(c) {
	            }

	            string_split_iterator(string_split_iterator const& orig)
			            : delimiter(orig.delimiter),
			              underlying(orig.underlying),
			              currPos(orig.currPos) {
	            }

	            string_split_iterator& operator=(string_split_iterator const& orig) {
		            delimiter = orig.delimiter;
		            underlying = orig.underlying;
		            currPos = orig.currPos;
		            return *this;
	            }

	            string_split_iterator(string_split_iterator&& orig)
			            : delimiter(std::move(orig.delimiter)),
			              underlying(std::move(orig.underlying)),
			              currPos(exchange(orig.currPos, std::string::npos)) {
	            }

	            string_split_iterator& operator=(string_split_iterator&& orig) {
		            delimiter = std::move(orig.delimiter);
		            underlying = std::move(orig.underlying);
		            currPos = exchange(orig.currPos, std::string::npos);
		            return *this;
	            }

	            bool operator==(string_split_iterator const& other) const {
		            return currPos == other.currPos
				            && delimiter == other.delimiter
				            && underlying == other.underlying;
	            }

	            bool operator!=(string_split_iterator const& other) const {
		            return currPos != other.currPos
				            && delimiter != other.delimiter
				            && underlying != other.underlying;
	            }

	            std::string operator*() const {
		            assert(currPos != std::string::npos);

		            auto next = underlying.find(delimiter, currPos);
		            next = (next == std::string::npos ? next : next - currPos);
		            return underlying.substr(currPos, next);
	            }

	            string_split_iterator& operator++() { //prefix
		            auto nextDelim = underlying.find(delimiter, currPos);
		            if (nextDelim != std::string::npos) {
						currPos = nextDelim + delimiter.size();
					} else {
						currPos = std::string::npos;
					}
					return *this;
	            }

	            string_split_iterator operator++(int) { // suffix
		            auto self = *this;
		            operator++();
		            return self;
	            }

	            explicit operator bool() const {
		            return currPos != std::string::npos;
	            }
            };

            inline iterator_pair<string_split_iterator> split(std::string source, std::string delim) {
	            string_split_iterator begin(std::move(delim), std::move(source), 0);
	            string_split_iterator end;

	            return iterator_pair<string_split_iterator>({begin, end});
            }

            inline iterator_pair<string_split_iterator> split(std::string source, char delim) {
	            return split(std::move(source), std::string(&delim, 1));
            }
        }
    }
}

#endif//PPR_TL_SPLIT_STRING_HPP_INCLUDED
