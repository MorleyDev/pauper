#ifndef PPR_TL_ITERATOR_PAIR_HPP_INCLUDED
#define PPR_TL_ITERATOR_PAIR_HPP_INCLUDED


namespace ppr {
    namespace tl {
        template<typename T>
        struct iterator_pair {
	        const T begin;
	        const T end;
        };
    }
}

#endif//PPR_TL_ITERATOR_PAIR_HPP_INCLUDED
