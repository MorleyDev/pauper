#ifndef PPR_TL_ASYNC_FORWARD_HPP_INCLUDED
#define PPR_TL_ASYNC_FORWARD_HPP_INCLUDED

#include <memory>

namespace ppr {
	namespace tl {
		// Taken from, and all credit to, https://kholdstare.github.io/technical/2012/12/19/perfect-forwarding-to-async-2.html
#ifdef _MSC_VER
		template <typename T>
		class async_forwarder
		{
			// Store value directly
			std::shared_ptr<T> val_;

		public:
			/**
			* Move an rvalue of T into the wrapper,
			* incurring no copies.
			*/
			async_forwarder(T&& t) : val_(std::make_shared<T>(std::move(t))) { }

			// Move the value out.
			// Note: can only occur once!
			operator T& ()       { return *val_; }
			operator T& () const { return *val_; }
		};
#else
	    template <typename T>
	    class async_forwarder
	    {
		    // Store value directly
		    T val_;
        
	    public:
		    /**
		    * Move an rvalue of T into the wrapper,
		    * incurring no copies.
		    */
		    async_forwarder(T&& t) : val_(std::move(t)) { }
        
		    // ensure no copies are made
		    async_forwarder(async_forwarder const& other) = delete;
		    async_forwarder& operator=(async_forwarder const& other) = delete;
        
		    // move constructor
		    async_forwarder(async_forwarder&& other)
				    : val_(std::move(other.val_)) { }
		    async_forwarder& operator=(async_forwarder&& other) {
				val_ = std::move(other.val_);
				return *this;
			}
        
		    // Move the value out.
		    // Note: can only occur once!
		    operator T&& ()       { return std::move(val_); }
		    operator T&& () const { return std::move(val_); }
	    };
#endif

	    template<typename T> async_forwarder<T> async_forward(T& t) {
		    return async_forwarder<T>(std::move(t));
	    }
	}
}

#endif//
