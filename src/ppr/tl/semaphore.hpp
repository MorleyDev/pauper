#ifndef PPR_TL_SEMAPHORE_HPP_INCLUDED
#define PPR_TL_SEMAPHORE_HPP_INCLUDED

#include <mutex>
#include <condition_variable>
#include <atomic>

namespace ppr {
	namespace tl {
		class semaphore {
		private:
			std::atomic<std::size_t> m_counter;
			std::condition_variable_any m_conditionalVariable;
			std::mutex m_mutex;

		public:
			explicit semaphore(std::size_t startingCount = 0)
					: m_counter(startingCount),
					  m_conditionalVariable(),
					  m_mutex() {
			}

			~semaphore() { }

			bool tryDecrementBy(std::size_t count) {
				std::unique_lock<std::mutex> lock(m_mutex);
				if (m_counter.load() < count) {
					return false;
				}

				m_counter -= count;
				return true;
			}

			void incrementBy(std::size_t count) {
				std::unique_lock<std::mutex> lock(m_mutex);
				m_counter += count;
				m_conditionalVariable.notify_all();
			}

			void decrementBy(std::size_t count) {
                std::unique_lock<std::mutex> lock(m_mutex);

                m_conditionalVariable.wait(lock, [this, count]() { return m_counter.load() >= count; });
                m_counter -= count;
            }

			semaphore(semaphore const&) = delete;
			semaphore& operator=(semaphore const&) = delete;

			semaphore(semaphore&&) = delete;
			semaphore& operator=(semaphore&&) = delete;
		};
	}
}

#endif//PPR_TL_SEMAPHORE_HPP_INCLUDED
