#ifndef PPR_TL_OPTIONAL_HPP_INCLUDED
#define PPR_TL_OPTIONAL_HPP_INCLUDED

#include "exchange.hpp"

#include <new>
#include <array>
#include <cstdint>
#include <utility>

namespace ppr {
	namespace tl {
		namespace _detail {
		    struct none_t { };
		}

	    template<typename T> class optional {
	    private:
		    bool m_isInitialised;
		    std::array<std::uint8_t, sizeof(T)> m_internalData;

	    public:
		    optional() : m_isInitialised(false), m_internalData() { }

		    optional(_detail::none_t) : m_isInitialised(false), m_internalData() { }

		    optional(T val) : m_isInitialised(true), m_internalData() {
			    construct(std::forward<T>(val));
		    }

		    optional(optional<T>&& other)
				    : m_isInitialised(exchange(other.m_isInitialised, false)),
				      m_internalData(std::move(other.m_internalData)) {
		    }

		    optional& operator=(optional<T>&& other) {
			    if (m_isInitialised)
				    destroy();

			    m_isInitialised = exchange(other.m_isInitialised, false);
			    m_internalData = std::move(other.m_internalData);
			    return *this;
		    }

		    optional(const optional<T>& other)
				    : m_isInitialised(other.m_isInitialised), m_internalData() {
			    if (m_isInitialised)
				    construct(*other);
		    }

		    optional<T>& operator=(const optional<T>& other) {
			    destroy();
			    m_isInitialised = other.m_isInitialised;
			    if (m_isInitialised) {
					construct(*other);
				}
			    return *this;
		    }

		    ~optional() {
			    destroy();
		    }

		    explicit operator bool() const {
			    return m_isInitialised;
		    }

		    T& operator*() {
			    return *unsafeGet();
		    }

		    T const& operator*() const {
			    return *unsafeGet();
		    }

		    T* operator->() {
			    return unsafeGet();
		    }

		    const T* operator->() const {
			    return unsafeGet();
		    }

		    T* get() {
			    return m_isInitialised
					    ? unsafeGet()
					    : nullptr;
		    }


		    const T* get() const {
			    return m_isInitialised
					    ? unsafeGet()
					    : nullptr;
		    }

	    private:

		    T* unsafeGet() {
			    return static_cast<T*>(static_cast<void*>(m_internalData.data()));
		    }


		    const T* unsafeGet() const {
				return static_cast<const T*>(static_cast<const void*>(m_internalData.data()));
		    }


		    void destroy() {
			    if (m_isInitialised) {
					operator*().~T();
				}
			    m_isInitialised = false;
		    }

		    void construct(T data) {
			    new (m_internalData.data()) T(std::forward<T>(data));
		    }
	    };

	    inline _detail::none_t none() { return _detail::none_t(); }

	    template<typename T> inline optional<T> some(T data) {
		    return optional<T>(std::move(data));
	    }

	}
}
#endif//PPR_TL_OPTIONAL_HPP_INCLUDED
