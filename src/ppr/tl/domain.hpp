#ifndef PAUPER_TL_DOMAIN_HPP_INCLUDED
#define PAUPER_TL_DOMAIN_HPP_INCLUDED

#include <utility>
#include <limits>
#include <cassert>
#include <stdexcept>
#include <algorithm>

namespace ppr {
    namespace tl {
        template<typename T, T (*_min)(), T (*_max)()>
        class domain {
        private:
	        T value;

	        T _validate_domain(T v) const {
		        return v >= min() && v <= max()
				        ? v
				        : throw std::out_of_range("Value must lie within valid domain range");
	        }

        public:
	        static T max() { return _max(); }
	        static T min() { return _min(); }

			static T constrain(T const& value) { return std::min(max(), std::max(min(), value)); }

	        T get() const {
		        return value;
	        }

	        domain() : value(min()) {
	        }

	        domain(T v)
			        : value(_validate_domain(v)) {
	        }

	        domain(const domain& other)
			        : value(other.value) {
	        }

	        domain(domain&& other)
			        : value(std::move(other.value)) {
	        }

	        domain& operator=(const domain& other) {
		        value = other.value;
		        return *this;
	        }

	        domain& operator=(domain&& other) {
		        value = std::move(other.value);
		        return *this;
	        }

	        domain& operator=(T v) {
		        value = _validate_domain(v);
		        return *this;
	        }

	        domain& operator+=(T v) { value = _validate_domain(value + v); return *this; }
	        domain& operator-=(T v) { value = _validate_domain(value - v); return *this; }
	        domain& operator*=(T v) { value = _validate_domain(value * v); return *this; }
	        domain& operator/=(T v) { value = _validate_domain(value / v); return *this; }
        };

        template <typename T, T (*_min)(), T (*_max)()> T operator+(domain<T,_min,_max> v, T a) { return v.get() + a; }
        template <typename T, T (*_min)(), T (*_max)()> T operator-(domain<T,_min,_max> v, T a) { return v.get() - a; }
        template <typename T, T (*_min)(), T (*_max)()> T operator*(domain<T,_min,_max> v, T a) { return v.get() * a; }
        template <typename T, T (*_min)(), T (*_max)()> T operator/(domain<T,_min,_max> v, T a) { return v.get() / a; }

        template <typename T, T (*_min)(), T (*_max)()> T operator+(domain<T,_min,_max> v, domain<T,_min,_max> a) { return v.get() + a.get(); }
        template <typename T, T (*_min)(), T (*_max)()> T operator-(domain<T,_min,_max> v, domain<T,_min,_max> a) { return v.get() - a.get(); }
        template <typename T, T (*_min)(), T (*_max)()> T operator*(domain<T,_min,_max> v, domain<T,_min,_max> a) { return v.get() * a.get(); }
        template <typename T, T (*_min)(), T (*_max)()> T operator/(domain<T,_min,_max> v, domain<T,_min,_max> a) { return v.get() / a.get(); }

        template <typename T, T (*_min)(), T (*_max)()> T operator+(T a, domain<T,_min,_max> v) { return a + v.get(); }
        template <typename T, T (*_min)(), T (*_max)()> T operator-(T a, domain<T,_min,_max> v) { return a - v.get(); }
        template <typename T, T (*_min)(), T (*_max)()> T operator*(T a, domain<T,_min,_max> v) { return a * v.get(); }
        template <typename T, T (*_min)(), T (*_max)()> T operator/(T a, domain<T,_min,_max> v) { return a / v.get(); }

        template <typename T, T (*_min)(), T (*_max)()> T& operator+=(T& a, domain<T,_min,_max> v) { return a += v.get(); }
        template <typename T, T (*_min)(), T (*_max)()> T& operator-=(T& a, domain<T,_min,_max> v) { return a -= v.get(); }
        template <typename T, T (*_min)(), T (*_max)()> T& operator*=(T& a, domain<T,_min,_max> v) { return a *= v.get(); }
        template <typename T, T (*_min)(), T (*_max)()> T& operator/=(T& a, domain<T,_min,_max> v) { return a /= v.get(); }

        template <typename T, T (*_min)(), T (*_max)()> bool operator> (domain<T,_min,_max> v, T other) { return v.get() > other; }
        template <typename T, T (*_min)(), T (*_max)()> bool operator< (domain<T,_min,_max> v, T other) { return v.get() < other; }
        template <typename T, T (*_min)(), T (*_max)()> bool operator>=(domain<T,_min,_max> v, T other) { return v.get() >= other; }
        template <typename T, T (*_min)(), T (*_max)()> bool operator<=(domain<T,_min,_max> v, T other) { return v.get() <= other; }
        template <typename T, T (*_min)(), T (*_max)()> bool operator==(domain<T,_min,_max> v, T other) { return v.get() == other; }
        template <typename T, T (*_min)(), T (*_max)()> bool operator!=(domain<T,_min,_max> v, T other) { return v.get() != other; }

        template <typename T, T (*_min)(), T (*_max)()> bool operator> (T other, domain<T,_min,_max> v) { return other >  v.get(); }
        template <typename T, T (*_min)(), T (*_max)()> bool operator< (T other, domain<T,_min,_max> v) { return other <  v.get(); }
        template <typename T, T (*_min)(), T (*_max)()> bool operator>=(T other, domain<T,_min,_max> v) { return other >= v.get(); }
        template <typename T, T (*_min)(), T (*_max)()> bool operator<=(T other, domain<T,_min,_max> v) { return other <= v.get(); }
        template <typename T, T (*_min)(), T (*_max)()> bool operator==(T other, domain<T,_min,_max> v) { return other == v.get(); }
        template <typename T, T (*_min)(), T (*_max)()> bool operator!=(T other, domain<T,_min,_max> v) { return other != v.get(); }


        template <typename T, T (*_min)(), T (*_max)()> bool operator> (domain<T,_min,_max> a, domain<T,_min,_max> b) { return a.get() > b.get(); }
        template <typename T, T (*_min)(), T (*_max)()> bool operator< (domain<T,_min,_max> a, domain<T,_min,_max> b) { return a.get() < b.get(); }
        template <typename T, T (*_min)(), T (*_max)()> bool operator>=(domain<T,_min,_max> a, domain<T,_min,_max> b) { return a.get() >= b.get(); }
        template <typename T, T (*_min)(), T (*_max)()> bool operator<=(domain<T,_min,_max> a, domain<T,_min,_max> b) { return a.get() <= b.get(); }
        template <typename T, T (*_min)(), T (*_max)()> bool operator==(domain<T,_min,_max> a, domain<T,_min,_max> b) { return a.get() == b.get(); }
        template <typename T, T (*_min)(), T (*_max)()> bool operator!=(domain<T,_min,_max> a, domain<T,_min,_max> b) { return a.get() != b.get(); }
    }
}

#endif// PAUPER_TL_DOMAIN_HPP_INCLUDED
