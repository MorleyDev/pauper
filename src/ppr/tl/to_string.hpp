#ifndef PPR_TL_TO_STRING_HPP_INCLUDED
#define PPR_TL_TO_STRING_HPP_INCLUDED

#include <sstream>
#include <string>

namespace ppr {
	namespace tl {
		namespace _detail {
			template<typename T> struct string_conversion {
				std::string operator()(T const& v) {
					std::stringstream str;
					str << v;
					return str.str();
				}
			};
		}

		template<typename T> std::string to_string(T const& v) {
			return _detail::string_conversion<T>()(v);
		}
	}
}

#endif//PPR_TL_TO_STRING_HPP_INCLUDED
