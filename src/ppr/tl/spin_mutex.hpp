#ifndef PAUPER_TL_SPIN_MUTEX_HPP_INCLUDED
#define PAUPER_TL_SPIN_MUTEX_HPP_INCLUDED

#include <atomic>

namespace ppr {
	namespace tl {
	    class spin_mutex {
	    private:
		    std::atomic_flag m_lock;

	    public:
		    spin_mutex() : m_lock() {
			    m_lock.clear();
		    }

		    spin_mutex(const spin_mutex&) = delete;

		    spin_mutex& operator=(const spin_mutex&) = delete;

		    ~spin_mutex() {
		    }

		    bool try_lock() {
			    return !m_lock.test_and_set(std::memory_order_acquire);
		    }

		    void lock() {
			    while (!try_lock());
		    }

		    void unlock() {
			    m_lock.clear(std::memory_order_release);
		    }
	    };
	}
}

#endif//PAUPER_TL_SPIN_MUTEX_HPP_INCLUDED
