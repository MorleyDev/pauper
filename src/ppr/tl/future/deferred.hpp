#ifndef PPR_TL_FUTURE_DEFERRED_HPP_INCLUDED
#define PPR_TL_FUTURE_DEFERRED_HPP_INCLUDED

#include "../async_forward.hpp"
#include <future>
#include <memory>

namespace ppr {
	namespace tl {
		namespace future {
			namespace detail {
				template<typename R, typename CALLABLE>
				std::future<R> deferred(CALLABLE c) {
					return std::async(std::launch::deferred, c);
				}

				template<typename R, typename CALLABLE, typename T1>
				std::future<R> deferred(CALLABLE c, T1 t1) {
#ifdef _MSC_VER
					return std::async(std::launch::deferred,
						[c](std::shared_ptr<T1> data) { return c(std::move(*data)); },
						std::make_shared<T1>(std::move(t1)));
#else //_MSC_VER
					return std::async(std::launch::deferred, c, tl::async_forward(t1));
#endif// _MSC_VER
				}

				template<typename R, typename CALLABLE, typename T1, typename T2>
				std::future<R> deferred(CALLABLE c, T1 t1, T2 t2) {
#ifdef _MSC_VER
					return std::async(std::launch::deferred,
						[c](std::shared_ptr<std::tuple<T1, T2>> p) {
							return c(std::move(std::get<0>(*p)), std::move(std::get<1>(*p)));
						}, std::make_shared<std::tuple<T1, T2>>(std::move(t1), std::move(t2)));
#else //_MSC_VER
					return std::async(std::launch::deferred, c, tl::async_forward(t1), tl::async_forward(t2));
#endif// _MSC_VER
				}

				template<typename R, typename CALLABLE, typename T1, typename T2, typename T3>
				std::future<R> deferred(CALLABLE c, T1 t1, T2 t2, T3 t3) {
#ifdef _MSC_VER
					return std::async(std::launch::deferred,
						[c](std::shared_ptr<std::tuple<T1, T2, T3>> p) {
							return c(std::move(std::get<0>(*p)), std::move(std::get<1>(*p)), std::move(std::get<2>(*p)));
						}, std::make_shared<std::tuple<T1, T2, T3>>(std::move(t1), std::move(t2), std::move(t3)));
#else //_MSC_VER
					return std::async(std::launch::deferred,
							c,
							tl::async_forward(t1),
							tl::async_forward(t2),
							tl::async_forward(t3));
#endif// _MSC_VER
				}
			}

			template<typename CALLABLE, typename... ARGS>
			auto deferred(CALLABLE c, ARGS&&... r) -> std::future<decltype(c(std::forward<ARGS>(r)...))> {
				return detail::deferred<decltype(c(std::forward<ARGS>(r)...))>(c, std::forward<ARGS>(r)...);
			}
		}
	}
}

#endif//PPR_TL_FUTURE_DEFERRED_HPP_INCLUDED
