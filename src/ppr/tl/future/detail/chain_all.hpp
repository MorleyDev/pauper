#ifndef PPR_TL_FUTURE_DETAIL_CHAIN_ALL_HPP_INCLUDED
#define PPR_TL_FUTURE_DETAIL_CHAIN_ALL_HPP_INCLUDED

#include <memory>
#include <future>

namespace ppr {
	namespace tl {
		namespace future {
			namespace detail {
				template<typename A, typename B, typename Interim = decltype(std::declval<A>()())>
				struct call_a_pass_to_b {
					void operator()(A& a, B& b) {
						auto result = a();
						b(std::move(result));
					}
				};
				template<typename A, typename B>
				struct call_a_pass_to_b<A,B,void> {
					void operator()(A& a, B& b) {
						a();
						b();
					}
				};


				template<typename A, typename B, typename C, typename Interim = decltype(std::declval<A>()(std::declval<C>()))>
				struct call_a_with_c_pass_to_b {
					void operator()(A& a, B& b, C c) {
						auto result = a(std::forward<C>(c));
						b(std::move(result));
					}
				};
				template<typename A, typename B, typename C>
				struct call_a_with_c_pass_to_b<A,B,C, void> {
					void operator()(A& a, B& b, C c) {
						a(std::forward<C>(c));
						b();
					}
				};

				template<typename T> struct FinalCallback {
					std::shared_ptr<std::promise<T>> promise;
					FinalCallback(std::promise<T> promise) : promise(std::make_shared<std::promise<T>>(std::move(promise))) {
					}

					void operator()(T value) {
						promise->set_value(value);
					}
				};
				template<> struct FinalCallback<void> {
					std::shared_ptr<std::promise<void>> promise;
					FinalCallback(std::promise<void> promise) : promise(std::make_shared<std::promise<void>>(std::move(promise))) {
					}

					void operator()() {
						promise->set_value();
					}
				};
				template<typename C, typename B> struct _Callback {
					C _callback;
					B _op;

					_Callback(C callback, B op) : _callback(std::move(callback)), _op(std::move(op)) {

					}

					template<typename V> void operator()(V value) {
						call_a_with_c_pass_to_b<B,C,V>()(_op, _callback, value);
					}

					void operator()() {
						call_a_pass_to_b<B,C>()(_op, _callback);
					}
				};

				template<typename A, typename B> struct result_of_A_with_B {
					typedef decltype(std::declval<A>()(std::declval<B>()))  type;
				};
				template<typename A> struct result_of_A_with_B<A,void> {
					typedef decltype(std::declval<A>()())  type;
				};

				template <typename B, typename A = void>
				struct future_bind_t {
					B _op;
					A _source;

					typedef typename A::return_type parent_return_type;
					typedef typename result_of_A_with_B<B,parent_return_type>::type return_type;

					future_bind_t(B op, A source) : _op(std::move(op)), _source(std::move(source)) {
					}

					template<typename R, typename C> void operator()(R runner, C callback) {
						runner([runner](C callback, B _op, A _source) {
							_Callback<C,B> callbackWrapper(std::move(callback), std::move(_op));

							_source(runner, std::move(callbackWrapper));
						}, std::move(callback), std::move(_op), std::move(_source));
					}
				};

				template <typename B>
				struct future_bind_t<B,void> {
					typedef decltype(std::declval<B>()()) return_type;

					B _op;

					future_bind_t(B op) : _op(std::move(op)) {
					}

					template<typename R, typename C> void operator()(R runner, C callback) {
						call_a_pass_to_b<B,C>()(_op, callback);
					}
				};

				struct default_runner_t {
					template <typename T, typename... ARGS> void operator()(T runnable, ARGS&&... args) {
						runnable(std::forward<ARGS>(args)...);
					}
				};

				struct async_runner_t {
					template <typename T, typename... ARGS> void operator()(T runnable, ARGS&&... args) {
						std::async(std::launch::async, runnable, std::forward<ARGS>(args)...);
					}
				};

				template<typename P> struct pool_runner_t {
					P* m_pool;
					pool_runner_t(P* pool) : m_pool(pool) { }

					template <typename T, typename... ARGS> void operator()(T runnable, ARGS&&... args) {
						m_pool->enqueue(runnable, std::forward<ARGS>(args)...);
					}
				};

				template<typename A, typename R, typename POOL> struct bind_to_thread_pool_t {
					POOL* _pool;
					A _op;
					FinalCallback<R> _callback;

					bind_to_thread_pool_t(POOL& pool, A op, FinalCallback<R> callback)
							: _pool(&pool), _op(std::move(op)), _callback(std::move(callback)) {
					}

					void operator()() {
						_op(pool_runner_t<POOL>(_pool), std::move(_callback));
					}
				};
			}
		}
	}
}

#endif//PPR_TL_FUTURE_DETAIL_CHAIN_ALL_HPP_INCLUDED
