#ifndef PPR_TL_FUTURE_VALUE_HPP_INCLUDED
#define PPR_TL_FUTURE_VALUE_HPP_INCLUDED

namespace ppr {
	namespace tl {
		namespace future {
			template<typename T> std::future<T> value(T value) {
				std::promise<T> promise;
				promise.set_value(value);
				return promise.get_future();
			};
			inline std::future<void> value() {
				std::promise<void> promise;
				promise.set_value();
				return promise.get_future();
			};
		}
	}
}

#endif//PPR_TL_FUTURE_VALUE_HPP_INCLUDED
