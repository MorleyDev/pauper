#ifndef PPR_TL_FUTURE_CHAIN_HPP_INCLUDED
#define PPR_TL_FUTURE_CHAIN_HPP_INCLUDED

#include "detail/chain_all.hpp"

namespace ppr {
	namespace tl {
		namespace future {
			namespace detail {
				template<typename A>
				struct future_chain_t {
				private:
					A operation;

				public:
					typedef typename A::return_type return_type;

					future_chain_t(A op)
							: operation(std::forward<A>(op)) {
					}

					template<typename B>
					auto map(B op) {
						return future_chain_t<future_bind_t<B,A>>(
								future_bind_t<B,A>(std::move(op), std::move(operation))
						);
					}

					std::future<return_type> to_deferred() {
						return std::async(std::launch::deferred, [](A operation) {
							std::promise<return_type> promise;
							auto future = promise.get_future();

							FinalCallback<return_type> callback(std::move(promise));
							operation(default_runner_t(), std::move(callback));
							return future.get();
						}, std::move(operation));
					}

					std::future<return_type> to_async() {
						std::promise<return_type> promise;
						auto future = promise.get_future();

						FinalCallback<return_type> callback(std::move(promise));
						std::async(std::launch::async,std::move(operation), default_runner_t(), std::move(callback));
						return future;
					}

					return_type to_value() {
						return to_deferred().get();
					}

					template<typename POOL> std::future<return_type> to_thread_pool(POOL& p) {
#ifdef PPR_SINGLE_THREADED
						return to_deferred();
#else//!PPR_SINGLE_THREADED
						std::promise<return_type> promise;
						auto future = promise.get_future();

						FinalCallback<return_type> callback(std::move(promise));
						p.enqueue(bind_to_thread_pool_t<A,return_type,POOL>(p, std::move(operation), std::move(callback)));
						return future;
#endif//PPR_SINGLE_THREADED
					}
				};
			}

			template<typename A> auto chain(A o) {
				return detail::future_chain_t<detail::future_bind_t<A>>( detail::future_bind_t<A>(std::move(o)) );
			}
		}
	}
}

#endif//PPR_TL_FUTURE_CHAIN_HPP_INCLUDED
