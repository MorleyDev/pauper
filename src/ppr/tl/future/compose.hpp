#ifndef PPR_TL_FUTURE_COMPOSE_HPP_INCLUDED
#define PPR_TL_FUTURE_COMPOSE_HPP_INCLUDED

#include "../async_forward.hpp"
#include <future>
#include <memory>

namespace ppr {
	namespace tl {
		namespace future {
			template<typename L> std::future<void> compose(L futureList) {
#ifdef _MSC_VER
			    return std::async(std::launch::deferred, [](std::shared_ptr<L> futures) {
			        for(auto& future : *futures)
				        future.wait();
			    }, std::make_shared<L>(std::move(futureList)));
#else //!_MSC_VER
				return std::async(std::launch::deferred, [](L futures) {
					for (auto& future : futures) {
						future.wait();
					}
				}, async_forward(futureList));
#endif//_MSC_VER
			}
		}
	}
}

#endif//PPR_TL_FUTURE_COMPOSE_HPP_INCLUDED
