#ifndef PAUPER_TL_SIGNALS_HPP_INCLUDED
#define PAUPER_TL_SIGNALS_HPP_INCLUDED

#include "immutable_list.hpp"
#include "spin_mutex.hpp"
#include <functional>
#include <algorithm>
#include <mutex>

namespace ppr {
    namespace tl {
        template<typename... ARGS>
        class signals {
        private:
	        ppr::tl::immutable_list<std::function<void (ARGS const& ...)>> m_handlers;

	        spin_mutex m_writeLock;

        public:
	        void listen(std::function<void (const ARGS& ...)> h) {
		        std::lock_guard<spin_mutex> lock(m_writeLock);
		        m_handlers = ppr::tl::immutable_list<std::function<void (ARGS const& ...)>>(h, m_handlers);
	        }

	        void signal(ARGS const& ... args) {
		        auto handlers = [&]() {
		            std::lock_guard<spin_mutex> lock(m_writeLock);
			        return m_handlers;
		        }();
		        for(auto i : handlers) {
					i(args...);
				}
	        }
        };

        template<>
        class signals<void> {
        private:
	        ppr::tl::immutable_list<std::function<void (void)>> m_handlers;

	        spin_mutex m_writeLock;

        public:
	        void listen(std::function<void (void)> h) {
		        std::lock_guard<spin_mutex> lock(m_writeLock);
		        m_handlers = ppr::tl::immutable_list<std::function<void (void)>>(h, m_handlers);
	        }

	        void signal() {
		        auto handlers = [&]() {
		            std::lock_guard<spin_mutex> lock(m_writeLock);
		            return m_handlers;
		        }();

		        for(auto i : handlers) {
					i();
				}
	        }
        };
    }
}

#endif//PAUPER_TL_SIGNALS_HPP_INCLUDED
