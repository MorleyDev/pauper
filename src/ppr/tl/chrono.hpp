#ifndef PAUPER_TL_CHRONO_HPP_INCLUDED
#define PAUPER_TL_CHRONO_HPP_INCLUDED

#include <chrono>

namespace ppr {
	namespace tl {
		namespace chrono {
			typedef std::chrono::system_clock clock;

			inline decltype(clock::now()) now() {
				return clock::now();
			}

			template<typename _Rep, typename _Period>
			inline std::chrono::duration<double> to_seconds(const std::chrono::duration<_Rep, _Period>& duration) {
				return std::chrono::duration_cast<std::chrono::duration<double>>(duration);
			}
		}
	}
}

#endif//PAUPER_TL_CHRONO_HPP_INCLUDED
