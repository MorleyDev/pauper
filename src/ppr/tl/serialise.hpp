#ifndef PPR_TL_SERIALISE_HPP_INCLUDED
#define PPR_TL_SERIALISE_HPP_INCLUDED

#include <cereal/cereal.hpp>
#include <cereal/archives/json.hpp>
#include <string>
#include <sstream>

namespace ppr {
    namespace tl {
        template<typename T>
        inline auto make_nvp(char const* name, T&& value) -> decltype(cereal::make_nvp(name,value)) {
	        return cereal::make_nvp(name,value);
        }

	    template<typename T> std::string to_json(T value) {
		    std::stringstream output;
		    {
			    cereal::JSONOutputArchive archive(output);
			    serialize(archive, value);
		    }
		    return output.str();
	    }

        template<typename T> T from_json(std::string value) {
	        T tmp;
	        {
		        std::stringstream input;
		        input << value;
		        cereal::JSONInputArchive archive(input);
		        serialize(archive, tmp);
	        }
	        return tmp;
        }
    }
}

#endif//PPR_TL_SERIALISE_HPP_INCLUDED
