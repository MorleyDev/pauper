#ifndef PAUPER_TL_THREAD_POOL_HPP_INCLUDED
#define PAUPER_TL_THREAD_POOL_HPP_INCLUDED

#include "../config.hpp"
#include "future.hpp"
#include <functional>
#include <vector>
#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <future>

namespace ppr {
	namespace tl {
#ifdef PPR_SINGLE_THREADED
		class thread_pool {
		public:
			explicit thread_pool(std::size_t size = 0) {
			}

			void growBy(std::size_t size) { }

			template<class Func, class... Args>
			std::future<typename std::result_of<Func(Args...)>::type> enqueue(Func&& f, Args&&... args) {
				typedef typename std::result_of<Func(Args...)>::type return_type;

				return future::deferred(std::forward<Func>(f), std::forward<Args>(args)...);
			}
		};

#else // !PPR_SINGLE_THREADED
		class thread_pool {
		private:
			static std::function<void (void)> _ThreadPullWork(thread_pool* self) {
				std::unique_lock<std::mutex> lock(self->m_mutex);
				while (!self->m_stop.load() && self->m_workQueue.empty()) {
					self->m_condition.wait(lock);
				}

				if (self->m_workQueue.empty()) {
					return std::function<void(void)>();
				}

				auto work = self->m_workQueue.front();
				self->m_workQueue.pop();
				return work;
			}

			static void _ThreadLoop(thread_pool* self) {
				while(!self->m_stop.load()) {
					std::function<void(void)> work;
					do {
						work = _ThreadPullWork(self);
						if (work) work();
					} while (work);
				}
			}

			template<typename R> struct _Work {
			private:
				std::shared_ptr<std::packaged_task<R ()>> task;

			public:
				_Work(_Work<R> const& work)
						: task(work.task) {
				}

				_Work& operator=(_Work<R> const& work) {
					task = work.task;
					return *this;
				}

				_Work(std::packaged_task<R ()> task)
						: task(std::make_shared<std::packaged_task<R ()>>(std::move(task))) {
				}

                _Work(std::shared_ptr<std::packaged_task<R ()>> task)
                        : task(std::move(task)) {
                }

				_Work(_Work<R>&& work)
						: task(std::move(work.task)) {
				}

				_Work& operator=(_Work<R>&& work) {
					task = std::move(work.task);
					return *this;
				}

				void operator()() {
					(*task)();
				}
			};

			template<typename R> void _pushWork(_Work<R> work) {

				if(m_stop.load()) { // Should be nearly impossible, since a stop only happens when the destructor is running.
					work(); // And if a destructor is running whilst thread_pool is shared,
					return;// you have bigger problems. May occur if a task queues another task though
				} // so in that scenario, run the task immediately. You should NOT have any endless tasks.

				std::lock_guard<std::mutex> lock(m_mutex);
				std::function<void ()> f(std::move(work));
				m_workQueue.emplace(std::move(f));
				m_condition.notify_one();
			}

			std::atomic<bool> m_stop;
			std::mutex m_mutex;
			std::condition_variable m_condition;

			std::queue< std::function<void (void)> > m_workQueue;
			std::vector< std::thread > m_workers;

		public:
			explicit thread_pool(std::size_t size = std::thread::hardware_concurrency())
					: m_stop(false),
					  m_mutex(),
					  m_condition(),
					  m_workQueue(),
					  m_workers() {
				growBy(size);
			}

			void growBy(std::size_t size) {
				std::lock_guard<std::mutex> lock(m_mutex);
				m_workers.reserve(m_workers.size() + size);
				for(size_t i = 0u; i< size; ++i)
					m_workers.emplace_back([this]() { _ThreadLoop(this); });
			}

			~thread_pool() {
				m_stop.store(true);
				m_condition.notify_all();
				for(size_t i = 0; i < m_workers.size(); ++i) {
					m_workers[i].join();
				}

				std::lock_guard<std::mutex> lock(m_mutex);
				while(!m_workQueue.empty()) {
					m_workQueue.front()();
					m_workQueue.pop();
				}
			}

			template<class Func, class... Args>
			std::future<typename std::result_of<Func(Args...)>::type> enqueue(Func&& f, Args&&... args) {
				typedef typename std::result_of<Func(Args...)>::type return_type;
				auto task = std::make_shared<std::packaged_task<return_type ()>>(std::bind(std::forward<Func>(f), std::forward<Args>(args)...));

				auto future = task->get_future();
				_pushWork(_Work<return_type>(std::move(task)));
				return std::move(future);
			}
		};
#endif//PPR_SINGLE_THREADED
	}
}

#endif//PAUPER_TL_THREAD_POOL_HPP_INCLUDED
