#ifndef PPR_TL_FUTURE_HPP_INCLUDED
#define PPR_TL_FUTURE_HPP_INCLUDED

#include "future/deferred.hpp"
#include "future/compose.hpp"
#include "future/chain.hpp"
#include "future/value.hpp"

#endif//PPR_TL_FUTURE_HPP_INCLUDED
