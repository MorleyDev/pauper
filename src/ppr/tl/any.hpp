#ifndef PAUPER_TL_ANY_HPP_INCLUDED
#define PAUPER_TL_ANY_HPP_INCLUDED

#include "make_unique.hpp"
#include "exchange.hpp"

#include <memory>
#include <typeinfo>
#include <cstdint>
#include <cassert>
#include <utility>
#include <iostream>

namespace ppr {
	namespace tl {
		class any {
		private:
			std::unique_ptr<std::uint8_t[]> m_data;
			std::type_info const* m_type;
			std::unique_ptr<std::uint8_t[]> (*m_copy)(std::unique_ptr<std::uint8_t[]> const&);
			void (*m_destructor)(std::unique_ptr<std::uint8_t[]>);

			template<typename T>
			static std::unique_ptr<std::uint8_t[]> construct(T data) {
				typedef typename std::remove_reference<T>::type value_type;

				auto ptr = make_unique<std::uint8_t[]>(sizeof(value_type));
				new (ptr.get()) value_type(std::forward<T>(data));
				return ptr;
			}
			template<typename T>
			static std::unique_ptr<std::uint8_t[]> copy(std::unique_ptr<std::uint8_t[]> const& data) {
				typedef typename std::remove_reference<T>::type value_type;

				return construct(*(static_cast<T const*>(static_cast<void const*>(data.get()))));
			}
			template<typename T>
			static void destroy(std::unique_ptr<std::uint8_t[]> data) {
				typedef typename std::remove_reference<T>::type value_type;

				static_cast<value_type*>(static_cast<void*>(data.get()))->~value_type();
			}

		private:
			void _destroy() {
				if (m_destructor) {
					assert(m_data && m_destructor != nullptr);
					m_destructor(std::move(m_data));
				}
			}

			template<typename T> bool assert_is() const {
				auto pass = is<T>();
				if (!pass) {
					std::cout << "Expected: " << m_type->name() << " - Actual: " << typeid(T).name() << std::endl;
				}
				return pass;
			}

		public:
			any()
					: m_data(nullptr),
					  m_type(nullptr),
					  m_copy(nullptr),
					  m_destructor(nullptr) {
			}

			template<typename T> static any create(T data) {
				return any(std::forward<T>(data));
			}

			template<typename T> any(T data)
					: m_data(construct<typename std::remove_reference<T>::type>(data)),
					  m_type(&typeid(typename std::remove_reference<T>::type)),
					  m_copy(copy<typename std::remove_reference<T>::type>),
					  m_destructor(destroy<typename std::remove_reference<T>::type>) {
			}

			any(any const& a)
					: m_data(a.m_copy ? std::move(a.m_copy(a.m_data)) : nullptr),
					  m_type(a.m_type),
					  m_copy(a.m_copy),
					  m_destructor(a.m_destructor) {
			}

			any& operator=(any const& a) {

				_destroy();
				if (a.m_copy)
					m_data = std::move(a.m_copy(a.m_data));
				else
					m_data = std::unique_ptr<std::uint8_t[]>();

				m_type = a.m_type;
				m_copy = a.m_copy;
				m_destructor = a.m_destructor;
				return *this;
			}

			any(any&& a)
					: m_data(std::move(a.m_data)),
					  m_type(exchange(a.m_type, nullptr)),
					  m_copy(exchange(a.m_copy, nullptr)),
					  m_destructor(exchange(a.m_destructor, nullptr)) {
			}

			any& operator=(any&& a) {
				_destroy();

				m_data = std::move(a.m_data);
				m_type = exchange(a.m_type, nullptr);
				m_copy = exchange(a.m_copy, nullptr);
				m_destructor = exchange(a.m_destructor, nullptr);
				return *this;
			}

		    ~any() {
			    _destroy();
		    }

			explicit operator bool() const {
				return static_cast<bool>(m_data);
			}

			template<typename T> bool is() const {
				return m_data && m_type != nullptr && typeid(T) == *m_type;
			}

			template<typename T> T& as() {
				assert(assert_is<T>());
				return *(static_cast<T*>(static_cast<void*>(m_data.get())));
			}

			template<typename T> T const& as() const {
				assert(assert_is<T>());
				return *(static_cast<T const*>(static_cast<void const*>(m_data.get())));
			}
		};
	}
}

#endif//PAUPER_TL_ANY_HPP_INCLUDED
