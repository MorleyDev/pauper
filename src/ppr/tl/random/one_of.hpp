#ifndef PPR_TL_RANDOM_ONE_OF_HPP_INCLUDED
#define PPR_TL_RANDOM_ONE_OF_HPP_INCLUDED

#include <random>
#include <stdexcept>

namespace ppr {
	namespace tl {
		namespace random {
			namespace detail {
				template<typename T>
				T _get_nth_one_of(std::size_t n) {
					throw std::runtime_error("Should not be possible");
				}

				template<typename T, typename... ARGS>
				T _get_nth_one_of(std::size_t n, T&& value, ARGS&&... args) {
					return (n == 0)
							? value
							: _get_nth_one_of<T>(n-1, std::forward<ARGS>(args)...);
				}
			}

			template<typename Generator, typename T, typename... ARGS>
			T one_of(Generator& gen, T&& arg, ARGS&&... args) {
				std::uniform_int_distribution<std::size_t> distribution(0, sizeof...(ARGS));
				return detail::_get_nth_one_of(distribution(gen), std::forward<T>(arg), std::forward<ARGS>(args)...);
			}
		}
	}
}

#endif//PPR_TL_RANDOM_ONE_OF_HPP_INCLUDED
