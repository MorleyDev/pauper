#ifndef PAUPER_TL_IS_ITERATOR_HPP_INCLUDED
#define PAUPER_TL_IS_ITERATOR_HPP_INCLUDED

#include <iterator>

namespace ppr {
    namespace tl {
        template<typename T, typename = void>
        struct is_iterator {
            enum : bool { value = false };
        };

        template<typename T>
        struct is_iterator<T, typename std::enable_if<!std::is_same<typename std::iterator_traits<T>::value_type, void>::value>::type> {
	        enum : bool { value = true };
        };

        template<typename T, typename = void>
        struct is_forward_iterator {
            enum : bool { value = false };
        };

        template<typename T>
        struct is_forward_iterator<T, typename std::enable_if<std::is_same<typename std::iterator_traits<T>::iterator_category, std::forward_iterator_tag>::value>::type> {
            enum : bool { value = true };
        };

        template<typename T, typename = void>
        struct is_bidirectional_iterator {
            enum : bool { value = false };
        };

        template<typename T>
        struct is_bidirectional_iterator<T, typename std::enable_if<std::is_same<typename std::iterator_traits<T>::iterator_category, std::bidirectional_iterator_tag>::value>::type> {
            enum : bool { value = true };
        };
    }
}

#endif//PAUPER_TL_IS_ITERATOR_HPP_INCLUDED
