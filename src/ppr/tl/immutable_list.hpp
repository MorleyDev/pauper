#ifndef PAUPER_TL_IMMUTABLE_LIST_HPP_INCLUDED
#define PAUPER_TL_IMMUTABLE_LIST_HPP_INCLUDED

#include "is_iterator.hpp"
#include "exchange.hpp"
#include <memory>
#include <cassert>

namespace ppr {
	namespace tl {
		namespace _detail {

		    template<typename T, typename N> class _immutable_map_const_iterator {
		    private:
			    typedef T _value_type;
			    typedef N node;

			    std::shared_ptr<const node> m_node;

		    public:
			    typedef std::forward_iterator_tag iterator_category;
			    typedef _value_type value_type;
			    typedef std::ptrdiff_t difference_type;
			    typedef value_type const*   pointer;
			    typedef value_type const& reference;

			    _immutable_map_const_iterator() : m_node() { }

			    explicit _immutable_map_const_iterator(std::shared_ptr<const node> n) : m_node(n) { }
			    _immutable_map_const_iterator(const _immutable_map_const_iterator& orig) : m_node(orig.m_node) { }
			    _immutable_map_const_iterator(_immutable_map_const_iterator&& orig) : m_node(std::move(orig.m_node)) { }

			    _immutable_map_const_iterator& operator=(const _immutable_map_const_iterator& orig) {
				    m_node = orig.m_node;
				    return *this;
			    }
			    _immutable_map_const_iterator& operator=(_immutable_map_const_iterator&& orig) {
				    m_node = std::move(orig.m_node);
				    return *this;
			    }

			    reference operator*() const {
				    assert(m_node && m_node->data);
				    return *m_node->data;
			    }

			    pointer operator->() const {
				    assert(m_node && m_node->data);
				    return m_node->data.get();
			    }

			    _immutable_map_const_iterator& operator++() { // prefix
				    assert(m_node);
				    m_node = m_node->next;
				    return *this;
			    }

			    _immutable_map_const_iterator operator++(int) { // prefix
				    assert(m_node);
				    _immutable_map_const_iterator prev(m_node);
				    m_node = m_node->next;
				    return prev;
			    }

			    bool operator==(const _immutable_map_const_iterator& other) const { return m_node == other.m_node; }
			    bool operator!=(const _immutable_map_const_iterator& other) const { return m_node != other.m_node; }
		    };
		}

		template< typename T, typename ALLOC = std::allocator<T> >
		class immutable_list {
		public:
			typedef T value_type;
			typedef ALLOC allocator_type;
			typedef std::size_t size_type;
			typedef std::ptrdiff_t difference_type;
			typedef value_type const& const_reference;

		private:
			struct node {
				node(std::shared_ptr<const value_type> data, std::shared_ptr<const node> next = std::shared_ptr<const node>())
						: data(data), next(next) {
				}

				std::shared_ptr<const value_type> data;
				std::shared_ptr<const node> next;
			};
		public:
			typedef _detail::_immutable_map_const_iterator<value_type, node> const_iterator;

		private:
			allocator_type m_allocator;
			size_type m_size;
			std::shared_ptr<const node> m_head;

			std::shared_ptr<const node> allocate_node(value_type data,
			                                          std::shared_ptr<const node> next = std::shared_ptr<const node>()) {
				return std::allocate_shared<node>(m_allocator, std::allocate_shared<value_type>(m_allocator, data), next);
			}

			//template<typename I, typename J> std::shared_ptr<const node> _iterators_to_node(
			//		I curr,
			//		J end,
			//		std::size_t& count,
			//		typename std::enable_if<is_bidirectional_iterator<I>::value && is_bidirectional_iterator<J>::value>::type* = nullptr) {
			//	std::reverse_iterator<I> rend(curr);
			//	std::reverse_iterator<j> rcurr(end);
			//	std::shared_ptr<const node> tail;
			//	for(auto i = rcurr; i != rend; ++i) {
			//		++count;
			//		tail = allocate_node(*i, tail);
			//	}
			//	return tail;
			//}

			template<typename I, typename J> std::shared_ptr<const node> _iterators_to_node(I curr, J end,
                    std::size_t& count) { //,
					//typename std::enable_if<!is_bidirectional_iterator<I>::value || !is_bidirectional_iterator<J>::value>::type* = nullptr) {
				if (curr == end)
					return std::shared_ptr<const node>();
				auto val = *curr;
				return allocate_node(val, _iterators_to_node(++curr, end, ++count));
			}

			immutable_list(allocator_type const& alloc, size_type size, std::shared_ptr<const node> head)
					: m_allocator(alloc),
					  m_size(size),
					  m_head(head) {
			}

		public:
			immutable_list(std::initializer_list<value_type> list, allocator_type const& alloc = allocator_type())
					: m_allocator(alloc),
					  m_size(0),
			          m_head(_iterators_to_node(list.begin(), list.end(), m_size)) {
			}

			explicit immutable_list(allocator_type const& alloc = allocator_type())
					: m_allocator(alloc),
					  m_size(0),
					  m_head() {
			}
			immutable_list(immutable_list const& orig)
					: m_allocator(orig.m_allocator),
					  m_size(orig.m_size),
					  m_head(orig.m_head) {
			}

			immutable_list(immutable_list&& move)
					: m_allocator(std::move(move.m_allocator)),
					  m_size(exchange(move.m_size, 0)),
					  m_head(std::move(move.m_head)) {
			}

			immutable_list(T data, allocator_type const& alloc = allocator_type())
				: m_allocator(alloc),
				  m_size(1),
				  m_head(allocate_node(data)) {
			}

			immutable_list(T data, immutable_list tail)
					: m_allocator(tail.m_allocator),
					  m_size(1 + tail.m_size),
					  m_head(allocate_node(data, tail.m_head)) {
			}

            template<typename I, typename J> static immutable_list from(I i, J end, allocator_type const& alloc = allocator_type()) {
                std::size_t size = 0;
                auto head = immutable_list(alloc)._iterators_to_node(i, end, size);
                return immutable_list(alloc, size, head);
            }

            //template<typename I, typename J> immutable_list(I i, J end,
            //        allocator_type const& alloc = allocator_type(),
            //        typename std::enable_if< is_iterator<I>::value && is_iterator<J>::value>::type* = nullptr)
            //        : m_allocator(alloc),
            //          m_size(0),
            //          m_head(_iterators_to_node(i, end, m_size)) {
            //}

			immutable_list& operator=(immutable_list const& other) {
				m_allocator = other.m_allocator;
				m_head = other.m_head;
				m_size = other.m_size;
				return *this;
			}

			immutable_list& operator=(immutable_list&& other) {
				m_allocator = std::move(other.m_allocator);
				m_head = std::move(other.m_head);
				m_size = exchange(other.m_size, 0);
				return *this;
			}

			const_iterator begin() const { return const_iterator(m_head); }
			const_iterator end() const { return const_iterator(); }

			bool empty() const {
				return !static_cast<bool>(m_head);
			}

			size_type size() const {
				return m_size;
			}

			const_reference head() const {
				assert(m_head != nullptr);
				assert(m_head->data != nullptr);
				return *(m_head->data);
			}

			immutable_list tail() const {
				assert(m_head != nullptr);
				return immutable_list(m_allocator, m_size - 1, m_head->next);
			}

			bool _same_head_ptr(immutable_list const& a) const {
				return m_head == a.m_head;
			}
		};

	    template<typename T, typename U> inline bool operator==(immutable_list<T,U> const& a, immutable_list<T,U> const& b) {
		    if (a._same_head_ptr(b))
			    return true;
		    if (a.size() != b.size() || a.head() != b.head())
			    return false;
		    return operator==(a.tail(), b.tail());
	    }

	    template<typename T, typename U> inline bool operator!=(immutable_list<T,U> const& a, immutable_list<T,U> const& b) {
		    return !(operator==(a,b));
	    }
	}
}

#endif//PAUPER_TL_IMMUTABLE_LIST_