#ifndef PPR_UTIL_LOG_HPP_INCLUDED
#define PPR_UTIL_LOG_HPP_INCLUDED

#include "../extern.hpp"
#include "../tl/string/format.hpp"
#include "../tl/string/split.hpp"
#include <cstdint>
#include <atomic>

namespace ppr {
	namespace util {
		class Log {
		public:
			enum class Level : std::uint8_t {
				Trace = 0,
				Debug = 1,
				Info = 2,
				Warn = 3,
				Error = 4,
			};

		private:
			PPR_EXTERN static std::atomic<ppr::util::Log::Level> g_logLevel;
			const char* m_logName;

		public:
			PPR_EXTERN static std::string GetBaseFilename(std::string source);

			Log(const char* name) : m_logName(name) { }

			PPR_EXTERN static void setGlobalLevel(Level level);
			PPR_EXTERN static Level getGlobalLevel();

			template<typename... ARGS>
			void operator()(const char* filename, std::uint32_t line, const char* func, Level level, ARGS&&... args) {
				if (level < g_logLevel.load()) {
					return;
				}

				std::stringstream stream;
				stream << "[FILE:" << filename << "|" << line << "] " << m_logName << "::" << func << " " << level_to_string(level) << " ";
				ppr::tl::string::format(stream, args...);
				stream << "\n";
				RecordLog(stream);
			}

		private:
			PPR_EXTERN static void RecordLog(std::stringstream& stream);

			static const char* level_to_string(Level l) {
				switch(l) {
					case Level::Trace: return "Trace";
					case Level::Debug: return "Debug";
					case Level::Info: return "Info";
					case Level::Warn: return "Warn";
					case Level::Error: return "Error";
					default: return "Unknown?";
				}
			}
		};
	}
}

#ifdef PPR_ENABLE_LOGS
	#define PPR_CREATE_LOG(name) const std::string __ppr__log__filename_##name = ::ppr::util::Log::GetBaseFilename(__FILE__); ::ppr::util::Log __ppr__log_##name(#name)
	#ifdef __PRETTY_FUNCTION__
		#define PPR_WRITE_LOG(log, level, ...) __ppr__log_##log(__ppr__log__filename_##log.c_str(), __LINE__, __PRETTY_FUNCTION__, ::ppr::util::Log::Level::level, __VA_ARGS__)
	#elif defined(__FUNCTION__)
		#define PPR_WRITE_LOG(log, level, ...) __ppr__log_##log(__ppr__log__filename_##log.c_str(), __LINE__, __FUNCTION__, ::ppr::util::Log::Level::level, __VA_ARGS__)
	#else // !__PRETTY_FUNCTION__
		#define PPR_WRITE_LOG(log, level, ...) __ppr__log_##log(__ppr__log__filename_##log.c_str(), __LINE__, __func__, ::ppr::util::Log::Level::level, __VA_ARGS__)
	#endif // !__PRETTY_FUNCTION__
#else//!PPR_ENABLE_LOGS
	#define PPR_CREATE_LOG(name)
	#define PPR_WRITE_LOG(log, level, ...)
#endif//PPR_ENABLE_LOGS

#endif//PPR_UTIL_LOG_HPP_INCLUDED
