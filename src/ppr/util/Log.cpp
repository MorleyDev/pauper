#include "Log.hpp"
#include <algorithm>

#ifdef PPR_DEFAULT_LOG_LEVEL_TRACE
std::atomic<ppr::util::Log::Level> ppr::util::Log::g_logLevel(ppr::util::Log::Level::Trace);
#else//!PPR_DEFAULT_LOG_LEVEL_TRACE
std::atomic<ppr::util::Log::Level> ppr::util::Log::g_logLevel(ppr::util::Log::Level::Warn);
#endif//PPR_DEFAULT_LOG_LEVEL_TRACE

void ppr::util::Log::setGlobalLevel(Level level)  {
	g_logLevel.store(level);
}

ppr::util::Log::Level ppr::util::Log::getGlobalLevel() {
	return g_logLevel.load();
}

void ppr::util::Log::RecordLog(std::stringstream& stream)  {
	std::printf(stream.str().c_str());
}

std::string ppr::util::Log::GetBaseFilename(std::string source) {
#if defined(WIN32) || defined(_WIN32)
	return std::string(std::find(source.rbegin(), source.rend(), '\\').base(), source.end());
#else // !WIN32
	return std::string(std::find(source.rbegin(), source.rend(), '/').base(), source.end());
#endif // WIN32
}
