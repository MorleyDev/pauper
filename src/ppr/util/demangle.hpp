#ifndef PPR_UTIL_DEMANGLE_HPP_INCLUDED
#define PPR_UTIL_DEMANGLE_HPP_INCLUDED

#include "../extern.hpp"
#include <string>

namespace ppr {
	namespace util {
		extern PPR_EXTERN std::string demangle(const char*);
	}
}

#endif//PPR_UTIL_DEMANGLE_HPP_INCLUDED
