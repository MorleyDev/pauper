#include "demangle.hpp"

#ifdef __GNUG__
#include <memory>
#include <cxxabi.h>

std::string ppr::util::demangle(char const* name) {

	int status = -1;
	std::unique_ptr<char, void(*)(void*)> res(abi::__cxa_demangle(name, NULL, NULL, &status), std::free);

	return status == 0
			? std::string(res.get())
			: std::string(name);
}

#else
std::string ppr::util::demangle(char const* name) { return name; }
#endif
