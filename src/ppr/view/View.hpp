#ifndef PAUPER_VIEW_VIEW_HPP_INCLUDED
#define PAUPER_VIEW_VIEW_HPP_INCLUDED

#include "../extern.hpp"
#include "../tl/any.hpp"
#include "../tl/thread_pool.hpp"
#include <vector>
#include <typeinfo>
#include <typeindex>
#include <memory>
#include <map>
#include <atomic>
#include <functional>

namespace ppr {
	namespace data {
		class Renderer;
	}
    namespace view {
        class View {
        private:
	        std::int32_t m_height;
	        std::atomic<bool> m_isAlive;
	        std::vector<std::type_index> m_events;
	        std::map<std::type_index, std::function<std::future<void> (tl::any const&, ppr::tl::thread_pool&)>> m_eventHandlers;

        public:
	        PPR_EXTERN explicit View(std::int32_t height = 0);
	        PPR_EXTERN virtual ~View();

	        inline void kill() {
		        m_isAlive.store(false);
	        }

	        inline bool isAlive() const {
		        return m_isAlive.load();
	        }

	        virtual void draw(data::Renderer&) = 0;

	        template<typename T> std::future<void> event(T const& e, tl::thread_pool& pool) {
		        return event(typeid(T), tl::any(e), pool);
	        }
	        PPR_EXTERN std::future<void> event(std::type_index type, tl::any const& e, tl::thread_pool& pool);

	        template<typename T> void onEvent(std::function<std::future<void> (T, ppr::tl::thread_pool&)> eve) {
		        m_events.push_back(std::type_index(typeid(T)));
		        m_eventHandlers[std::type_index(typeid(T))] = [eve](tl::any const& e, ppr::tl::thread_pool& pool) {
		            return eve( e.as<T>(), pool );
		        };
	        }

	        PPR_EXTERN std::vector<std::type_index> const& getPossibleEvents() const;

			inline std::int32_t getHeight() const { return m_height; }
        };
    }
}

#endif//PAUPER_VIEW_VIEW_HPP_INCLUDED
