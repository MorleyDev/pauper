#include "ViewStore.hpp"
#include "View.hpp"
#include "../tl/future.hpp"
#include "../util/Log.hpp"
#include "../util/demangle.hpp"
#include <algorithm>

namespace {
	PPR_CREATE_LOG(ViewStore);
}

std::future<void> ppr::view::ViewStore::event(std::type_index type, tl::any const& data) {
	auto viewEventMap = getViewEventMap();

	std::vector<std::future<void>> eventFutures;
	eventFutures.reserve(viewEventMap.size());

	auto range = viewEventMap.equal_range(type);
	for(auto i = range.first; i != range.second; ++i) {
		auto ptr = i->second.lock();
		if (!ptr || !ptr->isAlive()) {
			continue;
		}

		auto enqueuedEvent = ptr->event(type, data, m_threadPool);
		eventFutures.push_back(std::move(enqueuedEvent));
	}

	return tl::future::compose(std::move(eventFutures));
}

void ppr::view::ViewStore::draw(data::Renderer& renderer) {
	auto views = getViews();
	for(auto& view : views) {
		if (!view->isAlive()) {
			continue;
		}
		view->draw(renderer);
	}
}

void ppr::view::ViewStore::add(std::unique_ptr<View> view) {
	PPR_WRITE_LOG( ViewStore, Info, "Added view: %", ppr::util::demangle(typeid(*(view.get())).name()) );

	std::lock_guard<std::mutex> lock(m_mutex);
	std::shared_ptr<View> sharedView(std::move(view));

	for(auto e : sharedView->getPossibleEvents()) {
		m_viewEventMap.insert(std::make_pair(e, sharedView));
	}
	m_views.emplace_back(sharedView);

	std::sort(m_views.begin(), m_views.end(), [](std::shared_ptr<View> const& v, std::shared_ptr<View> const& w) {
	    return v->getHeight() < w->getHeight();
	});
}

ppr::view::ViewStore::ViewStore(ppr::tl::thread_pool& threadPool)
	: m_threadPool(threadPool) {
}

std::vector<std::shared_ptr<ppr::view::View>> ppr::view::ViewStore::getViews() const {
	std::lock_guard<std::mutex> lock(m_mutex);
	return m_views;
}

std::multimap<std::type_index, std::weak_ptr<ppr::view::View> > ppr::view::ViewStore::getViewEventMap() const {
	std::lock_guard<std::mutex> lock(m_mutex);
	return m_viewEventMap;
}

void ppr::view::ViewStore::clean() {
	std::lock_guard<std::mutex> lock(m_mutex);

	for (auto it = m_views.begin(); it != m_views.end(); ) {
		auto ptr = *it;
		if (ptr->isAlive()) {
			++it;
			continue;
		}

		m_views.erase(it);
		it = m_views.begin();

		for (auto t : ptr->getPossibleEvents()) {
			auto removeFrom = m_viewEventMap.equal_range(t);
			for (auto v = removeFrom.first; v != removeFrom.second;) {
				auto wPtr = v->second.lock();
				if (!wPtr || wPtr == ptr) {
					m_viewEventMap.erase(v++);
				}
				else {
					++v;
				}
			}
		}
	}
}
