#include "View.hpp"

ppr::view::View::~View() {

}

ppr::view::View::View(std::int32_t height)
		: m_height(height),
		  m_isAlive(true),
		  m_events(),
		  m_eventHandlers() {
}

std::future<void> ppr::view::View::event(std::type_index type, tl::any const& e, tl::thread_pool& pool) {
	return m_eventHandlers[type](e, pool);
}

std::vector<std::type_index> const& ppr::view::View::getPossibleEvents() const {
	return m_events;
}
