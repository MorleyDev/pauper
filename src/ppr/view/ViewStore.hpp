#ifndef PAUPER_VIEW_VIEWSTORE_HPP_INCLUDED
#define PAUPER_VIEW_VIEWSTORE_HPP_INCLUDED

#include "../extern.hpp"
#include "../tl/any.hpp"
#include "../tl/thread_pool.hpp"
#include <chrono>
#include <vector>
#include <memory>
#include <typeindex>
#include <map>

namespace ppr {
	namespace data {
		class Renderer;
	}

    namespace view {
        class View;
        class ViewStore {
        private:
	        tl::thread_pool& m_threadPool;
	        mutable std::mutex m_mutex;
	        std::vector< std::shared_ptr<View> > m_views;
	        std::multimap< std::type_index, std::weak_ptr<View> > m_viewEventMap;

	        PPR_LOCAL std::vector< std::shared_ptr<View> > getViews() const;
	        PPR_LOCAL std::multimap< std::type_index, std::weak_ptr<View> > getViewEventMap() const;

        public:
	        PPR_EXTERN explicit ViewStore(tl::thread_pool& threadPool);
	        PPR_EXTERN void add(std::unique_ptr<View> controller);
	        PPR_EXTERN void draw(data::Renderer&);

	        PPR_EXTERN void clean();

	        template<typename T> std::future<void> event(T const& data) {
		        return event(typeid(T), tl::any(data));
	        }
	        PPR_EXTERN std::future<void> event(std::type_index type, tl::any const& data);
        };
    }
}
#endif//PAUPER_VIEW_VIEWSTORE_HPP_INCLUDED
