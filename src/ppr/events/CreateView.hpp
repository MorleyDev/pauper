#ifndef PAUPER_EVENTS_CREATEVIEW_HPP_INCLUDED
#define PAUPER_EVENTS_CREATEVIEW_HPP_INCLUDED

#include <functional>
#include <memory>

namespace ppr {
    namespace view {
        class View;
    }

    namespace events {
        struct CreateView {
	        CreateView() : factory() { }
	        CreateView(std::function<std::unique_ptr<view::View> ()> f) : factory(f) { }

	        std::function<std::unique_ptr<view::View> ()> factory;
        };
    }
}

#endif//PAUPER_EVENTS_CREATEVIEW_HPP_INCLUDED
