#ifndef PAUPER_EVENTS_CREATECONTROLLER_HPP_INCLUDED
#define PAUPER_EVENTS_CREATECONTROLLER_HPP_INCLUDED

#include <functional>
#include <memory>

namespace ppr {
	namespace ctrl {
		class Controller;
	}

	namespace events {
		struct CreateController {
			CreateController() : factory() { }
			CreateController(std::function<std::unique_ptr<ctrl::Controller> ()> f) : factory(f) { }

			std::function<std::unique_ptr<ctrl::Controller> ()> factory;
		};
	}
}

#endif//PAUPER_EVENTS_CREATECONTROLLER_HPP_INCLUDED
