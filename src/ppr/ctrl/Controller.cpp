#include "Controller.hpp"
#include "../util/Log.hpp"
#include "../util/demangle.hpp"

namespace {
	PPR_CREATE_LOG(Controller);
}

ppr::ctrl::Controller::~Controller() {

}

ppr::ctrl::Controller::Controller()
		: m_isAlive(true),
		  m_events(),
		  m_eventHandlers(),
		  m_eventEmitter(nullptr) {
}

std::future<void> ppr::ctrl::Controller::event(std::type_index type, tl::any const& e, ppr::tl::thread_pool& pool) {
	return m_eventHandlers[type](e, pool);
}

std::vector<std::type_index> const& ppr::ctrl::Controller::getPossibleEvents() const {
	return m_events;
}

void ppr::ctrl::Controller::bind(data::EventQueue* eventQueue) {
	m_eventEmitter = eventQueue;
}

void ppr::ctrl::Controller::onFailureToEmitEvent(char const* name) {
	PPR_WRITE_LOG(Controller, Error, "Failed to emit event % in controller %", util::demangle(name), util::demangle(typeid(*this).name()));
}
