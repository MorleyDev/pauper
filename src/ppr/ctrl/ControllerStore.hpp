#ifndef PAUPER_CTRL_CONTROLLERSTORE_HPP_INCLUDED
#define PAUPER_CTRL_CONTROLLERSTORE_HPP_INCLUDED

#include "../extern.hpp"
#include "../data/EventQueue.hpp"
#include "../tl/thread_pool.hpp"

#include <chrono>
#include <vector>
#include <memory>
#include <typeindex>
#include <map>
#include <mutex>

namespace ppr {
	namespace ctrl {
		class Controller;
		class ControllerStore {
		private:
			tl::thread_pool& m_threadPool;
			data::EventQueue& m_eventQueue;
			mutable std::mutex m_mutex;

			std::vector<std::shared_ptr<Controller>> m_controllers;
			std::multimap<std::type_index, std::weak_ptr<Controller>> m_controllerEventMap;

			PPR_LOCAL std::vector<std::shared_ptr<Controller>> getControllers() const;
			PPR_LOCAL std::multimap<std::type_index, std::weak_ptr<Controller>> getControllerEvents() const;

		public:
			PPR_EXTERN ControllerStore(data::EventQueue& eventQueue, tl::thread_pool& threadPool);
			PPR_EXTERN void add(std::unique_ptr<Controller> controller);
			PPR_EXTERN std::future<void> update(std::chrono::microseconds time);

			template<typename T> std::future<void> event(T const& data) {
				return event(std::type_index(typeid(T)), tl::any(data));
			}
			PPR_EXTERN std::future<void> event(std::type_index type, tl::any const& data);

			PPR_EXTERN void clean();
		};
	}
}

#endif//PAUPER_CTRL_CONTROLLERSTORE_HPP_INCLUDED
