#ifndef PAUPER_CTRL_CONTROLLER_HPP_INCLUDED
#define PAUPER_CTRL_CONTROLLER_HPP_INCLUDED

#include "../extern.hpp"
#include "../data/EventQueue.hpp"
#include "../tl/any.hpp"
#include "../tl/thread_pool.hpp"
#include <chrono>
#include <vector>
#include <typeinfo>
#include <typeindex>
#include <memory>
#include <map>
#include <atomic>
#include <functional>

namespace ppr {
	namespace ctrl {
		class Controller {
		private:
			std::atomic<bool> m_isAlive;
			std::vector<std::type_index> m_events;
			std::map<std::type_index, std::function<std::future<void> (tl::any const&, tl::thread_pool&)>> m_eventHandlers;
			data::EventQueue* m_eventEmitter;

			PPR_EXTERN void onFailureToEmitEvent(const char*);

		public:
			PPR_EXTERN Controller();
			PPR_EXTERN virtual ~Controller();

			PPR_EXTERN std::vector<std::type_index> const& getPossibleEvents() const;

			virtual std::future<void> update(std::chrono::microseconds dt, tl::thread_pool& pool) = 0;

			inline void kill() { m_isAlive.store(false); }

			inline bool isAlive() const { return m_isAlive.load(); }

			template<typename T> std::future<void> event(T const& e, tl::thread_pool& pool) {
				return event(std::type_index(typeid(T)), e, pool);
			}
			PPR_EXTERN std::future<void> event(std::type_index type, tl::any const& e, tl::thread_pool& pool);

			PPR_EXTERN void bind(data::EventQueue* eventQueue);

			template<typename T> void emit(T const& e) {
				if (!m_eventEmitter) {
					onFailureToEmitEvent(typeid(e).name());
					return;
				}
				m_eventEmitter->enqueue(e);
			}

			template<typename T> void onEvent(std::function<std::future<void> (T, tl::thread_pool&)> eve) {
				m_events.push_back(std::type_index(typeid(T)));
				m_eventHandlers[std::type_index(typeid(T))] = [eve](tl::any const& e, tl::thread_pool& pool)
						-> std::future<void> {
				    return eve( e.as<T>(), pool );
				};
			}
		};
	}
}

#endif//PAUPER_CTRL_CONTROLLER_HPP_INCLUDED
