#include "ControllerStore.hpp"
#include "Controller.hpp"
#include "../tl/future.hpp"
#include "../util/Log.hpp"
#include "../util/demangle.hpp"
#include <functional>

namespace {
	PPR_CREATE_LOG(ControllerStore);
}

std::future<void> ppr::ctrl::ControllerStore::event(std::type_index type, tl::any const& data) {
	auto controllerEventMap = getControllerEvents();

	std::vector<std::future<void>> eventFutures;

	auto range = controllerEventMap.equal_range(type);
	for(auto i = range.first; i != range.second; ++i) {
		auto ptr = i->second.lock();
		if (!ptr || !(ptr->isAlive())) {
			continue;
		}

		auto enqueuedEvent = ptr->event(type, data, m_threadPool);
		eventFutures.push_back(std::move(enqueuedEvent));
	}

	return tl::future::compose(std::move(eventFutures));
}

std::future<void> ppr::ctrl::ControllerStore::update(std::chrono::microseconds time) {
	auto controllers = getControllers();

	std::vector<std::future<void>> updateFutures;
	updateFutures.reserve(controllers.size());

	for(auto& controller : controllers) {
		if (!controller->isAlive()) {
			continue;
		}
		auto enqueuedUpdate = controller->update(time, m_threadPool);
		updateFutures.push_back(std::move(enqueuedUpdate));
	}

	return tl::future::compose(std::move(updateFutures));
}

void ppr::ctrl::ControllerStore::add(std::unique_ptr<Controller> controller) {
	PPR_WRITE_LOG(ControllerStore, Info, "Added controller: %", ppr::util::demangle(typeid(*(controller.get())).name()));
	std::lock_guard<std::mutex> lock(m_mutex);

	controller->bind(&m_eventQueue);
	std::shared_ptr<Controller> shController(std::move(controller));
	for(auto e : shController->getPossibleEvents()) {
		m_controllerEventMap.insert(std::make_pair(e, shController));
	}

	m_controllers.push_back(std::move(shController));
}

ppr::ctrl::ControllerStore::ControllerStore(ppr::data::EventQueue& eventQueue, tl::thread_pool& threadPool)
		: m_threadPool(threadPool),
		  m_eventQueue(eventQueue),
		  m_controllers(),
		  m_controllerEventMap() {
}

void ppr::ctrl::ControllerStore::clean() {
	std::lock_guard<std::mutex> lock(m_mutex);
	for(auto it = m_controllers.begin(); it != m_controllers.end();) {
		if ((*it)->isAlive()) {
			++it;
			continue;
		}

		auto ptr = std::move(*it);
		m_controllers.erase(it);
		it = m_controllers.begin();

		for(auto t : ptr->getPossibleEvents()) {
			auto removeFrom = m_controllerEventMap.equal_range(t);
			for(auto v = removeFrom.first; v != removeFrom.second; ) {
				auto wPtr = v->second.lock();
				if (!wPtr || wPtr == ptr) {
					m_controllerEventMap.erase(v++);
				} else {
					++v;
				}
			}
		}
	}
}

std::vector<std::shared_ptr<ppr::ctrl::Controller>> ppr::ctrl::ControllerStore::getControllers() const {
	std::lock_guard<std::mutex> lock(m_mutex);
	return m_controllers;
}

std::multimap<std::type_index, std::weak_ptr<ppr::ctrl::Controller>> ppr::ctrl::ControllerStore::getControllerEvents() const {
	std::lock_guard<std::mutex> lock(m_mutex);
	return m_controllerEventMap;
}
