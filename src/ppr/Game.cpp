#include "Game.hpp"
#include "data/ContextFactory.hpp"
#include "data/SysContext.hpp"
#include "data/Window.hpp"
#include "data/Renderer.hpp"
#include "data/MusicPlayer.hpp"
#include "data/SoundEffectPlayer.hpp"
#include "data/InputSystem.hpp"
#include "data/ContentLoader.hpp"
#include "data/fs/FolderFileSystem.hpp"
#include "data/FileSystemStack.hpp"

#include "events/CreateController.hpp"
#include "events/CreateView.hpp"
#include "events/Close.hpp"
#include "tl/chrono.hpp"
#include "tl/semaphore.hpp"
#include "tl/future.hpp"

#include <fstream>
#include <ppr/util/Log.hpp>

ppr::model::Configuration ppr::Game::loadConfiguration() {
	std::ifstream config("./config.json");
	if (!config.is_open()) {
		auto defaultConfig = ppr::model::Configuration();
		writeConfiguration(defaultConfig);
		return defaultConfig;
	}

	std::stringstream strStream;
	strStream << config.rdbuf();
	auto configDto = ppr::tl::from_json<ppr::model::Configuration>(strStream.str());
	if (configDto.logging >= 0) {
		util::Log::setGlobalLevel(static_cast<ppr::util::Log::Level>(configDto.logging));
	}
	return configDto;
}

void ppr::Game::writeConfiguration(ppr::model::Configuration const& configuration) {
	std::ofstream cfgOut("./config.json");
	if (!cfgOut.is_open()) {
		return;
	}
	cfgOut << ppr::tl::to_json(configuration) << std::endl;
}

namespace {
	PPR_CREATE_LOG(Game);
}


ppr::Game::Game(std::string gameName, model::Configuration config)
		: m_config(config),
		  m_threadPool(m_config.threadPoolSize),
		  m_eventHandlers(),
		  m_context(data::createContext(m_config.driver)),
		  m_fileSystem(new data::FileSystemStack()),
		  m_window(m_context->createWindow(gameName, m_config.graphics.width, m_config.graphics.height, m_config.graphics.fullscreen, m_config.graphics.vsync)),
		  m_musicPlayer(m_context->createMusicPlayer()),
		  m_soundEffectPlayer(m_context->createSoundEffectPlayer()),
		  m_content(tl::make_unique<data::ContentLoader>(*m_fileSystem, m_window->renderer(), *m_musicPlayer, *m_soundEffectPlayer)),
		  events(),
		  renderer(m_window->renderer()),
		  input(m_window->input()),
		  controllers(events, m_threadPool),
		  views(m_threadPool),
		  context(*m_context),
		  files(*m_fileSystem),
		  window(*m_window),
		  music(*m_musicPlayer),
		  sound(*m_soundEffectPlayer),
		  content(*m_content),
		  m_isAlive(true) {
	PPR_WRITE_LOG(Game, Info, "Creating");

	files.push(tl::make_unique<data::fs::FolderFileSystem>("."));
	files.push(tl::make_unique<data::fs::FolderFileSystem>("resources"));

	music.setVolume(m_config.audio.musicVolume);
	sound.setVolume(m_config.audio.soundVolume);

	m_eventHandlers[std::type_index(typeid(events::Close))] = [&](tl::any const& data) {
		kill();
	};
	m_eventHandlers[std::type_index(typeid(events::CreateController))] = [&](tl::any const& data) {
	    controllers.add(data.as<events::CreateController>().factory());
	};
	m_eventHandlers[std::type_index(typeid(events::CreateView))] = [&](tl::any const& data) {
	    views.add(data.as<events::CreateView>().factory());
	};
}

ppr::Game::~Game() {
	PPR_WRITE_LOG(Game, Info, "Destroying");
}

std::future<void>  ppr::Game::update(std::chrono::microseconds dt) {
	return controllers.update(dt);
}

void ppr::Game::draw() {
	renderer.clear();
	views.draw(renderer);
	renderer.display();
}

void ppr::Game::poll_system() {
	window.poll();
	music.update();
	sound.update();
}

std::future<void> ppr::Game::poll_events() {
	std::vector<std::pair<std::function<void (tl::any const&)>, tl::any>> finalHandlers;
	std::vector<std::future<void>> waitFutures;

	events.dequeue([&](std::type_index type, tl::any data) {
		auto it = m_eventHandlers.find(type);
		if (it != m_eventHandlers.end()) {
			finalHandlers.push_back(std::make_pair(it->second, data));
			return;
		}

		waitFutures.push_back(views.event(type, data));
		waitFutures.push_back(controllers.event(type, data));
	});

	auto shouldRepeat = waitFutures.size() > 0u;
	auto composed = tl::future::compose(std::move(waitFutures));

	return tl::future::deferred([this](std::vector<std::pair<std::function<void(tl::any const&)>, tl::any>> finally,
		       std::future<void> futures,
			   bool repeat) {
		futures.wait();
		for (auto& i : finally) {
			i.first(i.second);
		}

		if (!repeat) {
			return;
		}
		poll_events().wait();
	}, std::move(finalHandlers), std::move(composed), shouldRepeat);
}

void ppr::Game::clean() {
	controllers.clean();
	views.clean();
}

bool ppr::Game::hasUniqueSystemThread() const {
	return !window.isPollTiedToCreationThread();
}
