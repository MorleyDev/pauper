#include <ppr/tl/signals.hpp>
#include <ppr/tl/make_unique.hpp>
#include <UnitTest11.hpp>
#include <UnitTest11/Mock.hpp>
#include <thread>

class SignalsTests : public ut11::TestFixture {

	std::unique_ptr<ppr::tl::signals<int,float>> signalsPtr;
	ut11::Mock<void (int, float)> mockHandler;

	std::atomic_uint_fast32_t nCallCount;

	virtual void Run() {
		Given("a signal collection with connected handlers", [&]() {
		    signalsPtr = std::move(ppr::tl::make_unique<ppr::tl::signals<int,float>>());
		    mockHandler = ut11::Mock<void (int, float)>();
		    for(auto u = 0u; u < 25u; ++u)
			    signalsPtr->listen([&](int i, float f) { mockHandler(i, f); });

		}); When("triggering the handlers", [&]() {
		    signalsPtr->signal(10152, 352.2f);
		}); Then("the handlers were triggered", [&]() {
		    MockVerifyTimes(25, mockHandler)(10152, 352.2f);
		});

		Given("concurrency tests", [&]() {
		    signalsPtr = std::move(ppr::tl::make_unique<ppr::tl::signals<int,float>>());
		    nCallCount.store(0);
		    std::vector<std::thread> threads;
		    for(auto u = 0u; u < 5u; ++u)
			    threads.push_back(std::thread([&]() {
				    for(auto v = 0u; v < 50u; ++v)
					    signalsPtr->listen([&](int i, float f) {
					        ++nCallCount;
					    });
			    }));
			for(auto& thread : threads)
				thread.join();
		    threads.clear();
		}); When("signal concurrently", [&]() {
		    signalsPtr->signal(25, 4652.0f);
		}); Then("the handler was triggered at least 250 times", [&]() {
			AssertThat(nCallCount.load(), ut11::Is::GreaterThan(25) || ut11::Is::EqualTo(25));
		});
	}
};
DeclareFixture(SignalsTests)(ut11::Category("tl"));


class SignalsVoidTests : public ut11::TestFixture {

	std::unique_ptr<ppr::tl::signals<void>> signalsPtr;
	ut11::Mock<void (void)> mockHandler;

	std::atomic_uint_fast32_t nCallCount;

	virtual void Run() {
		Given("a signal collection with connected handlers", [&]() {
		    signalsPtr = std::move(ppr::tl::make_unique<ppr::tl::signals<void>>());
		    mockHandler = ut11::Mock<void (void)>();
		    for(auto u = 0u; u < 25u; ++u)
			    signalsPtr->listen([&]() { mockHandler(); });

		}); When("triggering the handlers", [&]() {
		    signalsPtr->signal();
		}); Then("the handlers were triggered", [&]() {
		    MockVerifyTimes(25, mockHandler)();
		});

		Given("concurrency tests", [&]() {
		    signalsPtr = std::move(ppr::tl::make_unique<ppr::tl::signals<void>>());
		    nCallCount.store(0);
		    std::vector<std::thread> threads;
		    for(auto u = 0u; u < 5u; ++u)
			    threads.push_back(std::thread([&]() {
			        for(auto v = 0u; v < 50u; ++v)
				        signalsPtr->listen([&]() {
				            ++nCallCount;
				        });
			    }));
		    for(auto& thread : threads)
			    thread.join();
		    threads.clear();
		}); When("signal concurrently", [&]() {
		    std::vector<std::thread> threads;
		    for(auto u = 0u; u < 5u; ++u)
			    threads.push_back(std::thread([&]() {
			        for(auto v = 0u; v < 50u; ++v)
				        signalsPtr->listen([&]() {
				            ++nCallCount;
				        });
			    }));
		    signalsPtr->signal();
		    for(auto& thread : threads)
			    thread.join();
		    threads.clear();
		}); Then("the handler was triggered at least 250 times", [&]() {
		    AssertThat(nCallCount.load(), ut11::Is::GreaterThan(250) || ut11::Is::EqualTo(250));
		});
	}
};
DeclareFixture(SignalsVoidTests)(ut11::Category("tl"));
