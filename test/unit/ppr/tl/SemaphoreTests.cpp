#include <ppr/tl/semaphore.hpp>
#include <ppr/tl/make_unique.hpp>
#include <UnitTest11.hpp>
#include <thread>
#include <vector>

class SemaphoreTests : public ut11::TestFixture {

	std::unique_ptr<ppr::tl::semaphore> m_semaphore;

	virtual void Run() {
        Then("concurrent semaphore tests #1", [this]() {

            m_semaphore = ppr::tl::make_unique<ppr::tl::semaphore>(20);

            std::atomic<bool> decrementSuccess[11];
            for(auto i = 0u; i < 11; ++i)
                decrementSuccess[i].store(false);

            std::vector<std::thread> decrementThreads;
            for(auto i = 0u; i < 11; ++i)
                decrementThreads.push_back(std::thread([&, i]() {
                    decrementSuccess[i].store(m_semaphore->tryDecrementBy(2));
                }));
            for(auto i = 0u; i < 11; ++i)
                decrementThreads[i].join();

            auto trueCount = 0u;
            for(auto i = 0u; i < 11; ++i)
                if (decrementSuccess[i].load())
                    ++trueCount;
            AssertThat(trueCount, ut11::Is::EqualTo(10));

            m_semaphore->incrementBy(2);
            AssertThat(m_semaphore->tryDecrementBy(2), ut11::Is::True);
        });
		Then("concurrent semaphore tests #2", [this]() {
            m_semaphore = ppr::tl::make_unique<ppr::tl::semaphore>(1);

            std::atomic<bool> wasDecremented[2];
            wasDecremented[0].store(false);
            wasDecremented[1].store(false);

		    std::thread decrementThread0([&]() {
                m_semaphore->decrementBy(2);
                wasDecremented[0].store(true);
            });
            std::thread decrementThread1([&]() {
                m_semaphore->decrementBy(3);
                wasDecremented[1].store(true);
            });

            AssertThat(wasDecremented[0].load(), ut11::Is::False);
            AssertThat(wasDecremented[1].load(), ut11::Is::False);

            m_semaphore->incrementBy(3);
            m_semaphore->incrementBy(1);

			decrementThread0.join();
            decrementThread1.join();

            AssertThat(wasDecremented[0].load(), ut11::Is::True);
            AssertThat(wasDecremented[1].load(), ut11::Is::True);
        });
	}
};
DeclareFixture(SemaphoreTests)(ut11::Category("tl"));
