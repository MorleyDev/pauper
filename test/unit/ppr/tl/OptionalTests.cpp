#include <ppr/tl/optional.hpp>
#include <UnitTest11.hpp>

class OptionalTests : public ut11::TestFixture {

	ppr::tl::optional<float> m_optional;
	float m_expectedValue;

	virtual void Run() {
		Given("an unassigned optional", [&]() {
			m_optional = ppr::tl::none();
		}); Then("the value as a boolean is false", [&]() {
			AssertThat(static_cast<bool>(m_optional), ut11::Is::False);
		}); Then("the a get returns nullptr", [&]() {
			AssertThat(m_optional.get(), ut11::Is::Null);
		});Then("a copy of the value as a boolean is false", [&]() {
		    ppr::tl::optional<float> copy(m_optional);
		    AssertThat(static_cast<bool>(copy), ut11::Is::False);
		}); Then("the a get of a copy returns nullptr", [&]() {
		    ppr::tl::optional<float> copy(m_optional);
		    AssertThat(copy.get(), ut11::Is::Null);
		});

		Given("an assigned optional", [&]() {
		    m_expectedValue = 1052.452f;
		    m_optional = ppr::tl::some(m_expectedValue);
		}); Then("the value as a boolean is true", [&]() {
		    AssertThat(static_cast<bool>(m_optional), ut11::Is::True);
		}); Then("the a get returns a pointer for the value", [&]() {
		    AssertThat(*(m_optional.get()), ut11::Is::EqualTo(m_expectedValue));
		}); Then("the a dereference returns the value", [&]() {
		    AssertThat(*m_optional, ut11::Is::EqualTo(m_expectedValue));
		}); Then("a copy of the value as a boolean is true", [&]() {
		    ppr::tl::optional<float> copy(m_optional);
		    AssertThat(static_cast<bool>(copy), ut11::Is::True);
		}); Then("the a dereference of a copy returns the value", [&]() {
		    ppr::tl::optional<float> copy(m_optional);
		    AssertThat(*copy, ut11::Is::EqualTo(m_expectedValue));
		}); Then("a move of the value as a boolean is true and the original is false", [&]() {
		    ppr::tl::optional<float> move(std::move(m_optional));
		    AssertThat(static_cast<bool>(move), ut11::Is::True);
		    AssertThat(static_cast<bool>(m_optional), ut11::Is::False);
		}); Then("the a get of a move returns a pointer for the value and the original is nullptr", [&]() {
		    ppr::tl::optional<float> move(std::move(m_optional));
		    AssertThat(*(move.get()), ut11::Is::EqualTo(m_expectedValue));
		    AssertThat(m_optional.get(), ut11::Is::Null);
		}); Then("the a derefernece of a move returns the value", [&]() {
		    ppr::tl::optional<float> move(std::move(m_optional));
		    AssertThat(*move, ut11::Is::EqualTo(m_expectedValue));
		});
	}
};
DeclareFixture(OptionalTests)(ut11::Category("tl"));
