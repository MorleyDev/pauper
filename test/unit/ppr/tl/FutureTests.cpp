#include <UnitTest11/Macros.hpp>
#include <ppr/tl/future.hpp>
#include <UnitTest11.hpp>
#include <UnitTest11/Mock.hpp>

class FutureDeferredTests : public ut11::TestFixture {
	ut11::Mock<int (void)> mockParams0;
	ut11::Mock<int (int)> mockParams1;
	ut11::Mock<int (int, char)> mockParams2;
	ut11::Mock<int (int, char, float)> mockParams3;
	std::future<int> deferredFuture;

	int actualResult;
	int expectedResult;

	virtual void Run() {
		Given("a deferred function with no parameters and returning an int", [this]() {
			mockParams0 = ut11::Mock<int (void)>();
		    expectedResult = 12;
			mockParams0.SetReturn(expectedResult);
			deferredFuture = ppr::tl::future::deferred([this]() { return mockParams0(); });
		}); Then("the mock has not been executed", [this]() {
			MockVerifyTimes(0, mockParams0)();
		}); When("retrieving the mock", [this]() {
			actualResult = deferredFuture.get();
		}); Then("the mock was executed", [this]() {
			MockVerifyTimes(1, mockParams0)();
		}); Then("the expected result was returned", [this]() {
			AssertThat(actualResult, ut11::Is::EqualTo(expectedResult));
		});

		Given("a deferred function with one parameter and returning an int", [this]() {
		    mockParams1 = ut11::Mock<int (int)>();
		    expectedResult = 122;
		    mockParams1.SetReturn(expectedResult);
		    deferredFuture = ppr::tl::future::deferred([this](int i) { return mockParams1(i); }, 152);
		}); Then("the mock has not been executed", [this]() {
		    MockVerifyTimes(0, mockParams1)(ut11::Is::Any<int>());
		}); When("retrieving the mock", [this]() {
		    actualResult = deferredFuture.get();
		}); Then("the mock was executed", [this]() {
		    MockVerifyTimes(1, mockParams1)(152);
		}); Then("the expected result was returned", [this]() {
		    AssertThat(actualResult, ut11::Is::EqualTo(expectedResult));
		});

		Given("a deferred function with two parameters and returning an int", [this]() {
		    mockParams2 = ut11::Mock<int (int, char)>();
		    expectedResult = 222;
		    mockParams2.SetReturn(expectedResult);
		    deferredFuture = ppr::tl::future::deferred([this](int i, char c) { return mockParams2(i,c); }, 152, 'a');
		}); Then("the mock has not been executed", [this]() {
		    MockVerifyTimes(0, mockParams2)(ut11::Is::Any<int>(), ut11::Is::Any<char>());
		}); When("retrieving the mock", [this]() {
		    actualResult = deferredFuture.get();
		}); Then("the mock was executed", [this]() {
		    MockVerifyTimes(1, mockParams2)(152, 'a');
		}); Then("the expected result was returned", [this]() {
		    AssertThat(actualResult, ut11::Is::EqualTo(expectedResult));
		});

		Given("a deferred function with three parameters and returning an int", [this]() {
		    mockParams3 = ut11::Mock<int (int, char, float)>();
		    expectedResult = 322;
		    mockParams3.SetReturn(expectedResult);
		    deferredFuture = ppr::tl::future::deferred([this](int i, char c, float f) { return mockParams3(i,c,f); }, 152, 'a', 2.0f);
		}); Then("the mock has not been executed", [this]() {
		    MockVerifyTimes(0, mockParams3)(ut11::Is::Any<int>(), ut11::Is::Any<char>(), ut11::Is::Any<float>());
		}); When("retrieving the mock", [this]() {
		    actualResult = deferredFuture.get();
		}); Then("the mock was executed", [this]() {
		    MockVerifyTimes(1, mockParams3)(152, 'a', 2.0f);
		}); Then("the expected result was returned", [this]() {
		    AssertThat(actualResult, ut11::Is::EqualTo(expectedResult));
		});
	}
};
DeclareFixture(FutureDeferredTests)(ut11::Category("tl"));

class FutureComposeTests : public ut11::TestFixture {

	ut11::Mock<void (void)> futureFunctions[5];
	std::future<void> composite;

	virtual void Run() {
		Given("a series of futures composed together", [this]() {
			for(auto i = 0u; i < 5u; ++i)
				futureFunctions[i] = ut11::Mock<void (void)>();
		    std::list< std::future<void> > futureSet;
			for(auto i = 0u; i < 5u; ++i)
				futureSet.push_back(std::move(ppr::tl::future::deferred([this, i]() { futureFunctions[i](); })));
			composite = ppr::tl::future::compose(std::move(futureSet));
		}); Then("the futures have not been waited on", [this]() {
			for(auto i = 0u; i < 5u; ++i)
				MockVerifyTimes(0, futureFunctions[i])();
		}); When("the composite is waited on", [this]() {
			composite.wait();
		}); Then("the composed futures are waited on", [this]() {
		    for(auto i = 0u; i < 5u; ++i)
			    MockVerifyTimes(1, futureFunctions[i])();
		});
	}
};
DeclareFixture(FutureComposeTests)(ut11::Category("tl"));

class FutureChainTests : public ut11::TestFixture {

	ut11::Mock<int (void)> futureFunctions1;
	ut11::Mock<float (int)> futureFunctions2;
	ut11::Mock<char (float)> futureFunctions3;
	std::future<char> chained;

	char result;

	virtual void Run() {
		Given("a series of futures chained together to a deferred", [this]() {
			futureFunctions1 = ut11::Mock<int (void)>();
			futureFunctions1.SetReturn(10);

			futureFunctions2 = ut11::Mock<float (int)>();
			futureFunctions2.SetReturn(1.0f);

			futureFunctions3 = ut11::Mock<char (float)>();
			futureFunctions3.SetReturn('z');

			chained = ppr::tl::future::chain([this]() -> int { return futureFunctions1(); })
					.map([this](int c) -> float { return futureFunctions2(c); })
					.map([this](float c) -> char { return futureFunctions3(c); })
					.to_deferred();

		}); Then("the futures have not been waited on", [this]() {
			MockVerifyTimes(0, futureFunctions1)();
			MockVerifyTimes(0, futureFunctions2)(ut11::Is::Any<int>());
			MockVerifyTimes(0, futureFunctions3)(ut11::Is::Any<float>());
		}); When("the chain is retrieved", [this]() {
			result = chained.get();
		}); Then("the composed futures are waited on with the expected results", [this]() {
			MockVerifyTimes(1, futureFunctions1)();
			MockVerifyTimes(1, futureFunctions2)(10);
			MockVerifyTimes(1, futureFunctions3)(1.0f);
		}); Then("the expected result is returned", [this]() {
			AssertThat(result, ut11::Is::EqualTo('z'));
		});

		Given("a series of futures chained together to an async", [this]() {
			futureFunctions1 = ut11::Mock<int (void)>();
			futureFunctions1.SetReturn(10);

			futureFunctions2 = ut11::Mock<float (int)>();
			futureFunctions2.SetReturn(1.0f);

			futureFunctions3 = ut11::Mock<char (float)>();
			futureFunctions3.SetReturn('z');

			chained = ppr::tl::future::chain([this]() -> int { return futureFunctions1(); })
					.map([this](int c) -> float { return futureFunctions2(c); })
					.map([this](float c) -> char { return futureFunctions3(c); })
					.to_async();

		}); When("the chain is retrieved", [this]() {
			result = chained.get();
		}); Then("the composed futures are waited on with the expected results", [this]() {
			MockVerifyTimes(1, futureFunctions1)();
			MockVerifyTimes(1, futureFunctions2)(10);
			MockVerifyTimes(1, futureFunctions3)(1.0f);
		}); Then("the expected result is returned", [this]() {
			AssertThat(result, ut11::Is::EqualTo('z'));
		});

		Given("a series of futures chained together to a value", [this]() {
			futureFunctions1 = ut11::Mock<int (void)>();
			futureFunctions1.SetReturn(10);

			futureFunctions2 = ut11::Mock<float (int)>();
			futureFunctions2.SetReturn(1.0f);

			futureFunctions3 = ut11::Mock<char (float)>();
			futureFunctions3.SetReturn('z');

		}); When("the chain is retrieved", [this]() {
			result = ppr::tl::future::chain([this]() -> int { return futureFunctions1(); })
					.map([this](int c) -> float { return futureFunctions2(c); })
					.map([this](float c) -> char { return futureFunctions3(c); })
					.to_value();
		}); Then("the composed futures are waited on with the expected results", [this]() {
			MockVerifyTimes(1, futureFunctions1)();
			MockVerifyTimes(1, futureFunctions2)(10);
			MockVerifyTimes(1, futureFunctions3)(1.0f);
		}); Then("the expected result is returned", [this]() {
			AssertThat(result, ut11::Is::EqualTo('z'));
		});
	}
};
DeclareFixture(FutureChainTests)(ut11::Category("tl"));

class FutureChainVoidTests : public ut11::TestFixture {
	ut11::Mock<void (void)> mockVoidFunction;

	ut11::Mock<int (void)> mockIntFunction;
	ut11::Mock<void (int)> mockVoidIntFunction;
	ut11::Mock<char (void)> mockCharFunction;

	virtual void Run() {
		Then("a chained future that returns results in a void future that invokes the function", [this]() {
			mockVoidFunction = ut11::Mock<void (void)>();
			std::future<void> future = ppr::tl::future::chain([this]() { mockVoidFunction(); }).to_deferred();
			future.get();

			MockVerifyTimes(1, mockVoidFunction)();
		});
		Then("a chained of futures that have a void future invokes the functions as expected", [this]() {
			mockIntFunction = ut11::Mock<int (void)>();
			mockIntFunction.SetReturn(120);

			mockVoidIntFunction = ut11::Mock<void (int)>();
			mockCharFunction = ut11::Mock<char (void)> ();
			mockCharFunction.SetReturn('z');

			std::future<char> future = ppr::tl::future::chain([this]() { return mockIntFunction(); })
					.map([this](int i) { mockVoidIntFunction(i); })
					.map([this]() { return mockCharFunction(); })
					.to_deferred();
			char result = future.get();

			MockVerifyTimes(1, mockIntFunction)();
			MockVerifyTimes(1, mockVoidIntFunction)(120);
			MockVerifyTimes(1, mockCharFunction)();
			AssertThat(result, ut11::Is::EqualTo('z'));
		});
		Then("a chained of futures that end in a void future invokes the functions as expected", [this]() {
			mockIntFunction = ut11::Mock<int (void)>();
			mockIntFunction.SetReturn(120);

			mockVoidIntFunction = ut11::Mock<void (int)>();

			std::future<void> future = ppr::tl::future::chain([this]() { return mockIntFunction(); })
					.map([this](int i) { mockVoidIntFunction(i); })
					.to_deferred();
			future.get();

			MockVerifyTimes(1, mockIntFunction)();
			MockVerifyTimes(1, mockVoidIntFunction)(120);
		});
		Then("a chained of futures that are only voids the functions as expected", [this]() {
			mockVoidFunction = ut11::Mock<void (void)>();
			std::future<void> future = ppr::tl::future::chain([this]() { mockVoidFunction(); })
					.map([this]() { mockVoidFunction(); })
					.to_deferred();
			future.get();

			MockVerifyTimes(2, mockVoidFunction)();
		});
	}
}; DeclareFixture(FutureChainVoidTests)(ut11::Category("tl"));

class FutureValueTests : public ut11::TestFixture {
public:
	virtual void Run() {
		Then("Given a future value of an integral When the value is then gotten Then the result is as expected", [this]() {
			std::future<int> i = ppr::tl::future::value(10);
			int result = i.get();
			AssertThat(result, ut11::Is::EqualTo(10));
		});
		Then("Given a future value without a value When the value is then gotten Then nothing happens", [this]() {
			std::future<void> i = ppr::tl::future::value();
			i.get();
		});
	}
}; DeclareFixture(FutureValueTests)(ut11::Category("tl"));
