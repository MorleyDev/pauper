#include <ppr/tl/string/split.hpp>
#include <UnitTest11.hpp>
#include <iostream>

class StringSplitTests : public ut11::TestFixture {

	std::string m_sourceString;
	std::vector<std::string> m_resultStrings;
	std::vector<std::string> m_expectedStrings;

	virtual void Run() {
		Given("an empty string", [this]() {
			m_sourceString = "";
		}); When("splitting the string", [this]() {
		    auto strings = ppr::tl::string::split(m_sourceString, 'a');
			m_resultStrings = std::vector<std::string>(strings.begin, strings.end);
		    m_expectedStrings = std::vector<std::string>();
		}); Then("the expected strings are returned", [this]() {
			AssertThat(m_resultStrings, ut11::Is::EqualTo(m_expectedStrings));
		});

		Given("a string not split by the delimiter", [this]() {
		    m_sourceString = "hello world";
		}); When("splitting the string", [this]() {
		    auto strings = ppr::tl::string::split(m_sourceString, 'a');

		    m_resultStrings = std::vector<std::string>(strings.begin, strings.end);
		    m_expectedStrings = std::vector<std::string>({"hello world"});
		}); Then("the expected strings are returned", [this]() {
		    AssertThat(m_resultStrings, ut11::Is::EqualTo(m_expectedStrings));
		});

		Given("a string split by a string delimiter", [this]() {
		    m_sourceString = "hello___world__tree__piano__poket___";
		}); When("splitting the string", [this]() {
		    auto strings = ppr::tl::string::split(m_sourceString, "__");

		    m_resultStrings = std::vector<std::string>(strings.begin, strings.end);
		    m_expectedStrings = std::vector<std::string>({"hello", "_world", "tree", "piano", "poket", "_"});
		}); Then("the expected strings are returned", [this]() {
		    AssertThat(m_resultStrings, ut11::Is::EqualTo(m_expectedStrings));
		});

		Given("a string split by a string delimiter and ending with that delimiter", [this]() {
		    m_sourceString = "hello_";
		}); When("splitting the string", [this]() {
		    auto strings = ppr::tl::string::split(m_sourceString, '_');

		    m_resultStrings = std::vector<std::string>(strings.begin, strings.end);
		    m_expectedStrings = std::vector<std::string>({"hello", ""});
		}); Then("the expected strings are returned", [this]() {
		    AssertThat(m_resultStrings, ut11::Is::EqualTo(m_expectedStrings));
		});
	}
};
DeclareFixture(StringSplitTests)(ut11::Category("tl"));
