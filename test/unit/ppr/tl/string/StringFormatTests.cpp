#include <ppr/tl/string/format.hpp>
#include <UnitTest11.hpp>

#include <algorithm>
#include <vector>
#include <utility>

class StringFormatTests : public ut11::TestFixture {
	std::vector<std::pair<std::string,std::string>> NoVariableTestCase;
	std::vector<std::pair<std::string,std::string>> OneVariableTestCase;
	std::vector<std::pair<std::string,std::string>> TwoVariableTestCase;
	std::string m_result, m_expectedResult;

public:
	StringFormatTests()
			: NoVariableTestCase(),
			  OneVariableTestCase(),
			  TwoVariableTestCase(),
			  m_result(),
			  m_expectedResult() {
		NoVariableTestCase.emplace_back("Hello World", "Hello World");
		NoVariableTestCase.emplace_back("Hello % World", "Hello % World");
		NoVariableTestCase.emplace_back("Hello %% World", "Hello %% World");
		NoVariableTestCase.emplace_back("Hello \\% World", "Hello \\% World");

		OneVariableTestCase.emplace_back("Hello World", "Hello World10");
		OneVariableTestCase.emplace_back("Hello % %% World", "Hello 10 %% World");
		OneVariableTestCase.emplace_back("Hello \\% % World", "Hello % 10 World");
		OneVariableTestCase.emplace_back("Hello %% World", "Hello 10% World");

		TwoVariableTestCase.emplace_back("Hello World", "Hello World1020");
		TwoVariableTestCase.emplace_back("Hello %\\% Wor%%ld", "Hello 10% Wor20%ld");
		TwoVariableTestCase.emplace_back("% Hello \\% Wor%ld", "10 Hello % Wor20ld");
		TwoVariableTestCase.emplace_back("\\% Hello % World", "% Hello 10 World20");
	}

	virtual void Run() {

		Given("No variables passed to a format", [this]() { });
		std::for_each(NoVariableTestCase.begin(), NoVariableTestCase.end(), [this](std::pair<std::string,std::string> testCase) {
			WhenCalledWithNoVariables(testCase);
		});

		Given("One variables passed to a format", [this]() { });
		std::for_each(OneVariableTestCase.begin(), OneVariableTestCase.end(), [this](std::pair<std::string,std::string> testCase) {
			WhenCalledWithOneVariable(testCase);
		});

		Given("One variables passed to a format", [this]() { });
		std::for_each(TwoVariableTestCase.begin(), TwoVariableTestCase.end(), [this](std::pair<std::string,std::string> testCase) {
			WhenCalledWithTwoVariables(testCase);
		});
	}

	void WhenCalledWithNoVariables(std::pair<std::string,std::string> testCase) {
		auto in = testCase.first;
		auto expected = testCase.second;

		When("formatting a c-string called with " + in, [this, in]() {
			m_result = ppr::tl::string::format(in.c_str());
		});
		Then("The expected result is returned", [this, expected]() {
			AssertThat(m_result, ut11::Is::EqualTo(expected));
		});
		When("formatting a string called with " + in, [this, in]() {
			m_result = ppr::tl::string::format(in);
		});
		Then("The expected result is returned", [this, expected]() {
			AssertThat(m_result, ut11::Is::EqualTo(expected));
		});
		When("formatting a string called with stream and " + in, [this, in]() {
			std::stringstream stream;
			ppr::tl::string::format(stream, in);
			m_result = stream.str();
		});
		Then("The expected result is returned", [this, expected]() {
			AssertThat(m_result, ut11::Is::EqualTo(expected));
		});
		When("formatting a c-string called with stream and " + in, [this, in]() {
			std::stringstream stream;
			ppr::tl::string::format(stream, in.c_str());
			m_result = stream.str();
		});
		Then("The expected result is returned", [this, expected]() {
			AssertThat(m_result, ut11::Is::EqualTo(expected));
		});
	}

	void WhenCalledWithOneVariable(std::pair<std::string,std::string> testCase) {
		auto in = testCase.first;
		auto expected = testCase.second;

		When("formatting a c-string called with " + in, [this, in]() {
			m_result = ppr::tl::string::format(in.c_str(), 10);
		});
		Then("The expected result is returned", [this, expected]() {
			AssertThat(m_result, ut11::Is::EqualTo(expected));
		});
		When("formatting a string called with " + in, [this, in]() {
			m_result = ppr::tl::string::format(in, 10);
		});
		Then("The expected result is returned", [this, expected]() {
			AssertThat(m_result, ut11::Is::EqualTo(expected));
		});
		When("formatting a string called with stream and " + in, [this, in]() {
			std::stringstream stream;
			ppr::tl::string::format(stream, in, 10);
			m_result = stream.str();
		});
		Then("The expected result is returned", [this, expected]() {
			AssertThat(m_result, ut11::Is::EqualTo(expected));
		});
		When("formatting a c-string called with stream and " + in, [this, in]() {
			std::stringstream stream;
			ppr::tl::string::format(stream, in.c_str(), 10);
			m_result = stream.str();
		});
		Then("The expected result is returned", [this, expected]() {
			AssertThat(m_result, ut11::Is::EqualTo(expected));
		});
	}

	void WhenCalledWithTwoVariables(std::pair<std::string,std::string> testCase) {
		auto in = testCase.first;
		auto expected = testCase.second;

		When("formatting a c-string called with " + in, [this, in]() {
			m_result = ppr::tl::string::format(in.c_str(), 10, 20);
		});
		Then("The expected result is returned", [this, expected]() {
			AssertThat(m_result, ut11::Is::EqualTo(expected));
		});
		When("formatting a string called with " + in, [this, in]() {
			m_result = ppr::tl::string::format(in, 10, 20);
		});
		Then("The expected result is returned", [this, expected]() {
			AssertThat(m_result, ut11::Is::EqualTo(expected));
		});
		When("formatting a string called with stream and " + in, [this, in]() {
			std::stringstream stream;
			ppr::tl::string::format(stream, in, 10, 20);
			m_result = stream.str();
		});
		Then("The expected result is returned", [this, expected]() {
			AssertThat(m_result, ut11::Is::EqualTo(expected));
		});
		When("formatting a c-string called with stream and " + in, [this, in]() {
			std::stringstream stream;
			ppr::tl::string::format(stream, in.c_str(), 10, 20);
			m_result = stream.str();
		});
		Then("The expected result is returned", [this, expected]() {
			AssertThat(m_result, ut11::Is::EqualTo(expected));
		});
	}
};

DeclareFixture(StringFormatTests)(ut11::Category("tl"));