
#include <string>
#include <ppr/tl/any.hpp>
#include <UnitTest11.hpp>

class AnyTests : public ut11::TestFixture {

	std::string m_expectedValue;
	ppr::tl::any m_any;
	ppr::tl::any m_copy;
	ppr::tl::any m_move;
	std::string m_actualValue;

	virtual void Run() {
		Then("the an unassigned any evaluates to false", [&]() {
		    ppr::tl::any unassigned;
		    AssertThat(static_cast<bool>(unassigned), ut11::Is::False);
		    AssertThat(unassigned.is<bool>(), ut11::Is::False);
		    AssertThat(unassigned.is<void*>(), ut11::Is::False);
		});

		Given("an Any", [&]() {
		    m_expectedValue = std::string("hello_world");
		    m_any = ppr::tl::any(m_expectedValue);
		}); Then("the any evaluates to true", [&]() {
			AssertThat(static_cast<bool>(m_any), ut11::Is::True);
		}); Then("checking a value is it's type returns true", [&]() {
		    AssertThat(m_any.is<std::string>(), ut11::Is::True);
		}); Then("checking a value is a differents type returns false", [&]() {
		    AssertThat(m_any.is<float>(), ut11::Is::False);
		}); When("getting the actual value as the correct type", [&]() {
		    m_actualValue = m_any.as<std::string>();
		}); Then("the expected value is returned", [&]() {
			AssertThat(m_actualValue, ut11::Is::EqualTo(m_expectedValue));
		}); When("copying that any into another any before erasing the original any and then getting the value", [&]() {
		    m_copy = ppr::tl::any(m_any);
		    m_any = ppr::tl::any();
		    m_actualValue = m_copy.as<std::string>();
		}); Then("the expected value is returned", [&]() {
		    AssertThat(m_actualValue, ut11::Is::EqualTo(m_expectedValue));
		}); When("moving that any onto another any and getting the value", [&]() {
			m_move = std::move(m_any);
		    m_actualValue = m_move.as<std::string>();
		}); Then("the expected value is returned", [&]() {
		    AssertThat(m_actualValue, ut11::Is::EqualTo(m_expectedValue));
		}); When("setting that any to a different value then getting", [&]() {
		    m_any.as<std::string>() = std::string("Hello World");
			m_expectedValue = std::string("Hello World");
		    m_actualValue = m_any.as<std::string>();
		}); Then("the expected value is returned", [&]() {
		    AssertThat(m_actualValue, ut11::Is::EqualTo(m_expectedValue));
		});

		Given("an Any created via static constructors", [&]() {
			m_expectedValue = std::string("hello_world");

			m_any = ppr::tl::any::create<std::string>(m_expectedValue.c_str());
		}); Then("the any evaluates to true", [&]() {
			AssertThat(static_cast<bool>(m_any), ut11::Is::True);
		}); Then("checking a value is it's type returns true", [&]() {
			AssertThat(m_any.is<std::string>(), ut11::Is::True);
		}); Then("checking a value is a differents type returns false", [&]() {
			AssertThat(m_any.is<float>(), ut11::Is::False);
		}); When("getting the actual value as the correct type", [&]() {
			m_actualValue = m_any.as<std::string>();
		}); Then("the expected value is returned", [&]() {
			AssertThat(m_actualValue, ut11::Is::EqualTo(m_expectedValue));
		}); When("copying that any into another any before erasing the original any and then getting the value", [&]() {
			m_copy = ppr::tl::any(m_any);
			m_any = ppr::tl::any();
			m_actualValue = m_copy.as<std::string>();
		}); Then("the expected value is returned", [&]() {
			AssertThat(m_actualValue, ut11::Is::EqualTo(m_expectedValue));
		}); When("moving that any onto another any and getting the value", [&]() {
			m_move = std::move(m_any);
			m_actualValue = m_move.as<std::string>();
		}); Then("the expected value is returned", [&]() {
			AssertThat(m_actualValue, ut11::Is::EqualTo(m_expectedValue));
		}); When("setting that any to a different value then getting", [&]() {
			m_any.as<std::string>() = std::string("Hello World");
			m_expectedValue = std::string("Hello World");
			m_actualValue = m_any.as<std::string>();
		}); Then("the expected value is returned", [&]() {
			AssertThat(m_actualValue, ut11::Is::EqualTo(m_expectedValue));
		});
	}
};
DeclareFixture(AnyTests)(ut11::Category("tl"));
