#include <ppr/tl/immutable_list.hpp>
#include <UnitTest11.hpp>
#include <atomic>

class ImmutableListTests : public ut11::TestFixture {
	virtual void Run() {
		Then("Basic construction!", []() {
		    ppr::tl::immutable_list<int> empty;
		    ppr::tl::immutable_list<int> oneEmptyTail(1, empty);
		    ppr::tl::immutable_list<int> one(1);
		    ppr::tl::immutable_list<int> two(2, one);
		    ppr::tl::immutable_list<int> three(3, two);
		    ppr::tl::immutable_list<int> four_odd(5, three);

			AssertThat(empty.empty(), ut11::Is::True);
		    AssertThat(empty.size(), ut11::Is::EqualTo(0));

		    AssertThat(oneEmptyTail.empty(), ut11::Is::False);
		    AssertThat(oneEmptyTail.size(), ut11::Is::EqualTo(1));
			AssertThat(oneEmptyTail.head(), ut11::Is::EqualTo(1));

		    AssertThat(one.empty(), ut11::Is::False);
		    AssertThat(one.size(), ut11::Is::EqualTo(1));
		    AssertThat(one.head(), ut11::Is::EqualTo(1));

		    AssertThat(two.empty(), ut11::Is::False);
		    AssertThat(two.size(), ut11::Is::EqualTo(2));
		    AssertThat(two.head(), ut11::Is::EqualTo(2));

		    AssertThat(three.size(), ut11::Is::EqualTo(3));
		    AssertThat(three.head(), ut11::Is::EqualTo(3));

		    AssertThat(four_odd.head(), ut11::Is::EqualTo(5));
		});
		Then("construction from initializer_list", []() {
		    ppr::tl::immutable_list<int> from_init_list({ 5, 3, 2, 1 });
		    AssertThat(from_init_list.head(), ut11::Is::EqualTo(5));
		    AssertThat(from_init_list.size(), ut11::Is::EqualTo(4));
		    AssertThat(from_init_list.empty(), ut11::Is::False);
		});
		Then("construction from bidirectional iterators", []() {
		    std::vector<int> source({ 5, 3, 2, 1 });
		    auto from_init_iterators = ppr::tl::immutable_list<int>::from(source.begin(), source.end());

		    AssertThat(from_init_iterators.head(), ut11::Is::EqualTo(5));
		    AssertThat(from_init_iterators.size(), ut11::Is::EqualTo(4));
		    AssertThat(from_init_iterators.empty(), ut11::Is::False);
		});
		Then("construction from not-bidirectional iterators", []() {
		    std::vector<int> source({ 5, 3, 2, 1 });

			struct not_bidirectional {
				typedef std::ptrdiff_t difference_type; //almost always ptrdif_t
				typedef int value_type; //almost always T
				typedef int const& reference; //almost always T& or const T&
				typedef const int* pointer; //almost always T* or const T*
				typedef std::forward_iterator_tag iterator_category;  //usually std::forward_iterator_tag or similar

				std::vector<int>::iterator internal;

				not_bidirectional(std::vector<int>::iterator i) : internal(i) { }

				reference operator*() const { return *internal; }
				pointer operator->() const { return internal.operator->(); }

				bool operator==(const not_bidirectional& other) { return internal == other.internal; }
				bool operator!=(const not_bidirectional& other) { return internal == other.internal; }

				not_bidirectional& operator++() { ++internal; return *this; }
				not_bidirectional operator++(int) { auto prev = internal++; return not_bidirectional(prev); }
			};

		    auto from_init_iterators = ppr::tl::immutable_list<int>::from(not_bidirectional(source.begin()), not_bidirectional(source.end()));

		    AssertThat(from_init_iterators.head(), ut11::Is::EqualTo(5));
		    AssertThat(from_init_iterators.size(), ut11::Is::EqualTo(4));
		    AssertThat(from_init_iterators.empty(), ut11::Is::False);
		});
		Then("comparisons", []() {
		    ppr::tl::immutable_list<int> iParent({2,3,4,5,6});
		    ppr::tl::immutable_list<int> i(1, {2,3,4,5,6});
		    ppr::tl::immutable_list<int> iClone(i);
		    ppr::tl::immutable_list<int> iValCopy(1, ppr::tl::immutable_list<int>(2, ppr::tl::immutable_list<int>(3, ppr::tl::immutable_list<int>(4, ppr::tl::immutable_list<int>(5, ppr::tl::immutable_list<int>(6))))));
		    ppr::tl::immutable_list<int> iForkSameVal(1, i.tail());

		    ppr::tl::immutable_list<int> iForkedFromParentSameVal(1, iParent);
		    ppr::tl::immutable_list<int> iTailOfChild(ppr::tl::immutable_list<int>(1, i).tail());

		    AssertThat(i == i, ut11::Is::True);
		    AssertThat(i == iClone, ut11::Is::True);
		    AssertThat(i == iValCopy, ut11::Is::True);
		    AssertThat(i == iForkSameVal, ut11::Is::True);
		    AssertThat(i == iForkedFromParentSameVal, ut11::Is::True);
		    AssertThat(i == iTailOfChild, ut11::Is::True);
		    AssertThat(i.tail() == iParent, ut11::Is::True);

		    AssertThat(i != i, ut11::Is::False);
		    AssertThat(i != iClone, ut11::Is::False);
		    AssertThat(i != iValCopy, ut11::Is::False);
		    AssertThat(i != iForkSameVal, ut11::Is::False);
		    AssertThat(i != iForkedFromParentSameVal, ut11::Is::False);
		    AssertThat(i != iTailOfChild, ut11::Is::False);
		    AssertThat(i.tail() != iParent, ut11::Is::False);

		    ppr::tl::immutable_list<int> iDifferent(1);
		    ppr::tl::immutable_list<int> iDifferentButSameSize({1,2,3,5,4,6});
		    ppr::tl::immutable_list<int> iForkDifferentVal(8, i.tail());
		    ppr::tl::immutable_list<int> iForkParentDifferentVal(8, iParent);

		    AssertThat(i == iDifferent, ut11::Is::False);
		    AssertThat(i == iDifferentButSameSize, ut11::Is::False);
		    AssertThat(i == iForkDifferentVal, ut11::Is::False);
		    AssertThat(i == iForkParentDifferentVal, ut11::Is::False);

		    AssertThat(i != iDifferent, ut11::Is::True);
		    AssertThat(i != iDifferentButSameSize, ut11::Is::True);
		    AssertThat(i != iForkDifferentVal, ut11::Is::True);
		    AssertThat(i != iForkParentDifferentVal, ut11::Is::True);
		});
		Then("iterate tests", []() {
		    ppr::tl::immutable_list<int> iSomeSetPre({2,9,4,5,6});
		    ppr::tl::immutable_list<int> iSomeSetPre2(3, iSomeSetPre.tail().tail());
		    ppr::tl::immutable_list<int> iSomeSetPre3(2, iSomeSetPre2);
		    ppr::tl::immutable_list<int> iSomeSet(iSomeSetPre3);

			auto first = iSomeSet.begin();
			auto end = iSomeSet.end();

		    AssertThat(first, ut11::Is::Not::EqualTo(end));
			AssertThat(*(first++), ut11::Is::EqualTo(2));
		    AssertThat(*first, ut11::Is::EqualTo(3));
		    AssertThat(*++first, ut11::Is::EqualTo(4));
		    AssertThat(*++first, ut11::Is::EqualTo(5));
		    AssertThat(*++first, ut11::Is::EqualTo(6));
		    AssertThat(++first, ut11::Is::EqualTo(end));
		});
	}
};

DeclareFixture(ImmutableListTests)(ut11::Category("tl"));
