#include <ppr/tl/thread_pool.hpp>
#include <ppr/tl/make_unique.hpp>
#include <UnitTest11.hpp>
#include <UnitTest11/Mock.hpp>

class ThreadPoolTests : public ut11::TestFixture {

	std::unique_ptr<ppr::tl::thread_pool> m_pool;
	std::vector< std::pair<std::unique_ptr<std::mutex>, ut11::Mock<void (int)>> > m_work;
	std::future<int> m_future;

	virtual void Run() {
		Given("a thread pool", [&]() {
		    m_pool = ppr::tl::make_unique<ppr::tl::thread_pool>(std::thread::hardware_concurrency());

		    m_work.clear();
		    for(auto s = 0u; s < 100u; ++s)
			    m_work.emplace_back(ppr::tl::make_unique<std::mutex>(), ut11::Mock<void (int)>());

		}); When("work is enqueued on multiple threads and then the pool is running", [&]() {
		    std::function<void (void)> joinFunction = []() { };
		    for(auto u = 0u; u < m_work.size(); ++u) {
			    auto result = std::make_shared<std::future<void>>(m_pool->enqueue([&, u](int j) {
			        std::lock_guard<std::mutex> lock(*(m_work[u].first));
			        m_work[u].second(j);
			    }, u));
			    joinFunction = [joinFunction, result]() { joinFunction(); result->wait(); };
		    }
		    m_pool->growBy(std::thread::hardware_concurrency());
		    joinFunction();
		}); Then("the work is carried out within 5 seconds", [&]() {
			auto limit = std::chrono::system_clock::now() + std::chrono::seconds(5);
			while(true) {
				try {
					for (auto v = 0u; v < m_work.size(); ++v) {
						std::lock_guard<std::mutex> lock(*(m_work[v].first));
						MockVerifyTimes(1, m_work[v].second)(v);
					}
					return;
				} catch (...) {
					if (std::chrono::system_clock::now() > limit)
						ut11::Assert::Fail(__LINE__, __FILE__, "Not all work was carried out in 1 second");
				}
			}
		});  When("work is enqueued on multiple threads and then the pool is destroyed", [&]() {
		    std::function<void (void)> joinFunction = []() { };
		    for(auto i = 0u; i < m_work.size(); ++i) {
			    auto result = std::make_shared<std::future<void>>(m_pool->enqueue([&, i](int j) {
			        std::lock_guard<std::mutex> lock(*(m_work[i].first));
			        m_work[i].second(j);
			    }, i));
			    joinFunction = [joinFunction, result]() { joinFunction(); result->wait(); };
		    }

			m_pool = std::unique_ptr<ppr::tl::thread_pool>();
		    joinFunction();
		}); Then("the work has been carried out", [&]() {
		    for(auto i = 0u; i < m_work.size(); ++i) {
			    std::lock_guard<std::mutex> lock(*(m_work[i].first));
			    MockVerifyTimes(1, m_work[i].second)(i);
		    }
		}); When("a single piece of work is enqueued", [&]() {
		    m_future = std::move(m_pool->enqueue([]() -> int { return -23; }));
		}); Then("getting the future results in the expected value", [&]() {
			AssertThat(m_future.get(), ut11::Is::EqualTo(-23));
		}); Finally("destroy the thread pool", [&]() {
		    m_pool = std::unique_ptr<ppr::tl::thread_pool>();
		});
	}
};
DeclareFixture(ThreadPoolTests)(ut11::Category("tl"));
