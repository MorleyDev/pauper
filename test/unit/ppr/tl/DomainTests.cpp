#include <ppr/tl/domain.hpp>
#include <UnitTest11.hpp>
#include <stdexcept>
#include <random>
#include <sstream>

class DomainTests : public ut11::TestFixture {
private:
	static int min() {
		return 3;
	}

	static int max() {
		return 100;
	}

	ppr::tl::domain<int, min, max> m_domain;

public:
	int generateInvalidValue() {

		static std::default_random_engine generator;
		static std::uniform_int_distribution<int> distribution(min() * 100, max() * 100);
		auto dice_roll = min();
		do {
			dice_roll = distribution(generator);
		} while (dice_roll <= max() && dice_roll >= min());
		return dice_roll;
	}

	void ThenSettingTheDomainToTheValueOutsideTheRangeThrowsTheExpectedException(int i) {
		std::stringstream description;
		description << "setting the domain to the value "
				<< i
				<< " outside the min/max range throw the expected exception";

		Then(description.str(), [&, i]() {
		    try {
			    m_domain = (i);
		    } catch (std::out_of_range e) {
			    return;
		    }
		    ut11::Assert::Fail(__LINE__, __FILE__, "No exception thrown");
		});
	}

	void ThenConstructingADomainWithAValueOutsideTheRangeThrowsTheExpectedException(int i) {
		std::stringstream description;
		description << "setting the domain to the value "
				<< i
				<< " outside the min/max range throw the expected exception";

		Then(description.str(), [&, i]() {
		    try {
			    m_domain = ppr::tl::domain<int, min, max>(i);
		    } catch (std::out_of_range e) {
			    return;
		    }
		    ut11::Assert::Fail(__LINE__, __FILE__, "No exception thrown");
		});
	}

	virtual void Run() {
		Given("a domain with a value set to the min", [&]() {
		    m_domain = min();
		});
		Then("the value is as expected", [&]() {
		    AssertThat(m_domain.get(), ut11::Is::EqualTo(min()));
		});

		Given("a domain with a value set to the max", [&]() {
		    m_domain = max();
		}); Then("the value is as expected", [&]() {
		    AssertThat(m_domain.get(), ut11::Is::EqualTo(max()));
		});

		Given("a domain", [&]() {
		    m_domain = ppr::tl::domain<int, min, max>();
		}); Then("the value is min", [&]() {
			AssertThat(m_domain.get(), ut11::Is::EqualTo(min()));
		    AssertThat(m_domain, ut11::Is::EqualTo(ppr::tl::domain<int, min, max>(min())));
		});for (auto i = 0; i < 10; ++i)
			ThenSettingTheDomainToTheValueOutsideTheRangeThrowsTheExpectedException(generateInvalidValue());
		for (auto i = 0; i < 10; ++i)
			ThenConstructingADomainWithAValueOutsideTheRangeThrowsTheExpectedException(generateInvalidValue());

		Given("a domain with a value between min and max", [&]() {
			m_domain = ppr::tl::domain<int, min, max>(9);
		}); Then("The result of that value and a mathematical operation is as expected", [&]() {
			AssertThat(m_domain + 100, ut11::Is::EqualTo(109));
		    AssertThat(m_domain - 100, ut11::Is::EqualTo(-91));
		    AssertThat(m_domain * 100, ut11::Is::EqualTo(900));
		    AssertThat(m_domain / 3, ut11::Is::EqualTo(3));

		    AssertThat(100 + m_domain, ut11::Is::EqualTo(109));
		    AssertThat(100 - m_domain, ut11::Is::EqualTo(91));
		    AssertThat(100 * m_domain, ut11::Is::EqualTo(900));
		    AssertThat(90 / m_domain, ut11::Is::EqualTo(10));
		}); Then("The result of that value and a mathematical operation on another domain is as expected", [&]() {
		    AssertThat(m_domain + ppr::tl::domain<int, min, max>(100), ut11::Is::EqualTo(109));
		    AssertThat(m_domain - ppr::tl::domain<int, min, max>(100), ut11::Is::EqualTo(-91));
		    AssertThat(m_domain * ppr::tl::domain<int, min, max>(100), ut11::Is::EqualTo(900));
		    AssertThat(m_domain / ppr::tl::domain<int, min, max>(3), ut11::Is::EqualTo(3));
		}); Then("The result of that value and a self-mutating valid mathematical operation is as expected", [&]() {
		    AssertThat((m_domain += 50).get(), ut11::Is::EqualTo(59));
		    AssertThat((m_domain -= 50).get(), ut11::Is::EqualTo(9));
		    AssertThat((m_domain *= 9).get(), ut11::Is::EqualTo(81));
		    AssertThat((m_domain /= 3).get(), ut11::Is::EqualTo(27));
		}); Then("The result of that value and an invalid self-mutating valid mathematical += operation is the expected exception being thrown", [&]() {
		    try {
			    m_domain += 500;
		    } catch(std::out_of_range e) {
			    return;
		    }
		    ut11::Assert::Fail(__LINE__, __FILE__, "No exception thrown");
		}); Then("The result of that value and an invalid self-mutating valid mathematical -= operation is the expected exception being thrown", [&]() {
		    try {
			    m_domain -= 500;
		    } catch(std::out_of_range e) {
			    return;
		    }
		    ut11::Assert::Fail(__LINE__, __FILE__, "No exception thrown");
		}); Then("The result of that value and an invalid self-mutating valid mathematical -= operation is the expected exception being thrown", [&]() {
		    try {
			    m_domain *= 500;
		    } catch(std::out_of_range e) {
			    return;
		    }
		    ut11::Assert::Fail(__LINE__, __FILE__, "No exception thrown");
		}); Then("The result of that value and an invalid self-mutating valid mathematical -= operation is the expected exception being thrown", [&]() {
		    try {
			    m_domain /= 1000;
		    } catch(std::out_of_range e) {
			    return;
		    }
		    ut11::Assert::Fail(__LINE__, __FILE__, "No exception thrown");
		}); Then("The result of that value and a self-mutating valid mathematical operation on another value is as expected", [&]() {
			auto i = 5;
		    AssertThat(i += m_domain, ut11::Is::EqualTo(14));
		    AssertThat(i -= m_domain, ut11::Is::EqualTo(5));
		    AssertThat(i *= m_domain, ut11::Is::EqualTo(45));
		    AssertThat(i /= m_domain, ut11::Is::EqualTo(5));
		}); Then("The comparisons with another domain work as expected", [&]() {
		    AssertThat(m_domain == ppr::tl::domain<int, min, max>(9), ut11::Is::True);
		    AssertThat(m_domain != ppr::tl::domain<int, min, max>(9), ut11::Is::False);
		    AssertThat(m_domain == ppr::tl::domain<int, min, max>(8), ut11::Is::False);
		    AssertThat(m_domain != ppr::tl::domain<int, min, max>(8), ut11::Is::True);
		    AssertThat(m_domain >  ppr::tl::domain<int, min, max>(9), ut11::Is::False);
		    AssertThat(m_domain >  ppr::tl::domain<int, min, max>(8), ut11::Is::True);
		    AssertThat(m_domain <  ppr::tl::domain<int, min, max>(9), ut11::Is::False);
		    AssertThat(m_domain <  ppr::tl::domain<int, min, max>(8), ut11::Is::False);
		    AssertThat(m_domain <= ppr::tl::domain<int, min, max>(9), ut11::Is::True);
		    AssertThat(m_domain <= ppr::tl::domain<int, min, max>(8), ut11::Is::False);
		    AssertThat(m_domain <= ppr::tl::domain<int, min, max>(10), ut11::Is::True);
		    AssertThat(m_domain >= ppr::tl::domain<int, min, max>(9), ut11::Is::True);
		    AssertThat(m_domain >= ppr::tl::domain<int, min, max>(8), ut11::Is::True);
		    AssertThat(m_domain >= ppr::tl::domain<int, min, max>(10), ut11::Is::False);
		}); Then("The comparisons with a value work as expected", [&]() {
		    AssertThat(m_domain == 9, ut11::Is::True);
		    AssertThat(m_domain != 9, ut11::Is::False);
		    AssertThat(m_domain == 8, ut11::Is::False);
		    AssertThat(m_domain != 8, ut11::Is::True);
		    AssertThat(m_domain >  9, ut11::Is::False);
		    AssertThat(m_domain >  8, ut11::Is::True);
		    AssertThat(m_domain <  9, ut11::Is::False);
		    AssertThat(m_domain <  8, ut11::Is::False);
		    AssertThat(m_domain <= 9, ut11::Is::True);
		    AssertThat(m_domain <= 8, ut11::Is::False);
		    AssertThat(m_domain <= 10, ut11::Is::True);
		    AssertThat(m_domain >= 9, ut11::Is::True);
		    AssertThat(m_domain >= 8, ut11::Is::True);
		    AssertThat(m_domain >= 10, ut11::Is::False);

		    AssertThat(9 == m_domain,  ut11::Is::True);
		    AssertThat(9 != m_domain,  ut11::Is::False);
		    AssertThat(8 == m_domain,  ut11::Is::False);
		    AssertThat(8 != m_domain,  ut11::Is::True);
		    AssertThat(9 >  m_domain,  ut11::Is::False);
		    AssertThat(8 >  m_domain,  ut11::Is::False);
		    AssertThat(9 <  m_domain,  ut11::Is::False);
		    AssertThat(8 <  m_domain,  ut11::Is::True);
		    AssertThat(9 <= m_domain,  ut11::Is::True);
		    AssertThat(8 <= m_domain,  ut11::Is::True);
		    AssertThat(10 <= m_domain, ut11::Is::False);
		    AssertThat(9 >= m_domain,  ut11::Is::True);
		    AssertThat(8 >= m_domain,  ut11::Is::False);
		    AssertThat(10 >= m_domain, ut11::Is::True);
		}); Then("Constratining a value inside the domain gives the value", [this]() {
			AssertThat(ppr::tl::domain<int,min,max>::constrain(20), ut11::Is::EqualTo(20));
		}); Then("Constratining a value below the domain gives the minimum", [this]() {
			AssertThat(ppr::tl::domain<int,min,max>::constrain(2), ut11::Is::EqualTo(3));
		}); Then("Constratining a value above the domain gives the maximum", [this]() {
			AssertThat(ppr::tl::domain<int,min,max>::constrain(2542), ut11::Is::EqualTo(100));
		});
	}
};
DeclareFixture(DomainTests)(ut11::Category("tl"));
