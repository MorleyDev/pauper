#include <ppr/tl/random/one_of.hpp>

#include <UnitTest11.hpp>
#include <UnitTest11/Mock.hpp>

class RandomOneOfTests : public ut11::TestFixture {
private:
	struct Generator {
		typedef std::uint_fast32_t result_type;

		static result_type min() { return std::numeric_limits<std::uint_fast32_t>::min(); }
		static result_type max() { return std::numeric_limits<std::uint_fast32_t>::max(); }

		ut11::Mock<std::uint_fast32_t (void)> mock;

		result_type operator()() {
			return mock();
		}
	} m_mockRandomNumberGenerator;

	std::vector<std::int32_t> m_values;
	std::vector<std::int32_t> m_results;

public:
	virtual void Run() {
		Given("a random number generator", [this]() {
			m_mockRandomNumberGenerator = Generator();
		});
		When("getting a random one of a set of integers", [this]() {
			for(auto i = 0; i < 100; ++i) {
				m_mockRandomNumberGenerator.mock.SetReturn(i);

				m_results.push_back(ppr::tl::random::one_of(m_mockRandomNumberGenerator, 52, 65, 42, 45));
			}
			m_values.push_back(52);
			m_values.push_back(65);
			m_values.push_back(42);
			m_values.push_back(45);

		}); Then("random numbers are retrived", [this]() {
			MockVerify(m_mockRandomNumberGenerator.mock)();
		}); Then("every value can be returned", [this]() {
			AssertThat(m_results, ut11::Is::Iterable::Containing::Subset(m_results));
		});
	}
};
DeclareFixture(RandomOneOfTests)(ut11::Category("tl"));
