#include <ppr/tl/serialise.hpp>
#include <UnitTest11.hpp>

namespace {
    struct SerialisableNest {
	    std::size_t x;
    };
    struct SerialisableRoot {
	    std::size_t y;
	    std::size_t t;
	    std::size_t v;
	    SerialisableNest n;
    };

    template<typename ARCHIVE>
    void serialize(ARCHIVE& a, SerialisableNest& m) {
	    a(ppr::tl::make_nvp("x", m.x));
    }

    template<typename ARCHIVE>
    void serialize(ARCHIVE& a, SerialisableRoot& m) {
	    a(ppr::tl::make_nvp("y", m.y), ppr::tl::make_nvp("t", m.t), ppr::tl::make_nvp("v", m.v), ppr::tl::make_nvp("n", m.n));
    }
}

class SerialisationTests : public ut11::TestFixture {

	SerialisableRoot m_serial;
	std::string m_serialised;

	virtual void Run() {
		Given("a serialisable object", [this]() {
			m_serial.y = 10;
			m_serial.t = 52;
		    m_serial.v = 19;
			m_serial.n.x = 1234;
		}); When("serialising to json", [this]() {
		    m_serialised = ppr::tl::to_json(m_serial);
		}); Then("the expected result is returned", [this]() {
		    std::string expected =
				    "{\n"
				    "    \"y\": 10,\n"
				    "    \"t\": 52,\n"
				    "    \"v\": 19,\n"
				    "    \"n\": {\n"
				    "        \"x\": 1234\n"
				    "    }\n"
				    "}";

			AssertThat(m_serialised, ut11::Is::EqualTo(expected));
		});
	}
};
DeclareFixture(SerialisationTests)(ut11::Category("tl"));

class DeserialisationTests : public ut11::TestFixture {

	SerialisableRoot m_deserialised;
	std::string m_serialised;

	virtual void Run() {
		Given("a deserialisable string", [this]() {
		    m_serialised = "{\n"
				    "    \"y\": 10,\n"
				    "    \"t\": 52,\n"
				    "    \"v\": 19,\n"
				    "    \"n\": {\n"
				    "        \"x\": 1234\n"
				    "    }\n"
				    "}";

		}); When("serialising to json", [this]() {
		    m_deserialised = ppr::tl::from_json<SerialisableRoot>(m_serialised);
		}); Then("the expected result is returned", [this]() {
		    SerialisableRoot expected;
		    expected.y = 10;
		    expected.t = 52;
		    expected.v = 19;
		    expected.n.x = 1234;

		    AssertThat(m_deserialised.y, ut11::Is::EqualTo(expected.y));
		    AssertThat(m_deserialised.t, ut11::Is::EqualTo(expected.t));
		    AssertThat(m_deserialised.v, ut11::Is::EqualTo(expected.v));
		    AssertThat(m_deserialised.n.x, ut11::Is::EqualTo(expected.n.x));
		});
	}
};
DeclareFixture(DeserialisationTests)(ut11::Category("tl"));
