#include <ppr/tl/either.hpp>
#include <UnitTest11.hpp>
#include <UnitTest11/Mock.hpp>

class EitherTests : public ut11::TestFixture {

	ppr::tl::either<float, int> m_either = ppr::tl::either<float, int>::left(0.0f);
	float m_leftValue;
	int m_rightValue;

	virtual void Run() {
		Given("an either with a left value", [&]() {
		    m_leftValue = 52.0f;
			m_either = ppr::tl::either<float, int>::left(m_leftValue);
		}); Then("a fold calls the left function as expected", [&]() {
		    ut11::Mock<char (float)> mockLeft;
		    ut11::Mock<char (int)> mockRight;
			mockLeft.SetReturn('z');

			auto result = m_either.fold(std::ref(mockLeft), std::ref(mockRight));

		    MockVerifyTimes(1, mockLeft)(m_leftValue);
		    MockVerifyTimes(0, mockRight)(ut11::Is::Any<int>());
			AssertThat(result, ut11::Is::EqualTo('z'));

		}); Then("a fold with a void return calls the left function as expected", [&]() {
		    ut11::Mock<void (float)> mockLeft;
		    ut11::Mock<void (int)> mockRight;
		    m_either.fold(std::ref(mockLeft), std::ref(mockRight));
		    MockVerifyTimes(1, mockLeft)(m_leftValue);
		    MockVerifyTimes(0, mockRight)(ut11::Is::Any<int>());
		}); Then("the value is as expected", [&]() {
			AssertThat(m_either.left(), ut11::Is::EqualTo(m_leftValue));
		}); Then("the value is left", [&](){
			AssertThat(m_either.isLeft(), ut11::Is::True);
		}); Then("the value is not right", [&](){
		    AssertThat(m_either.isRight(), ut11::Is::False);
		}); Then("a get of the left returns a pointer to the expected value", [&]() {
		    AssertThat(*(m_either.getLeft()), ut11::Is::EqualTo(m_leftValue));
		}); Then("a get of the right returns nullptr", [&]() {
			AssertThat(m_either.getRight(), ut11::Is::Null);
		}); Then("the value of a copy is as expected", [&]() {
		    ppr::tl::either<float, int> copy(m_either);
		    AssertThat(copy.left(), ut11::Is::EqualTo(m_leftValue));
		}); Then("the value of a copy is left", [&](){
		    ppr::tl::either<float, int> copy(m_either);
		    AssertThat(copy.isLeft(), ut11::Is::True);
		}); Then("the value of a copy is not right", [&](){
		    ppr::tl::either<float, int> copy(m_either);
		    AssertThat(copy.isRight(), ut11::Is::False);
		}); Then("a get of the left of a copy returns a pointer to the expected value", [&]() {
		    ppr::tl::either<float, int> copy(m_either);
		    AssertThat(*(copy.getLeft()), ut11::Is::EqualTo(m_leftValue));
		}); Then("a get of the right of a copy  returns nullptr", [&]() {
		    ppr::tl::either<float, int> copy(m_either);
		    AssertThat(copy.getRight(), ut11::Is::Null);
		});

		Given("an either with a right value", [&]() {
		    m_rightValue = 1253;
		    m_either = ppr::tl::either<float, int>::right(m_rightValue);
		}); Then("a fold calls the right function as expected", [&]() {
		    ut11::Mock<char (float)> mockLeft;
		    ut11::Mock<char (int)> mockRight;
		    mockRight.SetReturn('z');

		    auto result = m_either.fold(std::ref(mockLeft), std::ref(mockRight));

		    MockVerifyTimes(0, mockLeft)(ut11::Is::Any<float>());
		    MockVerifyTimes(1, mockRight)(m_rightValue);
		    AssertThat(result, ut11::Is::EqualTo('z'));

		}); Then("a fold with a void return calls the right function as expected", [&]() {
		    ut11::Mock<void (float)> mockLeft;
		    ut11::Mock<void (int)> mockRight;
			m_either.fold(std::ref(mockLeft), std::ref(mockRight));
		    MockVerifyTimes(0, mockLeft)(ut11::Is::Any<float>());
		    MockVerifyTimes(1, mockRight)(m_rightValue);
		}); Then("the value is as expected", [&]() {
		    AssertThat(m_either.right(), ut11::Is::EqualTo(m_rightValue));
		}); Then("the value is right", [&](){
		    AssertThat(m_either.isRight(), ut11::Is::True);
		}); Then("the value is not left", [&](){
		    AssertThat(m_either.isLeft(), ut11::Is::False);
		}); Then("a get of the right returns a pointer to the expected value", [&]() {
		    AssertThat(*(m_either.getRight()), ut11::Is::EqualTo(m_rightValue));
		}); Then("a get of the left returns nullptr", [&]() {
		    AssertThat(m_either.getLeft(), ut11::Is::Null);
		}); Then("the value of a copy is as expected", [&]() {
		    ppr::tl::either<float, int> copy(m_either);
		    AssertThat(copy.right(), ut11::Is::EqualTo(m_rightValue));
		}); Then("the value of a copy is right", [&](){
		    ppr::tl::either<float, int> copy(m_either);
		    AssertThat(copy.isRight(), ut11::Is::True);
		}); Then("the value of a copy is not left", [&](){
		    ppr::tl::either<float, int> copy(m_either);
		    AssertThat(copy.isLeft(), ut11::Is::False);
		}); Then("a get of the right of a copy returns a pointer to the expected value", [&]() {
		    ppr::tl::either<float, int> copy(m_either);
		    AssertThat(*(copy.getRight()), ut11::Is::EqualTo(m_rightValue));
		}); Then("a get of the left of a copy  returns nullptr", [&]() {
		    ppr::tl::either<float, int> copy(m_either);
		    AssertThat(copy.getLeft(), ut11::Is::Null);
		});
	}
};
DeclareFixture(EitherTests)(ut11::Category("tl"));
