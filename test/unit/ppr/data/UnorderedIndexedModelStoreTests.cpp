#include <ppr/data/UnorderedIndexedModelStore.hpp>
#include <UnitTest11.hpp>
#include <UnitTest11/Mock.hpp>

class UnorderedIndexedModelStoreTests : public ut11::TestFixture {
private:
	struct MockModel {
		int i;
	};
	ppr::data::UnorderedIndexedModelStore<int, MockModel> modelStore;
	std::unordered_map<int,MockModel> filteredModels;
	std::vector<int> mappedModels;
	ut11::Mock<void (int)> mockEachOperand;
	ppr::tl::optional<std::pair<const int,MockModel>> firstResult;

public:
	virtual void Run() {
		Given("a model store with added and removed items", [this]() {
			modelStore = ppr::data::UnorderedIndexedModelStore<int, MockModel>();
			modelStore.add(std::make_pair<int,MockModel>(0, { 0 }));
			modelStore.add(1, { 1 });
			modelStore.add(2, { 2 });
			modelStore.add(2, { 2002 });
			modelStore.add(std::make_pair<int,MockModel>(3, { 3 }));
			modelStore.add(4, { 4 });
			modelStore.add(40, { 4 });
			modelStore.add(5, { 5 });
			modelStore.add(41, { 4 });
			modelStore.add(404, { 404 });

			modelStore.remove(404);
			modelStore.removeIf([](std::pair<int,MockModel> const& model) { return model.second.i == 4; });
		});
		Then("the model store is not empty", [this]() {
			AssertThat(modelStore.empty(), ut11::Is::False);
		});
		Then("an all with a predicate that is true returns true", [this]() {
			AssertThat(modelStore.all([](std::pair<int,MockModel> const& model) { return true; }), ut11::Is::True);
		});
		Then("an all with a predicate that is false for a single item returns false", [this]() {
			AssertThat(modelStore.all([](std::pair<int,MockModel> const& model) { return model.second.i != 3; }), ut11::Is::False);
		});
		Then("an any with a predicate that is false for all items returns false", [this]() {
			AssertThat(modelStore.any([](std::pair<int,MockModel> const& model) { return false; }), ut11::Is::False);
		});
		Then("an any with a predicate that is true for a single item returns true", [this]() {
			AssertThat(modelStore.any([](std::pair<int,MockModel> const& model) { return model.first == 3; }), ut11::Is::True);
		});
		Then("getting an item with a key that exists returns that item", [this]() {
			ppr::tl::optional<MockModel> result = modelStore.get(5);
			AssertThat(static_cast<bool>(result), ut11::Is::True);
			AssertThat(result->i, ut11::Is::EqualTo(5));
		});
		Then("getting an item with a key that does not exist returns an empty optional", [this]() {
			ppr::tl::optional<MockModel> result = modelStore.get(404);
			AssertThat(static_cast<bool>(result), ut11::Is::False);
		});
		When("getting items with a filter", [this]() {
			filteredModels = modelStore.filter([](std::pair<int,MockModel> const& model) { return (model.first % 2) == 0; });
		});
		Then("the expected items are returned", [this]() {
			AssertThat(filteredModels.size(), ut11::Is::EqualTo(2));
			AssertThat(filteredModels[0].i, ut11::Is::EqualTo(0));
			AssertThat(filteredModels[2].i, ut11::Is::EqualTo(2));
		});
		When("getting mapping the models to another value", [this]() {
			mappedModels = modelStore.map<int>([](std::pair<int,MockModel> const& model) { return model.second.i; });
		});
		Then("the expected items are returned", [this]() {
			std::vector<int> expected = { 0, 1, 2, 3, 5 };

			AssertThat(mappedModels, ut11::Is::Iterable::EquivalentTo(expected));
		});
		When("applying an each on every operand", [this]() {
			mockEachOperand = ut11::Mock<void (int)>();

			modelStore.each([this](std::pair<int,MockModel> const& model) { mockEachOperand(model.first); });
		});
		Then("the each operand is called on every model", [this]() {
			MockVerifyTimes(1, mockEachOperand)(0);
			MockVerifyTimes(1, mockEachOperand)(1);
			MockVerifyTimes(1, mockEachOperand)(2);
			MockVerifyTimes(1, mockEachOperand)(3);
			MockVerifyTimes(1, mockEachOperand)(5);
		});
		When("getting the first where model exists for which a predicate is true", [this]() {
			firstResult = modelStore.first([](std::pair<int,MockModel> const& model) { return model.first == 5; });
		});
		Then("an expected model is returned", [this]() {
			AssertThat(static_cast<bool>(firstResult), ut11::Is::True);
			AssertThat(firstResult->first, ut11::Is::EqualTo(5));
		});
		When("getting the first where model exists for which a predicate is not true", [this]() {
			firstResult = modelStore.first([](std::pair<const int,MockModel> const& model) { return false; });
		});
		Then("an expected none is returned", [this]() {
			AssertThat(static_cast<bool>(firstResult), ut11::Is::False);
		});
		When("mutating the data and iterating over each item", [this]() {
			modelStore.mutate([this](std::pair<const int,MockModel>& model) { model.second.i *= 2; });

			mockEachOperand = ut11::Mock<void (int)>();

			modelStore.each([this](std::pair<const int,MockModel> const& model) { mockEachOperand(model.second.i); });
		});
		Then("the each is ran over the updated items", [this]() {
			MockVerifyTimes(1, mockEachOperand)(0);
			MockVerifyTimes(1, mockEachOperand)(2);
			MockVerifyTimes(1, mockEachOperand)(4);
			MockVerifyTimes(1, mockEachOperand)(6);
			MockVerifyTimes(1, mockEachOperand)(10);
		});

		Given("a model store with added items and then all items removed", [this]() {
			modelStore = ppr::data::UnorderedIndexedModelStore<int, MockModel>();
			modelStore.add(0, { 0 });
			modelStore.add(1, { 1 });
			modelStore.add(2, { 2 });
			modelStore.add(3, { 3 });
			modelStore.add(4, { 4 });
			modelStore.add(5, { 5 });

			modelStore.removeAll();
		});
		Then("the model store is empty", [this]() {
			AssertThat(modelStore.empty(), ut11::Is::True);
		});
		When("applying an each on every operand", [this]() {
			mockEachOperand = ut11::Mock<void (int)>();

			modelStore.each([this](std::pair<int,MockModel> const& model) { mockEachOperand(model.first); });
		});
		Then("the operand is never invoked", [this]() {
			MockVerifyTimes(0, mockEachOperand)(ut11::Is::Any<int>());
		});
	}
}; DeclareFixture(UnorderedIndexedModelStoreTests)(ut11::Category("data"));
