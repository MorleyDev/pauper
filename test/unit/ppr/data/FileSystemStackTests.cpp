#include <ppr/data/FileSystemStack.hpp>
#include <ppr/tl/make_unique.hpp>
#include <UnitTest11.hpp>
#include <UnitTest11/Mock.hpp>

namespace {
	typedef ut11::Mock<std::shared_ptr<ppr::tl::optional<ppr::data::ReadOnlyFile>> (std::string)> MockFileSystemOpen;

	class MockSharedFileSystem : public ppr::data::FileSystem {
	public:
		std::shared_ptr<MockFileSystemOpen> mockopen;

		MockSharedFileSystem()
				: mockopen(std::make_shared<MockFileSystemOpen>()) {
			mockopen->SetReturn(std::make_shared<ppr::tl::optional<ppr::data::ReadOnlyFile>>());
		}

		~MockSharedFileSystem() { }

		virtual ppr::tl::optional<ppr::data::ReadOnlyFile> open(std::string filename) const {
			return std::move(*(*mockopen)(filename));
		}
	};
}

class FileSystemStackTests : public ut11::TestFixture {
	std::vector< std::shared_ptr<MockFileSystemOpen> > mockFileSystems;
	std::unique_ptr<ppr::data::FileSystemStack> fileSystemStack;
	std::string openedFileName;
	ppr::tl::optional<ppr::data::ReadOnlyFile> returnedFile;

	virtual void Run() {
		Given("a file system stack with added file systems", [this]() {
			mockFileSystems.clear();
			fileSystemStack = ppr::tl::make_unique<ppr::data::FileSystemStack>();

			std::vector< std::unique_ptr<MockSharedFileSystem> > fileSystems;
			fileSystems.push_back(ppr::tl::make_unique<MockSharedFileSystem>());
			fileSystems.push_back(ppr::tl::make_unique<MockSharedFileSystem>());
			fileSystems.push_back(ppr::tl::make_unique<MockSharedFileSystem>());
			fileSystems.push_back(ppr::tl::make_unique<MockSharedFileSystem>());
			fileSystems.push_back(ppr::tl::make_unique<MockSharedFileSystem>());

			for(auto& fs : fileSystems) {
				mockFileSystems.push_back(fs->mockopen);
				fileSystemStack->push(std::move(fs));
			}
		}); When("getting a file from the file systems", [this]() {
			mockFileSystems[2]->SetReturn( std::make_shared<ppr::tl::optional<ppr::data::ReadOnlyFile>>( ppr::data::ReadOnlyFile(std::unique_ptr<std::istream>(), 1253) ) );

			openedFileName = "some_file_.txt";
			returnedFile = fileSystemStack->open(openedFileName);

		}); Then("the file systems were queried in stack order until the file was loaded", [this]() {
			for(auto i = 2u; i < mockFileSystems.size(); ++i) {
				MockVerifyTimes(1, *(mockFileSystems[i]))(openedFileName);
			}
		}); Then("the file systems after the file was loaded were not queried", [this]() {
			for(auto i = 0u; i < 2u; ++i) {
				MockVerifyTimes(0, *(mockFileSystems[i]))(openedFileName);
			}
		}); Then("the expected file was returned", [this]() {
			AssertThat(static_cast<bool>(returnedFile), ut11::Is::True);
			AssertThat(returnedFile->size(), ut11::Is::EqualTo(1253));
		}); When("getting a file from the file systems fails", [this]() {
			openedFileName = "some_file_.txt";
			returnedFile = fileSystemStack->open(openedFileName);
		}); Then("the file systems were queried in stack order", [this]() {
			for(auto i = 0u; i < mockFileSystems.size(); ++i) {
				MockVerifyTimes(1, *(mockFileSystems[i]))(openedFileName);
			}
		});  Then("the expected None was returned", [this]() {
			AssertThat(static_cast<bool>(returnedFile), ut11::Is::False);
		});
	}
};
DeclareFixture(FileSystemStackTests)(ut11::Category("data"));
