#include <ppr/data/EventQueue.hpp>
#include <ppr/tl/make_unique.hpp>
#include <UnitTest11.hpp>
#include <UnitTest11/Mock.hpp>

class EventQueueTests : public ut11::TestFixture {

	ut11::Mock<void (int)> mockIntEventHandler;
	ut11::Mock<void (float)> mockFloatEventHandler;
	ut11::Mock<void (char)> mockCharEventHandler;
	ut11::Mock<void (std::string)> mockStringEventHandler;
	ut11::Mock<void (ppr::tl::any)> mockOtherEventHandler;

	std::unique_ptr<ppr::data::EventQueue> m_eventQueue;

	virtual void Run() {
		Given("an event queue with enqueued data", [&]() {

			m_eventQueue = ppr::tl::make_unique<ppr::data::EventQueue>();
			m_eventQueue->enqueue<int>(10);
		    m_eventQueue->enqueue<int>(15);
		    m_eventQueue->enqueue<int>(20);

		    m_eventQueue->enqueue<std::string>("hello world");

		    m_eventQueue->enqueue<float>(10.0f);
		    m_eventQueue->enqueue<float>(30.5f);
		    m_eventQueue->enqueue<float>(12.2f);

		    m_eventQueue->enqueue<char>('a');
		    m_eventQueue->enqueue<char>('Z');
		    m_eventQueue->enqueue<char>('b');


		}); When("dequeing the data", [&]() {
		    mockIntEventHandler = ut11::Mock<void (int)>();
		    mockFloatEventHandler = ut11::Mock<void (float)>();
		    mockCharEventHandler = ut11::Mock<void (char)>();
		    mockStringEventHandler = ut11::Mock<void (std::string)>();
		    mockStringEventHandler.SetCallback([this](std::string n) {
		        m_eventQueue->enqueue<float>(450.0f);
		    });

			m_eventQueue->dequeue([&](std::type_index info, ppr::tl::any data) {
				if (info == std::type_index(typeid(int)))
					mockIntEventHandler(data.as<int>());
				else if (info == std::type_index(typeid(float)))
					mockFloatEventHandler(data.as<float>());
				else if (info == std::type_index(typeid(char)))
					mockCharEventHandler(data.as<char>());
				else if (info == std::type_index(typeid(std::string)))
					mockStringEventHandler(data.as<std::string>());
				else
					mockOtherEventHandler(data);
			});
		}); Then("the data was dequeued", [&]() {
			MockVerifyTimes(1, mockIntEventHandler)(10);
		    MockVerifyTimes(1, mockIntEventHandler)(15);
		    MockVerifyTimes(1, mockIntEventHandler)(20);

		    MockVerifyTimes(1, mockFloatEventHandler)(10.0f);
		    MockVerifyTimes(1, mockFloatEventHandler)(30.5f);
		    MockVerifyTimes(1, mockFloatEventHandler)(12.2f);
		    MockVerifyTimes(1, mockFloatEventHandler)(450.0f);

		    MockVerifyTimes(1, mockCharEventHandler)('a');
		    MockVerifyTimes(1, mockCharEventHandler)('Z');
		    MockVerifyTimes(1, mockCharEventHandler)('b');

		    MockVerifyTimes(1, mockStringEventHandler)("hello world");
		}); Then("no other data was dequeued", [&]() {
			MockVerifyTimes(0, mockOtherEventHandler)(ut11::Is::Any<ppr::tl::any>());
		});
	}
};
DeclareFixture(EventQueueTests)(ut11::Category("data"));
