#include <ppr/data/FlatModelStore.hpp>
#include <UnitTest11.hpp>
#include <UnitTest11/Mock.hpp>

class FlatModelStoreTests : public ut11::TestFixture {
private:
	struct MockModel {
		int i;
	};
	ppr::data::FlatModelStore<MockModel> modelStore;
	std::vector<MockModel> filteredModels;
	std::vector<int> mappedModels;
	ut11::Mock<void (int)> mockEachOperand;
	ppr::tl::optional<MockModel> firstResult;

public:
	virtual void Run() {
		Given("a model store with added and removed items", [this]() {
			modelStore = ppr::data::FlatModelStore<MockModel>();
			modelStore.add({ 0 });
			modelStore.add({ 1 });
			modelStore.add({ 2 });
			modelStore.add({ 3 });
			modelStore.add({ 4 });
			modelStore.add({ 4 });
			modelStore.add({ 5 });
			modelStore.add({ 5 });

			modelStore.removeIf([](MockModel const& model) { return model.i == 4; });
		});
		Then("the model store is not empty", [this]() {
			AssertThat(modelStore.empty(), ut11::Is::False);
		});
		Then("an all with a predicate that is true returns true", [this]() {
			AssertThat(modelStore.all([](MockModel const& model) { return true; }), ut11::Is::True);
		});
		Then("an all with a predicate that is false for a single item returns false", [this]() {
			AssertThat(modelStore.all([](MockModel const& model) { return model.i != 3; }), ut11::Is::False);
		});
		Then("an any with a predicate that is false for all items returns false", [this]() {
			AssertThat(modelStore.any([](MockModel const& model) { return false; }), ut11::Is::False);
		});
		Then("an any with a predicate that is true for a single item returns true", [this]() {
			AssertThat(modelStore.any([](MockModel const& model) { return model.i == 3; }), ut11::Is::True);
		});
		When("getting items with a filter", [this]() {
			filteredModels = modelStore.filter([](MockModel const& model) { return (model.i % 2) == 0; });
		});
		Then("the expected items are returned", [this]() {
			std::sort(filteredModels.begin(), filteredModels.end(), [](MockModel const& a, MockModel const& b) {
				return a.i < b.i;
			});
			AssertThat(filteredModels.size(), ut11::Is::EqualTo(2));
			AssertThat(filteredModels.at(0).i, ut11::Is::EqualTo(0));
			AssertThat(filteredModels.at(1).i, ut11::Is::EqualTo(2));
		});
		When("getting mapping the models to another value", [this]() {
			mappedModels = modelStore.map<int>([](MockModel const& model) { return model.i; });
		});
		Then("the expected items are returned", [this]() {
			std::vector<int> expected = { 0, 1, 2, 3, 5, 5 };

			AssertThat(mappedModels, ut11::Is::Iterable::EquivalentTo(expected));
		});
		When("applying an each on every operand", [this]() {
			mockEachOperand = ut11::Mock<void (int)>();

			modelStore.each([this](MockModel const& model) { mockEachOperand(model.i); });
		});
		Then("the each operand is called on every model", [this]() {
			MockVerifyTimes(1, mockEachOperand)(0);
			MockVerifyTimes(1, mockEachOperand)(1);
			MockVerifyTimes(1, mockEachOperand)(2);
			MockVerifyTimes(1, mockEachOperand)(3);
			MockVerifyTimes(2, mockEachOperand)(5);
		});
		When("getting the first where model exists for which a predicate is true", [this]() {
			firstResult = modelStore.first([](MockModel const& model) { return model.i == 5; });
		});
		Then("an expected model is returned", [this]() {
			AssertThat(static_cast<bool>(firstResult), ut11::Is::True);
			AssertThat(firstResult->i, ut11::Is::EqualTo(5));
		});
		When("getting the first where model exists for which a predicate is not true", [this]() {
			firstResult = modelStore.first([](MockModel const& model) { return false; });
		});
		Then("an expected none is returned", [this]() {
			AssertThat(static_cast<bool>(firstResult), ut11::Is::False);
		});
		When("mutating the data and iterating over each item", [this]() {
			modelStore.mutate([this](MockModel& model) { model.i *= 2; });

			mockEachOperand = ut11::Mock<void (int)>();

			modelStore.each([this](MockModel const& model) { mockEachOperand(model.i); });
		});
		Then("the each is ran over the updated items", [this]() {
			MockVerifyTimes(1, mockEachOperand)(0);
			MockVerifyTimes(1, mockEachOperand)(2);
			MockVerifyTimes(1, mockEachOperand)(4);
			MockVerifyTimes(1, mockEachOperand)(6);
			MockVerifyTimes(2, mockEachOperand)(10);
		});

		Given("a model store with added items and then all items removed", [this]() {
			modelStore = ppr::data::FlatModelStore<MockModel>();
			modelStore.add({ 0 });
			modelStore.add({ 1 });
			modelStore.add({ 2 });
			modelStore.add({ 3 });
			modelStore.add({ 4 });
			modelStore.add({ 4 });
			modelStore.add({ 5 });

			modelStore.removeAll();
		});
		Then("the model store is empty", [this]() {
			AssertThat(modelStore.empty(), ut11::Is::True);
		});
		When("applying an each on every operand", [this]() {
			mockEachOperand = ut11::Mock<void (int)>();

			modelStore.each([this](MockModel const& model) { mockEachOperand(model.i); });
		});
		Then("the operand is never invoked", [this]() {
			MockVerifyTimes(0, mockEachOperand)(ut11::Is::Any<int>());
		});
	}
}; DeclareFixture(FlatModelStoreTests)(ut11::Category("data"));
