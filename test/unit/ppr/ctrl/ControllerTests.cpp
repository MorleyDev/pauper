#include <ppr/ctrl/Controller.hpp>
#include <ppr/tl/make_unique.hpp>
#include <UnitTest11.hpp>
#include <UnitTest11/Mock.hpp>

namespace {
	struct FakeController : public ppr::ctrl::Controller {
		std::future<void> update(std::chrono::microseconds dt, ppr::tl::thread_pool& pool) {
			return ppr::tl::future::value();
		}
	};
}

class ControllerTests : public ut11::TestFixture {
	ppr::data::EventQueue m_eventQueue;
	std::unique_ptr<ppr::ctrl::Controller> m_controller;
	ut11::Mock<void (int)> m_mockIntHandler;
	ut11::Mock<void (float)> m_mockFloatHandler;
	ut11::Mock<void (char)> m_mockCharHandler;

	std::vector<std::type_index> m_possibleEvents;
	ppr::tl::thread_pool m_threads;

	virtual void Run() {
		Given("a controller with registered event handlers and a bound event queue", [&]() {
			m_controller = ppr::tl::make_unique<FakeController>();
			m_controller->bind(&m_eventQueue);

		    m_mockIntHandler = ut11::Mock<void (int)>();
		    m_mockFloatHandler = ut11::Mock<void (float)>();
		    m_mockCharHandler = ut11::Mock<void (char)>();

			m_controller->onEvent<int>([&](int i, ppr::tl::thread_pool& pool) {
				return ppr::tl::future::deferred([this, i] {
					m_mockIntHandler(i);
				});
			});
		    m_controller->onEvent<float>([&](float i, ppr::tl::thread_pool& pool) {
				return ppr::tl::future::deferred([this, i] {
					m_mockFloatHandler(i);
				});
			});
		    m_controller->onEvent<char>([&](char i, ppr::tl::thread_pool& pool) {
				return ppr::tl::future::deferred([this, i] {
					m_mockCharHandler(i);
				});
			});
		}); Then("the controller is alive", [&]() {
			AssertThat(m_controller->isAlive(), ut11::Is::True);
		});
		When("getting the possible events", [&]() {
		    m_possibleEvents = m_controller->getPossibleEvents();
		}); Then("The expected result is returned", [&]() {
		    std::vector<std::type_index> expected({
				    std::type_index(typeid(int)),
				    std::type_index(typeid(float)),
				    std::type_index(typeid(char))
		    });
			AssertThat(m_possibleEvents, ut11::Is::Iterable::EquivalentTo(expected));
		});
		When("triggering an event directly", [&]() {
			m_controller->event<int>(10, m_threads).wait();
		}); Then("the expected handler was invoked", [&]() {
		    MockVerifyTimes(1, m_mockIntHandler)(10);
		}); Then("the other handlers were not invoked", [&]() {
		    MockVerifyTimes(0, m_mockFloatHandler)(ut11::Is::Any<float>());
		    MockVerifyTimes(0, m_mockCharHandler)(ut11::Is::Any<char>());
		});
		When("triggering an event via an any", [&]() {
			float f = 142.0f;
		    m_controller->event(typeid(float), ppr::tl::any(f), m_threads).wait();
		}); Then("the expected handler was invoked", [&]() {
		    MockVerifyTimes(1, m_mockFloatHandler)(142.0f);
		}); Then("the other handlers were not invoked", [&]() {
		    MockVerifyTimes(0, m_mockIntHandler)(ut11::Is::Any<int>());
		    MockVerifyTimes(0, m_mockCharHandler)(ut11::Is::Any<char>());
		});
		When("the controller is killed", [&]() {
			m_controller->kill();
		}); Then("the controller is alive", [&]() {
			AssertThat(m_controller->isAlive(), ut11::Is::False);
		});
		When("emitting an event", [&]() {
		    m_controller->emit<int>(10);
		}); Then("the expected event was enqueued", [&]() {
		    ut11::Mock<void (std::type_index, int)> mockEventHandler;
			m_eventQueue.dequeue([&](std::type_index index, ppr::tl::any const& data) {
			    mockEventHandler(index, data.as<int>());
			});
			MockVerifyTimes(1, mockEventHandler)(std::type_index(typeid(int)), 10);
		});
	}
};
DeclareFixture(ControllerTests)(ut11::Category("ctrl"));
