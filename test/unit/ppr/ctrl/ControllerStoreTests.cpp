#include <ppr/ctrl/ControllerStore.hpp>
#include <ppr/ctrl/Controller.hpp>
#include <ppr/data/EventQueue.hpp>
#include <ppr/tl/make_unique.hpp>

#include <UnitTest11.hpp>
#include <UnitTest11/Mock.hpp>
#include <iostream>

namespace {
	class MockController : public ppr::ctrl::Controller {
	private:
		std::mutex m_mutex;

	public:
		MockController() {
			onEvent<int>([this](int i, ppr::tl::thread_pool& pool) -> std::future<void> {
				return ppr::tl::future::deferred([this, i]() {
					std::lock_guard<std::mutex> lock(m_mutex);
					intEventHandler(i);
				});
			});
		}

		std::future<void> update(std::chrono::microseconds dt, ppr::tl::thread_pool& pool) {
			auto poolPtr = &pool;
			return ppr::tl::future::deferred([this, dt, poolPtr]() {
				std::lock_guard<std::mutex> lock(m_mutex);
				mockupdate(dt, poolPtr);
			});
		}

		ut11::Mock<void (std::chrono::microseconds, ppr::tl::thread_pool*)> mockupdate;
		ut11::Mock<void (int)> intEventHandler;
	};

	class MockSharedController : public ppr::ctrl::Controller {
	private:
		std::mutex m_mutex;
		std::shared_ptr<MockController> m_internal;

	public:
		MockSharedController()
				: m_internal(new MockController()) {
			onEvent<int>([this](int i, ppr::tl::thread_pool& pool) -> std::future<void> {
				std::lock_guard<std::mutex> lock(m_mutex);
				return m_internal->event<int>(i, pool);
			});
		}

		std::future<void> update(std::chrono::microseconds dt, ppr::tl::thread_pool& pool) {
			std::lock_guard<std::mutex> lock(m_mutex);
			return m_internal->update(dt, pool);
		}

		std::shared_ptr<MockController> mock() { return m_internal; }
	};
}

class ControllerStoreTests : public ut11::TestFixture {

public:
	std::unique_ptr<ppr::tl::thread_pool> m_threads;
	ppr::data::EventQueue m_eventQueue;
	MockController* m_mockController;
	std::vector<std::shared_ptr<MockController>> m_mockAliveControllers;
	std::vector<std::shared_ptr<MockController>> m_mockDeadControllers;
	std::unique_ptr<ppr::ctrl::ControllerStore> m_controllerStore;

	std::chrono::microseconds m_expectedUpdateTime;

	virtual void Run() {
		m_threads = ppr::tl::make_unique<ppr::tl::thread_pool>(1);

		Given("a controller store with alive and dead controllers", [&]() {
		    m_controllerStore = ppr::tl::make_unique<ppr::ctrl::ControllerStore>(m_eventQueue, *m_threads);
		    auto mockController = ppr::tl::make_unique<MockController>();
			m_mockController = mockController.get();
			m_controllerStore->add(std::move(mockController));

			m_mockAliveControllers.clear();
			for(auto i = 0; i < 5; ++i) {
				std::unique_ptr<MockSharedController> mock(new MockSharedController());
				m_mockAliveControllers.emplace_back(mock->mock());
				m_controllerStore->add(std::move(mock));
			}
		    for(auto i = 0; i < 5; ++i) {
			    std::unique_ptr<MockSharedController> mock(new MockSharedController());
			    mock->kill();
			    m_mockDeadControllers.emplace_back(mock->mock());
			    m_controllerStore->add(std::move(mock));
		    }
			
		    m_controllerStore->clean();
		});
		When("updating", [&]() {
		    m_expectedUpdateTime = std::chrono::microseconds(66575);
			m_controllerStore->update(m_expectedUpdateTime).wait();
			m_controllerStore->clean();
		    m_controllerStore->update(m_expectedUpdateTime).wait();
		}); Then("the expected controllers were updated", [&]() {
			for(auto i : m_mockAliveControllers)
				MockVerifyTimes(2, i->mockupdate)(m_expectedUpdateTime, m_threads.get());
		}); Then("the dead controllers were not updated", [&]() {
		    for(auto i : m_mockDeadControllers)
			    MockVerifyTimes(0, i->mockupdate)(ut11::Is::Any<std::chrono::microseconds>(),
						                          ut11::Is::Any<ppr::tl::thread_pool*>());
		});
		When("an event with subscribers is invoked", [&]() {
			m_controllerStore->event<int>(10).wait();
		    m_controllerStore->event<int>(10).wait();
		}); Then("the alive controllers registered to that event were invoked", [&]() {
			for(auto i : m_mockAliveControllers)
		        MockVerifyTimes(2, i->intEventHandler)(10);
		}); Then("the dead controllers registered to that event were not invoked", [&]() {
		    for(auto i : m_mockDeadControllers)
			    MockVerifyTimes(0, i->intEventHandler)(ut11::Is::Any<int>());
		});
		When("an update happens then an event with subscribers is invoked", [&]() {
		    m_controllerStore->update(m_expectedUpdateTime).wait();
		    m_controllerStore->event<int>(10).wait();
		}); Then("the alive controllers registered to that event were invoked", [&]() {
		    for(auto i : m_mockAliveControllers)
			    MockVerifyTimes(1, i->intEventHandler)(10);
		}); Then("the dead controllers registered to that event were not invoked", [&]() {
		    for(auto i : m_mockDeadControllers)
			    MockVerifyTimes(0, i->intEventHandler)(ut11::Is::Any<int>());
		}); Then("the expected controllers were updated", [&]() {
		    for(auto i : m_mockAliveControllers)
			    MockVerifyTimes(1, i->mockupdate)(m_expectedUpdateTime, m_threads.get());
		}); Then("the dead controllers were not updated", [&]() {
		    for(auto i : m_mockDeadControllers)
				MockVerifyTimes(0, i->mockupdate)(ut11::Is::Any<std::chrono::microseconds>(),
						                          ut11::Is::Any<ppr::tl::thread_pool*>());
		});
		When("an event with subscribers is invoked then an update happens", [&]() {
		    m_controllerStore->event<int>(10).wait();
		    m_controllerStore->update(m_expectedUpdateTime).wait();
		}); Then("the alive controllers registered to that event were invoked", [&]() {
		    for(auto i : m_mockAliveControllers)
			    MockVerifyTimes(1, i->intEventHandler)(10);
		}); Then("the dead controllers registered to that event were not invoked", [&]() {
		    for(auto i : m_mockDeadControllers)
			    MockVerifyTimes(0, i->intEventHandler)(ut11::Is::Any<int>());
		}); Then("the expected controllers were updated", [&]() {
		    for(auto i : m_mockAliveControllers)
			    MockVerifyTimes(1, i->mockupdate)(m_expectedUpdateTime, m_threads.get());
		}); Then("the dead controllers were not updated", [&]() {
		    for(auto i : m_mockDeadControllers)
				MockVerifyTimes(0, i->mockupdate)(ut11::Is::Any<std::chrono::microseconds>(),
						                         ut11::Is::Any<ppr::tl::thread_pool*>());
		});
		When("a controller emits an event", [&]() {
		    m_mockController->emit<int>(10);
		}); Then("the expected event was enqueued", [&]() {
		    ut11::Mock<void (std::type_index, int)> mockEventHandler;
		    m_eventQueue.dequeue([&mockEventHandler](std::type_index i, ppr::tl::any const& data) {
		        mockEventHandler(i, data.as<int>());
		    });
		    MockVerifyTimes(1, mockEventHandler)(std::type_index(typeid(int)), 10);
		});

		Finally("clean up", [this]() {
			m_threads = std::unique_ptr<ppr::tl::thread_pool>();
			m_mockController = nullptr;
			m_mockAliveControllers.clear();
			m_mockDeadControllers.clear();
			m_controllerStore = std::unique_ptr<ppr::ctrl::ControllerStore>();

		});
	}
};
DeclareFixture(ControllerStoreTests)(ut11::Category("ctrl"));
