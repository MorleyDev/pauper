#include <UnitTest11.hpp>
#include <ppr/model/unique_id.hpp>

class UniqueIdTests : public ut11::TestFixture {

	ppr::model::unique_id ids[5];
	ppr::model::unique_id empty;

	virtual void Run() {
		Given("several generated and empty ids", [&]() {
			for(auto i = 0u; i < 5u; ++i)
				ids[i] = ppr::model::unique_id::generate();
		     empty = ppr::model::unique_id();

		}); Then("none of the generated ids are empty", [&]() {
		    for(auto i = 0u; i < 5u; ++i)
			    AssertThat(ids[i].empty(), ut11::Is::False);
		}); Then("the empty ids are empty", [&]() {
			AssertThat(empty.empty(), ut11::Is::True);
		}); Then("the equality operations work as expected", [&]() {
			AssertThat(ids[0] == ids[0], ut11::Is::True);
		    AssertThat(ids[0] != ids[0], ut11::Is::False);

			for(auto i = 1u; i < 5u; ++i) {
				AssertThat(ids[i] == empty, ut11::Is::False);
				AssertThat(ids[i] != empty, ut11::Is::True);
				AssertThat(ids[0] == ids[i], ut11::Is::False);
				AssertThat(ids[0] != ids[i], ut11::Is::True);
			}
		}); Then("the ordering works as expected", [&]() {
		    for(auto i = 0u; i < 4u; ++i)
		        AssertThat(ids[i] < ids[i+1], ut11::Is::True);
		    for(auto i = 0u; i < 4u; ++i)
			    AssertThat(ids[i] <= ids[i+1], ut11::Is::True);
		    for(auto i = 0u; i < 4u; ++i)
			    AssertThat(ids[i] > ids[i+1], ut11::Is::False);
		    for(auto i = 0u; i < 4u; ++i)
			    AssertThat(ids[i] >= ids[i+1], ut11::Is::False);
		    for(auto i = 0u; i < 5u; ++i)
		    AssertThat(ids[i] > empty, ut11::Is::True);
		});
	}
};
DeclareFixture(UniqueIdTests)(ut11::Category("model"));
