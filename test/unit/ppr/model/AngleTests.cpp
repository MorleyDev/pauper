#include <ppr/model/angle.hpp>
#include <UnitTest11.hpp>

class AngleTests : public ut11::TestFixture {

	double radians;
	ppr::model::angle angle;

	virtual void Run() {
		Given("an angle created from radians", [&]() {
		    radians = 10.0;
		    angle = ppr::model::angle(radians);
		}); Then("the angle has the expected radians", [&]() {
			AssertThat(angle.asRadians(), ut11::Is::EqualTo(radians));
		}); Then("the angle has the expected degrees", [&]() {
		    AssertThat(angle.asDegrees(), ut11::Is::EqualTo(ppr::tl::maths::radians_to_degrees(radians)));
		}); Then("the angle is equal to an angle of the same radians", [&]() {
		    AssertThat(angle == ppr::model::angle::fromRadians(radians), ut11::Is::True);
		}); Then("the angle is not equal to an angle of different radians", [&]() {
		    AssertThat(angle != ppr::model::angle::fromRadians(120.0), ut11::Is::True);
		}); Then("the angle is less than or equal to an angle of the same radians", [&]() {
		    AssertThat(angle <= ppr::model::angle::fromRadians(radians), ut11::Is::True);
		}); Then("the angle is greater than or equal to an angle of the same radians", [&]() {
		    AssertThat(angle >= ppr::model::angle::fromRadians(radians), ut11::Is::True);
		}); Then("the angle is less than or equal to an angle of more radians", [&]() {
		    AssertThat(angle <= ppr::model::angle::fromRadians(radians * 2.0), ut11::Is::True);
		}); Then("the angle is greater than or equal to an angle of less radians", [&]() {
		    AssertThat(angle >= ppr::model::angle::fromRadians(radians / 2.0), ut11::Is::True);
		}); Then("the angle is less than an angle of more radians", [&]() {
		    AssertThat(angle < ppr::model::angle::fromRadians(radians * 2.0), ut11::Is::True);
		}); Then("the angle is greater than an angle of less radians", [&]() {
		    AssertThat(angle > ppr::model::angle::fromRadians(radians / 2.0), ut11::Is::True);
		}); Then("the angle is not equal to an angle of the same radians", [&]() {
		    AssertThat(angle == ppr::model::angle::fromRadians(12.0), ut11::Is::False);
		}); Then("the angle is not not equal to an angle of same radians", [&]() {
		    AssertThat(angle != ppr::model::angle::fromRadians(radians), ut11::Is::False);
		}); Then("the angle is not less than an angle of less radians", [&]() {
		    AssertThat(angle < ppr::model::angle::fromRadians(radians / 2.0), ut11::Is::False);
		}); Then("the angle is not greater than an angle of more radians", [&]() {
		    AssertThat(angle > ppr::model::angle::fromRadians(radians * 2.0), ut11::Is::False);
		});

		Given("an angle created from degrees", [&]() {
		    radians = 10.0;
		    angle = ppr::model::angle::fromDegrees(ppr::tl::maths::radians_to_degrees(radians));
		}); Then("the angle has the expected degrees", [&]() {
		    auto deg = ppr::tl::maths::radians_to_degrees(radians);
		    AssertThat(angle.asDegrees(), ut11::Is::Between(deg - 0.0001, deg + 0.0001));
		}); Then("the angle has the expected radians", [&]() {
		    AssertThat(angle.asRadians(), ut11::Is::Between(radians - 0.0001, radians + 0.0001));
		});

		Given("an angle", [&]() {
			radians = 10.0;
			angle = ppr::model::angle::fromRadians(radians);
		}); Then("the angle added to another angle is as expected", [&]() {
			AssertThat(angle + ppr::model::angle::fromRadians(15.0), ut11::Is::EqualTo(ppr::model::angle::fromRadians(radians + 15.0)));
		});  Then("the angle subtracted from another angle is as expected", [&]() {
			AssertThat(angle - ppr::model::angle::fromRadians(5.0), ut11::Is::EqualTo(ppr::model::angle::fromRadians(radians - 5.0)));
		}); Then("the angle multiplied by a value is as expected", [&]() {
			AssertThat(angle * 2.0, ut11::Is::EqualTo(ppr::model::angle::fromRadians(radians * 2.0)));
		});  Then("the angle divided by a value is as expected", [&]() {
			AssertThat(angle  / 2.0, ut11::Is::EqualTo(ppr::model::angle::fromRadians(radians / 2.0)));
		});
		Then("the angle self-modifying added to another angle is as expected", [&]() {
			angle += ppr::model::angle::fromRadians(15.0);
			AssertThat(angle, ut11::Is::EqualTo(ppr::model::angle::fromRadians(radians + 15.0)));
		});  Then("the angle self-modifying subtracted from another angle is as expected", [&]() {
			angle -= ppr::model::angle::fromRadians(5.0);
			AssertThat(angle, ut11::Is::EqualTo(ppr::model::angle::fromRadians(radians - 5.0)));
		}); Then("the angles elf-modifying multiplied by a value is as expected", [&]() {
			angle *= 2.0;
			AssertThat(angle, ut11::Is::EqualTo(ppr::model::angle::fromRadians(radians * 2.0)));
		});  Then("the angle self-modifying divided by a value is as expected", [&]() {
			angle /= 2.0;
			AssertThat(angle, ut11::Is::EqualTo(ppr::model::angle::fromRadians(radians / 2.0)));
		});
	}
};
DeclareFixture(AngleTests)(ut11::Category("model"));
