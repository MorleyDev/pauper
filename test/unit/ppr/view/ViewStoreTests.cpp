#include <ppr/view/ViewStore.hpp>
#include <ppr/view/View.hpp>
#include <ppr/data/Renderer.hpp>
#include <ppr/data/SpriteBatch.hpp>
#include <ppr/model/Texture.hpp>
#include <ppr/model/Shader.hpp>
#include <ppr/tl/make_unique.hpp>

#include <UnitTest11.hpp>
#include <UnitTest11/Mock.hpp>

#include <algorithm>

namespace {
	class FakeRenderer : public ppr::data::Renderer {
	public:

		virtual void setLogicalSize(ppr::model::rectf size) { }
		virtual ppr::model::rectf getLogicalSize() const { return ppr::model::rectf(); }

		virtual void clear() { }
		virtual void display() { }

		virtual std::unique_ptr<ppr::data::RenderTexture> createRenderTexture(std::size_t width, std::size_t height) {
			return std::unique_ptr<ppr::data::RenderTexture>();
		}

		virtual std::unique_ptr<ppr::data::SpriteBatch> createBatch(ppr::model::Texture&) {
			return std::unique_ptr<ppr::data::SpriteBatch>();
		}

		virtual std::unique_ptr<ppr::model::Texture> createTexture(ppr::data::ReadOnlyFile& sourceFile) {
			return std::unique_ptr<ppr::model::Texture>();
		}

		virtual std::unique_ptr<ppr::model::Texture> createTexture(std::uint8_t const *data, std::size_t width, std::size_t height) {
			return std::unique_ptr<ppr::model::Texture>();
		}

		virtual std::unique_ptr<ppr::model::Shader> createShader(ppr::data::ReadOnlyFile& sourceFile, ppr::model::ShaderType type) { return std::unique_ptr<ppr::model::Shader>(); }
		virtual std::unique_ptr<ppr::model::Shader> createShader(ppr::data::ReadOnlyFile& vertexSourceFile, ppr::data::ReadOnlyFile& fragmentSourceFile) { return std::unique_ptr<ppr::model::Shader>(); }

		virtual void drawTexture(ppr::model::Texture& texture, ppr::model::rectf destination, ppr::model::recti source, ppr::model::rotation rotation, ppr::model::colour colour) { }
		virtual void drawRectangle(ppr::model::rectf destination, ppr::model::colour colour, bool isFilled) { }

		virtual void bindShader(ppr::model::Shader& shader) { }
		virtual void unbindShader() { }
	};

    class MockView : public ppr::view::View {
	private:
		std::mutex m_mutex;

    public:
	    MockView(std::int32_t height = 0) : ppr::view::View(height) {
		    onEvent<int>([this](int i, ppr::tl::thread_pool& pool) {
				auto pool_ptr = &pool;
				return ppr::tl::future::deferred([this, i, pool_ptr]() {
					std::lock_guard<std::mutex> lock(m_mutex);
					intEventHandler(i, pool_ptr);
				});
			});
	    }

	    ut11::Mock<void (ppr::data::Renderer*)> mockdraw;
	    void draw(ppr::data::Renderer& r) { mockdraw(&r); }

	    ut11::Mock<void (int, ppr::tl::thread_pool*)> intEventHandler;
    };

    class MockSharedView : public ppr::view::View {
    private:
	    std::shared_ptr<MockView> m_internal;

    public:
	    MockSharedView(std::int32_t height = 0)
			    : ppr::view::View(height), m_internal(new MockView(height)) {
		    onEvent<int>([this](int i, ppr::tl::thread_pool& pool) {
				return m_internal->event<int>(i, pool);
			});
	    }

	    virtual void draw(ppr::data::Renderer& r) {
		    m_internal->draw(r);
	    }

	    std::shared_ptr<MockView> mock() { return m_internal; }
    };
}

class ViewStoreTests : public ut11::TestFixture {

	ppr::tl::thread_pool m_threads;
	std::vector<std::shared_ptr<MockView>> m_mockAliveViews;
	std::vector<std::shared_ptr<MockView>> m_mockDeadViews;
	std::vector<std::int32_t> m_drawOrder;
	std::unique_ptr<ppr::view::ViewStore> m_viewStore;

	FakeRenderer m_fakeRenderer;

	virtual void Run() {
		Given("a view store with alive and dead views", [&]() {

		    m_viewStore = ppr::tl::make_unique<ppr::view::ViewStore>(m_threads);
		    m_mockAliveViews.clear();
			m_mockDeadViews.clear();
		    m_drawOrder.clear();

		    std::vector<std::int32_t> mockDrawLevels;
		    mockDrawLevels.push_back(10);
		    mockDrawLevels.push_back(-5);
		    mockDrawLevels.push_back(7);
		    mockDrawLevels.push_back(-8);
		    mockDrawLevels.push_back(2);
		    mockDrawLevels.push_back(9);
		    mockDrawLevels.push_back(4);
		    mockDrawLevels.push_back(2);
		    mockDrawLevels.push_back(-1);
		    mockDrawLevels.push_back(20);

		    for(auto i = 0; i < 5; ++i) {
			    std::unique_ptr<MockSharedView> mock(new MockSharedView(mockDrawLevels[i]));
			    auto height = mock->mock()->getHeight();
			    mock->mock()->mockdraw.SetCallback([height, this] (ppr::data::Renderer*) { m_drawOrder.push_back(height); });

			    m_mockAliveViews.emplace_back(mock->mock());
			    m_viewStore->add(std::move(mock));
		    }
		    for(auto i = 0; i < 5; ++i) {
			    std::unique_ptr<MockSharedView> mock(new MockSharedView(mockDrawLevels[i+5]));
			    auto height = mock->mock()->getHeight();
			    mock->mock()->mockdraw.SetCallback([height, this] (ppr::data::Renderer*) { m_drawOrder.push_back(height); });
			    mock->kill();

			    m_mockDeadViews.emplace_back(mock->mock());
			    m_viewStore->add(std::move(mock));
		    }
			m_viewStore->clean();
		});
		When("drawing twice", [&]() {
		    m_viewStore->draw(m_fakeRenderer);
		    m_viewStore->draw(m_fakeRenderer);
		}); Then("the expected views were drawn", [&]() {
		    for(auto i : m_mockAliveViews)
			    MockVerifyTimes(2, i->mockdraw)(&m_fakeRenderer);
		}); Then("the views were drawn in the expected order", [&]() {
			for(auto i = 0; i < (m_drawOrder.size() / 2)-1; ++i)
				AssertThat(m_drawOrder[i], ut11::Is::LessThan(m_drawOrder[i+1]));
			for(auto i = (m_drawOrder.size() / 2); i < m_drawOrder.size()-1; ++i)
				AssertThat(m_drawOrder[i], ut11::Is::LessThan(m_drawOrder[i+1]));
		});
		Then("the dead views were not drawn", [&]() {
		    for(auto i : m_mockDeadViews)
			    MockVerifyTimes(0, i->mockdraw)(ut11::Is::Any<ppr::data::Renderer*>());
		});
		When("an event with subscribers is invoked", [&]() {
		    m_viewStore->event<int>(10).wait();
		    m_viewStore->event<int>(10).wait();
		}); Then("the alive views registered to that event were invoked", [&]() {
		    for(auto i : m_mockAliveViews)
			    MockVerifyTimes(2, i->intEventHandler)(10, &m_threads);
		}); Then("the dead views registered to that event were not invoked", [&]() {
		    for(auto i : m_mockDeadViews)
			    MockVerifyTimes(0, i->intEventHandler)(ut11::Is::Any<int>(), ut11::Is::Any<ppr::tl::thread_pool*>());
		});
		When("a draw happens then an event with subscribers is invoked", [&]() {
		    m_viewStore->draw(m_fakeRenderer);
		    m_viewStore->event<int>(10).wait();
		}); Then("the alive views registered to that event were invoked", [&]() {
		    for(auto i : m_mockAliveViews)
			    MockVerifyTimes(1, i->mockdraw)(&m_fakeRenderer);
		}); Then("the dead views registered to that event were not invoked", [&]() {
		    for(auto i : m_mockDeadViews)
			    MockVerifyTimes(0, i->intEventHandler)(ut11::Is::Any<int>(), ut11::Is::Any<ppr::tl::thread_pool*>());
		}); Then("the expected views were updated", [&]() {
		    for(auto i : m_mockAliveViews)
			    MockVerifyTimes(1, i->mockdraw)(&m_fakeRenderer);
		}); Then("the dead views were not updated", [&]() {
		    for(auto i : m_mockDeadViews)
			    MockVerifyTimes(0, i->mockdraw)(ut11::Is::Any<ppr::data::Renderer*>());
		});
		When(" an event with subscribers is invoked then a drawn happens", [&]() {
		    m_viewStore->event<int>(10).wait();
		    m_viewStore->draw(m_fakeRenderer);
		}); Then("the alive views registered to that event were invoked", [&]() {
		    for(auto i : m_mockAliveViews)
			    MockVerifyTimes(1, i->intEventHandler)(10, &m_threads);
		}); Then("the dead views registered to that event were not invoked", [&]() {
		    for(auto i : m_mockDeadViews)
			    MockVerifyTimes(0, i->intEventHandler)(ut11::Is::Any<int>(), ut11::Is::Any<ppr::tl::thread_pool*>());
		}); Then("the expected views were updated", [&]() {
		    for(auto i : m_mockAliveViews)
			    MockVerifyTimes(1, i->mockdraw)(&m_fakeRenderer);
		}); Then("the dead views were not updated", [&]() {
		    for(auto i : m_mockDeadViews)
			    MockVerifyTimes(0, i->mockdraw)(ut11::Is::Any<ppr::data::Renderer*>());
		});
	}
};
DeclareFixture(ViewStoreTests)(ut11::Category("view"));

