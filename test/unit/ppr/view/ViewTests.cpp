#include <ppr/view/View.hpp>
#include <ppr/data/Renderer.hpp>
#include <ppr/tl/make_unique.hpp>
#include <UnitTest11.hpp>
#include <UnitTest11/Mock.hpp>

namespace {
    struct FakeView : public ppr::view::View {
	    virtual void draw(ppr::data::Renderer&) { }
    };
}

class ViewTests : public ut11::TestFixture {
	std::unique_ptr<ppr::view::View> m_view;
	ut11::Mock<void (int, ppr::tl::thread_pool*)> m_mockIntHandler;
	ut11::Mock<void (float, ppr::tl::thread_pool*)> m_mockFloatHandler;
	ut11::Mock<void (char, ppr::tl::thread_pool*)> m_mockCharHandler;

	std::vector<std::type_index> m_possibleEvents;
	ppr::tl::thread_pool m_threads;

	virtual void Run() {
		Given("a view with registered event handlers", [&]() {
		    m_view = ppr::tl::make_unique<FakeView>();
		    m_mockIntHandler = ut11::Mock<void (int, ppr::tl::thread_pool*)>();
		    m_mockFloatHandler = ut11::Mock<void (float, ppr::tl::thread_pool*)>();
		    m_mockCharHandler = ut11::Mock<void (char, ppr::tl::thread_pool*)>();

		    m_view->onEvent<int>([&](int i, ppr::tl::thread_pool& pool) {
				auto pool_ptr = &pool;
				return ppr::tl::future::deferred([this, i, pool_ptr]() {
					m_mockIntHandler(i, pool_ptr);
				});
			});
		    m_view->onEvent<float>([&](float i, ppr::tl::thread_pool& pool) {
				auto pool_ptr = &pool;
				return ppr::tl::future::deferred([this, i, pool_ptr]() {
					m_mockFloatHandler(i, pool_ptr);
				});
			});
		    m_view->onEvent<char>([&](char i, ppr::tl::thread_pool& pool) {
				auto pool_ptr = &pool;
				return ppr::tl::future::deferred([this, i, pool_ptr]() {
					m_mockCharHandler(i, pool_ptr);
				});
			});
		}); Then("the view is alive", [&]() {
		    AssertThat(m_view->isAlive(), ut11::Is::True);
		});
		When("getting the possible events", [&]() {
		    m_possibleEvents = m_view->getPossibleEvents();
		}); Then("The expected result is returned", [&]() {
		    std::vector<std::type_index> expected({
				    std::type_index(typeid(int)),
				    std::type_index(typeid(float)),
				    std::type_index(typeid(char))
		    });
		    AssertThat(m_possibleEvents, ut11::Is::Iterable::EquivalentTo(expected));
		});
		When("triggering an event directly", [&]() {
		    m_view->event<int>(10, m_threads).wait();
		}); Then("the expected handler was invoked", [&]() {
		    MockVerifyTimes(1, m_mockIntHandler)(10, &m_threads);
		}); Then("the other handlers were not invoked", [&]() {
		    MockVerifyTimes(0, m_mockFloatHandler)(ut11::Is::Any<float>(), ut11::Is::Any<ppr::tl::thread_pool*>());
		    MockVerifyTimes(0, m_mockCharHandler)(ut11::Is::Any<char>(), ut11::Is::Any<ppr::tl::thread_pool*>());
		});
		When("triggering an event via a void pointer", [&]() {
		    float f = 142.0f;
		    m_view->event(typeid(float), ppr::tl::any(f), m_threads).wait();
		}); Then("the expected handler was invoked", [&]() {
		    MockVerifyTimes(1, m_mockFloatHandler)(142.0f, &m_threads);
		}); Then("the other handlers were not invoked", [&]() {
		    MockVerifyTimes(0, m_mockIntHandler)(ut11::Is::Any<int>(), ut11::Is::Any<ppr::tl::thread_pool*>());
		    MockVerifyTimes(0, m_mockCharHandler)(ut11::Is::Any<char>(), ut11::Is::Any<ppr::tl::thread_pool*>());
		});
		When("the view is killed", [&]() {
		    m_view->kill();
		}); Then("the view is alive", [&]() {
		    AssertThat(m_view->isAlive(), ut11::Is::False);
		});
	}
};
DeclareFixture(ViewTests)(ut11::Category("view"));
