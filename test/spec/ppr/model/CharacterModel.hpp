#ifndef TEST_SPEC_PPR_MODEL_CHARACTERMODEL_HPP
#define TEST_SPEC_PPR_MODEL_CHARACTERMODEL_HPP

#include <ppr/model/position.hpp>

namespace {
	struct CharacterModel {
		ppr::model::pos2f position = { 0.0, 0.0 };
		std::uint8_t frameIndex = 0;
	};
}

#endif //TEST_SPEC_PPR_MODEL_CHARACTERMODEL_HPP
