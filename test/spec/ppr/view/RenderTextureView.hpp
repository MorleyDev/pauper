//
// Created by Jason on 08/02/2016.
//

#ifndef TEST_SPEC_PPR_VIEW_RENDERTEXTUREVIEW_HPP
#define TEST_SPEC_PPR_VIEW_RENDERTEXTUREVIEW_HPP

#include <ppr/view/View.hpp>
#include <ppr/data/RenderTexture.hpp>
#include <ppr/model/Texture.hpp>
#include <ppr/model/rectangle.hpp>

namespace {
	class RenderTextureView : public ppr::view::View {
	private:
		std::unique_ptr<ppr::data::RenderTexture> m_renderTexture;
		std::unique_ptr<ppr::model::Texture> m_texture;

	public:
		RenderTextureView(ppr::data::Renderer& renderer, ppr::data::ContentLoader& content)
			: m_renderTexture( renderer.createRenderTexture(320, 240) ),
			  m_texture(content.load<ppr::model::Texture>("./spec/files/simple.bmp")) {
			m_renderTexture->setClearColour(ppr::model::colour(255,0,0,255));
		}

		virtual void draw(ppr::data::Renderer& r) {
			m_renderTexture->clear();
			m_renderTexture->drawRectangle(ppr::model::rectf(0.0, 0.0, 160.0, 120.0), ppr::model::colour(50, 50, 50, 255), true);
			m_renderTexture->drawRectangle(ppr::model::rectf(50.0, 50.0, 250.0, 200.0), ppr::model::colour(255, 100, 100, 255), false);
			m_renderTexture->drawRectangle(ppr::model::rectf(0.0, 0.0, 320.0, 100.0), ppr::model::colour(100, 255, 100, 100), true);
			m_renderTexture->drawTexture(*m_texture, ppr::model::rectf(0.0,0.0,320.0,240.0), ppr::model::recti(0,0,2,2), ppr::model::colour(255,255,255,155));
			m_renderTexture->display();

			r.drawTexture(m_renderTexture->getTexture(), ppr::model::rectf(1080.0, 240.0, 1280.0, 960.0) );
			r.drawRectangle(ppr::model::rectf(1080.0, 240.0, 1280.0, 960.0), ppr::model::colour(255, 0, 255, 255), false);
		}
	};
}

#endif //TEST_SPEC_PPR_VIEW_RENDERTEXTUREVIEW_HPP
