#ifndef TEST_SPEC_PPR_VIEW_WAVFILEMUSICVIEW_HPP
#define TEST_SPEC_PPR_VIEW_WAVFILEMUSICVIEW_HPP

#include <ppr/view/View.hpp>
#include <ppr/data/ContentLoader.hpp>
#include <ppr/data/loaders/MusicStreamLoader.hpp>
#include <ppr/data/MusicPlayer.hpp>
#include <ppr/data/ReadOnlyFile.hpp>
#include <ppr/model/MusicStream.hpp>
#include <ppr/tl/exchange.hpp>
#include "../events/CloseTestEvent.hpp"
#include <memory>

namespace {
	class WavFileMusicView : public ppr::view::View {
	private:
		ppr::data::MusicPlayer& m_music;
		bool m_wasPlayed;
		std::unique_ptr<ppr::model::MusicStream> m_musicStream;

	public:
		WavFileMusicView(ppr::data::ContentLoader& content, ppr::data::MusicPlayer& r)
			: m_music(r),
			  m_wasPlayed(false),
			  m_musicStream(content.load<ppr::model::MusicStream>("./spec/files/simple.wav")) {
			onEvent<CloseTestEvent>([this](CloseTestEvent, ppr::tl::thread_pool& pool) {
				return pool.enqueue([this]() {
					m_music.stop(*m_musicStream);
					m_musicStream.reset();
				});
			});
		}

		virtual void draw(ppr::data::Renderer& r) {
			if (ppr::tl::exchange(m_wasPlayed, true)) {
				return;
			}
			m_music.play(*m_musicStream, false);
		}
	};

}

#endif //TEST_SPEC_PPR_VIEW_WAVFILEMUSICVIEW_HPP
