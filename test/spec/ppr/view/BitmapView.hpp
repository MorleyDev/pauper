#ifndef TEST_SPEC_PPR_VIEW_BITMAPVIEW_HPP
#define TEST_SPEC_PPR_VIEW_BITMAPVIEW_HPP

#include <ppr/view/View.hpp>
#include <ppr/data/Renderer.hpp>
#include <ppr/data/ContentLoader.hpp>
#include <ppr/data/loaders/TextureLoader.hpp>
#include <ppr/model/Texture.hpp>
#include <ppr/data/ReadOnlyFile.hpp>

namespace {
	class BitmapView : public ppr::view::View {
	private:
		std::unique_ptr<ppr::model::Texture> m_simpleBitmapTexture;
		double m_angle;

	public:
		BitmapView(ppr::data::ContentLoader& content)
				: m_simpleBitmapTexture(content.load<ppr::model::Texture>("./spec/files/simple.bmp")),
				  m_angle(0.0) {
		}

		virtual void draw(ppr::data::Renderer& r) {
			m_angle += 1.0;
			r.drawTexture(
					*m_simpleBitmapTexture,
					{ 200, 200, 100,100 },
					{ 0,0,2,2 }
			);
			r.drawTexture(
					*m_simpleBitmapTexture,
					{200, 500, 100, 50},
					{0, 0, 2, 2},
					ppr::model::rotation(ppr::model::pos2f(-50.0, 25.0),
					                     ppr::model::angle::fromDegrees(45.0 + m_angle))
			);
			r.drawTexture(
					*m_simpleBitmapTexture,
					{500, 500, 100, 100},
					{0, 0, 2, 2},
					ppr::model::rotation(ppr::model::pos2f(50.0, 50.0), ppr::model::angle::fromDegrees(m_angle)),
					ppr::model::colour(0, 255, 255, 255)
			);
			r.drawTexture(
					*m_simpleBitmapTexture,
					{500, 500, 100, 100},
					{0, 0, 2, 2},
					ppr::model::rotation(ppr::model::pos2f(50.0, 50.0),
					                     ppr::model::angle::fromDegrees(360.0 - m_angle)),
					ppr::model::colour(255, 255, 0, 100)
			);
		}
	};

}

#endif //TEST_SPEC_PPR_VIEW_BITMAPVIEW_HPP
