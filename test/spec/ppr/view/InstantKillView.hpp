#ifndef TEST_SPEC_PPR_VIEW_INSTANTKILLVIEW_HPP
#define TEST_SPEC_PPR_VIEW_INSTANTKILLVIEW_HPP

#include <UnitTest11/Mock.hpp>
#include <ppr/view/View.hpp>

namespace {
	class InstantKillView : public ppr::view::View {
	private:
		ut11::Mock<void (void)>& m_mockDraw;

	public:
		InstantKillView(ut11::Mock<void (void)>& mockDraw)
				: m_mockDraw(mockDraw) {
		}

		virtual void draw(ppr::data::Renderer&) {
			m_mockDraw();
			kill();
		}
	};
}

#endif //TEST_SPEC_PPR_VIEW_INSTANTKILLVIEW_HPP
