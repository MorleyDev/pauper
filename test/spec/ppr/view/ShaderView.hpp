#ifndef TEST_SPEC_PPR_VIEW_SHADERVIEW_HPP
#define TEST_SPEC_PPR_VIEW_SHADERVIEW_HPP

#include <ppr/view/View.hpp>
#include <ppr/model/Shader.hpp>
#include <ppr/model/Texture.hpp>
#include <ppr/data/Renderer.hpp>
#include <ppr/data/ContentLoader.hpp>
#include <ppr/data/loaders/ShaderLoader.hpp>
#include <ppr/data/loaders/TextureLoader.hpp>
#include <memory>
#include <random>
#include <cmath>

namespace {
	namespace _detail {
		class ShaderViewRng {
		private:
			std::random_device r;
			std::default_random_engine e1;
			std::uniform_real_distribution<double> uniform_dist;

		public:
			ShaderViewRng()
				: r(),
				  e1(r()),
				  uniform_dist(0.0, 1.0) {
			}

			double operator()() {
				return uniform_dist(e1);
			}
		};
	}

	class ShaderView : public ppr::view::View {
	private:
		_detail::ShaderViewRng m_random;
		std::unique_ptr<ppr::model::Shader> m_invertShader;
		std::unique_ptr<ppr::model::Shader> m_warpShader;
		std::unique_ptr<ppr::model::Shader> m_flipPixelateShader;
		std::unique_ptr<ppr::model::Texture> m_teapotTexture;

	public:
		ShaderView(ppr::data::ContentLoader& content)
			: m_random(),
			  m_invertShader(content.load<ppr::model::Shader>("./spec/files/invert-colours.frag", ppr::model::ShaderType::Fragment)),
			  m_warpShader(content.load<ppr::model::Shader>("./spec/files/warp.vert", ppr::model::ShaderType::Vertex)),
			  m_flipPixelateShader(content.load<ppr::model::Shader>("./spec/files/flip.vert", "./spec/files/pixelate.frag")),
			  m_teapotTexture(content.load<ppr::model::Texture>("./spec/files/teapot.png")){
		}

		virtual void draw(ppr::data::Renderer& r) {
			drawInvertedTeapot(r);
			drawWavedTeapot(r);
			drawFlipPixelatedTeapot(r);
		}

	private:
		void drawInvertedTeapot(ppr::data::Renderer& r) {
			m_invertShader->setCurrentTexture("texture");
			r.bindShader(*m_invertShader);
			r.drawTexture(*m_teapotTexture, ppr::model::rectf(10.0, 10.0, 128.0, 128.0), ppr::model::recti(0, 0, 64, 64));
			r.unbindShader();
		}

		void drawWavedTeapot(ppr::data::Renderer &r) {
			m_warpShader->set("wave_amplitude", ppr::model::pos2f(m_random() * 40.0, m_random() * 40.0));
			m_warpShader->set("wave_phase", m_random() * 60.0);

			r.bindShader(*m_warpShader);
			r.drawTexture(*m_teapotTexture, ppr::model::rectf(250.0, 300.0, 256.0, 256.0), ppr::model::recti(0, 0, 64, 64));
			r.unbindShader();
		}

		void drawFlipPixelatedTeapot(ppr::data::Renderer &r) {
			m_flipPixelateShader->setCurrentTexture("texture");

			r.bindShader(*m_flipPixelateShader);
			r.drawTexture(*m_teapotTexture, ppr::model::rectf(700.0, 500.0, 256.0, 256.0), ppr::model::recti(0, 0, 64, 64));
			r.unbindShader();
		}
	};
}

#endif //TEST_SPEC_PPR_VIEW_SHADERVIEW_HPP
