#ifndef TEST_SPEC_PPR_VIEW_ENDLESSVIEW_HPP
#define TEST_SPEC_PPR_VIEW_ENDLESSVIEW_HPP

#include <UnitTest11/Mock.hpp>
#include <ppr/view/View.hpp>

namespace {
	class EndlessView : public ppr::view::View {
	private:
		ut11::Mock<void (void)>& m_mockDraw;

	public:
		EndlessView(ut11::Mock<void (void)>& mockDraw)
				: m_mockDraw(mockDraw) {
		}

		virtual void draw(ppr::data::Renderer& r) {
			m_mockDraw();
		}
	};
}

#endif //TEST_SPEC_PPR_VIEW_ENDLESSVIEW_HPP
