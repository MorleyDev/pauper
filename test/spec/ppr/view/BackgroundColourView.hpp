#ifndef TEST_SPEC_PPR_VIEW_BACKGROUNDCOLOURVIEW_HPP
#define TEST_SPEC_PPR_VIEW_BACKGROUNDCOLOURVIEW_HPP

#include <ppr/view/View.hpp>
#include <ppr/data/Renderer.hpp>
#include "../events/SwapBackgroundColourEvent.hpp"

namespace {
	class BackgroundColourView : public ppr::view::View {
	private:
		std::array<ppr::model::colour, 2> m_colours;
		bool m_isFirstColour;
		std::atomic<bool> m_swapBackground;

	public:
		BackgroundColourView()
				: m_colours({ ppr::model::colour(200,200,200, 255), ppr::model::colour(100, 149, 237, 255) }),
				  m_isFirstColour(true),
				  m_swapBackground(false) {
			onEvent<SwapBackgroundColourEvent>([this](SwapBackgroundColourEvent, ppr::tl::thread_pool& pool) {
				return pool.enqueue([this]() {
					m_swapBackground.store(true);
				});
			});
		}

		virtual void draw(ppr::data::Renderer& r) {
			if (!m_swapBackground.exchange(false)) {
				return;
			}
			m_isFirstColour = !m_isFirstColour;
			r.setClearColour( m_colours[m_isFirstColour ? 0 : 1] );
		}
	};
}

#endif //TEST_SPEC_PPR_VIEW_BACKGROUNDCOLOURVIEW_HPP
