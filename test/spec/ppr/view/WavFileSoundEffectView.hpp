#ifndef TEST_SPEC_PPR_VIEW_WAVFILESOUNDEFFECTVIEW_HPP
#define TEST_SPEC_PPR_VIEW_WAVFILESOUNDEFFECTVIEW_HPP

#include <ppr/view/View.hpp>
#include <ppr/data/ContentLoader.hpp>
#include <ppr/data/loaders/SoundEffectLoader.hpp>
#include <ppr/data/SoundEffectPlayer.hpp>
#include <ppr/data/ReadOnlyFile.hpp>
#include <ppr/model/SoundEffect.hpp>
#include <ppr/tl/exchange.hpp>
#include <memory>

namespace {
	class WavFileSoundEffectView : public ppr::view::View {
	private:
		ppr::data::SoundEffectPlayer& m_sounds;
		bool m_wasPlayed;
		std::unique_ptr<ppr::model::SoundEffect> m_soundEffect;

	public:
		WavFileSoundEffectView(ppr::data::ContentLoader& content, ppr::data::SoundEffectPlayer& r)
			: m_sounds(r),
			  m_wasPlayed(false),
			  m_soundEffect(content.load<ppr::model::SoundEffect>("./spec/files/simple2.wav")) {
		}

		virtual void draw(ppr::data::Renderer& r) {
			if (ppr::tl::exchange(m_wasPlayed, true)) {
				return;
			}
			m_sounds.play(*m_soundEffect, ppr::model::pos2f(0.0,0.0), 0.2);
		}
	};

}

#endif //TEST_SPEC_PPR_VIEW_WAVFILESOUNDEFFECTVIEW_HPP
