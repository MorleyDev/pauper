#ifndef TEST_SPEC_PPR_VIEW_RECTANGLEVIEW_HPP
#define TEST_SPEC_PPR_VIEW_RECTANGLEVIEW_HPP

#include <ppr/view/View.hpp>
#include <ppr/data/Renderer.hpp>

namespace {
	class RectangleView : public ppr::view::View {
	public:
		virtual void draw(ppr::data::Renderer& r) {
			r.drawRectangle(ppr::model::rectf(0.0, 0.0, 1920.0, 1080.0), ppr::model::colour(50, 50, 50, 255), true);
			r.drawRectangle(ppr::model::rectf(50.0, 50.0, 250.0, 200.0), ppr::model::colour(255, 100, 100, 255), false);
			r.drawRectangle(ppr::model::rectf(0.0, 0.0, 200.0, 200.0), ppr::model::colour(100, 255, 100, 100), true);
		}
	};
}

#endif //TEST_SPEC_PPR_VIEW_RECTANGLEVIEW_HPP
