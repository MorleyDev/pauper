#ifndef TEST_SPEC_PPR_VIEW_SPRITEBATCHVIEW_HPP
#define TEST_SPEC_PPR_VIEW_SPRITEBATCHVIEW_HPP

#include <ppr/view/View.hpp>
#include <ppr/model/Texture.hpp>
#include <ppr/data/Renderer.hpp>
#include <ppr/data/ContentLoader.hpp>
#include <ppr/data/loaders/TextureLoader.hpp>
#include <ppr/data/SpriteBatch.hpp>
#include <ppr/data/ReadOnlyFile.hpp>

namespace {
	class SpriteBatchView : public ppr::view::View {
	private:
		std::unique_ptr<ppr::model::Texture> m_simpleBitmapTexture;
		double m_angle;

	public:
		SpriteBatchView(ppr::data::ContentLoader& content)
			: m_simpleBitmapTexture(content.load<ppr::model::Texture>("./spec/files/simple2.png")),
			  m_angle(0.0) {
		}

		virtual void draw(ppr::data::Renderer& r) {
			(*r.createBatch(*m_simpleBitmapTexture))
					.draw({ 1000.0, 1000.0, 200.0, 200.0 }, { 0, 0, 2, 2 })
					.draw({ 1000.0, 1200.0, 200.0, 200.0 }, { 0, 0, 2, 2 }, ppr::model::colour(100,255,100,255))
					.draw({ 1200.0, 1000.0, 200.0, 200.0 }, { 0, 0, 2, 2 })
					.draw({ 1200.0, 1000.0, 200.0, 200.0 }, { 0, 0, 2, 2 }, ppr::model::rotation(ppr::model::pos2f(0.0, 0.0), ppr::model::angle::fromDegrees(45.0)))
					.draw({ 1600.0, 1600.0, 200.0, 200.0 }, { 0, 0, 2, 2 })
					.draw({ 1600.0, 1600.0, 200.0, 200.0 }, { 0, 0, 2, 2 }, ppr::model::rotation(ppr::model::pos2f(100.0, 100.0), ppr::model::angle::fromDegrees(45.0)))
					.draw({ 1500.0, 1000.0, 200.0, 200.0 }, { 0, 0, 2, 2 }, ppr::model::rotation(ppr::model::pos2f(0.0, 0.0), ppr::model::angle::fromDegrees(180.0)), ppr::model::colour(100,0,255,255))
					.draw({ 1200.0, 1000.0, 20.0, 20.0 }, { 0, 0, 2, 2 }, ppr::model::colour(0,0,0,255));

		}
	};
}

#endif //TEST_SPEC_PPR_VIEW_SPRITEBATCHVIEW_HPP
