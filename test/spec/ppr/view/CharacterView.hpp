#ifndef TEST_SPEC_PPR_VIEW_CHARACTERVIEW_HPP
#define TEST_SPEC_PPR_VIEW_CHARACTERVIEW_HPP

#include <ppr/view/View.hpp>
#include <ppr/data/Renderer.hpp>
#include <ppr/data/SpriteBatch.hpp>
#include <ppr/data/ModelStore.hpp>
#include <ppr/data/ContentLoader.hpp>
#include <ppr/data/loaders/TextureLoader.hpp>
#include <ppr/model/Texture.hpp>
#include "../model/CharacterModel.hpp"

namespace {
	class CharacterView : public ppr::view::View {
	private:
		ppr::data::ModelStore<CharacterModel>& m_modelStore;
		std::unique_ptr<ppr::model::Texture> m_spritesheet;

	public:
		CharacterView(ppr::data::ContentLoader& content, ppr::data::ModelStore<CharacterModel>& modelStore)
			: m_modelStore(modelStore),
			  m_spritesheet(std::move(*(content.tryLoad<ppr::model::Texture>("./spec/files/character.bmp")))) {
		}

		virtual void draw(ppr::data::Renderer& r) {
			auto batch = r.createBatch(*m_spritesheet);
			m_modelStore.each([this, &batch](CharacterModel const& model) {
				batch->draw({ model.position.x, model.position.y, 64.0, 128.0 }, { model.frameIndex * 8, 0, 8, 16 });
			});
		}
	};
}

#endif //TEST_SPEC_PPR_VIEW_CHARACTERVIEW_HPP
