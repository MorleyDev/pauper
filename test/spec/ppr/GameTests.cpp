#include <ppr/Game.hpp>
#include <ppr/events/Close.hpp>
#include <ppr/events/CreateController.hpp>
#include <ppr/events/CreateView.hpp>
#include <ppr/data/Renderer.hpp>
#include <ppr/data/ContextFactory.hpp>
#include <ppr/data/FileSystemStack.hpp>
#include <ppr/data/FlatModelStore.hpp>

#include <ppr/run.hpp>

#include <UnitTest11.hpp>
#include <UnitTest11/Mock.hpp>

#include "ctrl/InstantKillController.hpp"
#include "ctrl/GameEndController.hpp"
#include "ctrl/CharacterLuaScriptController.hpp"
#include "ctrl/BackgroundColourController.hpp"
#include "view/InstantKillView.hpp"
#include "view/EndlessView.hpp"
#include "view/RectangleView.hpp"
#include "view/BitmapView.hpp"
#include "view/SpriteBatchView.hpp"
#include "view/WavFileSoundEffectView.hpp"
#include "view/WavFileMusicView.hpp"
#include "view/CharacterView.hpp"
#include "view/BackgroundColourView.hpp"
#include "view/ShaderView.hpp"
#include "view/RenderTextureView.hpp"

namespace {
	class TestGame : public ppr::Game {
	public:
		ut11::Mock<void (std::chrono::microseconds)> mockInstantKillControllerUpdate;
		ut11::Mock<void (std::chrono::microseconds)> mockGameEndControllerUpdate;
		ut11::Mock<void (void)> mockInstantKillViewDraw;
		ut11::Mock<void (void)> mockEndlessViewDraw;
		ppr::data::FlatModelStore<CharacterModel> characterModelStore;

		TestGame(ppr::model::Configuration config)
				: ppr::Game("game", config) {
			renderer.setLogicalSize({ 0.0, 0.0, 3840.0, 2160.0 });

			events.enqueue<ppr::events::CreateView>({ [this]() { return ppr::tl::make_unique<InstantKillView>(mockInstantKillViewDraw); } });
			events.enqueue<ppr::events::CreateView>({ [this]() { return ppr::tl::make_unique<RectangleView>(); } });
			events.enqueue<ppr::events::CreateView>({ [this]() { return ppr::tl::make_unique<BitmapView>(content); } });
			events.enqueue<ppr::events::CreateView>({ [this]() { return ppr::tl::make_unique<SpriteBatchView>(content); } });
			events.enqueue<ppr::events::CreateView>({ [this]() { return ppr::tl::make_unique<WavFileSoundEffectView>(content, sound); } });
			events.enqueue<ppr::events::CreateView>({ [this]() { return ppr::tl::make_unique<WavFileMusicView>(content, music); } });
			events.enqueue<ppr::events::CreateView>({ [this]() { return ppr::tl::make_unique<CharacterView>(content, characterModelStore); } });
			events.enqueue<ppr::events::CreateView>({ [this]() { return ppr::tl::make_unique<BackgroundColourView>(); } });
			events.enqueue<ppr::events::CreateView>({ [this]() { return ppr::tl::make_unique<ShaderView>(content); } });
			events.enqueue<ppr::events::CreateView>({ [this]() { return ppr::tl::make_unique<RenderTextureView>(renderer, content); } });
			events.enqueue<ppr::events::CreateView>({ [this]() { return ppr::tl::make_unique<EndlessView>(mockEndlessViewDraw); } });

			events.enqueue<ppr::events::CreateController>({ [this]() { return ppr::tl::make_unique<InstantKillController>(mockInstantKillControllerUpdate); } });
			events.enqueue<ppr::events::CreateController>({ [this]() { return ppr::tl::make_unique<CharacterLuaScriptController>(content, characterModelStore); } });
			events.enqueue<ppr::events::CreateController>({ [this]() { return ppr::tl::make_unique<BackgroundColourController>(input); } });
			events.enqueue<ppr::events::CreateController>({ [this]() { return ppr::tl::make_unique<GameEndController>(mockGameEndControllerUpdate); } });
		}
	};
}

class GameTests : public ut11::TestFixture {
	std::unique_ptr<TestGame> testGame;

	virtual void Run() {
		auto drivers = ppr::data::getSupportedContexts();

		std::for_each(drivers.begin(), drivers.end(), [this](std::string driver) {
			Given("a game with " + driver + " drivers and a controller that closes the game after 10 updates", [this, driver](){
				ppr::model::Configuration configuration;
				configuration.driver = driver;
				testGame = ppr::tl::make_unique<TestGame>(configuration);
			}); When("running the game", [this](){
				ppr::run(*testGame);
			}); Then("spec tests", [this]() {
				// the controller that closes the game is updated at least 10 times
				MockVerifyTimes(ut11::Is::GreaterThan(9), testGame->mockGameEndControllerUpdate)(std::chrono::microseconds(10000));

				// the controller that immediately kills itself is updated only once
				MockVerifyTimes(1, testGame->mockInstantKillControllerUpdate)(std::chrono::microseconds(10000));

				// the view that immediately kills itself is drawn once
				MockVerifyTimes(1, testGame->mockInstantKillViewDraw)();

				// the view that endlessly draws itself several times
				MockVerifyTimes(ut11::Is::GreaterThan(1), testGame->mockEndlessViewDraw)();
			}); Finally("the game is destroyed", [this]() {
				testGame.reset();
			});
		});
	};
};
DeclareFixture(GameTests)();
