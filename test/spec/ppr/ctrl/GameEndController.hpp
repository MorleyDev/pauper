#ifndef TEST_SPEC_PPR_CTRL_GAMEENDCONTROLLER_HPP
#define TEST_SPEC_PPR_CTRL_GAMEENDCONTROLLER_HPP

#include <UnitTest11/Mock.hpp>
#include <ppr/ctrl/Controller.hpp>
#include <ppr/tl/thread_pool.hpp>
#include <ppr/events/Close.hpp>
#include "../events/CloseTestEvent.hpp"
#include <chrono>

namespace {
	class GameEndController : public ppr::ctrl::Controller {
	private:
		ut11::Mock<void (std::chrono::microseconds)>& m_mockUpdate;
		std::atomic<std::uint32_t> m_runtime;
		std::atomic<bool> m_killOnTimeout;

	public:
		GameEndController(ut11::Mock<void (std::chrono::microseconds)>& mockUpdate)
				: m_mockUpdate(mockUpdate),
				  m_runtime(0),
				  m_killOnTimeout(0) {
			onEvent<CloseTestEvent>([this](CloseTestEvent, ppr::tl::thread_pool& pool) {
				return pool.enqueue([this]() {
					m_killOnTimeout.store(true);
					m_runtime.store(0);
				});
			});
		}

		virtual std::future<void> update(std::chrono::microseconds dt, ppr::tl::thread_pool& pool) {
			return pool.enqueue([this, dt]() {
				m_mockUpdate(dt);

				m_runtime.store(m_runtime.load() + dt.count());
				if (std::chrono::microseconds(m_runtime.load()) < std::chrono::seconds(2)) {
					return;
				}

				if (m_killOnTimeout.load()) {
					emit<ppr::events::Close>({});
					return;
				}
				emit<CloseTestEvent>({});
			});
		}
	};
}

#endif //TEST_SPEC_PPR_CTRL_GAMEENDCONTROLLER_HPP
