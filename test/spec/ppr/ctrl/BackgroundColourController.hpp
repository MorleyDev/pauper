#ifndef TEST_SPEC_PPR_CTRL_BACKGROUNDCOLOURCONTROLLER_HPP
#define TEST_SPEC_PPR_CTRL_BACKGROUNDCOLOURCONTROLLER_HPP

#include <UnitTest11/Mock.hpp>
#include <ppr/ctrl/Controller.hpp>
#include <ppr/data/InputSystem.hpp>
#include <ppr/tl/thread_pool.hpp>
#include <ppr/tl/future/value.hpp>
#include "../events/SwapBackgroundColourEvent.hpp"
#include <chrono>

namespace {
	class BackgroundColourController : public ppr::ctrl::Controller {
	private:
		std::atomic<bool> m_triggerEvent;
	public:
		BackgroundColourController(ppr::data::InputSystem& inputSystem) : m_triggerEvent(false) {
			inputSystem.onKeyboard([this](ppr::model::KeyCode code, ppr::data::InputSystem::ButtonState state) {
				if (state != ppr::data::InputSystem::ButtonState::Released || code != ppr::model::KeyCode::Space) {
					return;
				}
				m_triggerEvent.store(true);
			});
		}

		virtual std::future<void> update(std::chrono::microseconds dt, ppr::tl::thread_pool& pool) {
			return pool.enqueue([this]() {
				if (!m_triggerEvent.exchange(false)) {
					return;
				}
				emit<SwapBackgroundColourEvent>(SwapBackgroundColourEvent());
			});
		}
	};
}

#endif //TEST_SPEC_PPR_CTRL_BACKGROUNDCOLOURCONTROLLER_HPP
