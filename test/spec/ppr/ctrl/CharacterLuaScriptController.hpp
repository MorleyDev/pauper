#ifndef TEST_SPEC_PPR_CTRL_CHARACTERLUASCRIPTCONTROLLER_HPP
#define TEST_SPEC_PPR_CTRL_CHARACTERLUASCRIPTCONTROLLER_HPP

#include <ppr/ctrl/Controller.hpp>
#include <ppr/data/VirtualMachine.hpp>
#include <ppr/data/lua/LuaVirtualMachine.hpp>
#include <ppr/data/ContentLoader.hpp>
#include <ppr/data/loaders/StringLoader.hpp>
#include <ppr/data/ModelStore.hpp>
#include <ppr/tl/chrono.hpp>
#include "../model/CharacterModel.hpp"

namespace {
	class CharacterLuaScriptController : public ppr::ctrl::Controller {
	private:
		ppr::data::lua::LuaVirtualMachine m_virtualMachine;
		ppr::data::ModelStore<CharacterModel>& m_modelStore;

	public:
		CharacterLuaScriptController(ppr::data::ContentLoader& content, ppr::data::ModelStore<CharacterModel>& modelStore)
			: m_virtualMachine(),
			  m_modelStore(modelStore) {
			m_modelStore.add(CharacterModel());

			auto script = content.load<std::string>("./spec/files/character.lua");
			m_virtualMachine.attach(script);
		}

		virtual std::future<void> update(std::chrono::microseconds dt, ppr::tl::thread_pool& pool) {
			return pool.enqueue([this, dt]() {
				try {
					m_modelStore.mutate([this, dt](CharacterModel& model) {
						m_virtualMachine.set("character_x", ppr::data::VirtualMachine::Number(model.position.x));
						m_virtualMachine.set("character_y", ppr::data::VirtualMachine::Number(model.position.y));
						m_virtualMachine.set("character_frame", ppr::data::VirtualMachine::Number(model.frameIndex));

						auto tickFunction = m_virtualMachine.get("tick").as<ppr::data::VirtualMachine::Function>();
						auto seconds = ppr::tl::chrono::to_seconds(dt);
						tickFunction({ ppr::tl::any::create<ppr::data::VirtualMachine::Number>(seconds.count()) });

						model.position.x = m_virtualMachine.get("character_x").as<ppr::data::VirtualMachine::Number>();
						model.position.y = m_virtualMachine.get("character_y").as<ppr::data::VirtualMachine::Number>();
						model.frameIndex = static_cast<std::uint8_t>(m_virtualMachine.get("character_frame").as<ppr::data::VirtualMachine::Number>());
					});
				} catch (std::exception const& err) {
					std::cerr << err.what() << std::endl;
				} catch (...) {
					std::cerr << "Unknown error" << std::endl;
				}
			});
		}
	};
}

#endif //TEST_SPEC_PPR_CTRL_CHARACTERLUASCRIPTCONTROLLER_HPP
