#ifndef TEST_SPEC_PPR_CTRL_INSTANTKILLCONTROLLER_HPP
#define TEST_SPEC_PPR_CTRL_INSTANTKILLCONTROLLER_HPP

#include <UnitTest11/Mock.hpp>
#include <ppr/ctrl/Controller.hpp>
#include <ppr/tl/thread_pool.hpp>
#include <chrono>

namespace {
	class InstantKillController : public ppr::ctrl::Controller {
	private:
		ut11::Mock<void(std::chrono::microseconds)>& m_mockUpdate;

	public:
		InstantKillController(ut11::Mock<void(std::chrono::microseconds)>& mockUpdate)
				: m_mockUpdate(mockUpdate) {
		}

		virtual std::future<void> update(std::chrono::microseconds dt, ppr::tl::thread_pool &pool) {
			return pool.enqueue([this, dt]() {
				m_mockUpdate(dt);
				kill();
			});
		}
	};
}

#endif //TEST_SPEC_PPR_CTRL_INSTANTKILLCONTROLLER_HPP
