-- character
--  _x
--  _y
--  _frame

timer = 0.25

function tick(dt)
    character_x = character_x + 100 * dt;
    character_y = character_y + 150 * dt;

    timer = timer - dt
    while (timer < 0.0) do
        timer = timer + 0.25
        if (character_frame == 0) then
            character_frame = 1
        else
            character_frame = 0
        end
    end
end
