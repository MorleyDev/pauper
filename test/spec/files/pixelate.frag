#version 120

uniform sampler2D texture;

void main()
{
    vec4 pixel = texture2D(texture, gl_TexCoord[0].xy);
    vec3 neighBourpixel1 = texture2D(texture, gl_TexCoord[0].xy - vec2(0.0, 0.1)).rgb;
    vec3 neighBourpixel2 = texture2D(texture, gl_TexCoord[0].xy - vec2(0.1, 0.0)).rgb;
    vec3 neighBourpixel3 = texture2D(texture, gl_TexCoord[0].xy - vec2(0.0, -0.1)).rgb;
    vec3 neighBourpixel4 = texture2D(texture, gl_TexCoord[0].xy - vec2(-0.1, 0.0)).rgb;
    vec3 finalPixel = pixel.rgb * neighBourpixel1 * neighBourpixel2 * neighBourpixel3 * neighBourpixel4;

    gl_FragColor =  vec4(finalPixel.rgb, pixel.a);
}
