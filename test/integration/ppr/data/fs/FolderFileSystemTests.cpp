#include <ppr/data/fs/FolderFileSystem.hpp>
#include <ppr/tl/make_unique.hpp>
#include <UnitTest11.hpp>
#include <fstream>

class FolderFileSystemTests : public ut11::TestFixture {
	std::string m_existingFileData;
	std::string m_actualFileData;
	std::unique_ptr<ppr::data::FileSystem> m_fileSystem;
	ppr::tl::optional<ppr::data::ReadOnlyFile> m_openedFile;

	virtual void Run() {

		Given("A folder file system with existing files", [&](){

		    m_existingFileData = "sadfashfaksjghkjaKFASHHKSKAF\nasdasfhasfkjaskjfakjsa\r\nasdkhaskhwiuga23\r4132890hkj";
		    m_actualFileData = "";

		    std::ofstream fileExistsStream("some_file.txt", std::ios::binary);
			fileExistsStream << m_existingFileData;
			fileExistsStream.close();

		    m_fileSystem = ppr::tl::make_unique<ppr::data::fs::FolderFileSystem>(".");

		}); When("open a file that does not exist", [&]() {
		    m_openedFile = m_fileSystem->open("some_not_file.txt");
		}); Then("a value is not returned", [&]() {
		    AssertThat(static_cast<bool>(m_openedFile), ut11::Is::False);
		});When("open a file that exists", [&]() {
		    m_openedFile = m_fileSystem->open("some_file.txt");
		}); Then("a value is returned", [&]() {
			AssertThat(static_cast<bool>(m_openedFile), ut11::Is::True);
		}); Then("the file's position is 0", [&]() {
		    AssertThat(m_openedFile->position(), ut11::Is::EqualTo(0));
		}); Then("the file is not at EOF", [&]() {
		    AssertThat(m_openedFile->eof(), ut11::Is::False);
		}); Then("the file has the expected size", [&]() {
		    AssertThat(m_openedFile->size(), ut11::Is::EqualTo(m_existingFileData.size()));
		}); When("open a file that exists and reading it's contents", [&]() {
		    m_openedFile = m_fileSystem->open("some_file.txt");
			if (m_openedFile) {
				auto datablock = ppr::tl::make_unique<char[]>(m_openedFile->size());
				auto readDataSize = m_openedFile->read(datablock.get(), m_openedFile->size());

				m_actualFileData = "";
				for (std::size_t i = 0u; i < readDataSize; ++i)
					m_actualFileData += datablock[i];
			}

		}); Then("the expected file data was loaded", [&]() {
			AssertThat(m_actualFileData, ut11::Is::EqualTo(m_existingFileData));
		}); Then("the file has the expected position", [&]() {
		    AssertThat(m_openedFile->position(), ut11::Is::EqualTo(m_actualFileData.size()));
		}); Then("the file has the expected size", [&]() {
		    AssertThat(m_openedFile->size(), ut11::Is::EqualTo(m_actualFileData.size()));
		}); Then("the file is at EOF", [&]() {
		    AssertThat(m_openedFile->eof(), ut11::Is::True);
		}); Then("reading more data gives no data", [&]() {
			char buffer[100];
		    AssertThat(m_openedFile->read(buffer, 100), ut11::Is::EqualTo(0));
		}); Finally("delete the existing files", [&]() {
		    std::remove("some_file.txt");
		});
	}
};
DeclareFixture(FolderFileSystemTests)({ ut11::Category("data"), ut11::Category("fs") });
