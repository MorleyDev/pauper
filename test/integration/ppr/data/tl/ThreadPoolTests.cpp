#include <ppr/tl/thread_pool.hpp>
#include <ppr/tl/make_unique.hpp>
#include <UnitTest11.hpp>
#include <ppr/tl/to_string.hpp>
#include <iostream>

class ThreadPoolTests : public ut11::TestFixture {
	std::unique_ptr<ppr::tl::thread_pool> pool;

	auto fib(std::uint64_t i) -> std::future<std::uint64_t> {
		if (i < 2)
			return std::async(std::launch::deferred, [i]() {return i;});

		auto fibPart1 = std::make_shared<std::future<std::future<std::uint64_t>>>(
			pool->enqueue([this, i]() -> std::future<std::uint64_t> {
		    	return fib(i - 1);
			}));
		auto fibPart2 = std::make_shared<std::future<std::future<std::uint64_t>>>(
			pool->enqueue([this, i]() -> std::future<std::uint64_t> {
		    	return fib(i - 2);
			}));

		return std::async(std::launch::deferred, [fibPart1, fibPart2]() -> std::uint64_t {
		    return fibPart1->get().get() + fibPart2->get().get();
		});
	};

	virtual void Run() {
		Then("Concurrent Fibonnaci Test #1", [this]() {
		    pool = ppr::tl::make_unique<ppr::tl::thread_pool>(std::max(10u, std::thread::hardware_concurrency() * 2u));

		    auto fibFuture = fib(20Lu);

		    AssertThat(fibFuture.get(), ut11::Is::EqualTo(6765Lu));
		});
		Then("Concurrent Fibonnaci Test #2", [this]() {
		    pool = ppr::tl::make_unique<ppr::tl::thread_pool>(0);

		    auto fibFuture = fib(20Lu);
			pool->growBy(std::max(10u, std::thread::hardware_concurrency() * 10u));

		   	AssertThat(fibFuture.get(), ut11::Is::EqualTo(6765Lu));
		});

		Then("Concurrent chain #2", [this]() {
			pool = ppr::tl::make_unique<ppr::tl::thread_pool>(0);

			auto future = ppr::tl::future::chain([]() { return 10; })
					.map([](int n) { return n * 100; })
					.map([](int n) { return n + 100; })
					.map([](int n) { return n % 255; })
					.map([](int n) { return static_cast<char>(n); })
					.to_thread_pool(*pool);

			pool->growBy(std::max(10u, std::thread::hardware_concurrency() * 10u));
			AssertThat(future.get(), ut11::Is::EqualTo(static_cast<char>(80)));
		});
	}
};
DeclareFixture(ThreadPoolTests)(ut11::Category("tl"));
