#include <ppr/data/lua/LuaVirtualMachine.hpp>
#include <UnitTest11.hpp>
#include <thread>

class LuaVirtualMachineTests : public ut11::TestFixture {
private:
	template<typename T, typename Op> void AssertAnyIs(int line, ppr::tl::any value, Op operand) {
		ut11::Assert::That(line, __FILE__, static_cast<bool>(value), ut11::Is::True);
		ut11::Assert::That(line, __FILE__, value.is<T>(), ut11::Is::True);
		ut11::Assert::That(line, __FILE__, value.as<T>(), operand);
	}

public:
	virtual void Run() {
		Then("Test #1", [this]() {
			ppr::data::lua::LuaVirtualMachine vm;
			std::thread t1([&, this]() {
				vm.attach(
						"x = 10\n"
								"y = true\n"
								"z = \"hello-world\"\n"
								"function f()\n"
								"end\n"
								"function f2()\n"
								"return 15\n"
								"end\n"
								"function f3()\n"
								"return \"hello-world\", 15\n"
								"end\n"
								"function vf(x)\n"
								"end\n"
								"function vf2(x)\n"
								"return x\n"
								"end\n"
				);
			});
			t1.join();

			AssertThat(static_cast<bool>(vm.get("nope")), ut11::Is::False);

			AssertAnyIs<ppr::data::VirtualMachine::Number>(__LINE__, vm.get("x"), ut11::Is::EqualTo(10.0));
			AssertAnyIs<ppr::data::VirtualMachine::Boolean>(__LINE__, vm.get("y"), ut11::Is::True);
			AssertAnyIs<ppr::data::VirtualMachine::String>(__LINE__, vm.get("z"), ut11::Is::EqualTo("hello-world"));

			auto f = vm.get("f");
			AssertThat(static_cast<bool>(f), ut11::Is::True);
			AssertThat(f.is<ppr::data::VirtualMachine::Function>(), ut11::Is::True);
			AssertThat(f.as<ppr::data::VirtualMachine::Function>()({}), ut11::Is::Empty);

			std::thread t2([&, this]() {
				auto f2 = vm.get("f2");
				AssertThat(static_cast<bool>(f2), ut11::Is::True);
				AssertThat(f2.is<ppr::data::VirtualMachine::Function>(), ut11::Is::True);
				auto f2_result = f2.as<ppr::data::VirtualMachine::Function>()({});
				AssertThat(f2_result.size(), ut11::Is::EqualTo(1));
				AssertAnyIs<ppr::data::VirtualMachine::Number>(__LINE__, f2_result[0], ut11::Is::EqualTo(15.0));
			});
			t2.join();

			auto f3 = vm.get("f3");
			AssertThat(static_cast<bool>(f3), ut11::Is::True);
			AssertThat(f3.is<ppr::data::VirtualMachine::Function>(), ut11::Is::True);
			auto f3_result = f3.as<ppr::data::VirtualMachine::Function>()({});
			AssertThat(f3_result.size(), ut11::Is::EqualTo(2));
			AssertAnyIs<ppr::data::VirtualMachine::String>(__LINE__, f3_result[0], ut11::Is::EqualTo("hello-world"));
			AssertAnyIs<ppr::data::VirtualMachine::Number>(__LINE__, f3_result[1], ut11::Is::EqualTo(15.0));

			auto vf = vm.get("vf");
			AssertThat(static_cast<bool>(vf), ut11::Is::True);
			AssertThat(vf.is<ppr::data::VirtualMachine::Function>(), ut11::Is::True);
			AssertThat(vf.as<ppr::data::VirtualMachine::Function>()({ ppr::tl::any(static_cast<ppr::data::VirtualMachine::Number>(10)) }), ut11::Is::Empty);

			auto vf2 = vm.get("vf2");
			AssertThat(static_cast<bool>(vf2), ut11::Is::True);
			AssertThat(vf2.is<ppr::data::VirtualMachine::Function>(), ut11::Is::True);

			std::thread t3([&, this]() {
				auto vf2_result = vf2.as<ppr::data::VirtualMachine::Function>()(
						{ppr::tl::any(static_cast<ppr::data::VirtualMachine::Number>(10.0))});
				AssertThat(vf2_result.size(), ut11::Is::EqualTo(1));
				AssertThat(vf2_result[0].is<ppr::data::VirtualMachine::Number>(), ut11::Is::True);
				AssertThat(vf2_result[0].as<ppr::data::VirtualMachine::Number>(), ut11::Is::EqualTo(10.0));
			});
			t3.join();
		});
		Then("Test #2", [this]() {
			ppr::data::lua::LuaVirtualMachine vm;
			vm.attach(
				"x = 10\n"
			);
			vm.set("x", 20.0);

			AssertAnyIs<ppr::data::VirtualMachine::Number>(__LINE__, vm.get("x"), ut11::Is::EqualTo(20.0));

			vm.set("tf", ppr::data::VirtualMachine::Function([](std::vector<ppr::tl::any> args) {
				args.pop_back();
				args.pop_back();
				args.emplace_back(static_cast<ppr::data::VirtualMachine::Number>(200));
				return args;
			}));
			vm.attach("y,z,t,q = tf(10, 20, 30, 50, 90)");
			AssertAnyIs<ppr::data::VirtualMachine::Number>(__LINE__, vm.get("y"), ut11::Is::EqualTo(10.0));
			AssertAnyIs<ppr::data::VirtualMachine::Number>(__LINE__, vm.get("z"), ut11::Is::EqualTo(20.0));
			AssertAnyIs<ppr::data::VirtualMachine::Number>(__LINE__, vm.get("t"), ut11::Is::EqualTo(30.0));
			AssertAnyIs<ppr::data::VirtualMachine::Number>(__LINE__, vm.get("q"), ut11::Is::EqualTo(200.0));
		});
	}
};
DeclareFixture(LuaVirtualMachineTests)({ut11::Category("data"), ut11::Category("lua")});
