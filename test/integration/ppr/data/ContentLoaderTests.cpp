#include <ppr/data/ContentLoader.hpp>
#include <ppr/data/fs/FolderFileSystem.hpp>
#include <ppr/data/null/NullSysContext.hpp>
#include <ppr/tl/make_unique.hpp>

#include <ppr/data/loaders/StringLoader.hpp>
#include <ppr/data/loaders/JsonLoader.hpp>

#include <UnitTest11.hpp>
#include <fstream>

namespace {
    struct SerializableObject {
        int value1 = 50;
	    char value2 = 'z';
    };

	template<typename ARCHIVE> void serialize(ARCHIVE& a, SerializableObject& o) {
		a(ppr::tl::make_nvp("v1", o.value1));
		a(ppr::tl::make_nvp("v2", o.value2));
	}
}
class ContentLoaderTests : public ut11::TestFixture {
	ppr::data::null::NullSysContext context;

	std::unique_ptr<ppr::data::Window> m_window;
	std::unique_ptr<ppr::data::MusicPlayer> m_music;
	std::unique_ptr<ppr::data::SoundEffectPlayer> m_sound;

	std::unique_ptr<ppr::data::FileSystem> m_fileSystem;
	std::unique_ptr<ppr::data::ContentLoader> m_contentLoader;

	std::string m_existingFileData;
	std::string m_actualFileData;

	SerializableObject m_actualJsonData;

	virtual void Run() {

		Given("A folder file system with existing files", [&](){
		    m_window = context.createWindow("",10,10,false,false);
			m_music = context.createMusicPlayer();
			m_sound = context.createSoundEffectPlayer();

		    m_existingFileData = "sadfashfaksjghkjaKFASHHKSKAF\nasdasfhasfkjaskjfakjsa\r\nasdkhaskhwiuga23\r4132890hkj";
		    m_actualFileData = "";

		    std::ofstream textFileStream("some_file.txt", std::ios::binary);
		    textFileStream << m_existingFileData;
		    textFileStream.close();

		    std::ofstream jsonFileStream("some_thing.json", std::ios::binary);
		    jsonFileStream << "{ \"v1\":12, \"v2\":97 }" << std::endl;
		    jsonFileStream.close();

		    m_fileSystem = ppr::tl::make_unique<ppr::data::fs::FolderFileSystem>(".");
		    m_contentLoader = ppr::tl::make_unique<ppr::data::ContentLoader>(*m_fileSystem, m_window->renderer(), *m_music, *m_sound);

		});
		Then("reading a file that does not exist as a string throws an exception", [&]() {
			AssertThat([&]() { m_contentLoader->load<std::string>("no_file.txt"); }, ut11::Will::Throw<std::exception>());
		});
		When("reading a file as a string", [&]() {
		    m_actualFileData = m_contentLoader->load<std::string>("some_file.txt");
		}); Then("the expected file data was loaded", [&]() {
		    AssertThat(m_actualFileData, ut11::Is::EqualTo(m_existingFileData));
		});
		When("reading a file as a json object", [&]() {
		    m_actualJsonData = m_contentLoader->load<SerializableObject>("some_thing.json");
		}); Then("the expected data was loaded", [&]() {
		    AssertThat(m_actualJsonData.value1, ut11::Is::EqualTo(12));
		    AssertThat(m_actualJsonData.value2, ut11::Is::EqualTo('a'));
		});
		When("reading a file that does not exist as a json object", [&]() {
		    m_actualJsonData = m_contentLoader->load<SerializableObject>("no_thing.json");
		}); Then("the default data was returned", [&]() {
		    AssertThat(m_actualJsonData.value1, ut11::Is::EqualTo(50));
		    AssertThat(m_actualJsonData.value2, ut11::Is::EqualTo('z'));
		});
		Finally("delete the existing files", [&]() {
		    std::remove("some_file.txt");
		    std::remove("some_thing.json");
		});
	}
};
DeclareFixture(ContentLoaderTests)({ ut11::Category("data") });
