SET(UNIT_TEST_FILES
	unit/ppr/ctrl/ControllerStoreTests.cpp
	unit/ppr/ctrl/ControllerTests.cpp
	unit/ppr/data/EventQueueTests.cpp
	unit/ppr/data/FileSystemStackTests.cpp
	unit/ppr/data/FlatModelStoreTests.cpp
	unit/ppr/data/UnorderedIndexedModelStoreTests.cpp
	unit/ppr/model/AngleTests.cpp
	unit/ppr/model/UniqueIdTests.cpp
	unit/ppr/tl/AnyTests.cpp
	unit/ppr/tl/DomainTests.cpp
	unit/ppr/tl/EitherTests.cpp
	unit/ppr/tl/FutureTests.cpp
	unit/ppr/tl/ImmutableListTests.cpp
	unit/ppr/tl/OptionalTests.cpp
	unit/ppr/tl/SemaphoreTests.cpp
	unit/ppr/tl/SerisalisationTests.cpp
	unit/ppr/tl/SignalsTests.cpp
	unit/ppr/tl/ThreadPoolTests.cpp
	unit/ppr/tl/random/RandomOneOfTests.cpp
	unit/ppr/tl/string/StringFormatTests.cpp
	unit/ppr/tl/string/StringSplitTests.cpp
	unit/ppr/view/ViewStoreTests.cpp
	unit/ppr/view/ViewTests.cpp
)

SET(INTEGRATION_TEST_FILES
	integration/ppr/data/ContentLoaderTests.cpp
	integration/ppr/data/fs/FolderFileSystemTests.cpp
	integration/ppr/data/tl/ThreadPoolTests.cpp
)

if (PPR_LUA_ENABLE)
	SET(INTEGRATION_TEST_FILES
		${INTEGRATION_TEST_FILES}
		integration/ppr/data/lua/LuaVirtualMachineTests.cpp
	)
endif(PPR_LUA_ENABLE)

SET(SPEC_TEST_FILES spec/ppr/GameTests.cpp)
SET(SPEC_TEST_HEADER_FILES
		spec/ppr/ctrl/BackgroundColourController.hpp
		spec/ppr/ctrl/CharacterLuaScriptController.hpp
		spec/ppr/ctrl/GameEndController.hpp
		spec/ppr/ctrl/InstantKillController.hpp
		spec/ppr/events/CloseTestEvent.hpp
		spec/ppr/events/SwapBackgroundColourEvent.hpp
		spec/ppr/model/CharacterModel.hpp
		spec/ppr/view/BackgroundColourView.hpp
		spec/ppr/view/BitmapView.hpp
		spec/ppr/view/CharacterView.hpp
		spec/ppr/view/EndlessView.hpp
		spec/ppr/view/InstantKillView.hpp
		spec/ppr/view/RectangleView.hpp
		spec/ppr/view/SpriteBatchView.hpp
		spec/ppr/view/ShaderView.hpp
		spec/ppr/view/WavFileMusicView.hpp
		spec/ppr/view/WavFileSoundEffectView.hpp
)

FILE(GLOB_RECURSE SCRIPT_FILES scripts/*)

set(TEST_SPEC_FILES
		spec/files/simple.bmp
		spec/files/simple2.png
		spec/files/simple.wav
		spec/files/simple2.wav
		spec/files/character.bmp
		spec/files/character.lua
		spec/files/teapot.png
		spec/files/invert-colours.frag
		spec/files/pixelate.frag
		spec/files/warp.vert
		spec/files/flip.vert
		)