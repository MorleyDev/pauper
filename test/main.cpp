#include <UnitTest11.hpp>
#include <ppr/util/Log.hpp>

int main(int nArgs, char** ppcArgs)
{
	ppr::util::Log::setGlobalLevel(ppr::util::Log::Level::Warn);
	return ut11::Run(nArgs, ppcArgs);
}

