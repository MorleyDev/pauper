###################################################
# Allow the enabling of gcov for coverage support #
###################################################

option(ENABLE_GCOV "Enable GCov on GCC" OFF)
option(ENABLE_GPROF "Enable GProf on GCC" OFF)

if(ENABLE_GCOV)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fprofile-arcs -ftest-coverage")
	set(CMAKE_EXE_LINKER_FLAGS="${CMAKE_EXE_LINKER_FLAGS} -fprofile-arcs -ftest-coverage")
endif()

if (ENABLE_GPROF)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pg")
	set(CMAKE_EXE_LINKER_FLAGS="${CMAKE_EXE_LINKER_FLAGS} -pg")
endif()
