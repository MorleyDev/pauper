mkdir build
pushd build
    mkdir gcc
    pushd gcc
        pushd install\test
            unittests.exe && integrationtests.exe && specificationtests.exe
        popd
    popd
popd
