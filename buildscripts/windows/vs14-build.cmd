mkdir build
pushd build
    mkdir vs14
    pushd vs14
        call cmake ../.. -G"Visual Studio 14 2015" -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=".\install"
        call cmake --build . --config debug --target install
        copy install\bin\*.dll install\test
        copy install\lib\*.dll install\test
    popd
popd
