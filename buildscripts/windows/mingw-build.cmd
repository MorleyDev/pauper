mkdir build
pushd build
    mkdir gcc
    pushd gcc
        call cmake ../.. -G"MinGW Makefiles" -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=".\install"
        call cmake --build . --config debug --target install -- -j8
        copy install\bin\*.dll install\test
        copy install\lib\*.dll install\test
    popd
popd
