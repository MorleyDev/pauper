mkdir build
pushd build
    mkdir vs14
    pushd vs14
        pushd install\test
            unittests.exe && integrationtests.exe && specificationtests.exe
        popd
    popd
popd
